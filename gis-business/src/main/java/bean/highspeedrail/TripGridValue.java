package bean.highspeedrail;

/**
 * Created by Xiaobing on 2016/5/27.
 */
public class TripGridValue extends TrackGridValue {
    private String tripId;
    private String tripName;

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTripName() {
        return tripName;
    }

    public void setTripName(String tripName) {
        this.tripName = tripName;
    }
}
