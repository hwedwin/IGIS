package bean.technicalinve;

import gis.algorithm.GeoPoint;
import gis.algorithm.grid.RqGridMap;
import org.apache.log4j.pattern.LineSeparatorPatternConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xiaobing on 2016/8/29.
 */
public class RqGridMapInve extends RqGridMap {
    private double area;
    private List<GeoPoint> ring;

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public List<GeoPoint> getRing() {
        if(ring == null)
        {
            ring = new ArrayList<>();
            ring.add(new GeoPoint(this.getExtent().getxMax(),this.getExtent().getyMax()));
            ring.add(new GeoPoint(this.getExtent().getxMin(),this.getExtent().getyMax()));
            ring.add(new GeoPoint(this.getExtent().getxMin(),this.getExtent().getyMin()));
            ring.add(new GeoPoint(this.getExtent().getxMax(),this.getExtent().getyMin()));
            ring.add(new GeoPoint(this.getExtent().getxMax(),this.getExtent().getyMax()));
        }
        return ring;
    }

    public RqGridMapInve clone() {
        RqGridMapInve inve = new RqGridMapInve();
        inve.setId(this.getId());
        inve.setName(this.getName());
        inve.setExtent(this.getExtent());
        inve.setValue(this.getValue());
        inve.setArea(this.getArea());
        return inve;
    }
}
