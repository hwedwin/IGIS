﻿
function bdexpressway() {

	var lineName = "bdexpressway_lineName";
	var layer = gis_core.addGraphicsLayer(lineName, "建议路线");
	var datas = null;

	var initData = function (start, end) {
		$.ajax({
			url : "/getddvisewayline",
			data : {
				start : start,
				end : end
			},
			type : "GET",
			beforeSend : function () {
				gis_common.isLoadingAddCount();
			},
			success : function (req) {
				var lines = eval(req);
				datas = lines;
				listHtml(lines);
			},
			complete : function (lines) {
				gis_common.isLoadingSunCount();
			},
			error : function (XMLHttpRequest, textStatus, errorThrown) {
				gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
			}
		});
	};

	var listHtml = function (lines) {
		var content = " <ul class=\"panel-list\" style=\"width:300px;\">";
		if (lines != null && lines != undefined && lines.length != 0) {
			for (var i = 0; i < lines.length; i++) {
				var line = lines[i];
				content += "<li id='" + line.id + "'>";
				content += "	<h3 class='panel-li-title'>";
				content += "		<span> " + line.startPlace + "</span>";
				for (var j = 0; j < line.expressWayLines.length - 1; j++) {
					var expressWayLines = line.expressWayLines[j];
					content += " -" + expressWayLines.endPlace;
				}
				content += " 		<span> - " + line.endPlace + " </span>";
				content += "	</h3>";
				content += "	<div class='list-cont'>";
				content += "		<div class='list-label'>经过高速：<span>";
				for (var j = 0; j < line.expressWayLines.length; j++) {
					var expressWayLines = line.expressWayLines[j];
					if (j == line.expressWayLines.length - 1) {
						content += expressWayLines.name;
					} else {
						content += expressWayLines.name + " - ";
					}
				}
				content += "		</span></div>";
				content += "		<span class='list-label'>过路费：<span class='c-red'>¥" + line.charging + "</span></span>";
				content += "		<span class='list-label'>里程：<span>" + line.distance + "公里</span></span>";
				content += "	</div>";
				content += "</li>";
			}
		}
		else{
			content += "<div class='list-label'>无数据</div>";
		}
		content += "</ul>";

		var div = $(content);
		div.find("li").bind("click", function () {
			layer.clear();
			var id = $(this).attr("id");
			if (datas != null && datas != undefined) {
				for (var i = 0; i < datas.length; i++) {
					var line = datas[i];
					if (line.id == id) {
						drawLine(line);
					}
				}
			}
		});

		gis_layout.createLeftWidget("bdexpressway_list", "路线", div);
	};

	var drawLine = function (line) {
		if (line != null && line != undefined) {
			var polygon = new esri.geometry.Polygon(gis_core.map.spatialReference);
			var rings = [];
			var xmax = 0,
			xmin = 0,
			ymax = 0,
			ymin = 0;
			for (var j = 0; j < line.expressWayLines.length; j++) {
				var expressWayLines = line.expressWayLines[j];
				for (var k = 0; k <= expressWayLines.ring.length; k++) {
					var r = expressWayLines.ring[k];
					if (r != null && r != undefined) {
						rings.push([r.x, r.y]);

						if (r.x < xmin || xmin == 0) {
							xmin = r.x;
						}
						if (r.x > xmax || xmax == 0) {
							xmax = r.x;
						}
						if (r.y < ymin || ymin == 0) {
							ymin = r.y;
						}
						if (r.y > ymax || ymax == 0) {
							ymax = r.y;
						}

					}
				}
			}
			polygon.addRing(rings);
			//var symbol = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SHORTDASHDOT, new esri.Color([255, 0, 0]), 3);
			//var symbol = new esri.symbol.SimpleLineSymbol().setColor(new esri.Color([0, 186, 0])).setWidth(3);
			var symbol = new esri.symbol.SimpleLineSymbol().setColor(new esri.Color([255, 0, 0])).setWidth(3);
			var graphic = new esri.Graphic(polygon, symbol);
			layer.add(graphic);

			if (xmax > 0 && xmin > 0 && ymax > 0 && ymin > 0) {
				var extent = new esri.geometry.Extent(xmin - 0.01, ymin - 0.01, xmax + 0.01, ymax + 0.01, new esri.SpatialReference({
							wkid : 4326
						}));
				gis_core.map.setExtent(extent);
			}
		}
	};

	//菜单
	this.addMenu = function (callback) {
		var content = "<div class=\"form-group\">";
		content += "	<label class='col-sm-3 control-label'>开始地点:</label>";
		content += "	<div id='res_type' class='col-sm-9'>";
		content += "			<input id='bdexpressway_menu_query_input_start' type='text' \"/>";
		content += " 	</div > ";
		content += " </div > "

		content += "<div class=\"form-group\">";
		content += "	<label class='col-sm-3 control-label'>结束地点:</label>";
		content += "	<div id='res_type' class='col-sm-9'>";
		content += "			<input id='bdexpressway_menu_query_input_end' type='text' \"/>";
		content += " 	</div > ";
		content += " </div > "

		content += "<div class='form-group'>";
		content += " 	<div class='col-sm-9 col-sm-offset-3'>";
		content += "		<button id='bdexpressway_menu_query_btn_load' class='btn btn-primary'>查 询</button>";
		content += "	</div>";
		content += "</div>";

		var div = $(content);
		div.find("#bdexpressway_menu_query_btn_load").bind("click", function () {
			var start = $("#bdexpressway_menu_query_input_start").val();
			var end = $("#bdexpressway_menu_query_input_end").val();

			if (gis_common.stringIsNullOrWhiteSpace(start)) {
				gis_common.showMessage("输入开始地点");
				return;
			}
			if (gis_common.stringIsNullOrWhiteSpace(end)) {
				gis_common.showMessage("输入结束地点");
				return;
			}			
			layer.clear();
			initData(start, end);
		});

		callback(div);
	};
	//初始化
	this.init = function () {
		//注册模块
		gis_core.addPlugin(this);
	};

	//销毁
	this.destroy = function () {
		$("#ebdExpressway_bdExpressway_addMenu_query").remove();
		bdExpressway = null;
	};

	//模块信息
	this.title = {
		id : "00_03_00",
		name : "高速公路"
	};
}
//功能类 全局类
var bdExpressway = null;

function initBDExpressway() {
	if (bdExpressway == null || bdExpressway == undefined) {
		bdExpressway = new bdexpressway();
		bdExpressway.init();
	}
}

function bdExpressway_addMenu_query(obj) {
	initBDExpressway();
	gis_core.pluginDestroyExceptId(bdExpressway.title.id);
	gis_layout.createMenubarWithFun("ebdExpressway_bdExpressway_addMenu_query", obj, "查询条件", bdExpressway.addMenu);
}
