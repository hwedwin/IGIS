<%@ page language="java" pageEncoding="UTF-8" %>

<ul class="docs-nav">
    <li><strong>从这里开始</strong></li>
    <li><a href="/api/welcome.jsp" pageid="welcome" class="cc-active">欢迎</a></li>
    <li><a href="/api/igis.jsp" pageid="igis" class="cc-active">iGIS简介</a></li>
    <li><a href="/api/arcgis.jsp" pageid="arcgis" class="cc-active">ArcGis介绍</a></li>
    <li><a href="/api/igisdeveloper.jsp" pageid="igisdeveloper" class="cc-active">iGIS插件开发</a></li>
    <li class="separator"></li>
    <li><strong>基础配置</strong></li>
    <li><a href="/api/baseinfoxml.jsp" pageid="baseinfoxml" class="cc-active">基础信息配置</a></li>
    <li><a href="/api/menuxml.jsp" pageid="menuxml" class="cc-active">菜单配置</a></li>
    <li><a href="/api/otherxml.jsp" pageid="otherxml" class="cc-active">其他配置</a></li>
    <li class="separator"></li>
    <li><strong>前端接口</strong></li>
    <li><a href="/api/mapapi.jsp" pageid="mapapi" class="cc-active">地图基础操作</a></li>
    <li><a href="/api/layoutapi.jsp" pageid="layoutapi" class="cc-active">通用页面布局</a></li>
    <li><a href="/api/commresapi.jsp" pageid="commresapi" class="cc-active">公共工具</a></li>
    <li class="separator"></li>
    <li><strong>算法说明</strong></li>
    <li><a href="operation.jsp" pageid="operation" class="cc-active">运算算法</a></li>
    <li><a href="rendering.jsp" pageid="rendering" class="cc-active">渲染算法</a></li>
    <li><strong>发布说明</strong></li>
    <li><a href="release.jsp" pageid="release" class="cc-active">发布</a></li>
</ul>
<script type="text/javascript">
    $(function () {
        var pageid = $("#pageid").val();
        $(".docs-nav").find("a").removeClass("active");
        $(".docs-nav").find("a[pageid=" + pageid + "]").addClass("active");
    })
</script>