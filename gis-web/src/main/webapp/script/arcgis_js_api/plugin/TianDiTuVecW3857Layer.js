/**
 * Created by xiexb on 2017/7/27.
 */
define(["dojo/_base/declare", "esri/layers/tiled"], function (declare) {
    return declare(esri.layers.TiledMapServiceLayer, {
        constructor: function () {
            this.spatialReference = new esri.SpatialReference({
                wkid: 102113
            });
            //this.initialExtent = (this.fullExtent = new esri.geometry.Extent(-20037507.067161843, -30240971.958386172, 20037507.067161843, 18422214.742251351, this.spatialReference));
            this.initialExtent = (this.fullExtent = new esri.geometry.Extent(-20037507.067161843, -30240971.958386172, 20037507.067161843, 18422214.742251351, this.spatialReference));
            this.tileInfo = new esri.layers.TileInfo({
                "dpi": 96,
                "format": "image/png",
                "compressionQuality": 0,
                "spatialReference": new esri.SpatialReference({
                    "wkid": 102113
                }),
                "rows": 256,
                "cols": 256,
                "origin": {
                    "x": -22041257.773878001,
                    "y": 30241100
                },
                "lods": [{
                    "level": "0",
                    "scale": 500000000,
                    "resolution": 132291.93125052919
                }, {
                    "level": "1",
                    "scale": 250000000,
                    "resolution": 66145.965625264595
                }, {
                    "level": "2",
                    "scale": 125000000,
                    "resolution": 33072.982812632297
                }, {
                    "level": "3",
                    "scale": 64000000,
                    "resolution": 16933.367200067736
                }, {
                    "level": "4",
                    "scale": 32000000,
                    "resolution": 8466.6836000338681
                }, {
                    "level": "5",
                    "scale": 16000000,
                    "resolution": 4233.341800016934
                }, {

                    "level": "6",
                    "scale": 8000000,
                    "resolution": 2116.670900008467
                }, {

                    "level": "7",
                    "scale": 4000000,
                    "resolution": 1058.3354500042335
                }]
            });
            this.loaded = true;
            this.onLoad(this);
        },
        getTileUrl: function (level, row, col) {
            return this.tileUrlFunction(level, row, col, "http://180.76.184.34:8085/title/dark_dark_basemap");
        },
        tileUrlFunction: function (level, row, col, url) {
            var y = 'R' + this.padLeft(-row - 1, 8, 16);
            console.log(row+"  "+(-row - 1));
            var x = 'C' + this.padLeft(col, 8, 16);
            var z = 'L' + this.padLeft(level, 2, 10);
            var u = url + '/Layers/_alllayers/' + z + '/' + y + '/' + x + '.png';
            return u;
        },
        padLeft: function (val, num, radix) {
            var str = val.toString(radix || 10);
            return (new Array(num).join('0') + str).slice(-num);
        }
    });
});

