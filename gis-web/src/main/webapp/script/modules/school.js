
function School() {
	var schoolsObj = null;
	var schoolLayer_onClick_handle = null;
	var intervalObj = null;

	var loadLayer = function () {
		gis_core.addGraphicsLayerWithIndex("schoolPoi","学校POI",4);
		gis_core.addGraphicsLayerWithIndex("schoolCover","学校覆盖",5);

		/*var schoolPoiLayer = gis_core.getLayer("schoolPoi");
		var schoolCoverLayer = gis_core.getLayer("schoolCover");

		if(schoolLayer_onClick_handle != null){
			dojo.disconnect(schoolLayer_onClick_handle);
		}

		schoolLayer_onClick_handle = schoolPoiLayer.on("click", function (evt) {
			alert("aaaaaaaaaaaaaaaaaaaaaa");
			schoolCoverLayer.clear();
			var type = evt.graphic.attributes.tag;
			if(type == "poi"){
				alert("sss");
			}
		}); // end on */
	};

	var registerExtentChanged = function(){
		gis_core.addExtentChange('school_extent_change', function(){
			createSchoolPoi(schoolsObj);
		});

		createSchoolPoi(schoolsObj);
	};

	var intervalUpdateSchoolPoi = function(){
		if(schoolsObj != null){
			for(var i=0;i<schoolsObj.length;i++){
				schoolsObj[i].value += 5;
			}
		}

		createSchoolPoi(schoolsObj);
	};

	var createSchoolPoi = function(data){
		var schoolPoiLayer = gis_core.getLayer("schoolPoi");
		schoolPoiLayer.clear();

		var lvl = gis_core.map.getLevel();

		require(["esri/graphic", "esri/geometry/Point", "esri/symbols/PictureMarkerSymbol", "esri/symbols/TextSymbol","esri/symbols/Font","esri/Color","esri/InfoTemplate", "dojo/domReady!"],
			function (Graphic, Point, PictureMarkerSymbol, TextSymbol, Font,Color,InfoTemplate) {
				var graphics = [];
				for(var i=0;i<data.length;i++){
					var schoolItem = data[i];

					var infoTemplate = new InfoTemplate();
					infoTemplate.setTitle(schoolItem.name);
					infoTemplate.setContent("招生："+schoolItem.value+" <br/>地址："+schoolItem.address+"<br/><img height='60' width='95' src='/images/schoolimgs/school00"+schoolItem.longitude.toString()[schoolItem.longitude.toString().length-1]+".jpg'>");

					var point = new esri.geometry.Point(schoolItem.longitude, schoolItem.latitude, gis_core.map.spatialReference);
					var symbol = new esri.symbol.PictureMarkerSymbol('/images/school.png', 26, 26);
					var graphic = new esri.Graphic(point, symbol, {
						"data" : schoolItem,
						"name" : schoolItem.name,
						"tag" : "poi",
						"lon" : schoolItem.longitude,
						"lat" : schoolItem.latitude
					},infoTemplate);
					graphics.push(graphic);

					/*if(lvl > 9){
						var textSymbol = new esri.symbol.TextSymbol(schoolItem.name);
						//textSymbol.setAlign(esri.symbol.TextSymbol.ALIGN_START);
						textSymbol.setColor(new esri.Color([32,32,32]));
						//textSymbol.setColor(new esri.Color([255,32,32]));
						textSymbol.setFont(new Font("8pt").setWeight(Font.WEIGHT_BOLD)) ;
						textSymbol.setOffset(0, -25);
						var textGraphic = new Graphic(point, textSymbol, {
							"data" : schoolItem,
							"name" : schoolItem.name,
							"tag" : "text"
						});
						graphics.push(textGraphic);

						var imgUrl = "";
						if(schoolItem.value<2000){
							imgUrl = "/images/tooltip-info.png";
						}else{
							imgUrl = "/images/tooltip-danger.png";
						}

						var tooltipSymbol = new esri.symbol.PictureMarkerSymbol(imgUrl, 59, 28);
						tooltipSymbol.setOffset(0, 25);
						var tooltipGraphic = new esri.Graphic(point, tooltipSymbol, {
							"data" : schoolItem,
							"name" : schoolItem.name,
							"tag" : "tooltip"
						});
						graphics.push(tooltipGraphic);

						var textNumSymbol = new esri.symbol.TextSymbol(schoolItem.value);
						//textNumSymbol.setAlign(esri.symbol.TextSymbol.ALIGN_START);
						textNumSymbol.setColor(new esri.Color([255,255,255]));
						textNumSymbol.setOffset(0, 25);
						var textNumGraphic = new Graphic(point, textNumSymbol, {
							"data" : schoolItem,
							"name" : schoolItem.name,
							"tag" : "num"
						});
						graphics.push(textNumGraphic);
					}*/
				}

				for(var i=0;i<graphics.length;i++){
					schoolPoiLayer.add(graphics[i]);
				} // end
			});
	};

	var query = function (keywords, callback) {
		$.ajax({
			url : "/queryschools",
			data : {
				key : keywords
			},
			dataType : "json",
			async : true,
			type : "GET",
			beforeSend : function () {
				gis_common.isLoadingAddCount();
			},
			success : function (req) {
				var resvalues = eval(req);
				callback(resvalues);
			},
			complete : function () {
				gis_common.isLoadingSunCount();
			},
			error : function (XMLHttpRequest, textStatus, errorThrown) {
				gis_common.showError("学校查询出错：" + XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
			}
		});
	};

	var createSchoolList = function(schools){
		var html = "";
		html += "<div class='row'>";
		html += "	<div class='col-md-6'>";
		html += "		<input id='school_text' type='text' class='form-control' placeholder='检索名称或地址'>";
		html += "	</div>";
		html += "	<div class='col-md-2'>";
		html += "		<button id='btn_school_query' class='btn btn-primary'>查 询</button>";
		html += "	</div>";
		html += "</div>";
		html += "<table class='table' style='width:380px;margin-top:5px;'>";
		/*html += "	<thead>";
		html += "		<tr>";
		html += "			<th>名称</th><th>招生</th><th>地址</th><th>经度</th><th>纬度</th>";
		html += "		</tr>";
		html += "	</thead>";*/
		html += "	<tbody>";
		html += "	</tbody>";
		html += "</table>";

		var htmlObj = $(html);
		var tbody = htmlObj.find("tbody");

		for(var i=0;i<schools.length;i++){
			var trHtml = "";
			trHtml += "<tr class='change' lon='"+schools[i].longitude+"' lat='"+schools[i].latitude+"'>";
			/*trHtml += "		<td>"+schools[i].name+"</td>";
			trHtml += "		<td>"+schools[i].value+"</td>";
			trHtml += "		<td>"+schools[i].address+"</td>";
			trHtml += "		<td>"+schools[i].longitude.toFixed(4)+"</td>";
			trHtml += "		<td>"+schools[i].latitude.toFixed(4)+"</td>";*/
			trHtml += "<td>";
			trHtml += "		<div class='row' style='height:65px;'>";
			trHtml += "			<div class='col-md-8'>";
			trHtml += "				<p><span>"+schools[i].name+"</span></p>";
			trHtml += "				<p style='margin-top:5px;'>招生："+schools[i].value+"</p>";
			trHtml += "				<p style='margin-top:5px;'>地址："+schools[i].address+"</p>";
			trHtml += "			</div>";
			trHtml += "			<div class='col-md-4'>";
			trHtml += "				<img height='60' width='95' src='/images/schoolimgs/school00"+schools[i].longitude.toString()[schools[i].longitude.toString().length-1]+".jpg'>";
			trHtml += "			</div>";
			trHtml += "		</div>";
			trHtml += "</td>";
			trHtml += "</tr>";
			tbody.append(trHtml);
		}

		htmlObj.find("#btn_school_query").unbind("click").bind("click",function(){
			var inputText = htmlObj.find("#school_text").val().split(' ').join('');
			if(inputText  == ""){
				tbody.empty();
				for(var i=0;i<schools.length;i++){
					var trHtml = "";
					trHtml += "<tr class='change' lon='"+schools[i].longitude+"' lat='"+schools[i].latitude+"'>";
					/*trHtml += "		<td>"+schools[i].name+"</td>";
					trHtml += "		<td>"+schools[i].value+"</td>";
					trHtml += "		<td>"+schools[i].address+"</td>";
					trHtml += "		<td>"+schools[i].longitude.toFixed(4)+"</td>";
					trHtml += "		<td>"+schools[i].latitude.toFixed(4)+"</td>";*/
					trHtml += "<td>";
					trHtml += "		<div class='row' style='height:65px;'>";
					trHtml += "			<div class='col-md-8'>";
					trHtml += "				<p><span>"+schools[i].name+"</span></p>";
					trHtml += "				<p style='margin-top:5px;'>招生："+schools[i].value+"</p>";
					trHtml += "				<p style='margin-top:5px;'>地址："+schools[i].address+"</p>";
					trHtml += "			</div>";
					trHtml += "			<div class='col-md-4'>";
					trHtml += "				<img height='60' width='95' src='/images/schoolimgs/school00"+schools[i].longitude.toString()[schools[i].longitude.toString().length-1]+".jpg'>";
					trHtml += "			</div>";
					trHtml += "		</div>";
					trHtml += "</td>";
					trHtml += "</tr>";
					tbody.append(trHtml);
				}
			}else{
				tbody.empty();
				var subSchools = [];
				for(var i=0;i<schools.length;i++){
					if(schools[i].name.indexOf(inputText)>-1 || schools[i].address.indexOf(inputText)>-1){
						subSchools.push(schools[i]);
					}
				}

				for(var i=0;i<subSchools.length;i++){
					var trHtml = "";
					trHtml += "<tr class='change' lon='"+schools[i].longitude+"' lat='"+schools[i].latitude+"'>";
					/*trHtml += "		<td>"+subSchools[i].name+"</td>";
					trHtml += "		<td>"+schools[i].value+"</td>";
					trHtml += "		<td>"+subSchools[i].address+"</td>";
					trHtml += "		<td>"+subSchools[i].longitude.toFixed(4)+"</td>";
					trHtml += "		<td>"+subSchools[i].latitude.toFixed(4)+"</td>";*/
					trHtml += "<td>";
					trHtml += "		<div class='row' style='height:65px;'>";
					trHtml += "			<div class='col-md-8'>";
					trHtml += "				<p><span>"+subSchools[i].name+"</span></p>";
					trHtml += "				<p style='margin-top:5px;'>招生："+subSchools[i].value+"</p>";
					trHtml += "				<p style='margin-top:5px;'>地址："+subSchools[i].address+"</p>";
					trHtml += "			</div>";
					trHtml += "			<div class='col-md-4'>";
					trHtml += "				<img height='60' width='95' src='/images/schoolimgs/school00"+subSchools[i].longitude.toString()[subSchools[i].longitude.toString().length-1]+".jpg'>";
					trHtml += "			</div>";
					trHtml += "		</div>";
					trHtml += "</td>";
					trHtml += "</tr>";
					tbody.append(trHtml);
				}
			}

			tbody.find("tr").unbind("click").bind("click",function(){
				var lon =  $(this).attr("lon");
				var lat = $(this).attr("lat");

				var level = gis_core.map.getMaxZoom() - 4;
				var point = new esri.geometry.Point(parseFloat(lon), parseFloat(lat), gis_core.map.spatialReference);
				gis_core.map.centerAndZoom(point, level);

				//打point
				var pointLayer = gis_core.getLayer("schoolCover");
				pointLayer.clear();
				var pointLabel = new esri.geometry.Point(parseFloat(lon), parseFloat(lat), gis_core.map.spatialReference);
				var symbolLabel = new esri.symbol.PictureMarkerSymbol('/images/point.png', 28, 28);
				symbolLabel.setOffset(0, 30);
				var graphicLabel = new esri.Graphic(pointLabel, symbolLabel,null);
				pointLayer.add(graphicLabel);
			});
		});

		//行点击
		htmlObj.find("tbody > tr").unbind("click").bind("click",function(){
			var lon = $(this).attr("lon");
			var lat = $(this).attr("lat");

			var level = gis_core.map.getMaxZoom() - 4;
			var point = new esri.geometry.Point(parseFloat(lon), parseFloat(lat), gis_core.map.spatialReference);
			gis_core.map.centerAndZoom(point, level);

			//打point
			var pointLayer = gis_core.getLayer("schoolCover");
			pointLayer.clear();
			var pointLabel = new esri.geometry.Point(parseFloat(lon), parseFloat(lat), gis_core.map.spatialReference);
			var symbolLabel = new esri.symbol.PictureMarkerSymbol('/images/point.png', 28, 28);
			symbolLabel.setOffset(0, 30);
			var graphicLabel = new esri.Graphic(pointLabel, symbolLabel,null);
			pointLayer.add(graphicLabel);
		});

		return htmlObj;
	};

	//初始化数据
	 this.initData = function () {
		$.ajax({
			url : "/getschools",
			type : "GET",
			beforeSend : function () {
				gis_common.isLoadingAddCount();
			},
			success : function (req) {
				var level = gis_core.map.getMaxZoom() - 6;
				var point = new esri.geometry.Point(116.37, 40.0012, gis_core.map.spatialReference);
				gis_core.map.centerAndZoom(point, level);

				loadLayer();
				schoolsObj = req;
				registerExtentChanged();
				//intervalObj = setInterval(intervalUpdateSchoolPoi,3000);

				//创建
				gis_layout.createLeftWidget("school_list_widget","学校列表",createSchoolList(schoolsObj));

				//注册查询事件
				gis_common.queryRegisterFun("school_res", query);
			},
			complete : function () {
				gis_common.isLoadingSunCount();
			},
			error : function (XMLHttpRequest, textStatus, errorThrown) {
				gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
			}
		});
	};

	this.createQueryParamHtml = function (callback) {
		var content = "";
		content += "<div class='form-group'>";
		content += " 	<div class='col-sm-9 col-sm-offset-3'>";
		content += "		<button id='endtoend_menu_btn_load' class='btn btn-primary'>查 询</button>";
		content += "	</div>";
		content += "</div>";

		var div = $(content);
		div.find("#endtoend_menu_btn_load").bind("click", function (e) {
			var level = gis_core.map.getMaxZoom() - 6;
			var point = new esri.geometry.Point(116.37, 40.0012, gis_core.map.spatialReference);
			gis_core.map.centerAndZoom(point, level);

			initData();
		});
		callback(div)
	};

	//初始化
	this.init = function () {
		//加载库文件
		dojo.require("esri.layers.WMSLayerInfo");
		dojo.require("esri.layers.WMSLayer");
		//注册模块
		gis_core.addPlugin(this);
	};

	//销毁  13415731471 CLnWsN83
	this.destroy = function () {
		//$("#school_list_widget").remove();
		gis_layout.removeLeftWidget("school_list_widget");
		//gis_core.removeLayer("schoolPoi");
		//gis_core.removeLayer("schoolCover");
		/*if(intervalObj != null){
			clearInterval(intervalObj);
			intervalObj = null;
		}*/

		schoolObj = null;
	};

	//模块信息
	this.title = {
		id : "00_01_13",
		name : "招生查询"
	};
}

//功能类 全局类
var schoolObj = null;
//初始LTE覆盖化
function initSchool() {
	if (schoolObj == null) {
		schoolObj = new School();
		schoolObj.init();
	}
}
//菜单 查询条件
function school_createQueryParamHtml(obj) {
	//gis_core.pluginDestroy();
	gis_core.pluginDestroyExceptId("00_01_13");
	initSchool();
	//gis_layout.createMenubarWithFun("school_createQueryParamHtml", obj, "招生查询", schoolObj.createQueryParamHtml)
	gis_layout.createMenubar("school_createQueryParamHtml", obj, schoolObj.initData)
}