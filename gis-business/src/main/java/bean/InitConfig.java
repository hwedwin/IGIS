package bean;

public class InitConfig {
	private String isTransfer;
	/**
	 * GIS类型
	 */
	private String gisType;
	/**
	 * 初始化位置
	 */
	private String extent;
	/**
	 * 坐标系
	 */
	private int wkid;
	/**
	 * arcgis 的请求地址
	 */
	private  String arcgisUrl;

	/**
	 * 标题
	 */
	private String title = "iGIS地理信息系统";

	public String getGisType() {
		return gisType;
	}

	public void setGisType(String gisType) {
		this.gisType = gisType;
	}

	public String getExtent() {
		return extent;
	}

	public void setExtent(String extent) {
		this.extent = extent;
	}

	public int getWkid() {
		return wkid;
	}

	public void setWkid(int wkid) {
		this.wkid = wkid;
	}

	public String getIsTransfer() {
		return isTransfer;
	}

	public void setIsTransfer(String isTransfer) {
		this.isTransfer = isTransfer;
	}

	public String getArcgisurl() {
		return arcgisUrl;
	}

	public void setArcgisurl(String arcgisUrl) {
		this.arcgisUrl = arcgisUrl;
	}

	public String getArcgisUrl() {
		return arcgisUrl;
	}

	public void setArcgisUrl(String arcgisUrl) {
		this.arcgisUrl = arcgisUrl;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
