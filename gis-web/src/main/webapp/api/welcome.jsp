<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
    <%@ include file="header.jsp" %>
    <section>
        <div class="container">
            <%@ include file="leftmenu.jsp" %>
            <input id="pageid" type="hidden" value="welcome">
            <div class="docs-content" style="height: 850px;">
                <h3 id="welcome" class="welcome"> Welcome</h3>

                <h2> 让我们开始iGIS之旅</h2>
                <br/>

                <p>程序员同学，你想做一些高大上的玩意儿吗，想做一些炫酷吹XX的应用吗，或者提高自己的竞争力成为GIS方面的稀缺人才吗。。。。。。</p>

                <p>那就让我们开始吧！</p>

                <p>如果你是第一次做GIS，那么我们得熟悉一些基础概念，比如：经度纬度、南北半球、投影、坐标系、点线面的关系、一次方程、二次方程、正余弦函数、圆相关的运算等等，
                    总之数学知识越多越好，思维越活跃越好，学习能力越强越好。然而这些都不是最重要的，最重要是加班越多进步越快！</p>

                <p>在iGIS的世界里，我们都得有对象，没对象也得创建对象林；用js定义对象，灵活小巧，使用方便，行云流水，让人欲罢不能。jquery也是一项必备技能，在项目中大量运用，如果不会并不代表
                    你的知识不足，只能说明你是领导或者你现在还很年轻。如果你很闲，还可以学习一项dojo；如果很忙，那你就去忙吧！</p>

                <p>现在让我们谈谈JAVA吧，你以为会写if...else...加for循环就够了吗，你以为写Hello word 就完成了吗？ No....... 你得知道怎么写循环执行最快、怎么封装方法通用性最好、
                    怎么设计算法效率最高、这样处理大数据量的运算、怎样合理使用缓存、怎样控制IO、怎样快速开发与部署。。。如果这些都搞定了，恭喜你，你得注意身体了，毕竟身体是革命的本钱。</p>
                <br/>

               <%-- <a href="#" type="button" class="btn-link">上一页</a>--%>
                <a href="/api/igis.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>

            </div>
        </div>
    </section>
</div>
</body>
</html>
