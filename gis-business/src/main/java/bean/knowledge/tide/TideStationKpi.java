package bean.knowledge.tide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xiexb on 2017/8/2.
 */
public class TideStationKpi {
    private String dateKey;
    private TideStationBean station;
    private TideDataDayBean tideDataDay;
    private TideWeatherBean tideWeather;
    private List<TideDataHourBean> tideDataHours = new ArrayList<>();
    private List<TideWeatherDayBean> tideWeatherDays = new ArrayList<>();
    private List<WeatherBean> tideWeatherOneDay = new ArrayList<>();

    public String getDateKey() {
        return dateKey;
    }

    public void setDateKey(String dateKey) {
        this.dateKey = dateKey;
    }

    public TideStationBean getStation() {
        return station;
    }

    public void setStation(TideStationBean station) {
        this.station = station;
    }

    public TideDataDayBean getTideDataDay() {
        return tideDataDay;
    }

    public void setTideDataDay(TideDataDayBean tideDataDay) {
        this.tideDataDay = tideDataDay;
    }

    public TideWeatherBean getTideWeather() {
        return tideWeather;
    }

    public void setTideWeather(TideWeatherBean tideWeather) {
        this.tideWeather = tideWeather;
    }

    public List<TideDataHourBean> getTideDataHours() {
        return tideDataHours;
    }

    public void setTideDataHours(List<TideDataHourBean> tideDataHours) {
        this.tideDataHours = tideDataHours;
    }

    public List<TideWeatherDayBean> getTideWeatherDays() {
        return tideWeatherDays;
    }

    public void setTideWeatherDays(List<TideWeatherDayBean> tideWeatherDays) {
        this.tideWeatherDays = tideWeatherDays;
    }

    public List<WeatherBean> getTideWeatherOneDay() {
        return tideWeatherOneDay;
    }

    public void setTideWeatherOneDay(List<WeatherBean> tideWeatherOneDay) {
        this.tideWeatherOneDay = tideWeatherOneDay;
    }
}
