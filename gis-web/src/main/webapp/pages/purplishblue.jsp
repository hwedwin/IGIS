<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="renderer" content="ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=7,IE=9">
    <meta http-equiv="X-UA-Compatible" content="IE=7,9">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">

    <title><c:out value="${module.name}"/></title>
    <%@ include file="jsbase.jsp" %>

    <link media="screen and (max-width:1650px) and (max-height:900px)" rel="stylesheet"
          href="/css/modules/tide/css/c-data-little.css"/>
    <link media="screen and (min-width:1650px) and (max-height:900px)" rel="stylesheet"
          href="/css/modules/tide/css/c-data-little.css"/>
    <link media="screen and (min-width:1650px) and (min-height:901px)" rel="stylesheet"
          href="/css/modules/tide/css/c-data.css"/>
    <link media="screen and (max-width:1650px) and (min-height:901px)" rel="stylesheet"
          href="/css/modules/tide/css/c-data-little.css"/>
    <link rel="stylesheet" type="text/css" href="/script/plugins/scrollbar/perfect-scrollbar-0.4.10.min.css">
    <style>
        .search-cont-list li:hover {
            background: #284f6f;
        }

        .srh-inp::-webkit-input-placeholder {
            color: #87b0d0;
        }

        .srh-inp:-moz-placeholder {
            color: #87b0d0;
        }

        .srh-inp::-moz-placeholder {
            color: #87b0d0;
        }

        .srh-inp:-ms-input-placeholder {
            color: #87b0d0;
        }
    </style>
    <script type="text/javascript" src="/script/base/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="/script/plugins/echartslcz/echarts.js"></script>

    <c:forEach items="${module.jsPath}" var="item">
        <script type="text/javascript" src="/script/<c:out value="${item}"></c:out>"></script>
    </c:forEach>
    <script type="text/javascript">
        requiretx.config({
            paths: {
                echarts: '/script/plugins/echartslcz/'
            }
        });

        function initgis() {
            <c:forEach items="${module.init}" var="item">
            <c:out value="${item}"></c:out>
            </c:forEach>
        }
        //浏览器版本是否低于IE8
        var lessThenIE8 = function () {
            var UA = navigator.userAgent,
                    isIE = UA.indexOf('MSIE') > -1,
                    v = isIE ? /\d+/.exec(UA.split(';')[1]) : 'no ie';
            return v < 8;
        }();
        //使用方法
        if (lessThenIE8) {
            dynamicLoading.css("/css/modules/tide/css/c-data-little.css");
            dynamicLoading.css("/css/modules/tide/css/c-data.css");
        }
    </script>
</head>
<body>
<div class="map-main">
    <%@ include file="mapbase.jsp" %>
    <!--搜索框-->
    <div class="search-panel">
        <div class="srh-l">
            <input id="tide-top-queryword" type="text" class="srh-inp"
                   style="width: 220px;background:#284f6f;border-color:#4D83b2;color:#82d1ff;" placeholder="关键字搜索"/>
        </div>
        <div class="srh-r">
            <button id="tide-top-btn-search" type="submit" class="btn btn-primary btn-search">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </div>
    <!--end搜索框-->

    <!--搜索弹出面板-->
    <div id='commonquery_content' class="modal-search" style="width: 260px;">
        <div>
            <div class="panel-body" style="padding:0;">
                <div class="search-cont">
                    <div class="hide-search" style="background: transparent;"></div>
                    <ul class="search-cont-list"
                        style="color: #82d1ff;background: url('/css/modules/tide/img/t-bg.png') repeat;">
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--end搜索弹出面板-->

    <!--港口提示弹出框-->
    <div class="map-tip" style="display: none;"><!--港口提示弹出框-->
        <div class="map-tip-title">
            <h3></h3>
        </div>
        <div class="map-tip-cont">
            <p></p>
        </div>
        <div class="map-tip-bottom">
        </div>
    </div>
    <!--港口提示弹出框结束-->

    <!--左侧弹框展开开始-->
    <div class="left-window-open">
        <div href="javacript:void(0);" class="close"></div>
        <%--<ul class="date-tab">
            &lt;%&ndash;<li><a data="yesterday" href="javascript:void (0);">昨天</a></li>&ndash;%&gt;
            &lt;%&ndash;<li><a data="today" href="javascript:void (0);" class="on">今日</a></li>&ndash;%&gt;
            &lt;%&ndash;<li><a data="nextday" href="javascript:void (0);">明天</a></li>&ndash;%&gt;
            &lt;%&ndash;<li><a data="week" href="javascript:void (0);">本周一</a></li>
            <li><a data="month" href="javascript:void (0);">本月一号</a></li>&ndash;%&gt;
        </ul>--%>
        <ul class="searchbar">
            <li><a type="select" href="javascript:void(0);" id="downmenu-select-continent" lab="continent" class="on">
                <span id="downmenu-select-item-continent-lable">亚洲</span><i class="down-arrow-ico"></i></a>
            </li>
            <li><a type="select" href="javascript:void(0);" id="downmenu-select-conutry" lab="conutry"><span
                    id="downmenu-select-item-conutry-lable">中国</span><i class="down-arrow-ico"></i></a>
            </li>
            <li><a type="select" href="javascript:void(0);" id="downmenu-select-prov" lab="prov"><span
                    id="downmenu-select-item-prov-lable">全部</span><i class="down-arrow-ico"></i></a>
            </li>
            <li><a type="select" href="javascript:void(0);" id="downmenu-select-city" lab="city"><span
                    id="downmenu-select-item-city-lable">全部</span><i class="down-arrow-ico"></i></a>
            </li>
            <%--<li><input id='endtoend_20_query_time_input' type='text' class="date-input" value="2017-07-10"
                       onFocus="WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd'});"></li>--%>
            <li><a id="tide-station-searchbtn" href="javascript:void(0);" class="searchbtn">查询</a></li>
        </ul>
        <div id="downmenu-select-item" class="downmenu" style="display: none;">
            <%--<a href="#" class="rodestyle">亚洲</a>--%>
        </div>
        <ul class="hot-port" style="position: relative;overflow-y:hidden;">
            <%--<li><a href="#">连云港</a></li>--%>
        </ul>
        <div class="thematic">
            <ul id="tide-station-tag-list" class="them-tab">
                <%--<li><a href="#" class="on">一带一路航线</a></li>
                <li><a href="#">热门沿海城市</a></li>
                <li><a href="#">热门航线</a></li>--%>
            </ul>
            <div class="tab-content" style="position: relative;overflow-y:hidden;overflow-x: hidden;">
                <p id="tab-content-p"></p>
            </div>
        </div>
        <div class="left-footer">
            <%--<a href="#" class="imger"><img src="/css/modules/tide/img/coord-red-ico.png"></a>
            <a href="#" class="imger"><img src="/css/modules/tide/img/coord-yellow-ico.png"></a>
            数据来源于：中国工程科技知识中心、气象分中心、地理分中心--%>
            数据来源：国家海洋信息中心、国家基础地理信息中心、国家气象信息中心、中国工程科技知识中心
        </div>
    </div>
    <!--左侧弹框展开结束-->

    <!--左侧弹框隐藏开始-->
    <div class="left-window-hidden" style="display: none;">
        <div class="open"></div>
        <%--<a href="#" class="imger"><img src="/css/modules/tide/img/coord-red-ico.png"></a>
        <a href="#" class="imger"><img src="/css/modules/tide/img/coord-yellow-ico.png"></a>--%>
        国家海洋信息中心、国家基础地理信息中心、国家气象信息中心、中国工程科技知识中心
    </div>
    <!--左侧弹框隐藏结束-->

    <!--右侧弹框展开开始-->
    <div class="right-window-open" style="right: -590px;">
        <div href="javascript:voud(0);" class="right-scroll"></div>
        <!--向右隐藏图标-->

        <div class="date-control">
            <input size="16" id='endtoend_20_query_time_input' type="text" value="2017-10-17" readonly
                   onFocus="WdatePicker({onpicking:timeChange,lang:'zh-cn',dateFmt:'yyyy-MM-dd'});">
        </div>
        <h2>港口名称：<span id="tide-station-detail-title">无</span></h2>

        <div class="tide-table"><!--潮汐表-->
            <h3>潮汐表 <span class="d-s">数据仅供参考</span></h3>
            <table class="tide-table-cont">
                <tbody id="tide-data-day-table">
                <tr>
                    <td class="f-b">潮时(Hrs)</td>
                    <td>无数据</td>
                </tr>
                <tr>
                    <td class="f-b">潮高(cm)</td>
                    <td>无数据td>
                </tr>
                </tbody>
            </table>
            <div class="other-tide" id="tide-data-day-other">
                时区:<span class="m-r-10"> -0800  (东八区) </span> 潮高基准面：在平均海平面上下241CN
            </div>
        </div>

        <div class="diagram"><!--曲线图-->
            <h3><span id="tide-data-hour-title"></span>潮汐曲线图</h3>

            <div id="tide-data-hour-line" class="diagram-cont">

            </div>
        </div>

        <div class="mete-data"><!--气象数据-->
            <h3>气象数据</h3>

            <div class="mete-model" style="display: none;"><!--气象弹出框-->
                <div class="mete-model-title"></div>
                <div class="mete-model-cont">
                    <table class="metemodel-table">
                        <tbody>
                        <tr>
                            <td class="t-b">潮时(Hrs)</td>
                            <td>03:49</td>
                            <td class="t-b">潮时(Hrs)</td>
                            <td>03:49</td>
                        </tr>
                        <tr>
                            <td class="t-b">潮时(Hrs)</td>
                            <td>03:49</td>
                            <td class="t-b">潮时(Hrs)</td>
                            <td>03:49</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="mete-model-bottom"></div>
            </div>

            <div class="mete-cont">
                <a class="front-scroll-ico"></a>
                <a class="back-scroll-ico"></a>

                <div class="mete-cont-list">
                    <ul id="tide-ul-weather-item">
                        <%--<li>
                            <p>08/17 15时</p>

                            <p>小雨转阴</p>

                            <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                            <p>21~29.</p>

                            <p>西北风</p>

                            <p>3.6 km/h</p>
                        </li>
                        <li>
                            <p>08/17 15时</p>

                            <p>小雨转阴</p>

                            <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                            <p>21~29.</p>

                            <p>西北风</p>

                            <p>3.6 km/h</p>
                        </li>
                        <li>
                            <p>08/17 15时</p>

                            <p>小雨转阴</p>

                            <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                            <p>21~29.</p>

                            <p>西北风</p>

                            <p>3.6 km/h</p>
                        </li>
                        <li>
                            <p>08/17 15时</p>

                            <p>小雨转阴转阴</p>

                            <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                            <p>21~29.</p>

                            <p>西北风</p>

                            <p>3.6 km/h</p>
                        </li>--%>
                    </ul>
                </div>
            </div>
            <%--<table class="time-weather" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td rowspan="4" width="35%">
                        <div class="t-l-max" style="width: 140px;">
                            &lt;%&ndash; <p class="max-font">18&#176;</p>&ndash;%&gt;
                            <p class="max-font">0&#176;</p>

                            <p class="time-text">2017年4月12日</p>
                        </div>
                        <div class="t-r-max">
                            <img id="tide-station-weather-temp-pic"
                                 src="/css/modules/tide/img/c-weather-ico/sun-ico.png" class="maximg">
                        </div>
                    </td>
                </tr>
                &lt;%&ndash;<tr>
                    <td class="tdline">6时</td>
                    <td class="tdline">7时</td>
                    <td class="tdline">8时</td>
                    <td class="tdline">9时</td>
                    <td class="tdline">10时</td>
                    <td class="tdline">11时</td>
                </tr>
                <tr>
                    <td class="tdline"><img src="/css/modules/tide/img/c-weather-ico/sun-ico.png"></td>
                    <td class="tdline"><img src="/css/modules/tide/img/c-weather-ico/sun-ico.png"></td>
                    <td class="tdline"><img src="/css/modules/tide/img/c-weather-ico/sun-ico.png"></td>
                    <td class="tdline"><img src="/css/modules/tide/img/c-weather-ico/sun-ico.png"></td>
                    <td class="tdline"><img src="/css/modules/tide/img/c-weather-ico/sun-ico.png"></td>
                    <td class="tdline"><img src="/css/modules/tide/img/c-weather-ico/sun-ico.png"></td>
                </tr>
                <tr>
                    <td class="tdline">18∘</td>
                    <td class="tdline">18∘</td>
                    <td class="tdline">18∘</td>
                    <td class="tdline">18∘</td>
                    <td class="tdline">18∘</td>
                    <td class="tdline">18∘</td>
                </tr>&ndash;%&gt;
                </tbody>
            </table>
            <table class="week-weather">
                <tbody id="tide-weather-day-table">
                &lt;%&ndash;<tr>
                    <td class="invalid">昨天</td>
                    <td>今天</td>
                    <td>周三</td>
                    <td>周四</td>
                    <td>周五</td>
                    <td>周六</td>
                </tr>
                <tr>
                    <td class="invalid">03/31</td>
                    <td>04/1</td>
                    <td>04/2</td>
                    <td>04/3</td>
                    <td>04/4</td>
                    <td>04/5</td>
                </tr>
                <tr class="wea">
                    <td class="invalid">多云</td>
                    <td>雷阵雨</td>
                    <td>多云</td>
                    <td>雷阵雨</td>
                    <td>多云</td>
                    <td>雷阵雨</td>
                </tr>
                <tr>
                    <td><img src="/css/modules/tide/img/c-weather-ico/sun-ico.png"></td>
                    <td><img src="/css/modules/tide/img/c-weather-ico/sun-ico.png"></td>
                    <td><img src="/css/modules/tide/img/c-weather-ico/sun-ico.png"></td>
                    <td><img src="/css/modules/tide/img/c-weather-ico/sun-ico.png"></td>
                    <td><img src="/css/modules/tide/img/c-weather-ico/sun-ico.png"></td>
                    <td><img src="/css/modules/tide/img/c-weather-ico/sun-ico.png"></td>
                </tr>&ndash;%&gt;
                </tbody>
            </table>--%>
        </div>
    </div>
    <!--右侧弹框展开结束-->

    <!--右侧弹框隐藏开始-->
    <div class="right-window-hidden" style="display: none;">
        <div class="left-scroll"></div>
        <!--向左展开图标-->
    </div>
    <!--右侧弹框隐藏结束-->

</div>
<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?789fd650fa0be6a2a064d019d890b87f";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>
</body>
</html>