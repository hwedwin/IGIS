/**
 * Created by Xiaobing on 2016/11/11.
 */
function Fivenet() {
    var cell_layer = gis_core.addGraphicsLayer("five_cell_rings_layer", "边界图层");
    var cpn_cell_layer = gis_core.addGraphicsLayer("five_cpn_cell_rings_layer", "驻地网小区");
    var wmsLayer = null;
    var time = "";
    var cityid = "";
    var citytype = "";
    var selectid = "";
    var cpn_cityid = null;
    var cpn_citytype = null;
    var cpnCellLayer_onClick_handle = null;

    //需要营销的家宽小区
    var loadSaleCellTable = function () {
        var content = "<div>";
        content += "<table id='fivenet_salecell_Table'></table>";
        content += "<div id='fivenet_salecell_Pager'></div>"
        content += "<div>";
        var div = $(content);

        gis_layout.createLeftWidget("fivenet_salecell_JQGrid_Div", "需要营销家宽小区", div);
        div.find("#fivenet_salecell_Table").jqGrid({
            url: '/getfivenetsalecelllist?time=' + time + "&timdim=60&cityid=" + cityid + "&citytype=" + citytype,
            datatype: "json",
            width: 400,
            height: 230,
            colNames: ['地市', '小区名称', 'id'],
            colModel: [{
                name: 'cityName',
                index: 'cityId',
                width: 40
            }, {
                name: 'name',
                index: 'name',
                width: 40
            }, {
                name: 'id',
                index: 'id',
                width: 40,
                align: "right",
                hidden: true
            }
            ],
            rowNum: 10,
            rowList: [10, 20, 30, 50],
            pager: '#fivenet_salecell_Pager',
            mtype: "get",
            viewrecords: true,
            caption: "",
            onSelectRow: function (id) {
                var data = div.find("#fivenet_salecell_Table").jqGrid('getRowData', id);
                loadCommuseCell(id, "commun");
                loadSaleCellDetail(id);
            }
        });

    };
    var loadSaleCellDetail = function (id) {
        $.ajax({
            url: "/getfivenetsalecelldetail",
            data: {
                time: time,
                timdim: 60,
                cityid: cityid,
                citytype: citytype,
                id: id
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.isLoadingSunCount();
                gis_common.outPut(req, function (data) {
                    var kpi = eval(data);
                    var div = gis_common.createKeyValueList(kpi);
                    gis_layout.createRightWidget("fivenet_createSaleCellDetailHtml", "小区明细", div);
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.isLoadingSunCount();
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    //需要建设的驻地网小区
    var loadBuildCellTable = function () {
        var content = "<div>";
        content += "<table id='fivenet_bulidcell_Table'></table>";
        content += "<div id='fivenet_bulidcell_Pager'></div>"
        content += "<div>";
        var div = $(content);

        gis_layout.createLeftWidget("fivenet_bulidcell_JQGrid_Div", "需要建设家宽小区", div);
        div.find("#fivenet_bulidcell_Table").jqGrid({
            url: '/getfivenetbuildcelllist?time=' + time + "&timdim=60&cityid=" + cityid + "&citytype=" + citytype,
            datatype: "json",
            width: 400,
            height: 230,
            colNames: ['地市', '小区名称', 'extnet', 'rings'],
            colModel: [{
                name: 'cityName',
                index: 'cityId',
                width: 40
            }, {
                name: 'name',
                index: 'name',
                width: 40
            }, {
                name: 'extent',
                index: 'extent',
                width: 40,
                align: "right",
                hidden: true
            }, {
                name: 'rings',
                index: 'rings',
                width: 80,
                align: "right",
                hidden: true
            }
            ],
            rowNum: 10,
            rowList: [10, 20, 30, 50],
            pager: '#fivenet_bulidcell_Pager',
            mtype: "get",
            viewrecords: true,
            caption: "",
            onSelectRow: function (id) {
                var data = div.find("#fivenet_bulidcell_Table").jqGrid('getRowData', id);
                loadCommuseCell(id, "commun");
                loadBuildCellDetail(id);
            }
        });

    };
    var loadBuildCellDetail = function (id) {
        $.ajax({
            url: "/getfivenetbuildcelldetail",
            data: {
                time: time,
                timdim: 60,
                cityid: cityid,
                citytype: citytype,
                id: id
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.isLoadingSunCount();
                gis_common.outPut(req, function (data) {
                    var kpi = eval(data);
                    var div = gis_common.createKeyValueList(kpi);
                    gis_layout.createRightWidget("fivenet_createSaleCellDetailHtml", "小区明细", div);
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.isLoadingSunCount();
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    //需要营销的用户
    var loadSaleUserTable = function (cpn) {
        var content = "<div>";
        content += "<table id='fivenet_sale_user_Table'></table>";
        content += "<div id='fivenet_sale_user_Pager'></div>"
        content += "<div>";
        var div = $(content);

        gis_layout.createLeftWidget("fivenet_sale_user_JQGrid_Div", "需要营销家宽用户", div);
        div.find("#fivenet_sale_user_Table").jqGrid({
            url: '/getfivenetsaleuserlist?time=' + time + "&timdim=60&cityid=" + cityid + "&citytype=" + citytype + "&bw=" + cpn,
            datatype: "json",
            width: 400,
            height: 230,
            colNames: ["msisdn", '地市', '小区名称'],
            colModel: [{
                name: 'msisdn',
                index: 'msisdn',
                width: 60
            }, {
                name: 'cityName',
                index: 'cityName',
                width: 20
            }, {
                name: 'cellName',
                index: 'cellName',
                align: "right"
            }
            ],
            rowNum: 10,
            rowList: [10, 20, 30, 50],
            pager: '#fivenet_sale_user_Pager',
            mtype: "get",
            viewrecords: true,
            caption: "",
            onSelectRow: function (id) {
                var data = div.find("#fivenet_sale_user_Table").jqGrid('getRowData', id);
                //loadCommuseCell(id, "commun");
                //loadBuildCellDetail(id);
                loadSaleUserDetail(data.msisdn);
            }
        });
    };
    var loadSaleUserDetail = function (id) {
        $.ajax({
            url: "/getfivenetsaleuserdetail",
            data: {
                time: time,
                timdim: 60,
                cityid: cityid,
                citytype: citytype,
                id: id
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.isLoadingSunCount();
                gis_common.outPut(req, function (data) {
                    var kpi = data.detail;
                    var div = gis_common.createKeyValueList(kpi);
                    gis_layout.createRightWidget("fivenet_saleuserdetailHtml", "用户明细", div);
                    if (data.cell1 != null && data.cell1 != undefined) {
                        createCellRing(data.cell1);
                    }
                    if (data.cell2 != null && data.cell2 != undefined) {
                        createCellRing(data.cell2);
                    }
                    if (data.cell3 != null && data.cell3 != undefined) {
                        createCellRing(data.cell3);
                    }
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.isLoadingSunCount();
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    //驻地网小区
    var laodCpnCell = function () {
        cpn_cell_layer.clear();
        if (gis_common.stringIsNullOrWhiteSpace(cpn_cityid) || gis_common.stringIsNullOrWhiteSpace(cpn_citytype)) {
            return;
        }

        $.ajax({
            url: "/getfivenetcpncells",
            data: {
                extent: JSON.stringify(gis_common.getMapExtent()),
                time: time,
                timdim: 60,
                cityid: cpn_cityid,
                citytype: cpn_citytype
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.isLoadingSunCount();
                gis_common.outPut(req, function (data) {
                    if (data != null && data != undefined) {
                        for (var i = 0; i < data.length; i++) {
                            var d = data[i];
                            var attr = {
                                "data": d
                            };
                            var point = new esri.geometry.Point(d.longitude, d.latitude, gis_core.getSpatialReference());
                            var symbol = new esri.symbol.PictureMarkerSymbol('/images/WLanInner.png', 20, 20);
                            var graphic = new esri.Graphic(point, symbol, attr);
                            cpn_cell_layer.add(graphic);
                        }
                    }
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.isLoadingSunCount();
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var cpnCellLayerClick = function (evt) {
        var data = evt.graphic.attributes.data;
        if (data == null) {
            return;
        }
        $.ajax({
            url: "/getfivecpncelldetail",
            data: {
                id: data.id
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.isLoadingSunCount();
                gis_common.outPut(req, function (data) {
                    var div = gis_common.createKeyValueList(data);
                    gis_layout.createRightWidget("fivenet_cpncelldetailHtml", "驻地网小区信息", div);
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.isLoadingSunCount();
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };


    //数据校验 //需要建设的驻地网小区
    this.loadGaode_Comm_CellTable = function () {
        var content = "<div>";
        content += "<table id='fivenet_gaodecomm_cell_Table'></table>";
        content += "<div id='fivenet_gaodecomm_cell_Pager'></div>"
        content += "<div>";
        var div = $(content);

        gis_layout.createLeftWidget("fivenet_gaodecomm_cell_JQGrid_Div", "需要建设家宽小区", div);
        div.find("#fivenet_gaodecomm_cell_Table").jqGrid({
            url: '/getgaode_comm_cellwithpage?time=' + time + "&timdim=60&cityid=" + cityid + "&citytype=" + citytype,
            datatype: "json",
            width: 400,
            height: 230,
            colNames: ['地市', '小区名称', 'extnet', 'rings', 'Delete'],
            colModel: [{
                name: 'cityName',
                index: 'cityId',
                width: 40
            }, {
                name: 'name',
                index: 'name',
                width: 40
            }, {
                name: 'extent',
                index: 'extent',
                width: 40,
                align: "right",
                hidden: true
            }, {
                name: 'rings',
                index: 'rings',
                width: 80,
                align: "right",
                hidden: true
            },
                {name: 'Delete', index: 'Delete', sortable: false, align: "center", width: 30}
            ],
            rowNum: 10,
            rowList: [10, 20, 30, 50],
            pager: '#fivenet_gaodecomm_cell_Pager',
            mtype: "get",
            viewrecords: true,
            caption: "",
            gridComplete: function () {
                var ids = jQuery("#fivenet_gaodecomm_cell_Table").jqGrid('getDataIDs');
                for (var i = 0; i < ids.length; i++) {
                    var id = ids[i];
                    var DeleteBtn = "<a href='#' style='color:#f60' onclick='fivenet_gaode_comm_delete_cell(" + id + ")' >删除</a>";
                    jQuery("#fivenet_gaodecomm_cell_Table").jqGrid('setRowData', ids[i], {Delete: DeleteBtn});
                }
            },
            onSelectRow: function (id) {
                var data = div.find("#fivenet_gaodecomm_cell_Table").jqGrid('getRowData', id);
                createCellRing(data)
                loadWmsLayer();
            }
        });

    };
    this.del = function (id) {
        $.ajax({
            url: "/del_getgaode_comm_cell",
            data: {
                id: id
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.isLoadingSunCount();
                $("#fivenet_gaodecomm_cell_Table").trigger("reloadGrid");
                cell_layer.clear();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.isLoadingSunCount();
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    //公用方法
    var loadCommuseCell = function (id, type) {
        $.ajax({
            url: "/getfivenetcommucellinfor",
            data: {
                type: type,
                id: id
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.isLoadingSunCount();
                gis_common.outPut(req, function (data) {
                    if (data != null) {
                        createCellRing(data);
                    }
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.isLoadingSunCount();
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var createCellRing = function (data) {
        cell_layer.clear();

        var extent = data.extent.split("|");
        gis_common.setExtent(parseFloat(extent[1]) + 0.002, parseFloat(extent[3]) + 0.002, parseFloat(extent[0]) - 0.002, parseFloat(extent[2]) - 0.002);
        var rings = data.rings.split(",");
        var ring = [];
        for (var i = 0; i < rings.length; i = i + 2) {
            ring.push([rings[i], rings[i + 1]])
        }
        var polygon = new esri.geometry.Polygon(gis_core.initconfig.wkid);
        polygon.addRing(ring);

        var symbol = new esri.symbol.SimpleFillSymbol();
        symbol.setColor(new esri.Color([180, 168, 192, 0.5]));
        symbol.setOutline(new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new esri.Color([200, 112, 181, 1]), 1));
        var graphic = new esri.Graphic(polygon, symbol);
        var infoTemplate = new esri.InfoTemplate("属性", "<tr>居民小区名称: <td>" + data.name + "</td></tr>");
        graphic.setInfoTemplate(infoTemplate);
        cell_layer.add(graphic);
    };
    var loadWmsLayer = function () {
        var url = "/dynamicmap/ltefivenetgaodecellcover/weak_rat/aaaa";
        if (wmsLayer != null) {
            return;
        }
        wmsLayer = new esri.layers.WMSLayer(url, {
            resourceInfo: {
                extent: new esri.geometry.Extent(73.2672757, 18.37574, -109.66552734375, 133.29657, {
                    wkid: 4326
                }),
                layerInfos: []
            },
            visibleLayers: ['fivenetInvewmsLayer']
        });
        wmsLayer.id = "fivenetInvewmsLayer";
        gis_core.insertLayer("fivenetInvewmsLayer_id", "网格覆盖", 2, wmsLayer);
    };
    var extentchange = function (evt) {
        laodCpnCell();
    };


    /**
     * 清理
     */
    this.clear = function () {
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();
        gis_layout.removeLegend();
        gis_core.removeLayer("fivenetInvewmsLayer");
        wmsLayer = null;
    };
    /**
     * 初始化
     */
    this.init = function () {
        dojo.require("esri.layers.WMSLayerInfo");
        dojo.require("esri.layers.WMSLayer");
        //加载库文件
        gis_core.addExtentChange("fivenet", extentchange);
        if (cpnCellLayer_onClick_handle != null) {
            dojo.disconnect(cpnCellLayer_onClick_handle);
        }
        cpnCellLayer_onClick_handle = cpn_cell_layer.on("click", cpnCellLayerClick);

        //注册模块
        gis_core.addPlugin(this);
    };
    /**
     * 销毁
     */
    this.destroy = function () {
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();
        gis_layout.removeLegend();
    };
    /**
     * 模块信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "00_06_00",
        name: "无线与有线协调分析"
    };

    /**
     *
     * @param callback
     */
    this.createSaleCellHtml = function (callback) {
        gis_resources.getRegions(function (regions) {
            var content = "<div class=\"form-group\">";
            content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
            content += "	<div id='res_type' class='col-sm-9'>";
            content += "			<input id='fivenet_query_sale_param_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd'});\"/>";
            content += " 	</div > ";
            content += " </div > "

            content += "<div class='form-group'>";
            content += "	<label for='' class='col-sm-3 control-label'>地市:</label>";
            content += " 	<div class='col-sm-9'>";
            content += "		<select id='fivenet_query_sale_param_region_select' class='form-control'>";
            if (regions != null && regions != undefined && regions.length > 0) {
                for (var i = 0; i < regions.length; i++) {
                    var r = regions[i];
                    if (r.type != 9000) {
                        content += "<option nename='" + r.name + "' neid='" + r.id + "' netype='" + r.type + "'>" + r.name + "</option>";
                    }
                }
            }
            content += "		</select>";
            content += "	</div>";
            content += "</div>";

            /*content += "<div class='form-group'>";
             content += "	<label for='' class='col-sm-3 control-label'>有无竞争对手:</label>";
             content += " 	<div class='col-sm-9'>";
             content += "		<select id='fivenet_query_sale_param_cpn_select' class='form-control'>";
             content += "        <option value='-1' selected='selected'>全部</option>";
             content += "        <option value='0'>无竞争对手</option>";
             content += "        <option value='1'>有竞争对手</option>";
             content += "		</select>";
             content += "	</div>";
             content += "</div>";*/

            content += "<div class='form-group'>";
            content += " 	<div class='col-sm-9 col-sm-offset-3'>";
            content += "		<button id='fivenet_query_sale_param_btn_load' class='btn btn-primary'>确 定</button>";
            content += "	</div>";
            content += "</div>";


            var div = $(content);
            div.find("#fivenet_query_sale_param_btn_load").bind("click", function () {
                //时间
                time = div.find("#fivenet_query_sale_param_time").val().replace(/-/g, '').replace(/ /g, '');
                if (gis_common.stringIsNullOrWhiteSpace(time)) {
                    gis_common.showMessage("请选择时间");
                    div.find("#fivenet_query_sale_param_time").focus();
                    return;
                }
                //地市
                var selects = div.find("#fivenet_query_sale_param_region_select option:selected");
                if (selects != null && selects != undefined) {
                    //cityid = selects.attr("neid");
                    cityid = selects.attr("neid");
                    citytype = selects.attr("netype");
                } else {
                    gis_common.showMessage("请选择地市");
                    div.find("#fivenet_query_sale_param_region_select").focus();
                    return;
                }
                var cpn_select = div.find("#fivenet_query_sale_param_cpn_select").val();
                loadSaleCellTable(cpn_select);
            });
            callback(div);
        });
    }

    /**
     *
     * @param callback
     */
    this.createBuildCellHtml = function (callback) {
        gis_resources.getRegions(function (regions) {
            var content = "<div class=\"form-group\">";
            content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
            content += "	<div id='res_type' class='col-sm-9'>";
            content += "			<input id='fivenet_query_build_param_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd'});\"/>";
            content += " 	</div > ";
            content += " </div > "


            content += "<div class='form-group'>";
            content += "	<label for='' class='col-sm-3 control-label'>地市:</label>";
            content += " 	<div class='col-sm-9'>";
            content += "		<select id='fivenet_query_build_param_region_select' class='form-control'>";
            if (regions != null && regions != undefined && regions.length > 0) {
                for (var i = 0; i < regions.length; i++) {
                    var r = regions[i];
                    if (r.type != 9000) {
                        content += "<option nename='" + r.name + "' neid='" + r.id + "' netype='" + r.type + "'>" + r.name + "</option>";
                    }
                }
            }
            content += "		</select>";
            content += "	</div>";
            content += "</div>";


            content += "<div class='form-group'>";
            content += " 	<div class='col-sm-9 col-sm-offset-3'>";
            content += "		<button id='fivenet_query_build_param_btn_load' class='btn btn-primary'>确 定</button>";
            content += "	</div>";
            content += "</div>";


            var div = $(content);
            div.find("#fivenet_query_build_param_btn_load").bind("click", function () {
                //时间
                time = div.find("#fivenet_query_build_param_time").val().replace(/-/g, '').replace(/ /g, '');
                if (gis_common.stringIsNullOrWhiteSpace(time)) {
                    gis_common.showMessage("请选择时间");
                    div.find("#fivenet_query_build_param_time").focus();
                    return;
                }
                //地市
                var selects = div.find("#fivenet_query_build_param_region_select option:selected");
                if (selects != null && selects != undefined) {
                    //cityid = selects.attr("neid");
                    cityid = selects.attr("neid");
                    citytype = selects.attr("netype");
                } else {
                    gis_common.showMessage("请选择地市");
                    div.find("#fivenet_query_build_param_region_select").focus();
                    return;
                }
                loadBuildCellTable();
            });
            callback(div);
        });
    }

    this.createSaleUserHtml = function (callback) {
        gis_resources.getRegions(function (regions) {
            var content = "<div class=\"form-group\">";
            content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
            content += "	<div id='res_type' class='col-sm-9'>";
            content += "			<input id='fivenet_query_sale_user_param_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd'});\"/>";
            content += " 	</div > ";
            content += " </div > "

            content += "<div class='form-group'>";
            content += "	<label for='' class='col-sm-3 control-label'>地市:</label>";
            content += " 	<div class='col-sm-9'>";
            content += "		<select id='fivenet_query_sale_user_param_region_select' class='form-control'>";
            if (regions != null && regions != undefined && regions.length > 0) {
                for (var i = 0; i < regions.length; i++) {
                    var r = regions[i];
                    if (r.type != 9000) {
                        content += "<option nename='" + r.name + "' neid='" + r.id + "' netype='" + r.type + "'>" + r.name + "</option>";
                    }
                }
            }
            content += "		</select>";
            content += "	</div>";
            content += "</div>";

            content += "<div class='form-group'>";
            content += "	<label for='' class='col-sm-3 control-label'>有无竞争对手:</label>";
            content += " 	<div class='col-sm-9'>";
            content += "		<select id='fivenet_query_sale_user_param_cpn_select' class='form-control'>";
            content += "        <option value='-1' selected='selected'>全部</option>";
            content += "        <option value='0'>无竞争对手</option>";
            content += "        <option value='1'>有竞争对手</option>";
            content += "		</select>";
            content += "	</div>";
            content += "</div>";

            content += "<div class='form-group'>";
            content += " 	<div class='col-sm-9 col-sm-offset-3'>";
            content += "		<button id='fivenet_query_sale_user_param_btn_load' class='btn btn-primary'>确 定</button>";
            content += "	</div>";
            content += "</div>";


            var div = $(content);
            div.find("#fivenet_query_sale_user_param_btn_load").bind("click", function () {
                //时间
                time = div.find("#fivenet_query_sale_user_param_time").val().replace(/-/g, '').replace(/ /g, '');
                if (gis_common.stringIsNullOrWhiteSpace(time)) {
                    gis_common.showMessage("请选择时间");
                    div.find("#fivenet_query_sale_user_param_time").focus();
                    return;
                }
                //地市
                var selects = div.find("#fivenet_query_sale_user_param_region_select option:selected");
                if (selects != null && selects != undefined) {
                    //cityid = selects.attr("neid");
                    cityid = selects.attr("neid");
                    citytype = selects.attr("netype");
                } else {
                    gis_common.showMessage("请选择地市");
                    div.find("#fivenet_query_sale_user_param_region_select").focus();
                    return;
                }
                var cpn = div.find("#fivenet_query_sale_user_param_cpn_select").val();
                loadSaleUserTable(cpn);
            });
            callback(div);
        });
    }

    this.createtoolBarsCpnCell = function (callback) {
        gis_resources.getRegions(function (regions) {
            var content = "";
            content += "<div class='form-group'>";
            content += "	<label class='col-sm-3 control-label'>地市:</label>";
            content += " 	<div class='col-sm-9'>";
            content += "		<select id='fivenet_query_toolbar_cpn_cell_param_region_select' class='form-control'>";
            if (regions != null && regions != undefined && regions.length > 0) {
                for (var i = 0; i < regions.length; i++) {
                    var r = regions[i];
                    if (r.type != 9000) {
                        content += "<option nename='" + r.name + "' neid='" + r.id + "' netype='" + r.type + "'>" + r.name + "</option>";
                    }
                }
            }
            content += "		</select>";
            content += "	</div>";
            content += "</div>";

            content += "<div class='form-group'>";
            content += " 	<div class='col-sm-9 col-sm-offset-3'>";
            content += "		<button id='fivenet_query_toolbar_cpn_cell_param_btn_load' class='btn btn-primary'>确 定</button>";
            content += "		<button id='fivenet_query_toolbar_cpn_cell_param_btn_cal' class='btn btn-primary' style='left: 30px'>取 消</button>";
            content += "	</div>";
            content += "</div>";

            var div = $(content);
            div.find("#fivenet_query_toolbar_cpn_cell_param_btn_load").bind("click", function () {
                var cid = null;
                var ctype = null;
                //地市
                var selects = div.find("#fivenet_query_toolbar_cpn_cell_param_region_select option:selected");
                if (selects != null && selects != undefined) {
                    cpn_cityid = selects.attr("neid");
                    cpn_citytype = selects.attr("netype");
                } else {
                    gis_common.showMessage("请选择地市");
                    div.find("#fivenet_query_toolbar_cpn_cell_param_region_select").focus();
                    return;
                }

                laodCpnCell();
            });
            div.find("#fivenet_query_toolbar_cpn_cell_param_btn_cal").bind("click", function () {
                cpn_cell_layer.clear();
                cpn_cityid = null;
                cpn_citytype = null;
            });
            callback(div);
        });
    };

    /**
     * 查询小区边界
     * @param id
     * @param type
     */
    this.loadCellRing = function (id, type) {
        $.ajax({
            url: "/getfivenetcommucellinfor",
            data: {
                type: type,
                id: id
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.isLoadingSunCount();
                gis_common.outPut(req, function (data) {
                    if (data != null && data != "" && data != undefined) {
                        createCellRing(data);
                    }
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.isLoadingSunCount();
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    this.loadSaleUser = function (tim, id) {
        time = tim;
        loadSaleUserDetail(id);
    };

    this.loadSaleCell = function (tim, cellid) {
        time = tim;
        loadBuildCellDetail(cellid);
        loadCommuseCell(cellid, "commun");
    };

    this.loadBuildCell = function (tim, cellid) {
        time = tim;
        loadBuildCellDetail(cellid);
        loadCommuseCell(cellid, "commun");
    };
}

var fivenet = null;

function initFivenet() {
    if (fivenet == null) {
        fivenet = new Fivenet();
    }
    fivenet.init();
}

function fivenet_createSaleCell_QueryParamHtml(obj) {
    initFivenet();
    gis_core.pluginDestroyExceptId(fivenet.title.id);
    fivenet.clear();
    gis_layout.createMenubarWithFun("fivenet_createSaleCell_QueryParamHtml", obj, "需要营销家宽小区", fivenet.createSaleCellHtml)
}

function fivenet_createBulidCell_QueryParamHtml(obj) {
    initFivenet();
    gis_core.pluginDestroyExceptId(fivenet.title.id);
    fivenet.clear();
    gis_layout.createMenubarWithFun("fivenet_createBulidCell_QueryParamHtml", obj, "需要建设家宽小区", fivenet.createBuildCellHtml)
}

function finvenet_createSaleUser_queryParamHtml(obj) {
    initFivenet();
    gis_core.pluginDestroyExceptId(fivenet.title.id);
    fivenet.clear();
    gis_layout.createMenubarWithFun("finvenet_createSaleUser_queryParamHtml", obj, "需要营销家宽用户", fivenet.createSaleUserHtml)
}

function fivenet_createtoolBarsCpnCell_queryParamHtem(obj) {
    initFivenet();
    gis_layout.createToolbarWithFun("fivenet_createtoolBarsCpnCell_queryParamHtem", obj, 200, fivenet.createtoolBarsCpnCell)
}

function fivenet_gaoce_comm_cell_table(obj) {
    initFivenet();
    gis_core.pluginDestroyExceptId(fivenet.title.id);
    fivenet.clear();
    gis_layout.createMenubar("fivenet_gaoce_comm_cell_table", obj, fivenet.loadGaode_Comm_CellTable)
}

function fivenet_gaode_comm_delete_cell(id) {
    var _id = $(id).prop("id")
    if (confirm("确定需要删除吗？")) {
        fivenet.del(_id);
    }
}

function fivenet_queryCellRing() {
    initFivenet();
    gis_core.pluginDestroyExceptId(fivenet.title.id);
    fivenet.clear();
    var id = gis_common.getQueryString("id");
    var type = gis_common.getQueryString("type");
    fivenet.loadCellRing(id, type);
}

function fivenet_init() {
    initFivenet();
    var fun = gis_common.getQueryString("fun");
    var time = gis_common.getQueryString("time");
    if (fun == "saleuser") {
        var msisdn = gis_common.getQueryString("msisdn");
        fivenet.loadSaleUser(time, msisdn);
    }
    else if (fun == "salecell") {
        var cellid = gis_common.getQueryString("cellid");
        fivenet.loadSaleCell(time, cellid);
    }
    else if (fun == "buildcell") {
        var cellid = gis_common.getQueryString("cellid");
        fivenet.loadBuildCell(time, cellid);
    }
}