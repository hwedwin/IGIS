package bean.highspeedrail;

/**
 * Created by Xiaobing on 2016/5/26.
 * 铁路指标 最小栅格数据
 */
public class TrackGridValue {
    private String time;
    private String trackId;
    private String trackName;
    private String cityId;
    private String cityName;
    private String stationInterId;
    private String stationInterName;
    private String roadgridId;
    private String roadId;
    private String roadName;
    private double value;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStationInterId() {
        return stationInterId;
    }

    public void setStationInterId(String stationInterId) {
        this.stationInterId = stationInterId;
    }

    public String getStationInterName() {
        return stationInterName;
    }

    public void setStationInterName(String stationInterName) {
        this.stationInterName = stationInterName;
    }

    public String getRoadGridId() {
        return roadgridId;
    }

    public void setRoadGridId(String roadGridId) {
        this.roadgridId = roadGridId;
    }

    public String getRoadId() {
        return roadId;
    }

    public void setRoadId(String roadId) {
        this.roadId = roadId;
    }

    public String getRoadName() {
        return roadName;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
