<%--
  Created by IntelliJ IDEA.
  User: Xiaobing
  Date: 2016/8/30
  Time: 14:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title><c:out value="${title}"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="/script/base/menu/metro.css">
    <script type="text/javascript" src="/script/base/menu/jquery.min.js"></script>
    <script type="text/javascript" src="/script/base/menu/jquery.plugins.min.js"></script>
    <script type="text/javascript" src="/script/base/menu/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="/script/base/menu/metro.js"></script>
</head>
<body>
<div class="metro-layout horizontal">
    <div class="header">
        <h1><c:out value="${title}"/></h1>
        <div class="controls">
            <span class="next" title="左 移" style=""></span>
            <span class="prev" title="右 移" style=""></span>
            <%-- <span class="down" title="Scroll down" style="display: none;"></span>
             <span class="up" title="Scroll up" style="display: none;"></span>
             <span class="toggle-view" title="Toggle layout" style="display: none;"></span>--%>
        </div>
    </div>
    <div id="com_menu" class="content clearfix">
        <div class="items isotope">
            <c:forEach items="${nav.navItems}" var="item">
            <a class="box isotope-item" href="/default?m=<c:out value="${item.fun}"></c:out>" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px);background: rgb(<c:out value="${item.bgcolor}"></c:out>);"><span><c:out value="${item.name}"></c:out></span><img class="icon" src="/images/nav/<c:out value="${item.ico}"></c:out>" alt=""></a>
            </c:forEach>
        </div>
    </div>
</div>
<%--<div style="text-align:center;margin:50px 0; font:normal 14px/24px 'MicroSoft YaHei';">
    <p>浪软件集团</p>
</div>--%>
</body>
</html>
