package bean.common;

import bean.enums.ResType;

/**
 * 小区
 *
 * @author Xiaobing
 */
public class Cell extends Res {

    public Cell() {
        super();
    }

    public Cell(ResType resType) {
        setResType(resType);
    }

    private String btsId;
    private String btsName;
    private String lac;
    private String ci;
    private String tac;
    private String eci;
    private int cell_order_id;
    /**
     * 覆盖半径
     */
    private double radius;
    /**
     * 方向角(0度为正北,顺时针旋转)
     */
    private double azimuth;
    /**
     * 半功率角(小区覆盖角度的一半)
     */
    private double antbw;
    /**
     * 覆盖场景编号
     */
    private String coverSceneId;
    /**
     * 覆盖场景名称
     */
    private String coverSceneName;
    /**
     * 是否室分
     */
    private String ifAlonecell;

    /**
     * 是否室分id
     */
    private String ifAloneCellId;

    /**
     * 基站ID
     *
     * @return
     */
    public String getBtsId() {
        return btsId;
    }

    /**
     * 基站ID
     *
     * @param btsId
     */
    public void setBtsId(String btsId) {
        this.btsId = btsId;
    }

    /**
     * 基站名称
     *
     * @return
     */
    public String getBtsName() {
        return btsName;
    }

    /**
     * 基站名称
     *
     * @param btsName
     */
    public void setBtsName(String btsName) {
        this.btsName = btsName;
    }

    public String getLac() {
        return lac;
    }

    public void setLac(String lac) {
        this.lac = lac;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getEci() {
        return eci;
    }

    public void setEci(String eci) {
        this.eci = eci;
    }

    /**
     * 覆盖半径
     *
     * @return
     */
    public double getRadius() {
        return radius;
    }

    /**
     * 覆盖半径
     *
     * @param radius
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * 方向角(0度为正北,顺时针旋转)
     *
     * @return
     */
    public double getAzimuth() {
        return azimuth;
    }

    /**
     * 方向角(0度为正北,顺时针旋转)
     *
     * @param azimuth
     */
    public void setAzimuth(double azimuth) {
        this.azimuth = azimuth;
    }

    /**
     * 半功率角(小区覆盖角度的一半)
     *
     * @return
     */
    public double getAntbw() {
        return antbw;
    }

    /**
     * 半功率角(小区覆盖角度的一半)
     *
     * @param antbw
     */
    public void setAntbw(double antbw) {
        this.antbw = antbw;
    }

    /**
     * 覆盖场景编号
     *
     * @return
     */
    public String getCoverSceneId() {
        return coverSceneId;
    }

    /**
     * 覆盖场景编号
     *
     * @param coverSceneId
     */
    public void setCoverSceneId(String coverSceneId) {
        this.coverSceneId = coverSceneId;
    }

    /**
     * 覆盖场景名称
     *
     * @return
     */
    public String getCoverSceneName() {
        return coverSceneName;
    }

    /**
     * 覆盖场景名称
     *
     * @param coverSceneName
     */
    public void setCoverSceneName(String coverSceneName) {
        this.coverSceneName = coverSceneName;
    }

    /**
     * 是否室分
     *
     * @return
     */
    public String getIfAlonecell() {
        return ifAlonecell;
    }

    /**
     * 是否室分
     *
     * @param ifAlonecell
     */
    public void setIfAlonecell(String ifAlonecell) {
        this.ifAlonecell = ifAlonecell;
    }

    /**
     * 是否室分ID
     *
     * @return
     */
    public String getIfAloneCellId() {
        return ifAloneCellId;
    }

    /**
     * 是否室分ID
     *
     * @param ifAloneCellId
     */
    public void setIfAloneCellId(String ifAloneCellId) {
        this.ifAloneCellId = ifAloneCellId;
    }

    /**
     * tac
     *
     * @return
     */
    public String getTac() {
        return tac;
    }

    /**
     * tac
     *
     * @param tac
     */
    public void setTac(String tac) {
        this.tac = tac;
    }

    public int getCell_order_id() {
        return cell_order_id;
    }

    public void setCell_order_id(int cell_order_id) {
        this.cell_order_id = cell_order_id;
    }


    public Cell clone() {
        Cell cell = new Cell(this.getResType());
        cell.setId(this.getId());
        cell.setName(this.getName());
        cell.setTag(this.getTag());
        cell.setLatitude(this.getLatitude());
        cell.setLongitude(this.getLongitude());
        cell.setValue(this.getValue());
        cell.getRoperties().putAll(this.getRoperties());
        cell.setOriginalLongitude(this.getOriginalLongitude());
        cell.setOriginalLongitude(this.getOriginalLongitude());
        cell.setCityId(this.getCityId());
        cell.setCityName(this.getCityName());
        cell.setCountryId(this.getCountryId());
        cell.setCountyName(this.getCountyName());
        cell.setBtsId(this.getBtsId());
        cell.setBtsName(this.getBtsName());
        cell.setLac(this.getLac());
        cell.setCi(this.ci);
        cell.setTac(this.getTac());
        cell.setEci(this.eci);
        cell.setCell_order_id(this.cell_order_id);
        cell.setRadius(this.getRadius());
        cell.setAntbw(this.getAntbw());
        cell.setAzimuth(this.getAzimuth());
        cell.setCoverSceneId(this.getCoverSceneId());
        cell.setCoverSceneName(this.getCoverSceneName());
        cell.setIfAlonecell(this.getIfAlonecell());
        return cell;
    }
}
