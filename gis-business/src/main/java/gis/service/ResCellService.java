package gis.service;

import gis.algorithm.PointTransform;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.common.*;
import bean.enums.ResType;

public abstract class ResCellService {

    /**
     * 获取GSM小区 接口
     *
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public abstract List<Cell> GetGSMCells(RqCell rqCell) throws Exception;

    /**
     * 获取LTE小区 接口
     *
     * @return
     * @throws SQLException
     */
    public abstract List<Cell> GetLTECells(RqCell rqCell) throws Exception;

    /**
     * 获取TD小区 接口
     *
     * @return
     * @throws SQLException
     */
    public abstract List<Cell> GetTDCells(RqCell rqCell) throws Exception;

    /**
     * 返回SQL语句条件
     *
     * @param rqCell
     * @return
     */
    protected String GetSqlPara(RqCell rqCell) {
        StringBuffer sb = new StringBuffer();
        if (rqCell.getRegion() != null && rqCell.getRegion().getType() == 9001) {
            sb.append(String.format(" and cityId = %s", rqCell.getRegion().getId()));
        } else if (rqCell.getRegion() != null && rqCell.getRegion().getType() == 9003) {
            sb.append(String.format(" and countryId = %s", rqCell.getRegion().getId()));
        }

        if (rqCell.getAlonecell() != 0) {
            sb.append(String.format(" and ifAlonecellId = %s", rqCell.getAlonecell()));
        }

        return sb.toString();
    }

    /**
     * 缓存KEY
     *
     * @param rqCell
     * @return
     */
    public static String GetKeyPara(RqCell rqCell) {
        StringBuffer sb = new StringBuffer();

        if (rqCell.getRegion() != null) {
            sb.append("_" + rqCell.getRegion().getId() + "_" + rqCell.getRegion().getType());
        }
        sb.append("_" + rqCell.getAlonecell());
        return sb.toString();
    }

    /**
     * 构建2G小区对象
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    public static List<Cell> GetGSMCellsEntity(ResultSet rs) throws SQLException {
        List<Cell> list = new ArrayList<Cell>();
        Cell cell;
        while (rs.next()) {
            cell = new Cell(ResType.GSMCELL);
            cell.setId(rs.getString("id"));
            cell.setName(rs.getString("name"));
            cell.setLac(rs.getString("lac"));
            cell.setCi(rs.getString("ci"));
            cell.setTac(rs.getString("lac"));
            cell.setEci(rs.getString("ci"));
            cell.setBtsId(rs.getString("btsid"));
            cell.setBtsName(rs.getString("btsname"));
            cell.setCountryId(rs.getString("countryId"));
            cell.setCountyName(rs.getString("countyName"));
            cell.setCityId(rs.getString("cityId"));
            cell.setCityName(rs.getString("cityName"));
            cell.setIfAloneCellId(rs.getString("ifAlonecellId"));
            cell.setIfAlonecell(rs.getString("ifAlonecell"));
            cell.setCoverSceneId(rs.getString("coverSceneId"));
            cell.setCoverSceneName(rs.getString("coverSceneName"));
            cell.setOriginalLongitude(rs.getDouble("longitude"));
            cell.setOriginalLatitude(rs.getDouble("latitude"));
            cell.setAzimuth(rs.getDouble("azimuth"));
            cell.setRadius(rs.getDouble("radius"));
            cell.setAntbw(rs.getDouble("antbw"));
            cell.setCell_order_id(rs.getInt("cell_order_id"));
            double[] p = PointTransform.Offset(cell.getOriginalLatitude(), cell.getOriginalLongitude());
            cell.setLatitude(p[0]);
            cell.setLongitude(p[1]);

            list.add(cell);
        }
        return list;
    }

    /**
     * 构建3G小区对象
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    public static List<Cell> GetTDCellsEntity(ResultSet rs) throws SQLException {
        List<Cell> list = new ArrayList<Cell>();
        Cell cell;
        while (rs.next()) {
            cell = new Cell(ResType.TDCELL);
            cell.setId(rs.getString("id"));
            cell.setName(rs.getString("name"));
            cell.setLac(rs.getString("lac"));
            cell.setCi(rs.getString("ci"));
            cell.setTac(rs.getString("lac"));
            cell.setEci(rs.getString("ci"));
            cell.setBtsId(rs.getString("btsid"));
            cell.setBtsName(rs.getString("btsname"));
            cell.setCountryId(rs.getString("countryId"));
            cell.setCountyName(rs.getString("countyName"));
            cell.setCityId(rs.getString("cityId"));
            cell.setCityName(rs.getString("cityName"));
            cell.setIfAloneCellId(rs.getString("ifAlonecellId"));
            cell.setIfAlonecell(rs.getString("ifAlonecell"));
            cell.setCoverSceneId(rs.getString("coverSceneId"));
            cell.setCoverSceneName(rs.getString("coverSceneName"));
            cell.setOriginalLongitude(rs.getDouble("longitude"));
            cell.setOriginalLatitude(rs.getDouble("latitude"));
            cell.setAzimuth(rs.getDouble("azimuth"));
            cell.setRadius(rs.getDouble("radius"));
            cell.setAntbw(rs.getDouble("antbw"));
            cell.setCell_order_id(rs.getInt("cell_order_id"));
            double[] p = PointTransform.Offset(cell.getOriginalLatitude(), cell.getOriginalLongitude());
            cell.setLatitude(p[0]);
            cell.setLongitude(p[1]);

            list.add(cell);
        }
        return list;
    }

    /**
     * 构建4G小区对象
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    public static List<Cell> GetLTECellsEntity(ResultSet rs) throws SQLException {
        List<Cell> list = new ArrayList<Cell>();
        Cell cell;
        while (rs.next()) {
            cell = new Cell(ResType.LTECELL);
            cell.setId(rs.getString("id"));
            cell.setName(rs.getString("name"));
            cell.setLac(rs.getString("tac"));
            cell.setCi(rs.getString("eci"));
            cell.setTac(rs.getString("tac"));
            cell.setEci(rs.getString("eci"));
            cell.setBtsId(rs.getString("btsid"));
            cell.setBtsName(rs.getString("btsname"));
            cell.setCountryId(rs.getString("countryId"));
            cell.setCountyName(rs.getString("countyName"));
            cell.setCityId(rs.getString("cityId"));
            cell.setCityName(rs.getString("cityName"));
            cell.setIfAloneCellId(rs.getString("ifAlonecellId"));
            cell.setIfAlonecell(rs.getString("ifAlonecell"));
            cell.setCoverSceneId(rs.getString("coverSceneId"));
            cell.setCoverSceneName(rs.getString("coverSceneName"));
            cell.setOriginalLongitude(rs.getDouble("longitude"));
            cell.setOriginalLatitude(rs.getDouble("latitude"));
            cell.setAzimuth(rs.getDouble("azimuth"));
            cell.setRadius(rs.getDouble("radius"));
            cell.setAntbw(rs.getDouble("antbw"));
            cell.setCell_order_id(rs.getInt("cell_order_id"));

            double[] p = PointTransform.Offset(cell.getOriginalLatitude(), cell.getOriginalLongitude());
            cell.setLatitude(p[0]);
            cell.setLongitude(p[1]);
            list.add(cell);
        }
        return list;
    }
}
