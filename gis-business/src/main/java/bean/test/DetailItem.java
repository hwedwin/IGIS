package bean.test;

/**
 * Created by Xiaobing on 2016/9/13.
 */
public class DetailItem {
    private String column;
    private String timestamp;
    private String $;

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String get$() {
        return $;
    }

    public void set$(String $) {
        this.$ = $;
    }
}
