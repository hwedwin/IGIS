/**
 * Created by hupeng on 16/5/13.
 */
Date.prototype.format = function (format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S+": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1
                ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};


function CityFunction() {
    //事件相关
    var scene_type_list = null;
    var tempDrawLayer = null;//框选结果区域
    var wmsLayer = null; //渲染图层
    var toolbar = null; //框选工具
    var rings = null;//框选区域坐标点
    var ringType = 0;//框选类型
    var delta_temp = null; //底图临时数据
    var grid_id = "";//栅格id
    var now = new Date();
    var time = (new Date(now.setMonth(now.getMonth() - 0))).format('yyyyMM');//查询日期
    var newSceneId = 0;
    var curSceneId = 0;
    var kpiDicTemp = null; //指标

    //初始化数据
    var initData = function () {
        //加载场景分类
        loadSceneTypeList();

        createPolygonListHtml();
        var url = "/dynamicmap/cityfun/1111/1111";
        loadWmsLayer(url);
    };

    /**
     * 加载场景分类
     * */
    var loadSceneTypeList = function () {
        $.ajax({
            url: "/getscenetypelist?r=" + Math.random(),
            type: "GET",
            async: false,
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (data) {
                scene_type_list = data;
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    //添加渲染图层
    var loadWmsLayer = function (url) {
        if (wmsLayer != null || wmsLayer != undefined) {
            gis_core.removeLayer("endtoendwmsLayer")
        }
        if (url == null || url == undefined) {
            return;
        }
        wmsLayer = new esri.layers.WMSLayer(url, {
            resourceInfo: {
                extent: new esri.geometry.Extent(73.2672757, 18.37574, -109.66552734375, 133.29657, {
                    wkid: 4326
                }),
                layerInfos: []
            },
            visibleLayers: ['lteconvergrid']
        });
        wmsLayer.id = "endtoendwmsLayer";
        gis_core.insertLayer("endtoendwmsLayer", "网格覆盖", 2, wmsLayer);
    };

    //地图改变事件
    var extentchange = function (delta) {
        delta_temp = delta;
    };

    //地图框选结束时间
    var onDrawEnd = function (event) {
        rings = event.geometry.rings[0];
        //绘制图层
        drawPolygonGraphic(rings);
        //弹框
        toolbar.deactivate();
        createSavePolygonHtml();

        $('a[divid]').attr('flage', 'hide');
        $('#_draw_toolbar_holder').hide();

    };

    //绘制多边形
    var drawPolygonGraphic = function (rings, sceneId, sceneName, poiCate, graphType) {
        if (tempDrawLayer) {
            tempDrawLayer.clear()
        }
        var polygon = new esri.geometry.Polygon(new esri.SpatialReference({wkid: 4326}));
        //添加多边形的各个角的顶点坐标，注意：首尾要链接，也就是说，第一个点和最后一个点要一致
        polygon.addRing(rings);
        var symbol = new esri.symbol.SimpleFillSymbol().setColor(new esri.Color([180, 168, 192, 0.2])).setOutline(new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new esri.Color([200, 112, 181, 0.9]), 1));
        var graphic = new esri.Graphic(polygon, symbol);
        tempDrawLayer.add(graphic);
        //dojo.connect(tempDrawLayer, "onClick", capitalClick);

        //偏移坐标
        if (sceneId) {
            //加载指标
            loadSceneKpiDetail(sceneId, sceneName);
            offsetMainLayer(rings);
            if (poiCate && poiCate.length > 0) {
                loadPoiListForScene(rings, poiCate, graphType);
            } else {
                removePoiKpiDetail();
            }
        }
    };

    var removePoiKpiDetail = function () {
        gis_layout.removeRightWidget('_city_fun_poi');
    };

    //多边形点击事件
    var capitalClick = function () {
        return false;
    };

    //偏移底图
    var offsetMainLayer = function (rings) {
        var xmax;
        var ymax;
        var xmin;
        var ymin;
        for (var i = 0, len = rings.length; i < len; i++) {
            var item = rings[i];
            if (!xmax || xmax < item[0]) {
                xmax = item[0];
            }

            if (!ymax || ymax < item[1]) {
                ymax = item[1];
            }

            if (!xmin || xmin > item[0]) {
                xmin = item[0];
            }

            if (!ymin || ymin > item[1]) {
                ymin = item[1];
            }
        }

        if (xmax && ymax && xmin && ymin) {
            gis_common.setExtent(xmax + 0.005, ymax + 0.005, xmin - 0.005, ymin - 0.005);
        }
    };
    //创建保存自定义场景html
    var createSavePolygonHtml = function () {
        var html = [];
        html.push('<div id="_polygon_div" class="wrap-cont" style="display: block;padding:20px;">');
        html.push("<div class='row form-group'>");
        html.push("     <label class='col-sm-3 control-label'>场景名称:</label>");
        html.push("     <div class='col-sm-6'>");
        html.push("         <input type='text' maxlength='25' class='form-control _rings_name'/>");
        html.push("     </div>");
        html.push("     <div class='col-sm-3'>(不超过25字)</div>");
        html.push("</div>");
        html.push("<div class='row form-group'>");
        html.push("     <label class='col-sm-3 control-label'>场景类型:</label>");
        html.push("     <div class='col-sm-9'>");
        html.push("         <select class='form-control _scene_type'>" +
            "</select>");
        html.push("     </div>");
        html.push("</div>");
        html.push("<div class='row form-group'>");
        html.push("     <div class='col-sm-9 col-sm-offset-3'>");
        html.push("		    <button class='btn btn-primary _city_fun_save_btn'>保 存</button>");
        html.push("	    </div>");
        html.push("</div>");
        html.push('</div>');
        var divContent = $(html.join(''));
        divContent.appendTo($('body'));
        divContent.dialog({
            onclose: function () {
                tempDrawLayer.clear();
            }
        });
        //gis_layout.createRightWidget('_save_polygon', '区域', divContent);

        divContent.find('._city_fun_save_btn').on('click', function () {
            newSceneId = 0;
            var ringName = divContent.find('._rings_name').val();
            var sceneTypeId = divContent.find('._scene_type').val();
            var sceneTypeName = divContent.find('option:selected').text();
            var cate = getSelectPoiCategory();
            $.ajax({
                url: "/addcityfun?r=" + Math.random(),
                data: {
                    ringName: encodeURIComponent(ringName),
                    rings: JSON.stringify(rings),
                    ringType: ringType,
                    sceneTypeId: sceneTypeId,
                    sceneTypeName: sceneTypeName,
                    area_extend: JSON.stringify(gis_common.getMapExtent()),
                    categorys: JSON.stringify(cate)
                },
                type: "POST",
                dataType: "json",
                beforeSend: function () {
                    gis_common.isLoadingAddCount();
                },
                success: function (req) {
                    gis_common.outPut(req, function (data) {
                        if (data > 0) {
                            gis_common.showMessage('保存成功');
                            newSceneId = data;
                            createPolygonListHtml();
                        } else {
                            gis_common.showMessage('保存失败');
                        }
                    });
                },
                complete: function () {
                    divContent.dialog('close');
                    gis_common.isLoadingSunCount();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
                }
            });
        });

        var $type = $('._scene_type', divContent);
        if (scene_type_list) {
            for (var i = 0, len = scene_type_list.length; i < len; i++) {
                $type.append('<option value="' + scene_type_list[i]['scene_type_id'] + '">' + scene_type_list[i]['scene_type_name'] + '</option>');
            }
        }
    };
    //创建自定义场景列表html
    var createPolygonListHtml = function () {
        var html = [];

        var timeStr = time.substr(0, 4) + '-' + time.substr(4, 2);

        html.push('<div class="form-horizontal">');
        html.push(' <div class="form-group"><label class="col-sm-3 control-label">时间:</label>');
        html.push('    <div class="col-sm-9">');
        html.push('        <input id="_sport_trace_begin_time" value="' + timeStr + '" type="text" class="form-control"');
        html.push('               onfocus="WdatePicker({lang:\'zh-cn\',dateFmt:\'yyyy-MM\'});">');
        html.push('    </div>');
        html.push(' </div>');
        html.push(' <div class="form-group"><label class="col-sm-3 control-label">分类:</label>');
        html.push('    <div class="col-sm-9">');
        html.push('         <select class="form-control _scene_type"></select>');
        html.push('    </div>');
        html.push(' </div>');
        html.push(' <div class="form-group"><label class="col-sm-3 control-label">场景:</label>');
        html.push('    <div class="col-sm-9">');
        html.push('     <div class="_polygon_list list-group" style="margin-bottom: 0;min-width:200px;margin-right:30px; max-height:175px;overflow-y:auto;"></div>');
        html.push('    </div>');
        html.push(' </div>');
        html.push(' <div class="form-group"><label class="col-sm-3 control-label"></label>');
        html.push('    <div class="col-sm-9">');
        html.push('         <button id="_query_kpi" class="btn btn-primary">查 询</button>');
        html.push('    </div>');
        html.push(' </div>');
        html.push('</div>');
        var divContent = $(html.join(''));
        var $type = $('._scene_type', divContent);
        if (scene_type_list) {
            $type.append('<option value="0">所有</option>');
            for (var i = 0, len = scene_type_list.length; i < len; i++) {
                $type.append('<option value="' + scene_type_list[i]['scene_type_id'] + '">' + scene_type_list[i]['scene_type_name'] + '</option>');
            }
        }
        $type.change(function () {
            var v = $(this).val();
            if (v == 0) {
                $('._scene_item').show();
            } else {
                $('._scene_item').hide();
                $('._scene_item[tid="' + v + '"]').show();
            }
        });


        divContent.find('#_sport_trace_begin_time').blur(function () {
            var t = $(this).val();
            if (t) {
                time = t.replace('-', '');
            }
        });

        divContent.find('#_query_kpi').click(function () {
            var $ac = $('._scene_item.active');
            if ($ac.length == 0) {
                gis_common.showError('未选择场景');
            } else {
                drawPolygonGraphic(JSON.parse($ac.data('rings')), $ac.attr('pid'), $ac.text(), JSON.parse($ac.data('cate')), JSON.parse($ac.attr('gid')));
            }
        });

        gis_layout.createLeftWidget('_polygon_list', '查询', divContent, true);
        loadPolygonList();
    };
    //加载自定义场景列表
    var loadPolygonList = function () {
        $.ajax({
            url: "/getcityfunlist?r=" + Math.random(),
            data: {},
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    addPolygonList(data);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    //添加自定义场景
    var addPolygonList = function (data) {
        var $list = $('._polygon_list');
        for (var i = 0, len = data.length; i < len; i++) {
            var $button = $('<div style="cursor:pointer;" class="list-group-item _scene_item _list_item_' + data[i]["scene_id"] + '">' +
                '<a href="javascript:;" style="float: right;margin-left:5px;"><i class="fa fa-remove _btn_del" title="删除" ></i></a>' +
                '<a href="javascript:;" style="float: right;"><i class="fa fa-download _btn_export " title="导出" ></i></a>' +
                '<span></span>' +
                '</div>');
            $button.attr({'pid': data[i]['scene_id'], 'tid': data[i]['scene_type'], 'gid': data[i]['graphic_type']})
                .data('rings', data[i]['rings']).data('cate', data[i]['poi_category']);
            $button.find('span').html(data[i]['scene_name']);

            $button.click(function (e) {
                $(this).addClass('active').siblings().removeClass('active');
                drawPolygonGraphic(JSON.parse($(this).data('rings')), $(this).attr('pid'), $(this).text(),
                    JSON.parse($(this).data('cate')), JSON.parse($(this).attr('gid')));
                curSceneId = $(this).attr('pid');
            });

            var $del = $button.find('._btn_del');
            $del.attr('tid', data[i]['scene_id']);
            $del.click(function (event) {
                if (confirm('确认删除？')) {
                    deletePolygon($(this).attr('tid'));
                    $(this).parent().parent().remove();
                    if ($('._scene_item.active').length == 0) {
                        tempDrawLayer.clear();
                        gis_layout.removeRightWidget('_city_fun_kpi');
                        gis_layout.removeRightWidget('_city_fun_poi');
                    }
                }
                event.stopPropagation();
            });

            var $export = $button.find('._btn_export');
            $export.attr('tid', data[i]['scene_id']);
            $export.click(function (event) {
                //导出
                var $parent = $(this).parent().parent();
                if ($parent.hasClass('active')) {
                    saveImage();
                } else {
                    //$parent.click();
                    gis_common.showMessage('请先查询生成数据');
                }

                event.stopPropagation();
            });

            $button.appendTo($list);
        }

        if (newSceneId != 0) {
            $list.find('._list_item_' + newSceneId).click();
            newSceneId = 0;
        }

    };

    var saveImage = function () {
        html2canvas($("body"), {
            allowTaint: true,
            taintTest: false,
            onrendered: function (canvas) {
                var image = canvas.toDataURL();
                $.ajax({
                    url: "/cityfun_export",
                    data: {
                        imagedata: image,
                        time: time,
                        sceneId: $('._scene_item.active').attr('pid'),
                        sceneName: $('._scene_item.active').find('span').text()
                    },
                    type: "POST",
                    beforeSend: function () {
                        gis_common.isLoadingAddCount();
                    },
                    success: function (req) {
                        gis_common.outPut(req, function (url) {
                            gis_common.windowOpen("/pages/export.jsp?typ=application/msexcel&fid=" + url, "_blank")
                        });
                    },
                    complete: function () {
                        gis_common.isLoadingSunCount();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
                    }
                });
            }
        });
    };


    //删除自定义场景
    var deletePolygon = function (tid) {
        $.ajax({
            url: "/delcityfun?r=" + Math.random(),
            data: {id: tid},
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {

                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    //获取栅格瓦块
    var getId = function (l) {
        var va = 0;
        l = l + 0.000001;
        var ls = (l + "").split(".");
        var l_4 = parseInt(ls[1].substring(3, 4));
        if (l_4 > 4) {
            va = ls[0] + "." + ls[1].substring(0, 3) + "5";
        } else {
            va = ls[0] + "." + ls[1].substring(0, 3) + "0";
        }
        return va;
    };

    /**
     * 加载场景指标详细信息
     * */
    var loadSceneKpiDetail = function (sceneId, sceneName) {
        if (gis_common.stringIsNullOrWhiteSpace(sceneId) || gis_common.stringIsNullOrWhiteSpace(time)) {
            gis_common.showMessage("未选择场景和日期");
            return;
        }

        $.ajax({
            url: "/getcityfunscenedetail?r=" + Math.random(),
            data: {
                time: time,
                id: sceneId,
                name: sceneName
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    if (data) {
                        createKpiDetailHtml(grid_id, data);
                    }
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };


    /**
     * 创建指标明细html
     * */
    var createKpiDetailHtml = function (id, data) {
        var kpi = [];
        kpiDicTemp = null;
        if (!data) {
            return;
        }

        if (data.flow) {
            kpi.push({"id": "流量(MB)", "name": data.flow});
        }
        if (data.usr_cnt) {
            kpi.push({"id": "用户数", "name": data.usr_cnt});
        }
        if (data.lte_usr_cnt) {
            kpi.push({"id": "4G用户数", "name": data.lte_usr_cnt});
        }
        if (data.permanent_usr_cnt) {
            kpi.push({"id": "常驻人口数", "name": data.permanent_usr_cnt});
        }
        if (data.flowing_usr_cnt) {
            kpi.push({"id": "流动人口", "name": data.flowing_usr_cnt});
        }
        if (data.per_traffic) {
            kpi.push({"id": "人均日通话分钟数", "name": data.per_traffic});
        }
        if (data.per_flow) {
            kpi.push({"id": "人均日流量（MB)", "name": data.per_flow});
        }
        if (data.per_consumer) {
            kpi.push({"id": "人均月消费(元）", "name": data.per_consumer});
        }
        if (data.per_consumer_max) {
            kpi.push({"id": "最大消费值(元）", "name": data.per_consumer_max});
        }
        if (data.high_value_usr_cnt) {
            kpi.push({"id": "高消费用户数", "name": data.high_value_usr_cnt});
        }
        if (data.normal_value_usr_cnt) {
            kpi.push({"id": "普通用户数", "name": data.normal_value_usr_cnt});
        }

        kpiDicTemp = kpi;

        var div = gis_common.createKeyValueList(kpi);
        gis_layout.createRightWidget("_city_fun_kpi", "KPI", div);
    };


    var checkGridCount = function () {
        if (delta_temp != null) {
            var xmax = delta_temp.extent.xmax;
            var xmin = delta_temp.extent.xmin;
            var ymax = delta_temp.extent.ymax;
            var ymin = delta_temp.extent.ymin;
            var p1 = GPS.mercator_encrypt(ymin, xmin);
            var p2 = GPS.mercator_encrypt(ymax, xmax);
            var w = p2.lon - p1.lon;
            var h = p2.lat - p1.lat;
            var count = w * h / (55 * 55);
            if (count <= 4000) {
                return true;
            }
        }
        return false;
    };

    this.createPolygonListHtml = function () {
        createPolygonListHtml();
    };

    this.createDrawToolbarHtml = function (callback) {
        var html = [];
        html.push('<div class="list-group">');
        html.push('    <button type="button" class="list-group-item _rectangle_btn"><i class="fa fa-square-o"></i> 矩形选择区域');
        html.push('    </button>');
        html.push('    <button type="button" class="list-group-item _polygon_btn"><i class="fa fa-star-o"></i> 不规则区域（打点）');
        html.push('    </button>');
        html.push('    <button type="button" class="list-group-item _free_polygon_btn"><i class="fa fa-star-o"></i> 不规则区域（划线）');
        html.push('    </button>');
        html.push('</div>');
        var $div = $(html.join(''));
        $div.find('._rectangle_btn').on('click', function () {
            if (checkGridCount()) {
                toolbar.activate(esri.toolbars.Draw.RECTANGLE);
                ringType = 1;
            } else {
                gis_common.showMessage('当前栅格数过多');
            }

        });
        $div.find('._polygon_btn').on('click', function () {
            if (checkGridCount()) {
                toolbar.activate(esri.toolbars.Draw.POLYGON);
                ringType = 2;
            } else {
                gis_common.showMessage('当前栅格数过多');
            }
        });
        $div.find('._free_polygon_btn').on('click', function () {
            if (checkGridCount()) {
                toolbar.activate(esri.toolbars.Draw.FREEHAND_POLYGON);
                ringType = 2;
            } else {
                gis_common.showMessage('当前栅格数过多');
            }
        });
        callback($div);
    };

    /**
     * poi 确定按钮点击事件
     * */
    this.poiBtnClickEvent = function () {
        var $scene = $('._scene_item.active');
        if ($scene.length > 0) {
            var rings = $('._scene_item.active').data('rings');
            if (rings) {
                var cate = getSelectPoiCategory();
                var sceneId = $scene.attr('pid');
                loadPoiListForScene(JSON.parse(rings), cate);
                updateScenePoiCate(sceneId, cate);
                $scene.data('cate', JSON.stringify(cate));
            }
        }
    };

    /**
     * 更新场景对应的poi 分类
     * */
    var updateScenePoiCate = function (sceneId, cate) {
        if (cate && cate.length > 0) {
            $.ajax({
                url: "/updatecityfunpoicate?r=" + Math.random(),
                data: {
                    sceneId: sceneId,
                    cate: JSON.stringify(cate)
                },
                dataType: "json",
                async: true,
                type: "GET",
                beforeSend: function () {
                    //gis_common.isLoadingAddCount();
                },
                success: function (req) {

                },
                complete: function () {
                    //gis_common.isLoadingSunCount();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    //gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
                }
            });
        }
    };

    /**
     * 获取poi分类
     * */
    var getSelectPoiCategory = function () {
        var cate = [];
        $('._category_item:checked').each(function () {
            var $ts = $(this);
            cate.push({
                fid: $ts.attr('fid'),
                sid: $ts.attr('sid'),
                name: $ts.attr('category_name'),
                level: $ts.attr('level')
            });
        });

        return cate;
    };

    /**
     * 加载场景对应的poi点
     * */
    var loadPoiListForScene = function (rings, cate, graphType) {
        if (cate && cate.length > 0) {
            $.ajax({
                url: "/getcityfunpoikpi?r=" + Math.random(),
                data: {
                    rings: JSON.stringify(rings),
                    cate: JSON.stringify(cate),
                    graphType: graphType
                },
                dataType: "json",
                async: true,
                type: "POST",
                beforeSend: function () {
                    gis_common.isLoadingAddCount();
                },
                success: function (poiKpi) {
                    drawPoiDetail(cate, poiKpi);
                },
                complete: function () {
                    gis_common.isLoadingSunCount();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
                }
            });
        }

    };

    /**
     * 绘制poi指标详情以及在地图上展示
     */
    var drawPoiDetail = function (cate, data) {
        if (!data || data.length == 0) {
            gis_layout.removeRightWidget("_city_fun_poi");
            return;
        }

        var div = gis_common.createKeyValueList(data);
        gis_layout.createRightWidget("_city_fun_poi", 'POI', div);
    };

    /**
     * 初始化
     */
    this.init = function () {
        //加载库文件
        dojo.require("esri.layers.WMSLayerInfo");
        dojo.require("esri.layers.WMSLayer");
        dojo.require("esri.toolbars.draw");
        tempDrawLayer = gis_core.addGraphicsLayerWithIndex('temp_draw_layer', '区域框选', 2);
        gis_core.addExtentChange("CityFunction", extentchange);
        //注册模块
        gis_core.addPlugin(this);

        //添加toolbar demo
        //初始化绘制工具条
        toolbar = new esri.toolbars.Draw(gis_core.map, {
            tooltipOffset: 20,
            drawTime: 90,
            showTooltips: true
        });
        toolbar.on("draw-end", onDrawEnd);

        initData();
    };
    /**
     * 销毁
     */
    this.destroy = function () {
        gis_core.removeExtentChange("CityFunction");
        scene_type_list = null;
        tempDrawLayer = null;//框选结果区域
        wmsLayer = null; //渲染图层
        toolbar = null; //框选工具
        rings = null;//框选区域坐标点
        ringType = 0;//框选类型
        delta_temp = null; //底图临时数据
        grid_id = "";//栅格id
        now = null;
        time = null;
        newSceneId = 0;
        curSceneId = 0;
        kpiDicTemp = null; //指标
        cityFunction = null;
    };
    /**
     * 模块信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "00_01_00",
        name: "城市功能区分析"
    };
}

//功能类 全局类
var cityFunction = null;


function initCityFunction() {
    if (cityFunction == null) {
        cityFunction = new CityFunction();
        cityFunction.init();
    }
}

//菜单 查询条件
function cityFunction_Init(obj) {
    initCityFunction();
}

function createDrawToolbar(obj) {
    gis_layout.createToolbarWithFun("_draw_toolbar_holder", obj, 194, cityFunction.createDrawToolbarHtml);
}


function loadPoiCategory(obj) {
    gis_common.poiLoad({
        obj: obj,
        showCate: true,
        poiCateBtnClick: function () {
            cityFunction.poiBtnClickEvent();
        }
    });
}


function cityFun_createQueryParamHtml() {
    if ($('#widget_left_title__polygon_list').length == 0) {
        cityFunction.createPolygonListHtml()
    }
}
