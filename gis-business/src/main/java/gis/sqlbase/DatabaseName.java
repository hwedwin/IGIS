package gis.sqlbase;

/**
 * 数据库
 *
 * @author Xiaobing
 */
public enum DatabaseName {
    /**
     * 通用连接数据库(默认连接)
     */
    common,
    /**
     * oracle连接数据库
     */
    oracle,
    /**
     * mysql连接数据库
     */
    mysql,
    /**
     * gbase连接数据库
     */
    gbase,

    /**
     * sybase连接数据库
     */
    sybase,

    /**
     * gp 数据库连接
     */
    gp
}
