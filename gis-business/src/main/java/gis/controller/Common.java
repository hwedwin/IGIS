package gis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.common.ModuleConfig;
import bean.common.Output;
import bean.enums.ErrorCode;
import cache.DataCache;
import gis.common.BaseAuth;
import gis.common.GisConfig;
import gis.core.utils.ImageUtils;
import gis.dao.CommonDao;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import bean.*;
import org.springframework.web.servlet.ModelAndView;

@Controller("Common")
public class Common {
    private static Logger logger = LoggerFactory.getLogger(Common.class);


    @RequestMapping(value = {"/", "/index"})
    public ModelAndView redirectIndexGet(Model model) {
        GisConfig gisConfig = GisConfig.GetInstance();
        String portal = gisConfig.getUtil().getProperty("portal");
        if (StringUtils.isEmpty(portal)) {
            portal = "/default";
        }

        ModelAndView view = new ModelAndView();
        view.setViewName(portal);
        return view;
    }

    @RequestMapping(value = "/getinitconfig", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Output GetInitConfig(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Output out = new Output();
        GisConfig gisConfig = GisConfig.GetInstance();
        boolean b = BaseAuth.GetInstance().isAuth(request, response);
        if (b) {
            //读取默认值
            InitConfig initConfig = new InitConfig();
            GisConfig config = GisConfig.GetInstance();
            initConfig.setGisType(config.getGisType());
            initConfig.setWkid(config.getWkid());
            initConfig.setExtent(config.getExtent());
            initConfig.setIsTransfer(config.getIstransfer());
            initConfig.setArcgisurl(config.getArcgisUrl());

            //加载 菜单地图配置
            String m = request.getParameter("m");
            ModuleConfig module = gisConfig.getModuleConfig(m);
            if (StringUtils.isNotEmpty(module.getIsTransfer())) {
                initConfig.setIsTransfer(module.getIsTransfer());
            }
            if (StringUtils.isNotEmpty(module.getGisType())) {
                initConfig.setGisType(module.getGisType());
            }
            if (StringUtils.isNotEmpty(module.getArcgisUrl())) {
                initConfig.setArcgisUrl(module.getArcgisUrl());
            }

            out.setData(initConfig);
        } else {
            out.setErrorCode(-1001);
        }

        return out;
    }

    @RequestMapping(value = "/getregions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Output GetRegions(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Output output = new Output();
        try {
            output.setData(new CommonDao().GetRegions());
        } catch (Exception e) {
            e.printStackTrace();
            output.setErrorMessage("查询地市失败：" + e.getMessage());
            output.setException(e);
            logger.error("GetRegions错误:", e);
        }
        return output;
    }

    @RequestMapping(value = "/saveimage", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public Output SaveImage(HttpServletRequest request, HttpServletResponse response) {
        Output output = new Output();
        String imsStrBase64 = StringUtils.replace(request.getParameter("imagedata"), "data:image/png;base64,", "");
        try {
            String fileName = new ImageUtils().saveImage(imsStrBase64);
            output.setData(fileName);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error("保存图片失败:" + e.getMessage() + " imsStrBase64：" + imsStrBase64, e);
            output.setErrorMessage("保存图片失败:" + e.getMessage());
            output.setException(e);
        }
        return output;
    }

    @RequestMapping(value = "/cleardatacache", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Output ClearDataCache(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Output out = new Output();
        try {
            DataCache.Clear();
            out.setData("true");
        } catch (Exception e) {
            logger.error("清理缓存出错" + e.getMessage(), e);
            out.setErrorCode(ErrorCode.getErrorCode(e));
            out.setErrorMessage("清理缓存出错：" + e.getMessage());
            out.setException(e);
            e.printStackTrace();
        }
        return out;
    }
}
