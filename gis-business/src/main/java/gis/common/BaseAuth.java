package gis.common;

import gis.auth.CommonAuth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Xiaobing on 2016/9/21.
 */
public class BaseAuth {
    private static Logger logger = LoggerFactory.getLogger(BaseAuth.class);
    private static BaseAuth baseAuth = null;
    private CommonAuth commonAuth = new CommonAuth();

    public static BaseAuth GetInstance() {
        if (baseAuth == null) {
            baseAuth = new BaseAuth();
        }
        return baseAuth;
    }

    public ModelAndView getModelAndView() {
        ModelAndView view = new ModelAndView();
        if (!commonAuth.authLicense()) {
            view.setViewName("index.jsp");
        }
        return view;
    }

    public boolean isAuth(HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (!commonAuth.isAuth()) {
            //response.sendRedirect("index.jsp");
            return false;
        }
        return true;
    }

}
