package bean.cityfun;

/**
 * Created by hupeng on 16/5/23.
 * 场景性能
 */
public class AreaMScene {
    /**
     * 场景标识
     */
    private String scene_id;

    /**
     * 场景标识
     */
    private String scene_name;

    /**
     * 流量(mb)
     */
    private String flow;

    /**
     * 用户数
     */
    private String usr_cnt;

    /**
     * 4g用户数
     */
    private String lte_usr_cnt;

    /**
     * 常驻人口数
     */
    private String permanent_usr_cnt;

    /**
     * 流动人口
     */
    private String flowing_usr_cnt;

    /**
     * 人均日通话分钟数
     */
    private String per_traffic;

    /**
     * 人均日流量（mb)
     */
    private String per_flow;

    /**
     * 人均月消费(元）
     */
    private String per_consumer;

    /**
     * 高消费用户数
     */
    private String high_value_usr_cnt;

    /**
     * 普通用户数
     */
    private String normal_value_usr_cnt;

    /**
     * 消费最大值
     * */
    private String per_consumer_max;
    /**
     * 月标识
     */
    private int event_month;

    public int getEvent_month() {
        return event_month;
    }

    public void setEvent_month(int event_month) {
        this.event_month = event_month;
    }

    public String getScene_id() {
        return scene_id;
    }

    public void setScene_id(String scene_id) {
        this.scene_id = scene_id;
    }

    public String getScene_name() {
        return scene_name;
    }

    public void setScene_name(String scene_name) {
        this.scene_name = scene_name;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getUsr_cnt() {
        return usr_cnt;
    }

    public void setUsr_cnt(String usr_cnt) {
        this.usr_cnt = usr_cnt;
    }

    public String getLte_usr_cnt() {
        return lte_usr_cnt;
    }

    public void setLte_usr_cnt(String lte_usr_cnt) {
        this.lte_usr_cnt = lte_usr_cnt;
    }

    public String getPermanent_usr_cnt() {
        return permanent_usr_cnt;
    }

    public void setPermanent_usr_cnt(String permanent_usr_cnt) {
        this.permanent_usr_cnt = permanent_usr_cnt;
    }

    public String getFlowing_usr_cnt() {
        return flowing_usr_cnt;
    }

    public void setFlowing_usr_cnt(String flowing_usr_cnt) {
        this.flowing_usr_cnt = flowing_usr_cnt;
    }

    public String getPer_traffic() {
        return per_traffic;
    }

    public void setPer_traffic(String per_traffic) {
        this.per_traffic = per_traffic;
    }

    public String getPer_flow() {
        return per_flow;
    }

    public void setPer_flow(String per_flow) {
        this.per_flow = per_flow;
    }

    public String getPer_consumer() {
        return per_consumer;
    }

    public void setPer_consumer(String per_consumer) {
        this.per_consumer = per_consumer;
    }

    public String getHigh_value_usr_cnt() {
        return high_value_usr_cnt;
    }

    public void setHigh_value_usr_cnt(String high_value_usr_cnt) {
        this.high_value_usr_cnt = high_value_usr_cnt;
    }

    public String getNormal_value_usr_cnt() {
        return normal_value_usr_cnt;
    }

    public void setNormal_value_usr_cnt(String normal_value_usr_cnt) {
        this.normal_value_usr_cnt = normal_value_usr_cnt;
    }

    public String getPer_consumer_max() {
        return per_consumer_max;
    }

    public void setPer_consumer_max(String per_consumer_max) {
        this.per_consumer_max = per_consumer_max;
    }
}
