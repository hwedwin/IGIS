<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@ include file="common/reference.jsp" %>
    <title></title>
    <script type="text/javascript">
        var dojoConfig = {
            parseOnLoad: true,
            packages: [{
                name: 'plugin',
                location: root + "script/arcgis_js_api/plugin"
            }]
        };
    </script>
    <link rel="stylesheet" type="text/css"
          href="<%=root%>script/arcgis_js_api/library/3.9/3.9/js/dojo/dijit/themes/tundra/tundra.css"/>
    <link rel="stylesheet" type="text/css" href="<%=root%>script/arcgis_js_api/library/3.9/3.9/js/esri/css/esri.css"/>
    <script type="text/javascript" src="<%=root%>script/arcgis_js_api/library/3.9/3.9/init.js"></script>
    <script type="text/javascript">
        // 功能插件
        var features = [];
        features.push("esri/map");
        features.push("esri/config");
        features.push("esri/geometry/Point");
        features.push("esri/geometry/Extent");
        features.push("dojo/promise/all");
        features.push("esri/geometry/webMercatorUtils");
        features.push("esri/SpatialReference");
        features.push("esri/symbols/SimpleMarkerSymbol");
        features.push("esri/symbols/SimpleLineSymbol");
        features.push("esri/symbols/SimpleFillSymbol");
        features.push("esri/graphic");
        features.push("esri/layers/GraphicsLayer");
        features.push("esri/layers/ArcGISDynamicMapServiceLayer");
        features.push("esri/tasks/query");
        features.push("esri/tasks/QueryTask");
        features.push("dijit/form/Button");
        features.push("dojo/_base/Color");
        features.push("dojo/_base/array");
        features.push("plugin/CQArcGisLayer");
        features.push("dojo/domReady!");

        var app = {};
        require(features, function (Map, esriConfig, Point, Extent, all, webMercatorUtils, SpatialReference, SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol, Graphic, GraphicsLayer, ArcGISDynamicMapServiceLayer, Query, QueryTask, Button, Color, arrayUtils, CQArcGisLayer) {
            app.map = new Map("map", {
                logo: false
            });

            esriConfig.defaults.io.corsDetection = false;

            app.map.addLayer(new CQArcGisLayer());
            app.map.addLayer(new ArcGISDynamicMapServiceLayer("http://10.190.145.130:8399/arcgis/rest/services/woss/MapServer"));

            // 初始定位
            app.map.centerAndZoom(new Point({
                x: 106.55548282458164,
                y: 29.559427429311895,
                spatialReference: {
                    wkid: 4326
                }
            }), 8);

            var border = new SimpleLineSymbol();
            border.setStyle(SimpleLineSymbol.STYLE_NULL);
            var symbol = new SimpleFillSymbol();
            symbol.setOutline(border);
            symbol.setColor(new Color([255, 0, 0, 1]));

            app.qtCells = new QueryTask("http://10.190.145.130:8399/arcgis/rest/services/woss/MapServer/1");
            app.map.on("click", function (e) {
                var point = e.mapPoint;
                var pxWidth = app.map.extent.getWidth() / app.map.width;
                var padding = 3 * pxWidth;
                var qCells = new Query();
                qCells.returnGeometry = true;
                qCells.outFields = ["*"];
                qCells.geometry = new Extent({
                    "xmin": point.x - padding,
                    "ymin": point.y - padding,
                    "xmax": point.x + padding,
                    "ymax": point.y + padding,
                    "spatialReference": point.spatialReference
                });
                var cells = app.qtCells.execute(qCells);
                all([cells]).then(function (results) {
                    var feats = results[0].features;
                    app.map.graphics.clear();
                    arrayUtils.forEach(feats, function (feat) {
                        feat.setSymbol(symbol);
                        app.map.graphics.add(feat);
                    });
                });
            });

            var button = new Button({
                label: "Locate",
                onClick: function () {
                    debugger;
                    var qCells1 = new Query();
                    qCells1.text = "-7760643585512001668";
                    var qCells2 = new Query();
                    qCells2.text = "4995005439932400643";
                    qCells1.returnGeometry = qCells2.returnGeometry = true;
                    qCells1.outFields = qCells2.outFields = ["*"];

                    var cells1 = app.qtCells.execute(qCells1);
                    var cells2 = app.qtCells.execute(qCells2);

                    all([cells1, cells2]).then(function (results) {
                        app.map.graphics.clear();

                        var feats1 = results[0].features;
                        arrayUtils.forEach(feats1, function (feat) {
                            feat.setSymbol(symbol);
                            app.map.graphics.add(feat);
                        });

                        var feats2 = results[1].features;
                        arrayUtils.forEach(feats2, function (feat) {
                            feat.setSymbol(symbol);
                            app.map.graphics.add(feat);
                        });
                    });
                }
            }, "locateHome");
        });
    </script>
</head>
<body>
<div class="tundra">
    <div>重庆移动ArcGIS集成示例</div>
    <button id="locateHome" type="button"></button>
    <div id="map"></div>
</div>
<div id="labelL" style="position: fixed; bottom: 20px; left: 0px"></div>
<div id="labelR" style="position: fixed; bottom: 0px; right: 0px"></div>
</body>
</html>
