package bean.common;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xiaobing on 2016/6/8.
 */
public class ModuleConfig {
    private String name;
    private String showSearch = "true";
    private String page;
    private List<ToolBar> toolBars;
    private List<Menu> menus;
    private List<String> jsPath;
    private List<String> init;
    private String map;
    private String mapService;
    private String gisType;
    private String arcgisUrl;
    private String isTransfer;
    private String showToolBars;

    public ModuleConfig() {
        this.jsPath = new ArrayList<>();
        this.toolBars = new ArrayList<>();
        this.menus = new ArrayList<>();
        this.init = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getJsPath() {
        return jsPath;
    }

    public void setJsPath(List<String> jsPath) {
        this.jsPath = jsPath;
    }

    public String getShowSearch() {
        return this.showSearch;
    }

    public void setShowSearch(String showSearch) {
        this.showSearch = showSearch;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public List<ToolBar> getToolBars() {
        return toolBars;
    }

    public void setToolBars(List<ToolBar> toolBars) {
        this.toolBars = toolBars;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public List<String> getInit() {
        return init;
    }

    public void setInit(List<String> funs) {
        this.init = funs;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }

    public String getMapService() {
        return mapService;
    }

    public void setMapService(String mapService) {
        this.mapService = mapService;
    }

    public String getGisType() {
        return gisType;
    }

    public void setGisType(String gisType) {
        this.gisType = gisType;
    }

    public String getArcgisUrl() {
        return arcgisUrl;
    }

    public void setArcgisUrl(String arcgisUrl) {
        this.arcgisUrl = arcgisUrl;
    }

    public String getIsTransfer() {
        return isTransfer;
    }

    public void setIsTransfer(String isTransfer) {
        this.isTransfer = isTransfer;
    }

    public String getShowToolBars() {
        return showToolBars;
    }

    public void setShowToolBars(String showToolBars) {
        this.showToolBars = showToolBars;
    }
}
