$(function(){
    //tooltip
    $('[data-toggle="tooltip"]').tooltip();


    //内容区域高度计算
    var bodyH = $(window).height();
    var headH = $('.navbar').outerHeight();
    var headout = parseInt($('.navbar').css('margin-bottom'));
    var padding = (parseInt($('.panel-body').css('padding')))*2;
    var panelout = parseInt($('.panel').css('margin-bottom'))+parseInt($('.panel').css('border-top'))+parseInt($('.panel').css('border-bottom'));
    var toolbarH = $('.page-search').height();
    var panelheadH = $('.panel-heading').outerHeight();
    var contH = bodyH - headH - padding - headout - panelout - toolbarH;
    $('.idvm-cont').height(contH);
    $('#rig_property .idvm-cont').height(contH - panelheadH);
    $(window).resize(function(){
        var bodyH = $(window).height();
        var headH = $('.navbar').outerHeight();
        var headout = parseInt($('.navbar').css('margin-bottom'));
        var padding = (parseInt($('.panel-body').css('padding')))*2;
        var panelout = parseInt($('.panel').css('margin-bottom'))+parseInt($('.panel').css('border-top'))+parseInt($('.panel').css('border-bottom'));
        var toolbarH = $('.page-search').height();
        var panelheadH = $('.panel-heading').outerHeight();
        var contH = bodyH - headH - padding - headout - panelout - toolbarH;
        $('.idvm-cont').height(contH);
        $('#rig_property .idvm-cont').height(contH - panelheadH);
    });


    //右侧面板固定
    $('#pushpin').click(function(){
        var bodyH = $(window).height();
        var headH = $('.navbar').outerHeight();
        $('#view_cont').removeClass('col-md-9').addClass('col-md-12');
        $(this).closest('.panel').parent().css({
            position:'fixed',
            top:56,
            right:0,
        });
        $(this).closest('.panel').css({
            border:'1px solid #ddd',
            boxShadow:'-3px 2px 5px rgba(0,0,0,0.2)'
        });
        $('#rig_property').find('.idvm-cont').height(bodyH - headH);
    });
})