<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
  <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
  <%@ include file="header.jsp" %>
  <section>
    <div class="container">
      <%@ include file="leftmenu.jsp" %>
      <input id="pageid" type="hidden" value="commresapi">

      <div class="docs-content" style="height: 7400px;">
        <h3 class="welcome">公共工具</h3>

        <p>common.js文件中 gis_common 类和 gis_resources 类，封装了关于iGIS提供的公共工具库，下面对该部分进行介绍</p>
        <p>gis_common 类 主要是整个前端呈现的公共手段</p>
        <p>gis_resources 类 主要对数据缓存的操作</p>

        <h3 class="welcome">common类工具</h3>

        <li>gis_layout.showError 错误 异常信息显示</li>
        <pre class="prettyprint">
      输入参数:
          @param error 错误信息
      样例:
           gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
        </pre>
        <li>gis_layout.showMessage 信息显示</li>
        <pre class="prettyprint">
      输入参数:
          @param msg 消息
      样例:
          gis_common.showMessage("请选择时间");
        </pre>
        <li>gis_layout.outPut 统一处理错误信息，错误码</li>
        <pre class="prettyprint">
      输入参数:
          @param output
      样例:
          success: function (req) {
                gis_common.outPut(req, function (datas) {
                    var btses = eval(datas);
                    var graphics = createCellCover(btses);
                    for (var i = 0; i < graphics.length; i++) {
                        cell_layer.add(graphics[i]);
                    }
                });
            },
        </pre>
        <li>gis_layout.mapLoadingShow 显示Loading</li>
        <pre class="prettyprint">
      输入参数:
          无
      样例:
          gis_common.mapLoadingShow();
        </pre>
        <li>gis_layout.mapLoadingHide 隐藏Loading</li>
        <pre class="prettyprint">
      输入参数:
          无
      样例:
          gis_common.mapLoadingHide();
        </pre>
        <li>gis_layout.isLoadingAddCount 显示Loading  以计数器的方式控制 计数器加1</li>
        <pre class="prettyprint">
      输入参数:
          无
      样例:
        var initData = function () {
		  $.ajax({
			  url : "/getltecovercells",
			  data : {
				  time : time + "",
				  kpi : kpi
			  },
			  type : "GET",
			  beforeSend : function () {
			    	gis_common.isLoadingAddCount();
			  },
			  success : function (req) {
				  var legends = eval(req);
				  loadLayer();
			  },
			  complete : function () {
				  gis_common.isLoadingSunCount();
			  },
			  error : function (XMLHttpRequest, textStatus, errorThrown) {
				  gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
			  }
		  });
	  };
        </pre>
        <li>gis_layout.isLoadingSunCount Loading 计算减1 如果计数器为0  就关闭Loading</li>
        <pre class="prettyprint">
      输入参数:
          无
      样例:
          complete : function () {
            gis_common.isLoadingSunCount();
          },
        </pre>
        <li>gis_layout.stringIsNullOrWhiteSpace 判断字符串是否为空 undefined null ""</li>
        <pre class="prettyprint">
      输入参数:
          @param str 需要判断的字符串
          @returns {undefined、null、 "" 返回true 其他返回false }
      样例:
          if (!gis_common.stringIsNullOrWhiteSpace(legend.title)) {
                content += " title='" + legend.title + "' ";
            }
        </pre>
        <li>gis_layout.windowOpen 打开窗口 用来下载文件 等</li>
        <pre class="prettyprint">
      输入参数:
          @param url
          @param target
      样例:
          var downloadSceneDemo = function () {
            var rul = "/demo/scenedemo.csv";
            gis_common.windowOpen(rul, "_blank");
          };
        </pre>
        <li>gis_layout.showMapExtent 显示当前地图范围</li>
        <pre class="prettyprint">
      输入参数:
          无
      样例:
          gis_layout.showMapExtent();
        </pre>
        <li>gis_layout.setExtent 设置地图显示范围</li>
        <pre class="prettyprint">
      输入参数:
          @param xmax
          @param ymax
          @param xmin
          @param ymin
      样例:
          gis_common.setExtent(parseFloat(data.maxLongitude) + 0.0005, parseFloat(data.maxLatitude) + 0.0005, parseFloat(data.minLongitude) - 0.0005, parseFloat(data.minLatitude) - 0.0005);
        </pre>
        <li>gis_layout.setPoint 根据点定位</li>
        <pre class="prettyprint">
      输入参数:
          @param lon
          @param lat
      样例:
          gis_common.setPoint(lon, lat);
        </pre>
        <li>gis_layout.getMapExtent 显示当前地图范围</li>
        <pre class="prettyprint">
      输入参数:
          @returns {返回Extent对象}
      样例:
          gis_common.getMapExtent()
        </pre>
        <li>gis_layout.getQueryString 获取URL参数</li>
        <pre class="prettyprint">
      输入参数:
          @param key 参数名称
      样例:
           var typ = gis_common.getQueryString("wmstyp");
        </pre>
        <li>gis_layout.getMapExtentDelta 根据 delta 获取 Extent 的json 对象</li>
        <pre class="prettyprint">
      输入参数:
          @param delta 范围数据
          @returns {返回Extent对象}
      样例:
          gis_common.getMapExtentDelta(delta);
        </pre>
        <li>gis_layout.createBtsGraphic 生成基站图形</li>
        <pre class="prettyprint">
      输入参数:
           @param bts 基站对象
           @returns {返回Graphic对象}
      样例:
          var b = bts[i];
          var graphics = gis_common.createBtsGraphic(b);
        </pre>
        <li>gis_layout.createCellGraphic 生成小区图形</li>
        <pre class="prettyprint">
      输入参数:
           @param cell 小区对象
           @returns {返回Graphic对象}
      样例:
          var g = gis_common.createCellGraphic(cell);
          var symbol = createSymbol(cell.value);
          g.setSymbol(symbol);
          cell_layer.add(g);
        </pre>
        <li>gis_layout.createCellCoverRing 生成小区覆盖</li>
        <pre class="prettyprint">
      输入参数:
           @param longitude 经度
           @param latitude 纬度
           @param radius 半径
           @param azimuth  方向角(0度为正北,顺时针旋转)
           @param antbw 半功率角(小区覆盖角度的一半)
           @returns {返回经纬度数组}
      样例:
           var rings = gis_common.createCellCoverRing(cell.longitude, cell.latitude, cell.radius, cell.azimuth, cell.antbw);
        </pre>
        <li>gis_layout.createCellCover 生成小区示意图形</li>
        <pre class="prettyprint">
      输入参数:
           @param btses
           @returns {Array}
      样例:
          var btses = eval(datas);
          var graphics = createCellCover(btses);
        </pre>
        <li>gis_layout.createKeyValueList 生成KeyValse 形式的list 的列表</li>
        <pre class="prettyprint">
      输入参数:
            @param keyValues
            @returns {*|jQuery|HTMLElement}
      样例:
            var div = gis_common.createKeyValueList(data);
        </pre>
        <li>gis_layout.queryRegisterFun 通用查询 注册查询方法</li>
        <pre class="prettyprint">
      输入参数:
            @param key
            @param fun 查询方法
      样例:
            gis_common.queryRegisterFun("endToEnd_loadCompetitorCoverCellKpi", loadCompetitorCoverCellKpiTopn);

            var loadCompetitorCoverCellKpiTopn = function (keywords, callback) {
              var arr = wmsLayer_type.split("_");
              if (time == null || !(arr.length == 2 && (arr[0] == "competitorcover" || arr[0] == "competitorcoverspots") && arr[1] == "1")) {
                  gis_common.queryRemoveFun("endToEnd_loadCompetitorCoverCellKpi");
                  return;
              }
              $.ajax({
                  url: "/getcovercellkpitopn",
                  data: {
                      key: keywords,
                      time: time,
                      kpi: kpi,
                      where: where_sql,
                      wmsLayer_type: wmsLayer_type
                  },
                  dataType: "json",
                  async: true,
                  type: "GET",
                  beforeSend: function () {
                      gis_common.isLoadingAddCount();
                  },
                  success: function (req) {
                      gis_common.outPut(req, function (datas) {
                          callback(datas);
                      });
                  },
                  complete: function () {
                      gis_common.isLoadingSunCount();
                  },
                  error: function (XMLHttpRequest, textStatus, errorThrown) {
                      gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
                  }
              });
            };
        </pre>
        <li>gis_layout.queryRemoveFun 通用查询 移除查询方法</li>
        <pre class="prettyprint">
      输入参数:
            @param key
            @param fun 查询方法
      样例:
            gis_common.queryRemoveFun("endToEnd_getmroltebtstopn");
        </pre>
        <li>gis_layout.queryPointFun 通用查询方法  点 定位</li>
        <pre class="prettyprint">
      输入参数:
            @param obj 对象
      样例:
           gis_common.queryPointFun(this)
        </pre>
        <li>gis_layout.query 通用查询 初始化方法</li>
        <pre class="prettyprint">
      输入参数:
            无
      样例:
            if ((event.keyCode || event.which) == 13) {
                gis_common.query();
            }
        </pre>


        <li>gis_layout.poiLoad Poi查询</li>
        <pre class="prettyprint">
      输入参数:
            @param obj 对象
            @param showCate
      样例:
            gis_common.poiLoad(this,true)
        </pre>
        <li>gis_layout.poiDestroy Poi销毁</li>
        <pre class="prettyprint">
      输入参数:
            @param obj 对象
            @param showCate
      样例:
            gis_common.poiDestroy(this,true)
        </pre>
        <li>gis_layout.resLoad 资源res</li>
        <pre class="prettyprint">
      输入参数:
            @param obj 对象
      样例:
            gis_common.resLoad(this)
        </pre>
        <li>gis_layout.resSetQueryParam 自定义资源查询参数</li>
        <pre class="prettyprint">
      输入参数:
            @param rqCell
      样例:
            gis_common.resSetQueryParam(RqCell);
        </pre>
        <li>gis_layout.resDestroy 资源res 销毁</li>
        <pre class="prettyprint">
      输入参数:
            无
      样例:
            gis_common.resDestroy();
        </pre>
        <li>gis_layout.distanceLoad 测距</li>
        <pre class="prettyprint">
      输入参数:
            无
      样例:
           gis_common.distanceLoad(this);
        </pre>
        <li>gis_layout.clearDataCache 清理缓存</li>
        <pre class="prettyprint">
      输入参数:
            无
      样例:
           gis_common.clearDataCache();
        </pre>





        <h3 class="welcome">resources类工具</h3>
        <li>gis_resources.addCache 添加缓存 如果KEY 相同  则覆盖以前的</li>
        <pre class="prettyprint">
      输入参数:
          @param key 缓存关键字
          @param data  数据
      样例:
        gis_resources.addCache(key,data);
        </pre>
        <li>gis_resources.removerCache 移除缓存</li>
        <pre class="prettyprint">
      输入参数:
          @param key 缓存关键字
      样例:
          gis_resources.removerCache(key);
        </pre>
        <li>gis_resources.getCache 获取缓存</li>
        <pre class="prettyprint">
      输入参数:
          @param key 缓存关键字
      样例:
          var data = gis_resources.getCache(key);
        </pre>
        <br/>

        <a href="layoutapi.jsp" type="button" class="btn-link">上一页</a>
        <a href="layoutapi.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>
      </div>
    </div>
  </section>
</div>
</body>
</html>
