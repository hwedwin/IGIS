package bean.common;

import bean.enums.ResType;

/**
 * 告警基站pojo 类
 *
 * @author Xiaobing
 */
public class WarnBaseStation {
    private String id;
    private String name;
    private double longitude;
    private double latitude;
    private double originalLongitude;
    private double originalLatitude;
    private String cityId;
    private String cityName;
    private String countryId;
    private String countyName;
    private ResType resType;
    private String warnType;

    /**
     * 告警级别
     *
     * @return
     */
    public String getWarnType() {
        return warnType;
    }

    /**
     * 告警级别
     *
     * @return
     */
    public void setWarnType(String warnType) {
        this.warnType = warnType;
    }

    /**
     * 原始经度
     *
     * @return
     */
    public double getOriginalLongitude() {
        return originalLongitude;
    }

    /**
     * 原始经度
     *
     * @param originalLongitude
     */
    public void setOriginalLongitude(double originalLongitude) {
        this.originalLongitude = originalLongitude;
    }

    /**
     * 原始纬度
     *
     * @return
     */
    public double getOriginalLatitude() {
        return originalLatitude;
    }

    /**
     * 原始纬度
     *
     * @param originalLatitude
     */
    public void setOriginalLatitude(double originalLatitude) {
        this.originalLatitude = originalLatitude;
    }

    /**
     * 地市ID
     *
     * @return
     */
    public String getCityId() {
        return cityId;
    }

    /**
     * 地市ID
     *
     * @param cityId
     */
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    /**
     * 地市名称
     *
     * @return
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * 地市名称
     *
     * @param cityName
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * 区县ID
     *
     * @return
     */
    public String getCountryId() {
        return countryId;
    }

    /**
     * 区县ID
     *
     * @param countryId
     */
    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    /**
     * 区县名称
     *
     * @return
     */
    public String getCountyName() {
        return countyName;
    }

    /**
     * 区县名称
     *
     * @param countyName
     */
    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    /**
     * 资源类型
     *
     * @return
     */
    public ResType getResType() {
        return resType;
    }

    /**
     * 资源类型
     *
     * @param resType
     */
    public void setResType(ResType resType) {
        this.resType = resType;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

}
