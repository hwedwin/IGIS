<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@ include file="reference.jsp" %>
    <title></title>
    <script type="text/javascript">
        var dojoConfig = {
            parseOnLoad: true,
            packages: [{
                name: 'plugin',
                location: root + "script/arcgis_js_api/plugin"
            }]
        };
    </script>
    <link rel="stylesheet" type="text/css"
          href="<%=root%>script/arcgis_js_api/library/3.9/3.9/js/dojo/dijit/themes/tundra/tundra.css"/>
    <link rel="stylesheet" type="text/css" href="<%=root%>script/arcgis_js_api/library/3.9/3.9/js/esri/css/esri.css"/>
    <script type="text/javascript" src="<%=root%>script/arcgis_js_api/library/3.9/3.9/init.js"></script>
    <script type="text/javascript">
        // 功能插件
        var features = [];
        features.push("esri/map");
        features.push("esri/geometry/Point");
        features.push("esri/geometry/webMercatorUtils");
        features.push("esri/SpatialReference");
        features.push("esri/symbols/SimpleMarkerSymbol");
        features.push("esri/symbols/SimpleLineSymbol");
        features.push("esri/graphic");
        features.push("dijit/form/Button");
        features.push("dojo/_base/Color");
        features.push("plugin/Tide4326Layer");
        features.push("plugin/TianDiTuVecLayer");
        features.push("plugin/TianDiTuCvaLayer");
        features.push("dojo/domReady!");

        require(features, function (Map, Point, webMercatorUtils, SpatialReference, SimpleMarkerSymbol, SimpleLineSymbol, Graphic, Button, Color, Tide4326Layer, TianDiTuVecLayer, TianDiTuCvaLayer) {
            var map = new Map("map", {
                logo: false
            });

            var vec = new TianDiTuVecLayer();
            map.addLayer(vec);
            var tide = new Tide4326Layer();
            map.addLayer(tide);
            var cva = new TianDiTuCvaLayer();
            map.addLayer(cva);

            // 标记边框
            var outSymbol = new SimpleLineSymbol();
            outSymbol.setStyle(SimpleLineSymbol.STYLE_NULL);

            // 地图点击事件
            map.on("click", function (evt) {
                var obj = {};
                obj.wkid = map.spatialReference.wkid;
                obj.level = map.getLevel();
                obj.x = evt.mapPoint.x;
                obj.y = evt.mapPoint.y;
                dojo.byId("label").innerHTML = $.toJSON(obj);

                var point = new Point(evt.mapPoint.x, evt.mapPoint.y, new SpatialReference({
                    wkid: map.spatialReference.wkid
                }));

                var symbol = new SimpleMarkerSymbol();
                symbol.setOutline(outSymbol);
                symbol.setSize(14);
                symbol.setColor(new Color([255, 0, 0, 1]));

                var graphic = new Graphic(point, symbol);
                map.graphics.clear();
                map.graphics.add(graphic);
            });

            // 初始定位
            map.centerAndZoom(new Point({
                x: 106.55548282458164,
                y: 29.559427429311895,
                spatialReference: {
                    wkid: 4326
                }
            }), 12);

            // 点击定位
            var button = new Button({
                label: "Locate",
                onClick: function () {
                    var point = new Point(106.5185541708646, 29.621794153326178, new SpatialReference({
                        wkid: map.spatialReference.wkid
                    }));

                    var symbol = new SimpleMarkerSymbol();
                    symbol.setOutline(outSymbol);
                    symbol.setSize(14);
                    symbol.setColor(new Color([255, 0, 0, 1]));

                    map.centerAndZoom(point, 15);

                    var graphic = new Graphic(point, symbol);
                    map.graphics.clear();
                    map.graphics.add(graphic);
                }
            }, "locateHome");
        });
    </script>
</head>
<body>
<div class="tundra">
    <div>高级指标渲染图:TianDiTuVecLayer,TianDiTuCvaLayer:4326</div>
    <button id="locateHome" type="button"></button>
    <div id="map"></div>
</div>
<div id="label" style="position: fixed; bottom: 0px; right: 0px"></div>
</body>
</html>
