<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%String root_gisapp = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>
<%String root_arcgis = request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>

<link rel="stylesheet" href="/script/arcgis_js_api/library/3.9/3.9/js/dojo/dijit/themes/dijit.css">
<link rel="stylesheet" href="/script/arcgis_js_api/library/3.9/3.9/js/esri/css/esri.css">

<link rel="stylesheet" type="text/css" media="screen" href="/script/base/jqueryui/jquery-ui.min.css"/>
<link rel="stylesheet" type="text/css" media="screen" href="/script/base/jqgrid/ui.jqgrid.css"/>

<link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/css/font-awesome-4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/css/public.css">
<link rel="stylesheet" type="text/css" href="/css/topic.css">
<link rel="stylesheet" type="text/css" href="/script/base/artcloudui/skins/default/css/artcloudui.css">

<script type="text/javascript" src="/script/base/My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript" src="/script/base/jquery-1.11.0.js"></script>
<script type="text/javascript" src="/script/base/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/script/base/json-js-master/json2.js"></script>
<script type="text/javascript" src="/script/base/artcloudui/artcloudui.dialog.min.js"></script>
<%--<script type="text/javascript" src="/script/base/jqgrid/js/i18n/grid.locale-cn.js"></script>--%>
<script type="text/javascript" src="/script/base/jqgrid/js/jquery.jqGrid.min.js"></script>
<%--<script type="text/javascript" src="/script/base/uploadify/jquery.uploadify-3.1.js"></script>
<script type="text/javascript" src="/script/base/html2canvas.js"></script>--%>

<script type="text/javascript" src="/script/plugins/scrollbar/perfect-scrollbar.js"></script>
<script type="text/javascript" src="/script/plugins/scrollbar/jquery.mousewheel.js"></script>

<script type="text/javascript">
  var root_gisapp = '<%=root_gisapp%>';
  var root_arcgis = '<%=root_arcgis%>';
  var dojoConfig = {
    parseOnLoad: true,
    packages: [{
      name: 'plugin',
      location: "/script/arcgis_js_api/plugin"
    }, {
      name: 'modules',
      location: "/script/modules"
    }]
  };
</script>

<script type="text/javascript" src="/script/arcgis_js_api/library/3.9/3.9/init.js"></script>
<script type="text/javascript" src="/script/base.js"></script>
<script type="text/javascript" src="/script/modules/common.js"></script>
