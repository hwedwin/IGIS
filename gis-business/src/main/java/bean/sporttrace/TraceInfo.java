package bean.sporttrace;

import gis.core.bean.ResValue;

/**
 * 运功轨迹
 * Created by hupeng on 16/4/26.
 */
public class TraceInfo extends ResValue {

    /**
     * 用户IMSI bigint
     */
    private long IMSI;
    /**
     * 终端IMEI bigint
     */
    private long IMEI;
    /**
     * 用户号码 bigint
     */
    private long MSISDN;
    /**
     * TCP/UDP流开始时间(YYYYMMDDHH24MIssms) bigint
     */
    private long Procedure_Start_Time;
    /**
     * TCP/UDP流结束时间(YYYYMMDDHH24MIssms) bigint
     */
    private long Procedure_End_Time;
    /**
     * 应用大类 int
     */
    private int App_Type;
    /**
     * 应用小类 int
     */
    private int App_Sub_type;
    /**
     * 标识业务是否成功 int
     * 0：业务成功
     * 1：业务失败
     * 2：业务未识别
     */
    private int app_status;
    /**
     * 应用内容细分/应用小类的内容细分，如微信文本、微信语言等 int  1.文本，2.图片，3.音频，4.视频，5.其他文件 如无则填全F
     */
    private int app_content;
    /**
     * L4协议类型 int 0：TCP 1：UDP
     */
    private int l4_protocal;
    /**
     * tac bigint
     */
    private long TAC;
    /**
     * UE所在小区的ECI bigint
     */
    private long Cell_ID;
    /**
     * 上行流量(B) bigint
     */
    private long ul_data;
    /**
     * 下行流量(B) bigint
     */
    private long dl_data;
    /**
     * 上行IP包数 bigint
     */
    private long ul_ip_packet;
    /**
     * 下行IP包数 bigint
     */
    private long dl_ip_packet;
    /**
     * TCP建链响应时延（ms）bigint
     */
    private long tcp_build_chain_re_delay;
    /**
     * TCP建链确认时延（ms）bigint
     */
    private long tcp_build_chain_confirm_delay;
    /**
     * 第一条事物请求到其第一个响应包时延（ms）bigint
     */
    private long tcp_link_suc_to_fi_delay_tran_req;
    ;
    /**
     * 第一条事务请求到其第一个响应包时延（ms）bigint
     */
    private long delay_fr_fi_tra_re_to_fi_re_pac;
    /**
     * TCP建链尝试次数（TCP_SYN的次数）bigint
     */
    private long tcp_attempts_to_build_chain;
    /**
     * TCP连接状态指示 int
     */
    private int tcp_connection_status_indication;
    /**
     * 会话是否结束标志 int
     */
    private int marks_end_of_session;
    /**
     * HTTP/WAP事务状态 int
     */
    private int http_wap_state_of_affairs;
    /**
     * 第一个HTTP响应包时延（ms） bigint
     */
    private long first_http_response_packet_delay;
    /**
     * 最后一个HTTP内容包的时延（ms）bigint
     */
    private long final_http_content_package_delay;
    /**
     * 最后一个ACK确认包的时延（ms） bigint
     */
    private long final_confirmation_packet_ack_delay;
    /**
     * 访问域名 varchar(2000)
     */
    private String host;
    /**
     * 访问的URI varchar(4000)
     */
    private String uri;
    /**
     * 终端向访问网站提供的终端信息 varchar(1000)
     */
    private String user_agent;
    /**
     * 小区经度 decimal(11,8)
     */
    private float cell_lon;
    /**
     * 小区纬度 decimal(11,8)
     */
    private float cell_lat;
    /**
     * url上的经度 decimal(11,8)
     */
    private float url_lon;
    /**
     * url上的纬度 decimal(11,8)
     */
    private float url_lat;
    /**
     * 距离(米) decimal(20,2)
     */
    private float range;


    public long getIMSI() {
        return IMSI;
    }

    public void setIMSI(long IMSI) {
        this.IMSI = IMSI;
    }

    public long getIMEI() {
        return IMEI;
    }

    public void setIMEI(long IMEI) {
        this.IMEI = IMEI;
    }

    public long getMSISDN() {
        return MSISDN;
    }

    public void setMSISDN(long MSISDN) {
        this.MSISDN = MSISDN;
    }

    public long getProcedure_Start_Time() {
        return Procedure_Start_Time;
    }

    public void setProcedure_Start_Time(long procedure_Start_Time) {
        Procedure_Start_Time = procedure_Start_Time;
    }

    public long getProcedure_End_Time() {
        return Procedure_End_Time;
    }

    public void setProcedure_End_Time(long procedure_End_Time) {
        Procedure_End_Time = procedure_End_Time;
    }

    public int getApp_Type() {
        return App_Type;
    }

    public void setApp_Type(int app_Type) {
        App_Type = app_Type;
    }

    public int getApp_Sub_type() {
        return App_Sub_type;
    }

    public void setApp_Sub_type(int app_Sub_type) {
        App_Sub_type = app_Sub_type;
    }

    public int getApp_status() {
        return app_status;
    }

    public void setApp_status(int app_status) {
        this.app_status = app_status;
    }

    public int getApp_content() {
        return app_content;
    }

    public void setApp_content(int app_content) {
        this.app_content = app_content;
    }

    public int getL4_protocal() {
        return l4_protocal;
    }

    public void setL4_protocal(int l4_protocal) {
        this.l4_protocal = l4_protocal;
    }

    public long getTAC() {
        return TAC;
    }

    public void setTAC(long TAC) {
        this.TAC = TAC;
    }

    public long getCell_ID() {
        return Cell_ID;
    }

    public void setCell_ID(long cell_ID) {
        Cell_ID = cell_ID;
    }

    public long getUl_data() {
        return ul_data;
    }

    public void setUl_data(long ul_data) {
        this.ul_data = ul_data;
    }

    public long getDl_data() {
        return dl_data;
    }

    public void setDl_data(long dl_data) {
        this.dl_data = dl_data;
    }

    public long getUl_ip_packet() {
        return ul_ip_packet;
    }

    public void setUl_ip_packet(long ul_ip_packet) {
        this.ul_ip_packet = ul_ip_packet;
    }

    public long getDl_ip_packet() {
        return dl_ip_packet;
    }

    public void setDl_ip_packet(long dl_ip_packet) {
        this.dl_ip_packet = dl_ip_packet;
    }

    public long getTcp_build_chain_re_delay() {
        return tcp_build_chain_re_delay;
    }

    public void setTcp_build_chain_re_delay(long tcp_build_chain_re_delay) {
        this.tcp_build_chain_re_delay = tcp_build_chain_re_delay;
    }

    public long getTcp_build_chain_confirm_delay() {
        return tcp_build_chain_confirm_delay;
    }

    public void setTcp_build_chain_confirm_delay(long tcp_build_chain_confirm_delay) {
        this.tcp_build_chain_confirm_delay = tcp_build_chain_confirm_delay;
    }

    public long getTcp_link_suc_to_fi_delay_tran_req() {
        return tcp_link_suc_to_fi_delay_tran_req;
    }

    public void setTcp_link_suc_to_fi_delay_tran_req(long tcp_link_suc_to_fi_delay_tran_req) {
        this.tcp_link_suc_to_fi_delay_tran_req = tcp_link_suc_to_fi_delay_tran_req;
    }

    public long getDelay_fr_fi_tra_re_to_fi_re_pac() {
        return delay_fr_fi_tra_re_to_fi_re_pac;
    }

    public void setDelay_fr_fi_tra_re_to_fi_re_pac(long delay_fr_fi_tra_re_to_fi_re_pac) {
        this.delay_fr_fi_tra_re_to_fi_re_pac = delay_fr_fi_tra_re_to_fi_re_pac;
    }

    public long getTcp_attempts_to_build_chain() {
        return tcp_attempts_to_build_chain;
    }

    public void setTcp_attempts_to_build_chain(long tcp_attempts_to_build_chain) {
        this.tcp_attempts_to_build_chain = tcp_attempts_to_build_chain;
    }

    public int getTcp_connection_status_indication() {
        return tcp_connection_status_indication;
    }

    public void setTcp_connection_status_indication(int tcp_connection_status_indication) {
        this.tcp_connection_status_indication = tcp_connection_status_indication;
    }

    public int getMarks_end_of_session() {
        return marks_end_of_session;
    }

    public void setMarks_end_of_session(int marks_end_of_session) {
        this.marks_end_of_session = marks_end_of_session;
    }

    public int getHttp_wap_state_of_affairs() {
        return http_wap_state_of_affairs;
    }

    public void setHttp_wap_state_of_affairs(int http_wap_state_of_affairs) {
        this.http_wap_state_of_affairs = http_wap_state_of_affairs;
    }

    public long getFirst_http_response_packet_delay() {
        return first_http_response_packet_delay;
    }

    public void setFirst_http_response_packet_delay(long first_http_response_packet_delay) {
        this.first_http_response_packet_delay = first_http_response_packet_delay;
    }

    public long getFinal_http_content_package_delay() {
        return final_http_content_package_delay;
    }

    public void setFinal_http_content_package_delay(long final_http_content_package_delay) {
        this.final_http_content_package_delay = final_http_content_package_delay;
    }

    public long getFinal_confirmation_packet_ack_delay() {
        return final_confirmation_packet_ack_delay;
    }

    public void setFinal_confirmation_packet_ack_delay(long final_confirmation_packet_ack_delay) {
        this.final_confirmation_packet_ack_delay = final_confirmation_packet_ack_delay;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUser_agent() {
        return user_agent;
    }

    public void setUser_agent(String user_agent) {
        this.user_agent = user_agent;
    }

    public float getCell_lon() {
        return cell_lon;
    }

    public void setCell_lon(float cell_lon) {
        this.cell_lon = cell_lon;
    }

    public float getCell_lat() {
        return cell_lat;
    }

    public void setCell_lat(float cell_lat) {
        this.cell_lat = cell_lat;
    }

    public float getUrl_lon() {
        return url_lon;
    }

    public void setUrl_lon(float url_lon) {
        this.url_lon = url_lon;
    }

    public float getUrl_lat() {
        return url_lat;
    }

    public void setUrl_lat(float url_lat) {
        this.url_lat = url_lat;
    }

    public float getRange() {
        return range;
    }

    public void setRange(float range) {
        this.range = range;
    }
}
