/**
 * Created by Yun on 2016-01-21.
 */
public class Main {
    /**
     * @param args
     */
    public static void main(String[] args) {
        try {

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    System.out.println("System is about to exit");
                }
            });
            JettyStartup.getInstance().start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
