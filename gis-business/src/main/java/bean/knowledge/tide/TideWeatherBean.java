package bean.knowledge.tide;

/**
 * Created by xiexb on 2017/8/4.
 */
public class TideWeatherBean {
    private String stationNo;
    private String stationName;
    private String ew;
    private String ns;
    private String timeZone;
    private int daykey;
    private Integer weather;
    private double temp;
    private String winD;
    private String winS;
    private String weatherLable;

    public String getStationNo() {
        return stationNo;
    }

    public void setStationNo(String stationNo) {
        this.stationNo = stationNo;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getEw() {
        return ew;
    }

    public void setEw(String ew) {
        this.ew = ew;
    }

    public String getNs() {
        return ns;
    }

    public void setNs(String ns) {
        this.ns = ns;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public int getDaykey() {
        return daykey;
    }

    public void setDaykey(int daykey) {
        this.daykey = daykey;
    }

    public Integer getWeather() {
        return weather;
    }

    public void setWeather(Integer weather) {
        this.weather = weather;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public String getWinD() {
        return winD;
    }

    public void setWinD(String winD) {
        this.winD = winD;
    }

    public String getWinS() {
        return winS;
    }

    public void setWinS(String winS) {
        this.winS = winS;
    }

    public String getWeatherLable() {
        return weatherLable;
    }

    public void setWeatherLable(String weatherLable) {
        this.weatherLable = weatherLable;
    }
}
