<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@ include file="reference.jsp" %>
    <title></title>
    <script type="text/javascript">
        var dojoConfig = {
            parseOnLoad: true,
            packages: [{
                name: 'arcgisplugin',
                location:"/script/arcgis_js_api/plugin"
            },{
                name: 'gis',
                location:"/script/plugin"
            }]
        };
    </script>
    <link rel="stylesheet" type="text/css"
          href="<%=root%>script/arcgis_js_api/library/3.9/3.9/js/dojo/dijit/themes/tundra/tundra.css"/>
    <link rel="stylesheet" type="text/css" href="<%=root%>script/arcgis_js_api/library/3.9/3.9/js/esri/css/esri.css"/>
    <script type="text/javascript" src="<%=root%>script/arcgis_js_api/library/3.9/3.9/init.js"></script>
    <script type="text/javascript">
        // 功能插件
        var features = [];
        features.push("esri/map");
        features.push("esri/geometry/Point");
        features.push("esri/SpatialReference");
        features.push("esri/symbols/SimpleMarkerSymbol");
        features.push("esri/symbols/SimpleLineSymbol");
        features.push("esri/graphic");
        features.push("dijit/form/Button");
        features.push("dojo/_base/Color");
        features.push("arcgisplugin/ArcGisOpenStreetMapLayer");
        features.push("dojo/domReady!");

        require(features, function (Map, Point, SpatialReference, SimpleMarkerSymbol, SimpleLineSymbol, Graphic, Button, Color, ArcGisOpenStreetMapLayer) {
            var map = new Map("map", {
                logo: false
            });

            // 添加底图
            var layer = new ArcGisOpenStreetMapLayer();
            map.addLayer(layer);

            // 标记边框
            var outSymbol = new SimpleLineSymbol();
            outSymbol.setStyle(SimpleLineSymbol.STYLE_NULL);

            // 地图点击事件
            map.on("click", function (evt) {
                var obj = {};
                obj.wkid = map.spatialReference.wkid;
                obj.level = map.getLevel();
                obj.x = evt.mapPoint.x;
                obj.y = evt.mapPoint.y;
                dojo.byId("label").innerHTML = $.toJSON(obj);

                var point = new Point(evt.mapPoint.x, evt.mapPoint.y, new SpatialReference({
                    wkid: map.spatialReference.wkid
                }));

                var symbol = new SimpleMarkerSymbol();
                symbol.setOutline(outSymbol);
                symbol.setSize(14);
                symbol.setColor(new Color([255, 0, 0, 1]));

                var graphic = new Graphic(point, symbol);
                map.graphics.clear();
                map.graphics.add(graphic);
            });

            // 初始定位
            map.centerAndZoom(new Point({
                x: 11861464.307236375,
                y: 3447250.081128183,
                spatialReference: {
                    wkid: 102100
                }
            }), 14);

            // 点击定位
            var button = new Button({
                label: "Locate",
                onClick: function () {
                    var point = new Point(11857601.848650842, 3455027.5487561985, new SpatialReference({
                        wkid: map.spatialReference.wkid
                    }));

                    var symbol = new SimpleMarkerSymbol();
                    symbol.setOutline(outSymbol);
                    symbol.setSize(14);
                    symbol.setColor(new Color([255, 0, 0, 1]));

                    map.centerAndZoom(point, 15);

                    var graphic = new Graphic(point, symbol);
                    map.graphics.clear();
                    map.graphics.add(graphic);
                }
            }, "locateHome");
        });
    </script>
</head>
<body>
<div class="tundra">
    <div>ArcGisOpenStreetMapLayer:102100</div>
    <button id="locateHome" type="button"></button>
    <div id="map"></div>
</div>
<div id="label" style="position: fixed; bottom: 0px; right: 0px"></div>
</body>
</html>
