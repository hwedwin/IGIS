<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!-- Bootstrap Styles-->
<link href="/ma/assets/css/bootstrap.css" rel="stylesheet" />
<!-- FontAwesome Styles-->
<link href="/ma/assets/css/font-awesome.css" rel="stylesheet" />
<!-- Morris Chart Styles-->
<link href="/ma/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- Custom Styles-->
<link href="/ma/assets/css/custom-styles.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="/script/plugins/scrollbar/perfect-scrollbar-0.4.10.min.css">


<script src="/ma/assets/js/jquery-1.10.2.js"></script>
<!-- Bootstrap Js -->
<script src="/ma/assets/js/bootstrap.min.js"></script>
<!-- Metis Menu Js -->
<script src="/ma/assets/js/jquery.metisMenu.js"></script>
<!-- Morris Chart Js -->
<script src="/ma/assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="/ma/assets/js/morris/morris.js"></script>


<link rel="stylesheet" type="text/css" href="/script/base/artcloudui/skins/default/css/artcloudui.css">
<script type="text/javascript" src="/script/base/artcloudui/artcloudui.dialog.min.js"></script>
<script type="text/javascript" src="/script/plugins/scrollbar/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/script/plugins/scrollbar/perfect-scrollbar.js"></script>


<!-- Custom Js -->
<script src="/ma/assets/js/custom-scripts.js"></script>