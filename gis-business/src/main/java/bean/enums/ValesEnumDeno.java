package bean.enums;

/**
 * Created by Xiaobing on 2016/5/26.
 */
public enum ValesEnumDeno {
    APPLE(1), ORANGE(2);    //    调用构造函数来构造枚举项

    private int value = 0;

    private ValesEnumDeno(int value) {    //    必须是private的，否则编译错误
        this.value = value;
    }

    public static ValesEnumDeno valueOf(int value) {    //    手写的从int到enum的转换函数
        switch (value) {
            case 1:
                return APPLE;
            case 2:
                return ORANGE;
            default:
                return null;
        }
    }

    public int value() {
        return this.value;
    }


    /*System.out.println(ValesEnumDeno.APPLE.value());    //    1
    System.out.println(ValesEnumDeno.ORANGE.value());    //   2
    System.out.println(ValesEnumDeno.valueOf(1));        //  ValesEnumDeno.APPLE
    System.out.println(ValesEnumDeno.valueOf(2));*/
}
