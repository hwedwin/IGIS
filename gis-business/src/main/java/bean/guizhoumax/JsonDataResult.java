package bean.guizhoumax;

import java.util.List;

/**
 * Created by Xiaobing on 2016/5/9.
 */
public class JsonDataResult {
    private  boolean status;
    private String message;
    private List<HotCell> data;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<HotCell> getData() {
        return data;
    }

    public void setData(List<HotCell> data) {
        this.data = data;
    }
}
