package bean.common;

import bean.enums.ResType;
import com.google.common.base.Enums;

/**
 * Created by hupeng on 16/6/7.
 */
public class CellRefDimScene extends Cell {
    /**
     * 场景标识
     */
    private String scene_id;
    /**
     * 场景名称
     */
    private String scene_name;
    /**
     * 场景类型标识
     */
    private String scene_type_id;
    /**
     * 场景类型名称
     */
    private String scene_type_name;

    private String cell_type;

    public String getScene_id() {
        return scene_id;
    }

    public void setScene_id(String scene_id) {
        this.scene_id = scene_id;
    }

    public String getScene_name() {
        return scene_name;
    }

    public void setScene_name(String scene_name) {
        this.scene_name = scene_name;
    }

    public String getScene_type_id() {
        return scene_type_id;
    }

    public void setScene_type_id(String scene_type_id) {
        this.scene_type_id = scene_type_id;
    }

    public String getScene_type_name() {
        return scene_type_name;
    }

    public void setScene_type_name(String scene_type_name) {
        this.scene_type_name = scene_type_name;
    }

    public String getStrCellType() {
        return Enums.getField(getResType()).getName();
    }

    public void setStrCellType(String strCellType) {
        strCellType = strCellType.toUpperCase();
        switch (strCellType) {
            case "GSMCELL":
                setResType(ResType.GSMCELL);
                break;
            case "TDCELL":
                setResType(ResType.TDCELL);
                break;
            case "LTECELL":
                setResType(ResType.LTECELL);
                break;
            default:
                setResType(ResType.none);
                break;
        }
        this.cell_type = strCellType;
    }

    public String getCell_type() {
        return cell_type;
    }

    public CellRefDimScene() {
        this.setRadius(50);
        this.setAntbw(60);
    }
}
