package gis.business;

import bean.common.Legend;
import bean.common.LegendItem;
import gis.algorithm.ColorHelper;
import gis.algorithm.IColorReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Created by Xiaobing on 2016/8/27.
 */
public class TechnicalInveColor extends IColorReader {
    private static Logger logger = LoggerFactory.getLogger(TechnicalInveColor.class);
    private String tableName = "";
    public Color r = new Color(255, 0, 0, 125);// 红
    public Color g = new Color(0, 255, 0, 125);// 黄
    public Color b = new Color(0, 0, 255, 125);// 蓝

    int r1 = ColorHelper.getColorInt(r);
    int g1 = ColorHelper.getColorInt(g);
    int b1 = ColorHelper.getColorInt(b);


    private List<LegendItem> legendItems = new ArrayList<LegendItem>();// 图例
    private Legend legend = new Legend();

    public Legend getLegend() {
        return legend;
    }

    public void setLegend(Legend legend) {
        this.legend = legend;
    }

    @Override
    public Integer GetColor(Double vales) {
        int v = 0;
        /*if (tableName.equals("p_beijing_new")) {
            if (vales >= 100) {
                v = r1;
            } else if (vales < 100 && vales >= 40) {
                v = g1;
            } else {
                v = b1;
            }
        } else if (tableName.equals("p_shanghai_new")) {
            if (vales >= 230) {
                v = r1;
            } else if (vales < 230 && vales >= 90) {
                v = g1;
            } else {
                v = b1;
            }
        } else if (tableName.equals("p_tianjin_new")) {
            if (vales >= 15) {
                v = r1;
            } else if (vales < 15 && vales >= 8) {
                v = g1;
            } else {
                v = b1;
            }
        } else if (tableName.equals("p_chongqing_new")) {
            if (vales >= 40) {
                v = r1;
            } else if (vales < 40 && vales >= 25) {
                v = g1;
            } else {
                v = b1;
            }
        } else if (tableName.equals("p_heinan_new")) {
            if (vales >= 90) {
                v = r1;
            } else if (vales < 90 && vales >= 50) {
                v = g1;
            } else {
                v = b1;
            }
        } else if (tableName.equals("p_guangdong_new")) {
            if (vales >= 70) {
                v = r1;
            } else if (vales < 70 && vales >= 45) {
                v = g1;
            } else {
                v = b1;
            }
        } else if (tableName.equals("p_heilongjiang_new")) {
            if (vales >= 18) {
                v = r1;
            } else if (vales < 18 && vales >= 10) {
                v = g1;
            } else {
                v = b1;
            }
        } else if (tableName.equals("p_gansu_new")) {
            if (vales >= 100) {
                v = r1;
            } else if (vales < 100 && vales >= 50) {
                v = g1;
            } else {
                v = b1;
            }
        } else {*/
        if (vales >= 15) {
            v = r1;
        } else if (vales < 15 && vales >= 5) {
            v = g1;
        } else {
            v = b1;
        }
        //}
        //logger.debug(tableName + "  " + vales + "  " + v);
        return v;
    }

    public TechnicalInveColor() {
        legend.setName("");
        legendItems.add(CreateLegend(0, 0, 255, 125, b1, "0", "10"));// 蓝
        legendItems.add(CreateLegend(0, 255, 0, 125, g1, "10", "50"));// 黄
        legendItems.add(CreateLegend(255, 0, 0, 125, r1, "50", "MAX"));// 红
        legend.setLegendItems(legendItems);
    }

    public TechnicalInveColor(String tableName) {
        this.tableName = tableName;

        /*logger.debug("TechnicalInveColor" + tableName);
        if (tableName.equals("p_beijing_new")) {
            legend.setName("北京");
            legendItems.add(CreateLegend(0, 0, 255, 125, b1, "0", "40"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 125, g1, "40", "100"));// 黄
            legendItems.add(CreateLegend(255, 0, 0, 125, r1, "100", "MAX"));// 红
            legend.setLegendItems(legendItems);
        } else if (tableName.equals("p_shanghai_new")) {
            legend.setName("上海");
            legendItems.add(CreateLegend(0, 0, 255, 125, b1, "0", "90"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 125, g1, "90", "230"));// 黄
            legendItems.add(CreateLegend(255, 0, 0, 125, r1, "230", "MAX"));// 红
            legend.setLegendItems(legendItems);
        } else if (tableName.equals("p_tianjin_new")) {
            legend.setName("天津");
            legendItems.add(CreateLegend(0, 0, 255, 125, b1, "0", "8"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 125, g1, "8", "15"));// 黄
            legendItems.add(CreateLegend(255, 0, 0, 125, r1, "15", "MAX"));// 红
            legend.setLegendItems(legendItems);
        } else if (tableName.equals("p_chongqing_new")) {
            legend.setName("重庆");
            legendItems.add(CreateLegend(0, 0, 255, 125, b1, "0", "25"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 125, g1, "25", "40"));// 黄
            legendItems.add(CreateLegend(255, 0, 0, 125, r1, "40", "MAX"));// 红
            legend.setLegendItems(legendItems);
        } else if (tableName.equals("p_heinan_new")) {
            legend.setName("河南");
            legendItems.add(CreateLegend(0, 0, 255, 125, b1, "0", "50"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 125, g1, "50", "90"));// 黄
            legendItems.add(CreateLegend(255, 0, 0, 125, r1, "90", "MAX"));// 红
            legend.setLegendItems(legendItems);
        } else if (tableName.equals("p_guangdong_new")) {
            legend.setName("广东");
            legendItems.add(CreateLegend(0, 0, 255, 125, b1, "0", "45"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 125, g1, "45", "70"));// 黄
            legendItems.add(CreateLegend(255, 0, 0, 125, r1, "70", "MAX"));// 红
            legend.setLegendItems(legendItems);
        } else if (tableName.equals("p_heilongjiang_new")) {
            legend.setName("黑龙江");
            legendItems.add(CreateLegend(0, 0, 255, 125, b1, "0", "10"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 125, g1, "10", "18"));// 黄
            legendItems.add(CreateLegend(255, 0, 0, 125, r1, "18", "MAX"));// 红
            legend.setLegendItems(legendItems);
        } else if (tableName.equals("p_gansu_new")) {
            legend.setName("甘肃");
            legendItems.add(CreateLegend(0, 0, 255, 125, b1, "0", "50"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 125, g1, "50", "100"));// 黄
            legendItems.add(CreateLegend(255, 0, 0, 125, r1, "100", "MAX"));// 红
            legend.setLegendItems(legendItems);
        } else {*/
        legend.setName("");
        legendItems.add(CreateLegend(0, 0, 255, 125, b1, "0", "5"));// 蓝
        legendItems.add(CreateLegend(0, 255, 0, 125, g1, "5", "15"));// 黄
        legendItems.add(CreateLegend(255, 0, 0, 125, r1, "15", "MAX"));// 红
        legend.setLegendItems(legendItems);
        //}

    }

    /**
     * @param r        红
     * @param g        黄
     * @param b        蓝
     * @param a        透明度
     * @param colorInt 颜色值
     * @param valueMin 指标最大值
     * @param valueMax 指标最小值
     * @return
     */
    private LegendItem CreateLegend(int r, int g, int b, int a, int colorInt, String valueMin, String valueMax) {
        LegendItem legend = new LegendItem();
        legend.setValueMin(valueMin);
        legend.setValueMax(valueMax);
        legend.setR(r);
        legend.setG(g);
        legend.setB(b);
        legend.setA(a);
        legend.setColorInt(colorInt);
        return legend;
    }

}
