/**
 * 
 */

/**
 * @author zhangyu
 * 
 */
public interface Startup {
	/**
	 * 
	 * @throws Exception
	 */
	public void init() throws Exception;

	/**
	 * 
	 * @throws Exception
	 */
	public void start() throws Exception;

	/**
	 * 
	 * @throws Exception
	 */
	public void stop() throws Exception;
}
