import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Yun on 2016-01-21.
 */
public class JettyStartup implements Startup {

    private static Logger logger = LoggerFactory.getLogger(JettyStartup.class);
    private static Startup context = null;
    private int port = 6001;
    private String contextPath = "/";
    private String resourceBase = "src/main/webapp";

    /**
     *
     * @return
     */
    
    public static synchronized Startup getInstance() {
        if (context == null) {
            context = new JettyStartup();
        }
        return context;
    }

    /**
     * @throws Exception
     */
    @Override
    public void init() throws Exception {

    }

    /**
     * @throws Exception
     */
    @Override
    public void start() throws Exception {
        Server server = new Server(port);
        server.setStopAtShutdown(true);
        server.setAttribute("org.eclipse.jetty.server.Request.maxFormContentSize", -1);

        WebAppContext context = new WebAppContext();
        context.setContextPath(contextPath);
        context.setResourceBase(resourceBase);
        context.setConfigurationDiscovered(true);
        context.setParentLoaderPriority(true);
        context.setDefaultsDescriptor("/webdefault.xml");
        server.setHandler(context);

        server.start();

        logger.info("Jetty path:{}", contextPath);
        logger.info("Jetty port:{}", port);
        server.join();
    }

    /**
     * @throws Exception
     */
    @Override
    public void stop() throws Exception {

    }
}
