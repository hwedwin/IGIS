package bean.common;

/**
 * 地市信息
 *
 * @author Xiaobing
 */
public class Region {
    private String id;
    private String name;
    private int type;
    private String ZnChar;

    /**
     * ID
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * ID
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 名称
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * 名称
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 类型  9000 省份    9001 地市   9003 区县
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * 类型  9000 省份    9001 地市   9003 区县
     *
     * @param type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * 首字母 第一个英文字母
     *
     * @return
     */
    public String getZnChar() {
        return ZnChar;
    }

    /**
     * 首字母 第一个英文字母
     *
     * @param znChar
     */
    public void setZnChar(String znChar) {
        ZnChar = znChar;
    }

    @Override
    public String toString() {
        return id + "_" + type;
    }
}
