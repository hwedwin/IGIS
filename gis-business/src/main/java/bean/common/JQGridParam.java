package bean.common;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Xiaobing on 2016/4/19.
 * 接受jqgrid的传入参数
 */
public class JQGridParam {
    /**
     * 每页显示条数
     */
    private int pageSize = 10;
    /**
     * 当前页索引
     */
    private int pageIndex=1;
    /**
     * 排序字段
     */
    private String sordField;
    /**
     * 排序类型
     */
    private String sordType;

    public JQGridParam() {
    }

    public JQGridParam(HttpServletRequest request) {
        if (request.getParameterMap().containsKey("rows")) {
            pageSize = Integer.parseInt(request.getParameter("rows"));
        }
        if (request.getParameterMap().containsKey("page")) {
            pageIndex = Integer.parseInt(request.getParameter("page"));
        }
        if (request.getParameterMap().containsKey("sidx")) {
            sordField = request.getParameter("sidx");
        }
        if (request.getParameterMap().containsKey("sord")) {
            sordType =request.getParameter("sord");
        }
    }

    /**
     * 每页显示条数
     *
     * @return
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * \每页显示条数
     *
     * @param pageSize
     */
    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    /**
     * 当前页索引
     *
     * @return
     */
    public int getPageIndex() {
        return pageIndex;
    }

    /**
     * 当前页索引
     *
     * @param pageIndex
     */
    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    /**
     * 排序字段
     *
     * @return
     */
    public String getSordField() {
        return sordField;
    }

    /**
     * 排序字段
     *
     * @param sordField
     */
    public void setSordField(String sordField) {
        this.sordField = sordField;
    }

    /**
     * 排序类型
     *
     * @return
     */
    public String getSordType() {
        return sordType;
    }

    /**
     * 排序类型
     *
     * @param sordType
     */
    public void setSordType(String sordType) {
        this.sordType = sordType;
    }


}
