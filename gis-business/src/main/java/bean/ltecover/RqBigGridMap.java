package bean.ltecover;

import gis.algorithm.grid.RqGridMap;

import java.awt.*;

/**
 * Created by Xiaobing on 2016/5/19.
 */
public class RqBigGridMap extends RqGridMap {
    /**
     * 质优MR点数量
     */
    private int goodRsrpCnt;
    /**
     * 质差MR点数量
     */
    private int badRsrpCnt;
    /**
     * 所有MR点数量
     */
    private int totalRsrpCnt;
    /**
     * 所有栅格数量
     */
    private int allGridCnt;
    /**
     * 0<=weak_rat<20 的栅格数量
     */
    private int blueGridCnt;
    /**
     * 20<=weak_rat<50 的栅格数量
     */
    private int yellowGridCnt;
    /**
     * 50<=weak_rat 的栅格数量
     */
    private int redGridCnt;

    /**
     * 质优MR点数量
     * @return
     */
    public int getGoodRsrpCnt() {
        return goodRsrpCnt;
    }

    /**
     * 质差MR点数量
     * @param goodRsrpCnt
     */
    public void setGoodRsrpCnt(int goodRsrpCnt) {
        this.goodRsrpCnt = goodRsrpCnt;
    }

    /**
     * 质差MR点数量
     * @return
     */
    public int getBadRsrpCnt() {
        return badRsrpCnt;
    }

    /**
     * 质差MR点数量
     * @param badRsrpCnt
     */
    public void setBadRsrpCnt(int badRsrpCnt) {
        this.badRsrpCnt = badRsrpCnt;
    }

    /**
     *  所有MR点数量
     * @return
     */
    public int getTotalRsrpCnt() {
        return totalRsrpCnt;
    }

    /**
     *  所有MR点数量
     * @param totalRsrpCnt
     */
    public void setTotalRsrpCnt(int totalRsrpCnt) {
        this.totalRsrpCnt = totalRsrpCnt;
    }

    /**
     * 所有栅格数量
     * @return
     */
    public int getAllGridCnt() {
        return allGridCnt;
    }

    /**
     * 所有栅格数量
     * @param allGridCnt
     */
    public void setAllGridCnt(int allGridCnt) {
        this.allGridCnt = allGridCnt;
    }

    /**
     * 0<=weak_rat<20 的栅格数量
     * @return
     */
    public int getBlueGridCnt() {
        return blueGridCnt;
    }

    /**
     * 0<=weak_rat<20 的栅格数量
     * @param blueGridCnt
     */
    public void setBlueGridCnt(int blueGridCnt) {
        this.blueGridCnt = blueGridCnt;
    }

    /**
     * 20<=weak_rat<50 的栅格数量
     * @return
     */
    public int getYellowGridCnt() {
        return yellowGridCnt;
    }

    /**
     * 20<=weak_rat<50 的栅格数量
     * @param yellowGridCnt
     */
    public void setYellowGridCnt(int yellowGridCnt) {
        this.yellowGridCnt = yellowGridCnt;
    }

    /**
     * 50<=weak_rat 的栅格数量
     * @return
     */
    public int getRedGridCnt() {
        return redGridCnt;
    }

    /**
     * 50<=weak_rat 的栅格数量
     * @param redGridCnt
     */
    public void setRedGridCnt(int redGridCnt) {
        this.redGridCnt = redGridCnt;
    }
}
