package gis.common;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.Module;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Xiaobing on 2016/5/9.
 */
public class JsonUtils {
    private static org.slf4j.Logger logger = LoggerFactory.getLogger(JsonUtils.class);
    private static ObjectMapper objectMapper = new ObjectMapper();
    /**
     * @param json
     * @param clazz
     * @param modules
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    public static <T> T deserializeJson(String json, Class<T> clazz, Module... modules) throws JsonParseException, JsonMappingException,
            IOException {
        ObjectMapper objMapper = new ObjectMapper();

        if (modules != null) {
            for (int i = 0; i < modules.length; i++) {
                objMapper.registerModule(modules[i]);
            }
        }

        return objMapper.readValue(json, clazz);
    }

    /**
     * @param json
     * @param clazz
     * @param modules
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    public static <T> List<T> deserializeJson(String json, TypeReference clazz, Module... modules) throws JsonParseException, JsonMappingException,
            IOException {
        ObjectMapper objMapper = new ObjectMapper();

        if (modules != null) {
            for (int i = 0; i < modules.length; i++) {
                objMapper.registerModule(modules[i]);
            }
        }

        return objMapper.readValue(json, clazz);
    }

    /**
     * @param obj
     * @param modules
     * @return
     * @throws JsonGenerationException
     * @throws JsonMappingException
     * @throws IOException
     */
    public static <T> String serializeJson(T obj, Module... modules)
            throws JsonGenerationException, JsonMappingException, IOException {
        ObjectMapper objMapper = new ObjectMapper();

        StringWriter writer = new StringWriter();

        if (modules != null) {
            for (int i = 0; i < modules.length; i++) {
                objMapper.registerModule(modules[i]);
            }
        }

        objMapper.writeValue(writer, obj);
        String json = writer.toString();

        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    public static JavaType getCollectionType(ObjectMapper mapper, Class<?> collectionClass, Class<?>... elementClasses) {
        return mapper.getTypeFactory().constructParametricType(collectionClass, elementClasses);
    }

    /**
     * 将对象序列化为JSON字符串
     *
     * @param object
     * @return JSON字符串
     */
    public static String serialize(Object object) {
        Writer write = new StringWriter();
        try {
            objectMapper.writeValue(write, object);
        } catch (JsonGenerationException e) {
            logger.error("JsonGenerationException when serialize object to json", e);
        } catch (JsonMappingException e) {
            logger.error("JsonMappingException when serialize object to json", e);
        } catch (IOException e) {
            logger.error("IOException when serialize object to json", e);
        }
        return write.toString();
    }

}
