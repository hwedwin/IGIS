/**
 * Created by hupeng on 16/6/13.
 */
Date.prototype.format = function (format) {
    var date = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S+": this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1
                ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
        }
    }
    return format;
};


function ScenicSchool() {
    //事件相关
    var map_mouse_move = null;
    var map_mouse_out = null;
    var grid_high_layer = gis_core.addGraphicsLayerWithIndex("_scenic_spot_grid_high_layer", "高亮图层", 5);
    var selected_layer = gis_core.addGraphicsLayerWithIndex("_scenic_spot_selected_layer", "选中图层", 5);
    var tempDrawLayer = null;//框选结果区域
    var wmsLayer = null; //渲染图层
    var rings = null;//框选区域坐标点
    var delta_temp = null; //底图临时数据
    var scenic_school_list = null;
    var scenic_school_filter_list = null;
    var curpage = 1;
    var now = new Date();
    var time = (new Date(now.setMonth(now.getMonth() - 0))).format('yyyyMM');//查询日期
    var school_id = 0;
    var grid_id = '';

    //初始化数据
    var initData = function () {
        loadScenicSchoolList();
        createScenicListHtml();
        /*var url = "/dynamicmap/scenicschool/1111/1111";
         loadWmsLayer(url);*/
    };


    /**
     * 加载高校列表
     * */
    var loadScenicSchoolList = function () {
        $.ajax({
            url: "/getschoolsceniclist?r=" + Math.random(),
            type: "GET",
            async: false,
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    scenic_school_list = data;
                    scenic_school_filter_list = data;
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    //添加渲染图层
    var loadWmsLayer = function (url) {
        if (wmsLayer != null || wmsLayer != undefined) {
            gis_core.removeLayer("scenicschoolWmsLayer")
        }
        if (url == null || url == undefined) {
            return;
        }
        wmsLayer = new esri.layers.WMSLayer(url, {
            resourceInfo: {
                extent: new esri.geometry.Extent(73.2672757, 18.37574, -109.66552734375, 133.29657, {
                    wkid: 4326
                }),
                layerInfos: []
            },
            visibleLayers: ['lteconvergrid']
        });
        wmsLayer.id = "scenicschoolWmsLayer";
        gis_core.insertLayer("scenicschoolWmsLayer", "网格覆盖", 2, wmsLayer);
    };

    //地图改变事件
    var extentchange = function (delta) {
        delta_temp = delta;
    };

    //绘制多边形
    var drawPolygonGraphic = function (rings) {
        if (tempDrawLayer) {
            tempDrawLayer.clear()
        }
        var polygon = new esri.geometry.Polygon(new esri.SpatialReference({wkid: 4326}));
        //添加多边形的各个角的顶点坐标，注意：首尾要链接，也就是说，第一个点和最后一个点要一致
        polygon.addRing(rings);
        var symbol = new esri.symbol.SimpleFillSymbol().setColor(new esri.Color([180, 168, 192, 0.2])).setOutline(new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new esri.Color([200, 112, 181, 0.9]), 1));
        var graphic = new esri.Graphic(polygon, symbol);
        tempDrawLayer.add(graphic);
        //dojo.connect(tempDrawLayer, "onClick", capitalClick);
        offsetMainLayer(rings);
    };

    //偏移底图
    var offsetMainLayer = function (rings) {
        var xmax;
        var ymax;
        var xmin;
        var ymin;
        for (var i = 0, len = rings.length; i < len; i++) {
            var item = rings[i];
            if (!xmax || xmax < item[0]) {
                xmax = item[0];
            }

            if (!ymax || ymax < item[1]) {
                ymax = item[1];
            }

            if (!xmin || xmin > item[0]) {
                xmin = item[0];
            }

            if (!ymin || ymin > item[1]) {
                ymin = item[1];
            }
        }

        if (xmax && ymax && xmin && ymin) {
            gis_common.setExtent(xmax + 0.005, ymax + 0.005, xmin - 0.005, ymin - 0.005);
        }
    };

    var createScenicListHtml = function () {
        var html = [];
        //var timeStr = time.substr(0, 4) + '-' + time.substr(4, 2);
        //默认时间
        var timeStr = Date.getDateOfPreMonth(new Date()).format("yyyy-MM");

        html.push('<div class="form-horizontal" style="max-width:250px;">');
        html.push(' <div class="form-group"><label class="col-sm-3 control-label">时间:</label>');
        html.push('    <div class="col-sm-9">');
        html.push('        <input id="_sport_trace_begin_time" value="' + timeStr + '" type="text" class="form-control"');
        html.push('               onfocus="WdatePicker({lang:\'zh-cn\',dateFmt:\'yyyy-MM\'});">');
        html.push('    </div>');
        html.push(' </div>');
        html.push(' <div class="form-group"><label class="col-lg-3 control-label">名称:</label>');
        html.push('    <div class="col-lg-9">');
        html.push('     <div class="input-group">');
        html.push('         <input type="text" class="form-control _scenic_name_txt">');
        html.push('            <span class="input-group-btn">');
        html.push('                <button class="btn btn-default _scenic_name_query" type="button">');
        html.push('                    Go');
        html.push('                </button>');
        html.push('        </span>');
        html.push('     </div>');
        html.push('    </div>');
        html.push(' </div>');
        html.push(' <div style="max-height:150px;overflow-y:auto;"><div class="list-group _scenic_spot_list">');
        html.push(' </div></div>');
        html.push(' <div class="form-group"><label class="col-lg-3 control-label"></label>');
        html.push('    <div class="col-lg-9">');
        html.push('         <ul class="pagination _scenic_spot_page">');
        html.push('             ');
        html.push('         </ul>');
        html.push('    </div>');
        html.push(' </div>');
        html.push(' <div class="form-group"><label class="col-sm-3 control-label"></label>');
        html.push('    <div class="col-sm-9">');
        html.push('         <button id="_scenic_school_query_kpi" class="btn btn-primary">查询高校指标</button>');
        html.push('    </div>');
        html.push(' </div>');
        html.push('</div>');
        var divContent = $(html.join(''));
        var $btn = $('._scenic_name_query', divContent);
        var $txt = $('._scenic_name_txt', divContent);

        divContent.find('#_sport_trace_begin_time').blur(function () {
            var t = $(this).val();
            if (t) {
                time = t.replace('-', '');
            }
        });

        $btn.click(function () {
            var v = $txt.val();
            if (v) {
                scenic_school_filter_list = [];
                for (var i = 0, len = scenic_school_list.length; i < len; i++) {
                    if (scenic_school_list[i]['school_name'].indexOf(v) > -1) {
                        scenic_school_filter_list.push(scenic_school_list[i]);
                    }
                }
            } else {
                scenic_school_filter_list = scenic_school_list;
            }
            fillPageContent(1);
            fillScenicSpot(1);
        });

        $('._scenic_spot_page', divContent).on('click', '._scenic_spot_page_item', function () {
            var $ts = $(this);
            var page = $ts.attr('page');
            fillPageContent(page);
            fillScenicSpot(page);
        });

        $('._scenic_spot_list', divContent).on('click', '._scenic_spot_item', function () {
            var $ts = $(this);
            $ts.addClass('active').siblings().removeClass('active');
            var rings = $ts.data('rings');
            var pid = $ts.attr('pid');
            drawPolygonGraphic(JSON.parse(rings));
            //loadSchoolKpi(pid);

        });

        divContent.find('#_scenic_school_query_kpi').click(function () {
            var $ac = $('._scenic_spot_item.active');
            if ($ac.length == 0) {
                gis_common.showError('未选择高校');
            } else {
                var pid = $ac.attr('pid');
                school_id = pid;
                loadSchoolKpi(pid);
            }
        });

        gis_layout.createLeftWidget('_scenic_spot_list', '高校', divContent, true);

        fillPageContent(1);
        fillScenicSpot(1);
    };

    var fillPageContent = function (page) {
        var $page = $('._scenic_spot_page');
        $page.empty();
        if (scenic_school_filter_list) {
            var count = scenic_school_filter_list.length;
            var total = count / 10;
            if (total <= 1) {
                $page.hide();
                return;
            } else {
                $page.show();
            }
            var pages = [];
            if (page == 1 || page == 2) {
                pages.push(1, 2, 3, 4, 5);
            } else if (page == total) {
                pages.push(page - 4, page - 3, page - 2, page - 1, page);
            } else if (page == total - 1) {
                pages.push(page - 3, page - 2, page - 1, page, page * 1 + 1);
            }
            else {
                pages.push(page - 2, page - 1, page, page * 1 + 1, page * 1 + 2);
            }
            for (var i = 0, len = pages.length; i < len; i++) {
                if (pages[i] > 0 && pages[i] <= total) {
                    var $li = $('<li><a href="javascript:void(0);">' + pages[i] + '</a></li>')
                        .addClass('_scenic_spot_page_item').attr('page', pages[i]).appendTo($page);
                    if (pages[i] == page) {
                        $li.addClass('active disabled');
                    }
                }
            }

        } else {
            $page.hide();
        }
    };

    var fillScenicSpot = function (page) {
        curpage = page;
        var $spot = $('._scenic_spot_list');
        $spot.empty();
        if (scenic_school_filter_list) {
            var start = (curpage - 1) * 10;
            var end = curpage * 10;
            var len = scenic_school_filter_list.length;
            if (end > len) {
                end = len;
            }
            if (start > len) {
                start = len;
            }

            for (var i = start; i < end; i++) {
                var $button = $('<button type="button" class="list-group-item"><span>' + scenic_school_filter_list[i]["school_name"] + '</span></button>');
                $button.attr('pid', scenic_school_filter_list[i]['school_id']).data('rings', scenic_school_filter_list[i]['bord']).addClass('_scenic_spot_item');
                $spot.append($button);
            }
        }
    };

    /**
     * 加载场景指标详细信息
     * */
    var loadSchoolKpi = function (schoolId) {
        if (gis_common.stringIsNullOrWhiteSpace(schoolId) || gis_common.stringIsNullOrWhiteSpace(time)) {
            gis_common.showMessage("未选择高校和日期");
            return;
        }

        var url = "/dynamicmap/scenicschoolgriddata/" + schoolId + "/" + time;
        loadWmsLayer(url);
        loadLegend("weak_rat");

        $.ajax({
            url: "/getschoolscenicdata?r=" + Math.random(),
            data: {
                time: time,
                id: schoolId
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    createKpiDetailHtml(data, 1);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };


    var loadLegend = function (key) {
        $.ajax({
            url: "/getltecoverlegend?time=" + Math.random(),
            data: {
                kpi: key,
                wmsLayer_type: "ltegridcover"
            },
            type: "POST",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    initLegend(data.tag);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    //加载图例
    var initLegend = function (legends) {
        if (legends == null || legends == undefined || legends.length == 0) {
            gis_common.showMessage("初始化图例失败");
            return;
        }
        var content = "";
        for (var i = 0; i < legends.legendItems.length; i++) {
            var legend = legends.legendItems[i];
            var sRgb = "RGB(" + legend.r + ", " + legend.g + ", " + legend.b + ")"; //sRgb.colorHex()
            content += "<div class=\"legend-box\" style=\"background:rgba(" + legend.r + ", " + legend.g + ", " + legend.b + ",0.5);color:#fff;\"> " + legend.valueMin + "-" + legend.valueMax + "</div>";
        }
        var div = $(content);
        gis_layout.createLegend("scenic_grid_legend", legends.name + "图例", div);
    };

    /**
     * 创建指标明细html
     * */
    var createKpiDetailHtml = function (data, type) {
        var kpi = [];

        if (data) {

            if (data.good_rsrp_cnt) {
                kpi.push({"id": "达标RSRP次数(次)", "name": data.good_rsrp_cnt});
            }

            if (data.bad_rsrp_cnt) {
                kpi.push({"id": "未达标RSRP次数(次)", "name": data.bad_rsrp_cnt});
            }

            if (data.total_rsrp_cnt) {
                kpi.push({"id": "总RSRP次数(次)", "name": data.total_rsrp_cnt});
            }

            if (data.weak_rat) {
                kpi.push({"id": "栅格弱覆盖比例", "name": (data.weak_rat * 100).toFixed(2) + '%'});
            }

            if (data.lte_flow) {
                kpi.push({"id": "日均4G流量(GB)", "name": (data.lte_flow / (1024 * 1024 * 1024)).toFixed(4)});
            }

            if (data.lte_user_cnt) {
                kpi.push({"id": "日均4G用户数(个)", "name": data.lte_user_cnt});
            }

            if (data.gsm_flow) {
                kpi.push({"id": "日均2G流量(GB)", "name": (data.gsm_flow / (1024 * 1024 * 1024)).toFixed(4)});
            }

            if (data.gsm_user_cnt) {
                kpi.push({"id": "日均2G栅格用户数(个)", "name": data.gsm_user_cnt});
            }

            if (data.page_resp_succ_cnt) {
                kpi.push({"id": "4G页面响应成功次数(个)", "name": data.page_resp_succ_cnt});
            }

            if (data.page_req_cnt) {
                kpi.push({"id": "4G页面请求次数(个)", "name": data.page_req_cnt});
            }

            if (data.page_resp_succ_rat) {
                kpi.push({"id": "4G页面响应成功率", "name": (data.page_resp_succ_rat * 100).toFixed(2) + '%'});
            }

            if (data.page_resp_succ_tot_delay) {
                kpi.push({"id": "4G页面响应成功总时延(ms)", "name": data.page_resp_succ_tot_delay});
            }

            if (data.page_resp_dur) {
                kpi.push({"id": "4G页面响应时长(ms)", "name": data.page_resp_dur});
            }

            if (data.page_display_succ_cnt) {
                kpi.push({"id": "4G页面显示成功次数", "name": data.page_display_succ_cnt});
            }

            if (data.page_display_succ_rat) {
                kpi.push({"id": "4G页面显示成功率", "name": (data.page_display_succ_rat * 100).toFixed(2) + '%'});
            }

            if (data.page_display_succ_tot_delay) {
                kpi.push({"id": "4G页面显示成功总时延(ms)", "name": data.page_display_succ_tot_delay});
            }

            if (data.page_display_dur) {
                kpi.push({"id": "4G页面显示时长(ms)", "name": data.page_display_dur});
            }

            if (data.video_play_succ_cnt) {
                kpi.push({"id": "4G视频播放成功次数", "name": data.video_play_succ_cnt});
            }

            if (data.video_play_req_cnt) {
                kpi.push({"id": "4G视频播放请求次数", "name": data.video_play_req_cnt});
            }

            if (data.video_play_succ_rat) {
                kpi.push({"id": "4G视频播放成功率", "name": (data.video_play_succ_rat * 100).toFixed(2) + '%'});
            }

            if (data.video_dl_flow) {
                kpi.push({"id": "4G视频下行流量(GB)", "name": (data.video_dl_flow / (1024 * 1024 * 1024)).toFixed(4)});
            }

            if (data.video_dl_dur) {
                kpi.push({"id": "4G视频下行传输时长(ms)", "name": data.video_dl_dur});
            }

            if (data.video_dl_rate) {
                kpi.push({"id": "4G视频下载速率(Kbps)", "name": data.video_dl_rate});
            }

            if (data.im_load_succ_cnt) {
                kpi.push({"id": "4Gim业务登陆成功次数(次)", "name": data.im_load_succ_cnt});
            }

            if (data.im_load_req_cnt) {
                kpi.push({"id": "4Gim业务登陆请求次数(次)", "name": data.im_load_req_cnt});
            }

            if (data.im_load_succ_rat) {
                kpi.push({"id": "4Gim业务登陆成功率", "name": (data.im_load_succ_rat * 100).toFixed(2) + '%'});
            }

            if (data.app_dl_succ_cnt) {
                kpi.push({"id": "4G应用下载成功次数(次)", "name": data.app_dl_succ_cnt});
            }

            if (data.app_dl_req_cnt) {
                kpi.push({"id": "4G应用下载请求次数(次)", "name": data.app_dl_req_cnt});
            }

            if (data.app_dl_succ_rat) {
                kpi.push({"id": "4G应用下载成功率", "name": (data.app_dl_succ_rat * 100).toFixed(2) + '%'});
            }

            if (data.app_dl_flow) {
                kpi.push({"id": "4G应用下行流量(GB)", "name": (data.app_dl_flow / (1024 * 1024 * 1024)).toFixed(4)});
            }

            if (data.app_dl_dur) {
                kpi.push({"id": "4G应用下行传输时长(ms)", "name": data.app_dl_dur});
            }

            if (data.app_dl_rate) {
                kpi.push({"id": "4G应用下载速率(Kbps)", "name": data.app_dl_rate});
            }
        }

        var div = gis_common.createKeyValueList(kpi);
        if (type == 1) {
            gis_layout.createRightWidget("_scenic_school_kpi", "高校指标", div);
            gis_layout.removeRightWidget("_scenic_school_grid_kpi");
        } else {
            gis_layout.createRightWidget("_scenic_school_grid_kpi", "高校栅格指标", div);
        }

    };

    var loadGridKpi = function (evt) {
        var lon = evt.mapPoint.x;
        var lat = evt.mapPoint.y;
        var gridId = getId(lon) + getId(lat);

        if (gis_common.stringIsNullOrWhiteSpace(school_id) || gis_common.stringIsNullOrWhiteSpace(time)) {
            //gis_common.showMessage("未选择高校和日期");
            return;
        }

        selected_layer.clear();
        var point_xmin = getFiexedId(lon);
        var point_ymin = getFiexedId(lat);
        var rings = [];
        var x = parseFloat(point_xmin);
        var y = parseFloat(point_ymin);
        rings.push([x, y]);
        rings.push([x + 0.0005, y]);
        rings.push([x + 0.0005, y + 0.0005]);
        rings.push([x, y + 0.0005]);
        rings.push([x, y]);
        var polygon = new esri.geometry.Polygon(gis_core.map.spatialReference);
        polygon.addRing(rings);
        var symbol = new esri.symbol.SimpleLineSymbol().setColor(new esri.Color([255, 0, 0])).setWidth(2);
        var graphic = new esri.Graphic(polygon, symbol);
        selected_layer.add(graphic);

        $.ajax({
            url: "/getscenicgriddata?r=" + Math.random(),
            data: {
                time: time,
                id: school_id,
                grid: gridId,
                type: 2
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    createKpiDetailHtml(data, 2);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };


    //获取栅格瓦块
    var getId = function (l) {
        var va = 0;
        l = l + 0.000001;
        var ls = (l + "").split(".");
        var l_4 = parseInt(ls[1].substring(3, 4));
        if (l_4 > 4) {
            va = ls[0] + "" + ls[1].substring(0, 3) + "5";
        } else {
            va = ls[0] + "" + ls[1].substring(0, 3) + "0";
        }
        return va;
    };

    //高亮栅格块
    var createHighGrid = function (lon, lat) {
        if (delta_temp != null) {
            var xmax = delta_temp.extent.xmax;
            var xmin = delta_temp.extent.xmin;
            var ymax = delta_temp.extent.ymax;
            var ymin = delta_temp.extent.ymin;
            var p1 = GPS.mercator_encrypt(ymin, xmin);
            var p2 = GPS.mercator_encrypt(ymax, xmax);
            var w = p2.lon - p1.lon;
            var h = p2.lat - p1.lat;
            var count = w * h / (55 * 55);
            if (count <= 4000) {
                var point_xmin = getFiexedId(lon);
                var point_ymin = getFiexedId(lat);
                //var _id = (point_xmin + "").replace(".", "").substring(0, 7) + (point_ymin + "").replace(".", "").substring(0, 6);
                var _id = point_xmin.replace(".", "") + point_ymin.replace(".", "");
                if (grid_id != _id) {
                    grid_high_layer.clear();
                    grid_id = _id;
                    var rings = [];
                    var x = parseFloat(point_xmin);
                    var y = parseFloat(point_ymin);
                    rings.push([x, y]);
                    rings.push([x + 0.0005, y]);
                    rings.push([x + 0.0005, y + 0.0005]);
                    rings.push([x, y + 0.0005]);
                    rings.push([x, y]);
                    var polygon = new esri.geometry.Polygon(gis_core.map.spatialReference);
                    polygon.addRing(rings);
                    var symbol = new esri.symbol.SimpleLineSymbol().setColor(new esri.Color([255, 0, 0])).setWidth(2);
                    var graphic = new esri.Graphic(polygon, symbol);
                    grid_high_layer.add(graphic);
                }
            } else {
                grid_high_layer.clear();
                grid_id = null;
            }
        }
    };

    //获取栅格瓦块
    var getFiexedId = function (l) {
        var va = 0;
        l = l + 0.000001;
        var ls = (l + "").split(".");
        var l_4 = parseInt(ls[1].substring(3, 4));
        if (l_4 > 4) {
            va = ls[0] + "." + ls[1].substring(0, 3) + "5";
        } else {
            va = ls[0] + "." + ls[1].substring(0, 3) + "0";
        }
        return va;
    };

    /**
     * 初始化
     */
    this.init = function () {
        //加载库文件
        dojo.require("esri.layers.WMSLayerInfo");
        dojo.require("esri.layers.WMSLayer");
        tempDrawLayer = gis_core.addGraphicsLayerWithIndex('temp_draw_layer', '区域框选', 2);
        gis_core.addExtentChange("ScenicSchool", extentchange);

        gis_core.addMapClick("scenicschool_map_click", function (evt) {
            loadGridKpi(evt);
        });

        //注册事件
        if (map_mouse_move != null) {
            dojo.disconnect(map_mouse_move);
        }
        map_mouse_move = gis_core.map.on("mouse-move", function (delta) {
            //console.log(delta);
            createHighGrid(delta.mapPoint.x, delta.mapPoint.y);
        });

        if (map_mouse_out != null) {
            dojo.disconnect(map_mouse_out);
        }
        map_mouse_out = gis_core.map.on("mouse-out", function (delta) {
            grid_high_layer.clear();
        });

        //注册模块
        gis_core.addPlugin(this);
        gis_core.pluginDestroyExceptId(this.title.id);
        initData();
    };
    /**
     * 销毁
     */
    this.destroy = function () {
        gis_core.removeExtentChange("ScenicSchool");
        if (tempDrawLayer) {
            tempDrawLayer.clear();
        }

        if (wmsLayer != null || wmsLayer != undefined) {
            gis_core.removeLayer("scenicschoolWmsLayer");
        }

        if (grid_high_layer) {
            grid_high_layer.clear();
        }

        if (selected_layer) {
            selected_layer.clear();
        }

        dojo.disconnect(map_mouse_move);
        dojo.disconnect(map_mouse_out);

        rings = null;//框选区域坐标点
        delta_temp = null; //底图临时数据
        scenic_school_list = null;
        scenic_school_filter_list = null;
        curpage = 1;
        gis_core.removeClick("scenicschool_map_click");
        //now = null;
        //time = null;
        gis_layout.removeLeftWidget('_scenic_spot_list');
        gis_layout.removeRightWidget("_scenic_school_kpi");
        gis_layout.removeRightWidget("_scenic_school_grid_kpi");
        //scenicSchool = null;
    };
    /**
     * 模块信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "01_02_00",
        name: "高校栅格分析"
    };
}

//功能类 全局类
var scenicSchool = null;


function initScenicSchool() {
    if (scenicSchool == null) {
        scenicSchool = new ScenicSchool();
    }
    scenicSchool.init();
}

function scenicSchool_Init() {
    initScenicSchool();
    gis_core.pluginDestroyExceptId(scenicSchool.title.id)
    gis_layout.hideMenubar();
}