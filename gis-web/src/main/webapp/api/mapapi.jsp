<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
    <%@ include file="header.jsp" %>
    <section>
        <div class="container">
            <%@ include file="leftmenu.jsp" %>
            <input id="pageid" type="hidden" value="mapapi">

            <div class="docs-content" style="height: 4000px;">
                <h3 class="welcome">地图基础操作</h3>

                <p>common.js文件中gis_core类，封装了关于地图的部分操作，下面对部分接口进行介绍</p>
                <br/>

                <h3 class="welcome">gis_core.map</h3>

                <p>map对象，页面初始化完成候对象便存在，一切关于地图的操作都在次对象上完成。<a
                        href="https://developers.arcgis.com/javascript/3/jsapi/map-amd.html"
                        target="_blank">解关于map对象信息</a></p>
                <br/>

                <h3 class="welcome">相关事件</h3>
                <li>gis_core.mapLoaded 地图初始化完成后掉用</li>
                <pre class="prettyprint">
    gis_core.mapLoaded(function () {
       $("#map_zoom_slider").css("display", "none");//地图加载完成后 隐藏地图界面放大缩小按钮
    });
                </pre>

                <li>gis_core.addExtentChange 注册地图位置改变事件，可注册多个事件，地图缩放或者位置改变时依次调用</li>
                <pre class="prettyprint">
    //地图改变事件
    var extentchange = function (delta) {
         //业务逻辑
    };

     /**
     * 添加地图改变事件
     * @param key 关键字
     * @param extentChange 事件
     */
    gis_core.addExtentChange("EndToEnd", extentchange);
                </pre>

                <li>gis_core.removeExtentChange 根据关键字移除地图位置改变事件</li>
                <pre class="prettyprint">
     /**
     * 移除地图改变事件
     * @param key 关键字
     */
    gis_core.removeExtentChange("EndToEnd");
                </pre>

                <li>gis_core.addMapClick 添加地图点击事件 左键点击地图调用该事件</li>
                <pre class="prettyprint">
     /**
     * 添加地图点击事件
     * @param key 关键字
     * @param click 事件
     */
    gis_core.addMapClick("endtoend_map_click", function (evt) {
       //业务逻辑
    });
                </pre>

                <li>gis_core.removeClick 移除地图点击事件</li>
                <pre class="prettyprint">
     /**
     * 移除地图点击事件
     * @param key 关键字
     */
    gis_core.removeClick("endtoend_map_click");
                </pre>

                <h3 class="welcome">方法</h3>
                <li>gis_core.addGraphicsLayer 添加<a
                        href="https://developers.arcgis.com/javascript/3/jsapi/graphicslayer-amd.html" target="_blank">GraphicsLayer</a>图层,可以向该图层添加<a
                        href="https://developers.arcgis.com/javascript/3/jsapi/graphic-amd.html" target="_blank">Graphics</a>
                </li>
                <pre class="prettyprint">
    //输入参数
    * @param layerid 图层ID
    * @param layerName 图层名称
    * @returns {返回图层对象}

    //样例
    var cell_layer = gis_core.addGraphicsLayer("endtoEnd_cell_layer", "小区图层");
                </pre>

                <li>gis_core.addGraphicsLayerWithIndex 添加图层，并指定在map对象上的层级位置</li>
                <pre class="prettyprint">
    //输入参数
     * @param layerid 图层id
     * @param layerName 图层名称
     * @param index 图层层级
     * @returns {返回图层对象}

    //样例
    var cell_layer = gis_core.addGraphicsLayerWithIndex("endtoEnd_cell_layer", "小区图层",5);
                </pre>

                <li>gis_core.addGraphicsLayerWithCallBack 添加图层,完成后回调方法</li>
                <pre class="prettyprint">
    //输入参数
     * @param layerid 图层ID
     * @param layerName 图层名称
     * @param callback 回掉方法

    //样例
    var cell_layer = gis_core.addGraphicsLayerWithCallBack("endtoEnd_cell_layer", "小区图层",function(){
        });
                </pre>

                <li>gis_core.insertLayer 添加通用图层</li>
                <pre class="prettyprint">
    //输入参数
     * @param layerid 图层ID
     * @param layerName 图层名称
     * @param index  图层层级索引
     * @param layer  需要添加到地图上的图层

    //样例
    var wmsLayer = new esri.layers.WMSLayer(url, {
    resourceInfo: {
                extent: new esri.geometry.Extent(73.2672757, 18.37574, -109.66552734375, 133.29657, {
                    wkid: 4326
                }),
                layerInfos: []
            },
            visibleLayers: ['lteconvergrid']
        });
    wmsLayer.id = "endtoendwmsLayer";
    gis_core.insertLayer("endtoendwmsLayer", "网格覆盖", 2, wmsLayer);
                </pre>

                <li>gis_core.removeLayer 根据ID移除图层</li>
                <pre class="prettyprint">
    //输入参数
     * @param layerid 图层ID

    //样例
    gis_core.removeLayer("endtoEnd_cell_layer");
                </pre>

                <li>gis_core.addPlugin 根据ID获取已添加图层对象</li>
                <pre class="prettyprint">
    //输入参数
     * @param layerid 图层ID
     * @returns {图层对象}

    //样例
    var layer = gis_core.getLayer("endtoEnd_cell_layer");
                </pre>

                <li>gis_core.addPlugin 添加组件到框架中</li>
                <pre class="prettyprint">
    //样例
function Demo() {
    /**
     * 接口 初始化  插件入口
     */
    this.init = function () {
        //注册组件
        gis_core.addPlugin(this);
    };
    this.destroy = function () {

    };
    this.title = {
        id: "01_00_01",
        name: "测试Demo"
    };
}
                </pre>

                <li>gis_core.pluginDestroyExceptId 销毁除传入ID外的其他对象</li>
                <pre class="prettyprint">
//输入参数
* 销毁模块（除了传入模块的ID）
* @param pluginId 模块ID  title.id

//样例
gis_core.pluginDestroyExceptId(id);
                </pre>

                <li>gis_core.getSpatialReference 获取配置的坐标系</li>
                <pre class="prettyprint">

//样例
var reference = gis_core.getSpatialReference();
                </pre>

                <a href="/api/otherxml.jsp" type="button" class="btn-link">上一页</a>
                <a href="/api/layoutapi.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>
            </div>
        </div>
    </section>
</div>
</body>
</html>
