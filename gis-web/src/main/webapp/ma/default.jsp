<%--
  Created by IntelliJ IDEA.
  User: Xiaobing
  Date: 2017/2/15
  Time: 11:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">
<html>
<head>
    <title>iGIS后台管理</title>
    <jsp:include page="ref.jsp"/>
    <style type="text/css">
        body {
            overflow-x: hidden;
            overflow-y: hidden;
        }
    </style>

    <script type="text/javascript">

        function setFrameSize() {
            $('#iframeContent').width($(window).width());
            $('#iframeContent').height($(window).height() - $('#header').height() - 15);
        }

        function initMenu() {
            $("#ul-menu-list > li").unbind("click").bind("click", function () {
                var src = $(this).attr("src");
                $("#iframeContent").attr("src", src);
                $("#ul-menu-list > li").removeClass("active");
                $(this).addClass("active");
            });
        }

        $(document).ready(function () {
            setFrameSize();
            initMenu();
            window.onresize = function () {
                setFrameSize();
            };
        });
    </script>
</head>
<body>
<div id="header" class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
        <!-- 移动端菜单 -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="images/logo.png">iGIS-Manage</a>
        </div>
        <!-- /移动端菜单 -->
        <!-- 导航 -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul id="ul-menu-list" class="nav navbar-nav">
                <li class="active" src="sub/baseinfo.jsp"><a href="javascript:void(0);">基础信息<span class="sr-only">(current)</span></a></li>
                <li src="sub/resbase.jsp"><a href="javascript:void(0);" >资源配置</a></li>
                <li src="http://www.qq.com"><a href="javascript:void(0);" >算法服务</a></li>
                <li><a href="javascript:void(0);">运行日志</a></li>
                <li><a href="javascript:void(0);">系统功能</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false"><i class="icon-user"></i> Administrator <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#"><i class="icon-power-off"></i> Login out</a></li>
                    </ul>
                </li>
                <li><a href="#"><i class="icon-cog"></i> </a></li>
            </ul>
        </div>
        <!-- /导航 -->
    </div>
</div>
<iframe name="iframeContent" id="iframeContent" width="100%" frameborder="0" scrolling="auto"
        src="sub/baseinfo.jsp"></iframe>
</body>
</html>
