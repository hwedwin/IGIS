<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@ include file="reference.jsp" %>
    <title></title>
    <script type="text/javascript">
        var dojoConfig = {
            parseOnLoad: true,
            packages: [{
                name: 'plugin',
                location: root + "script/arcgis_js_api/plugin"
            }]
        };
    </script>
    <link rel="stylesheet" type="text/css"
          href="<%=root%>script/arcgis_js_api/library/3.9/3.9/js/dojo/dijit/themes/tundra/tundra.css"/>
    <link rel="stylesheet" type="text/css" href="<%=root%>script/arcgis_js_api/library/3.9/3.9/js/esri/css/esri.css"/>
    <script type="text/javascript" src="<%=root%>script/arcgis_js_api/library/3.9/3.9/init.js"></script>
    <script type="text/javascript">
        // 功能插件
        var features = [];
        features.push("esri/map");
        features.push("esri/geometry/Point");
        features.push("esri/SpatialReference");
        features.push("esri/symbols/Font");
        features.push("esri/symbols/TextSymbol");
        features.push("esri/symbols/SimpleMarkerSymbol");
        features.push("esri/symbols/SimpleLineSymbol");
        features.push("esri/graphic");
        features.push("esri/layers/GraphicsLayer");
        features.push("dijit/form/Button");
        features.push("dojo/_base/Color");
        features.push("plugin/ArcGisOpenStreetMapLayer");
        features.push("dojo/domReady!");

        // 加载地图
        require(features, function (Map, Point, SpatialReference, Font, TextSymbol, SimpleMarkerSymbol, SimpleLineSymbol, Graphic, GraphicsLayer, Button, Color, ArcGisOpenStreetMapLayer) {
            var map = new Map("map", {
                logo: false
            });

            // 添加底图
            var baseMap = new ArcGisOpenStreetMapLayer();
            map.addLayer(baseMap);

            // 添加画板
            var markerLayer = new GraphicsLayer();
            map.addLayer(markerLayer);
            var textLayer = new GraphicsLayer();
            map.addLayer(textLayer);

            // 标记边框
            var outSymbol = new SimpleLineSymbol();
            outSymbol.setStyle(SimpleLineSymbol.STYLE_NULL);

            // 地图点击事件
            map.on("click", function (evt) {
                dojo.stopEvent(evt);
                var obj = {};
                obj.wkid = map.spatialReference.wkid;
                obj.level = map.getLevel();
                obj.x = evt.mapPoint.x;
                obj.y = evt.mapPoint.y;
                dojo.byId("label").innerHTML = $.toJSON(obj);

                var point = new Point(evt.mapPoint.x, evt.mapPoint.y, new SpatialReference({
                    wkid: map.spatialReference.wkid
                }));

                var symbol = new SimpleMarkerSymbol();
                symbol.setOutline(outSymbol);
                symbol.setSize(14);
                symbol.setColor(new Color([255, 0, 0, 1]));

                var graphic = new Graphic(point, symbol);
                map.graphics.clear();
                map.graphics.add(graphic);
            });

            // 可视区域范围改变
            map.on("extent-change", function (evt) {
                var obj = {};
                obj.xMin = evt.extent.xmin;
                obj.xMax = evt.extent.xmax;
                obj.yMin = evt.extent.ymin;
                obj.yMax = evt.extent.ymax;
                obj.level = evt.lod.level;
                obj.resolution = evt.lod.resolution;
                obj.wkid = map.spatialReference.wkid;

                markerLayer.clear();
                textLayer.clear();
                dojo.byId("label").innerHTML = "";

                $.ajax({
                    url: root + "service/cluster",
                    type: "POST",
                    data: {
                        obj: $.toJSON(obj)
                    },
                    success: function () {
                        var result = arguments[0];
                        var clusterd = result[0]; // 是否进行了聚合算法

                        var points = eval(result.substring(1));
                        if (points.length == 0) {
                            return;
                        }

                        var markerSymbol = new SimpleMarkerSymbol();
                        markerSymbol.setOutline(outSymbol);
                        markerSymbol.setSize(16);
                        markerSymbol.setColor(new Color([0, 0, 255, 0.9]));

                        var font = new Font();
                        font.setFamily("Tahoma");
                        font.setSize("10px");
                        var textSymbol = new TextSymbol();
                        textSymbol.setColor(new Color([255, 255, 255, 1]));
                        textSymbol.setAlign(TextSymbol.ALIGN_MIDDLE);
                        textSymbol.setOffset(0, -3);
                        textSymbol.setFont(font);

                        var count = 0;

                        for (var i = 0; i < points.length; i++) {
                            var point = new Point(points[i].x, points[i].y, new SpatialReference({
                                wkid: map.spatialReference.wkid
                            }));

                            textSymbol.setText(points[i].count);
                            count += points[i].count;

                            markerLayer.add(new Graphic(point, markerSymbol));
                            textLayer.add(new Graphic(point, textSymbol));
                        }

                        var obj = {};
                        obj.cluster = clusterd;
                        obj.level = evt.lod.level;
                        obj.ratio = points.length + "/" + count;
                        dojo.byId("label").innerHTML = $.toJSON(obj);
                    },
                    error: function () {
                        alert(arguments[0].responseText);
                    }
                });
            });

            // 初始定位
            map.centerAndZoom(new Point({
                x: 11861464.307236375,
                y: 3447250.081128183,
                spatialReference: {
                    wkid: 102100
                }
            }), 14);

            // 点击定位
            var button = new Button({
                label: "Locate",
                onClick: function () {
                    var point = new Point(11857601.848650842, 3455027.5487561985, new SpatialReference({
                        wkid: map.spatialReference.wkid
                    }));

                    var symbol = new SimpleMarkerSymbol();
                    symbol.setOutline(outSymbol);
                    symbol.setSize(14);
                    symbol.setColor(new Color([255, 0, 0, 1]));

                    map.centerAndZoom(point, 15);

                    var graphic = new Graphic(point, symbol);
                    map.graphics.clear();
                    map.graphics.add(graphic);
                }
            }, "locateHome");
        });
    </script>
</head>
<body>
<div class="tundra">
    <div>聚合算法:ArcGisOpenStreetMapLayer:102100</div>
    <button id="locateHome" type="button"></button>
    <div id="map"></div>
</div>
<div id="label" style="position: fixed; bottom: 0px; right: 0px"></div>
</body>
</html>
