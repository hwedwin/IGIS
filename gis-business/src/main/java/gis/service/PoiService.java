package gis.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import bean.poi.PoiCategory;
import bean.poi.PoiInfo;
import gis.algorithm.GeoPoint;
import gis.algorithm.GeoUtils;
import gis.algorithm.PointTransform;
import gis.common.GisConfig;

public abstract class PoiService {

    /*
     * 获取poi
     */
    public abstract List<PoiInfo> GetPois() throws SQLException;

    /**
     * 获取poi 分类列表
     *
     * @return poi分类列表
     */
    public abstract List<PoiCategory> GetPoiCategory() throws Exception;

    /**
     * 创建POI对象
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    public static List<PoiInfo> GetPoiEntity(ResultSet rs) throws SQLException {
        String area = GisConfig.GetInstance().getGisType();
        List<PoiInfo> list = new ArrayList<PoiInfo>();
        PoiInfo entityInfo;
        while (rs.next()) {
            entityInfo = new PoiInfo();
            entityInfo.setId(rs.getString("poi_id"));
            entityInfo.setName(rs.getString("poi_name"));
            entityInfo.setLongitude(rs.getDouble("gps_longitude"));
            entityInfo.setLatitude(rs.getDouble("gps_latitude"));
            entityInfo.setAddr(rs.getString("addr"));
            entityInfo.setTel(rs.getString("tel"));
            entityInfo.setGroupID(rs.getString("sv_id"));
            entityInfo.setGroupName(rs.getString("sv_name"));
            entityInfo.setTypeID(rs.getString("sv_cat_id"));
            entityInfo.setTypeName(rs.getString("sv_cat_name"));
            entityInfo.setRegionID(rs.getString("region_id"));
            entityInfo.setRegionName(rs.getString("region_name"));
            entityInfo.setNamePinying(rs.getString("poi_name_char"));
            entityInfo.setQuery(entityInfo.getName() + entityInfo.getAddr());
            if (area.equals("gaode")) {
                //GeoPoint p = GeoUtils.lonLatToMercator(entityInfo.getLongitude(), entityInfo.getLatitude());
                double[] p = PointTransform.gcj_encrypt(entityInfo.getLatitude(), entityInfo.getLongitude());
                entityInfo.setLongitude(p[1]);
                entityInfo.setLatitude(p[0]);
            }

            list.add(entityInfo);
        }
        return list;
    }
}
