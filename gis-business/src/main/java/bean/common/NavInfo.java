package bean.common;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xiaobing on 2016/9/1.
 */
public class NavInfo {
    private String navPage;
    private List<NavItemInfo> navItems = new ArrayList<>();

    public String getNavPage() {
        return navPage;
    }

    public void setNavPage(String navPage) {
        this.navPage = navPage;
    }

    public List<NavItemInfo> getNavItems() {
        return navItems;
    }

    public void setNavItems(List<NavItemInfo> navItems) {
        this.navItems = navItems;
    }
}
