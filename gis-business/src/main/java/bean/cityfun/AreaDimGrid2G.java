package bean.cityfun;

/**
 * Created by hupeng on 16/5/20.
 * 栅格与4G小区对应关系
 */
public class AreaDimGrid2G {
    /**
     * 栅格标识
     */
    private String grid_id;
    /**
     * 最小经度
     */
    private String min_longitude;
    /**
     * 最大经度
     */
    private String max_longitude;
    /**
     * 最小纬度
     */
    private String min_latitude;
    /**
     * 最大纬度
     */
    private String max_latitude;
    /**
     * 小区标识
     */
    private String cell_id;
    /**
     * 小区名称
     */
    private String cell_name;
    /**
     * 栅格对小区面积占比
     */
    private String grid_cell_rate;


    public String getGrid_id() {
        return grid_id;
    }

    public void setGrid_id(String grid_id) {
        this.grid_id = grid_id;
    }

    public String getMin_longitude() {
        return min_longitude;
    }

    public void setMin_longitude(String min_longitude) {
        this.min_longitude = min_longitude;
    }

    public String getMax_longitude() {
        return max_longitude;
    }

    public void setMax_longitude(String max_longitude) {
        this.max_longitude = max_longitude;
    }

    public String getMin_latitude() {
        return min_latitude;
    }

    public void setMin_latitude(String min_latitude) {
        this.min_latitude = min_latitude;
    }

    public String getMax_latitude() {
        return max_latitude;
    }

    public void setMax_latitude(String max_latitude) {
        this.max_latitude = max_latitude;
    }

    public String getCell_id() {
        return cell_id;
    }

    public void setCell_id(String cell_id) {
        this.cell_id = cell_id;
    }

    public String getCell_name() {
        return cell_name;
    }

    public void setCell_name(String cell_name) {
        this.cell_name = cell_name;
    }

    public String getGrid_cell_rate() {
        return grid_cell_rate;
    }

    public void setGrid_cell_rate(String grid_cell_rate) {
        this.grid_cell_rate = grid_cell_rate;
    }
}
