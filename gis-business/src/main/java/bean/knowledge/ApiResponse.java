package bean.knowledge;

/**
 * Created by xiexb on 2017/7/28.
 * 接口返回对象
 */
public class ApiResponse<T> {
    /**
     * 100开头全代表成功
     * 200开头全代表失败
     */
    private int code;

    /**
     * 成功or失败提示信息
     */
    private String msg;

    /**
     * 返回对象
     */
    private T results;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getResults() {
        return results;
    }

    public void setResults(T results) {
        this.results = results;
    }
}
