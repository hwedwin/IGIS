$(function(){

    $('[data-toggle="tooltip"]').tooltip();


    $(".toggle-page-tool").click(function(){
        var thisFocus = $(this).children("i");
        var thisLi = $(this).siblings("li");
        if(thisFocus.hasClass("icon-chevron-right")){
            thisLi.hide();
            $(this).css({
                background:"#333",
                color:"#fff"
            });
            thisFocus.removeClass("icon-chevron-right").addClass("icon-chevron-left");
        }else if(thisFocus.hasClass("icon-chevron-left")){
            thisLi.show();
            $(this).removeAttr("style");
            thisFocus.removeClass("icon-chevron-left").addClass("icon-chevron-right");
        }

    });


    //loading高度
    var outloadingH = $(".inner-box").height();
    $(".loading-rel").height(outloadingH);

    var headH = $(".navbar").outerHeight();
    $(window).scroll(function(){
        var scrT = $(this).scrollTop();
        if(scrT > headH){
            $(".page-tool-bar").animate({
                top:0
            },300);
        }else if(scrT < headH){
            $(".page-tool-bar").animate({
                top:headH
            },300);
        }
    });
});