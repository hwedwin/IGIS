package bean.scenicgrid;

/**
 * Created by hupeng on 16/6/15.
 * 学校场景
 */
public class SchoolScenic {
    /**
     * 风景区标识 bigint
     */
    private String school_id;

    /**
     * 风景区名称 string
     */
    private String school_name;
    /**
     * 地市标识 int
     */
    private String city_id;
    /**
     * 地市标识
     */
    private String city_name;
    /**
     * 边界
     */
    private String bord;
    /**
     * 标签
     */
    private String labels;

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getBord() {
        return bord;
    }

    public void setBord(String bord) {
        this.bord = bord;
    }

    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }
}
