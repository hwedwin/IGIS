<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
    <%@ include file="header.jsp" %>
    <section>
        <div class="container">
            <%@ include file="leftmenu.jsp" %>
            <input id="pageid" type="hidden" value="igis">

            <div class="docs-content" style="height: 850px;">
                <h3 id="welcome" class="welcome"> iGIS简介</h3>
                <br/>

                <p>地理信息系统（GIS）在日常生产、生活中起到越来越重的作用。GIS涉及到的行业多，覆盖面广，有着广阔的业务前景。将含有地理信息的数据，
                    在地图上呈现出来，从一个全新的视角来观察这些数据，给使用者一种新的感知。</p>

                <p>iGIS是一套开放的平台。灵活的部署方法，插件式的开发模式，使平台以更灵活的方式存在，更利于扩展、二次开发和集成。平台集成了ArcGis、高德地图、
                    天地图、百度地图等主流底图服务，无论是在内网还是外网，都能完成部署。iGIS前端开发框架以模块化的设计思路为主，对主要的方法和功能进行深度封装；
                    在平台API的支持下，功能开发人员或二次开发人员不用关心底层的逻辑关系，将更多的注意力放在业务功能上，便可轻松的完成插件功能开发。
                    除了前端的开发框架外，后端框架还提供了大量的算法支持，实现了一大批有关GIS的算法；
                    算法以接口的方式提供给使用者，使用者只需按照模型输入数据便可以得到想要的结果。后期的平台将提供更为强大功能来满足用户的需求，
                    例如：自定义资源显示，用户以SQL配置的方式来添加，部分基于算法的图形渲染也走配置化的道路，以此来简化开发过程。</p>

                <p>在后期开发中，我们还将对平台级功能进行优化，前端JS框架优化和算法优化，同时对iGIS的管理平台的开发也在进行中；
                    最终将iGIS打造成一款有良好的用户体验、功能齐全的软件产品。 </p>

                <a href="/api/welcome.jsp" type="button" class="btn-link">上一页</a>
                <a href="/api/arcgis.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>

            </div>
        </div>
    </section>
</div>
</body>
</html>
