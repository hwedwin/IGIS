/**
 * Created by xiexb on 2017/7/26.
 */
function Tide() {
    var tide_ststion_layer = gis_core.addGraphicsLayer("tide_ststion_layer", "高亮图层");
    var showTextLayer = gis_core.addGraphicsLayer("tide_showTextLayer", "名称提示");

    var start = null;//下拉选择 时间定时器
    var allDataSource = {};
    var currentStationGraphic = null;//已高亮的监测站
    var currentStationInfo = null;

    //选择条件
    var getTideStationMold = function () {
        $.ajax({
            url: "/tide/gettidestationmold",
            data: {},
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    $("#downmenu-select-continent").data("next", datas);
                    for (var i = 0; i < datas.length; i++) {
                        if (datas[i].name == "亚洲") {
                            var yaz = datas[i].tag;
                            for (var j = 0; j < yaz.length; j++) {
                                if (yaz[j].name = "中国") {
                                    $("#downmenu-select-conutry").data("next", yaz)
                                    $("#downmenu-select-prov").data("next", yaz[j].tag)
                                    break;
                                }
                            }
                            break;
                        }
                    }
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
        });
    };
    var bandTideStationMoldSelect = function (data) {
        if (data == null || data.length == 0) {
            return;
        }

        var $div = $("#downmenu-select-item").empty();
        $div.show();
        for (var i = 0; i < data.length; i++) {
            var $item = $("<a href='javascript:void(0);' class='rodestyle' lab='" + data[i].id + "'>" + data[i].name + "</a>");
            $item.data("next", data[i].tag);
            $item.click(function () {
                    var a = $(this);
                    if (a.attr("lab") == "continent") {
                        $("#downmenu-select-conutry").data("next", getNullDataSoruce("prov"));
                        $("#downmenu-select-prov").data("next", getNullDataSoruce("prov"));
                        $("#downmenu-select-city").data("next", getNullDataSoruce("city"));
                    }
                    var d = a.data("next");

                    $("#downmenu-select-item-" + a.attr("lab") + "-lable").html(a.html());
                    var next_lable = null;
                    if (d != null && d.length > 0) {
                        next_lable = d[0].id;
                        $("#downmenu-select-item-" + next_lable + "-lable").text(d[0].name);
                        $("#downmenu-select-" + next_lable).data("next", d);
                        if (d[0].tag != null && d[0].tag.length > 0) {
                            $("#downmenu-select-prov").data("next", d[0].tag);
                        }
                    }
                    $("#downmenu-select-prov").show();
                    $("#downmenu-select-city").show();
                    if (a.attr("lab") == "continent" && next_lable == "conutry" && d[0].tag == null) {
                        $("#downmenu-select-prov").hide();
                        $("#downmenu-select-city").hide();
                    }
                    else if (a.attr("lab") == "conutry" && next_lable == null) {
                        $("#downmenu-select-prov").hide();
                        $("#downmenu-select-city").hide();
                    }
                    /*else if (a.attr("lab") == "prov" ||  next_lable == null) {
                     $("#downmenu-select-prov").hide();
                     $("#downmenu-select-city").hide();
                     }*/

                    //$("#downmenu-select-prov").show();
                    //$("#downmenu-select-city").show();
                    //标签显示
                    if (next_lable == "prov" || next_lable == "conutry") {
                        $("#downmenu-select-item-prov-lable").html("全部");
                        $("#downmenu-select-item-city-lable").html("全部");
                    } else if (next_lable == "city") {
                        $("#downmenu-select-item-city-lable").html("全部");
                    }
                    else if (next_lable == null && a.attr("lab") != "city") {
                        $("#downmenu-select-item-prov-lable").html("全部");
                        $("#downmenu-select-item-city-lable").html("全部");
                    }

                    if (d == null && a.attr("lab") == "conutry") {
                        $("#downmenu-select-prov").data("next", getNullDataSoruce("prov"));
                        $("#downmenu-select-city").data("next", getNullDataSoruce("city"));
                    }
                    else if (d == null && a.attr("lab") == "prov") {
                        $("#downmenu-select-city").data("next", getNullDataSoruce("city"));
                    }
                }
            );
            $div.append($item)
            if (i == 0) {
                $item.click();
            }
        }

    };
    var getNullDataSoruce = function (id) {
        var data = [];
        data.push({id: id, name: "全部"});
        return data;
    };
    //热门港口
    var getHotStationInfo = function () {
        $(".hot-port").empty();
        $.ajax({
            url: "/tide/gethotstationinfo",
            data: {},
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    bandTagPort(datas);
                    //默认显示第一个

                    if (datas.hotStations != null && datas.hotStations.length > 0) {

                        var stations = [];
                        stations.push(datas.hotStations[0]);
                        showStationGraphics(stations);

                        var point = new esri.geometry.Point(datas.hotStations[0].ew, datas.hotStations[0].ns, gis_core.getSpatialReference());
                        gis_core.map.centerAndZoom(point, 3)
                    }
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
        });
    };
    var bandTagPort = function (data) {
        if (data == null) {
            return;
        }
        //热门港口
        var hots = data.hotStations;
        var hot_port = $(".hot-port").empty();
        for (var i = 0; i < hots.length; i++) {
            var content = $("<li><a href='javascript:void(0);' title='点击查看'>" + hots[i].stationName + "</a></li>");
            content.find("a").data("station", hots[i]);
            content.find("a").click(function () {
                var d = $(this).data("station");
                var point = new esri.geometry.Point(d.ew, d.ns, gis_core.getSpatialReference());
                gis_core.map.centerAndZoom(point, 7);

                var stations = [];
                stations.push(d);
                showStationGraphics(stations);
                //loadStationTideData(d);
                //bandTipStationData(d);
            });
            hot_port.append(content);
            if (i == 0) {
                //content.find("a").click();
            }
        }
        //查询专题
        var tags = data.stationTag;
        var tags_list = $("#tide-station-tag-list").empty();
        for (var i = 0; i < tags.length; i++) {
            var tag = tags[i];
            var content = $("<li><a href='javascript:void(0);'>" + tag.name + "</a></li>");
            content.find("a").data("station", tag);
            content.find("a").click(function () {
                $("#tide-station-tag-list").find(".on").removeClass("on");
                $(this).addClass("on");

                var stations = $(this).data("station").tag;
                bandTagPortList(stations);
                //自动折叠
                //$(".right-scroll").click();
                //$(".map-tip").hide();
                showStationGraphics(stations);
                var point = new esri.geometry.Point(stations[0].ew, stations[0].ns, gis_core.getSpatialReference());
                gis_core.map.centerAndZoom(point, 3)
            });
            tags_list.append(content);
        }
        if (tags != null && tags.length > 0) {
            bandTagPortList(tags[0].tag);
        }
    };
    var bandTagPortList = function (stations) {
        var tag_content = $("#tab-content-p").empty();
        for (var j = 0; j < stations.length; j++) {
            var $a = $("<a href='javascript:void(0);' title='点击查看'>" + stations[j].stationName + "</a>");
            $a.data("station", stations[j]);
            $a.click(function () {
                var d = $(this).data("station");
                var point = new esri.geometry.Point(d.ew, d.ns, gis_core.getSpatialReference());
                gis_core.map.centerAndZoom(point, 7)
                var g = findSatationGraphic(d);
                if (g != null) {
                    stationGraphicsClickEvent(g);
                }
                else {
                    showStationGraphics(stations);
                    //loadStationTideData(d);
                }
                bandTipStationData(d);
            });
            tag_content.append($a);
        }
    };

    //查询监测站
    var eventLoadStations = function () {
        var tim = $("#endtoend_20_query_time_input").val();
        var queryKey = "continent";
        var queryWord = "";
        var cityWord = $("#downmenu-select-item-city-lable").html();
        if (queryWord == "" && cityWord != "全部") {
            queryWord = cityWord;
            queryKey = "city";
        }
        var provWord = $("#downmenu-select-item-prov-lable").html();
        if (queryWord == "" && provWord != "全部") {
            queryWord = provWord;
            queryKey = "prov";
        }
        var conutryWord = $("#downmenu-select-item-conutry-lable").html();
        if (queryWord == "" && conutryWord != "全部") {
            queryWord = conutryWord;
            queryKey = "conutry";
        }
        var continentWord = $("#downmenu-select-item-continent-lable").html();
        if (queryWord == "" && continentWord != "全部") {
            queryWord = continentWord;
            queryKey = "continent";
        }
        loadStations(queryKey, queryWord);
    };
    var queryLoadStations = function () {
        var queryKey = "query";
        var queryWord = $("#tide-top-queryword").val();
        if (!gis_common.stringIsNullOrWhiteSpace(queryWord)) {
            loadStations("query", queryWord);
        }
    };
    var loadStations = function (queryKey, queryWord) {
        $.ajax({
            url: "/tide/loadtidestations",
            data: {querykey: queryKey, queryword: queryWord},
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                //gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    //自动折叠
                    //$(".right-scroll").click();
                    //$(".map-tip").hide();
                    showStationGraphics(datas);
                    if (datas != null && datas.length > 0) {
                        var point = new esri.geometry.Point(datas[0].ew, datas[0].ns, gis_core.getSpatialReference());
                        gis_core.map.centerAndZoom(point, 3)
                    }
                });
            },
            complete: function () {
                //gis_common.isLoadingSunCount();
            },
        });
    };

    //地图元素
    var showStationGraphics = function (stations) {
        tide_ststion_layer.clear();
        if (stations == null || stations.length == 0) {
            return;
        }
        var fristGraphic = null;
        var fristStation = null;
        for (var i = 0; i < stations.length; i++) {
            var s = stations[i];
            var point = new esri.geometry.Point(s.ew, s.ns, gis_core.getSpatialReference());
            var symbol = new esri.symbol.PictureMarkerSymbol('/css/modules/tide/img/coord-yellow-ico.png', 28, 25);
            var attr = {"station": s, "name": s.stationName};
            var graphic = new esri.Graphic(point, symbol, attr);
            tide_ststion_layer.add(graphic);
            if (i == 0) {
                fristGraphic = graphic;
                fristStation = s;
            }
        }

        //显示第一个
        stationGraphicsClickEvent(fristGraphic);
        bandTipStationData(fristStation);
    };
    //根据名称 查找Graphics
    var findSatationGraphic = function (station) {
        var graphic = null;
        var graphics = tide_ststion_layer.graphics;
        for (var i = 0; i < graphics.length; i++) {
            if (graphics[i].attributes.station.stationNo == station.stationNo) {
                graphic = graphics[i];
            }
        }
        return graphic;
    };
    //鼠标点击事件
    var stationGraphicsClickEvent = function (graphic) {
        var g = graphic;
        var station = graphic.attributes.station;
        var symbol = new esri.symbol.PictureMarkerSymbol('/css/modules/tide/img/coordinate-on.png', 50, 50);
        g.setSymbol(symbol);
        if (currentStationGraphic != null) {
            var _symbol = new esri.symbol.PictureMarkerSymbol('/css/modules/tide/img/coord-yellow-ico.png', 28, 25);
            currentStationGraphic.setSymbol(_symbol);
        }
        currentStationGraphic = g;
        clearTideStationDetail();
        loadStationTideData(station);
    };
    //清理数据
    var clearTideStationDetail = function () {
        $("#tide-station-detail-title").empty();
    };
    //获取单个监测站的信息
    var loadStationTideData = function (station) {
        $(".right-window-hidden").click();
        //$("right-window-open").show();

        var time = $("#endtoend_20_query_time_input").val();
        //$("#tide-station-detail-title").html(station.stationName + "(" + time + ")");
        $("#tide-station-detail-title").html(station.stationName);
        $("#tide-data-hour-title").empty().html(station.stationName + "" + time + "");
        $(".time-text").empty().html("" + (new Date()).Format("yyyy年MM月dd日hh时"));
        currentStationInfo = station;
        $.ajax({
            url: "/tide/gettidestationkpi",
            data: {stationno: station.stationNo, time: time},
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                //gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas, station) {
                    bandStationTideData(datas);
                });
            },
            complete: function () {
                //gis_common.isLoadingSunCount();
            },
        });
    };
    var bandStationTideData = function (data, station) {
        //潮汐表
        var table = $("#tide-data-day-table").empty();
        var timeDim = $("#tide-data-day-other").empty();
        var weaterDayTable = $("#tide-weather-day-table").empty();
        //绑定表格
        var time_row = "<tr><td class='f-b'>潮时(Hrs)</td>";
        var data_row = "<tr><td class='f-b'>潮高(cm)</td>";
        if (data.tideDataDay.tideLst != null && data.tideDataDay.tideLst.length > 0) {
            for (var i = 0; i < data.tideDataDay.tideLst.length; i++) {
                time_row += "<td>" + data.tideDataDay.tideLst[i].id + "</td>";
                data_row += "<td>" + data.tideDataDay.tideLst[i].name + "</td>";
            }
        }
        else {
            time_row += "<td>无数据</td>";
            data_row += "<td>无数据</td>";
        }
        time_row += "</tr>";
        data_row += "</tr>"
        table.append($(time_row + data_row));
        //绑定时区
        var t = "时区:<span class='m-r-10'>" + data.tideDataDay.timeZone + " </span>   潮高基准面：" + data.tideDataDay.tideDatum;
        timeDim.html(t);

        //潮汐折线图
        var dataHours = data.tideDataHours;
        var xAxis_data = [];
        var series_data = [];
        for (var i = 0; i < dataHours.length; i++) {
            xAxis_data.push(dataHours[i].hourKey + "时");
            series_data.push(dataHours[i].timeHeight);
        }
        var option = {
            tooltip: {
                trigger: 'axis'
            },
            grid: {
                x: 45,
                y: 5,
                x2: 2,
                y2: 30
            },
            calculable: true,
            xAxis: [
                {
                    show: true,
                    type: 'category',
                    boundaryGap: false,
                    axisLabel: {
                        show: true,
                        textStyle: {
                            color: '#30D8E6'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#2A597D'
                        }
                    },
                    data: xAxis_data
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    axisLabel: {
                        formatter: '{value}cm',
                        textStyle: {
                            color: '#30D8E6'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#2A597D'
                        }
                    }
                }
            ],
            series: [
                {
                    name: '潮高(CM)',
                    type: 'line',
                    itemStyle: {
                        normal: {
                            color: '#2E74A6',
                            lineStyle: {
                                color: '#2E74A6'
                            }
                        }
                    },
                    data: series_data
                }
            ]
        };
        requiretx(
            [
                'echarts',
                'echarts/chart/line' // 使用柱状图就加载bar模块，按需加载
            ],
            function (ec) {
                // 基于准备好的dom，初始化echarts图表
                var myChart = ec.init(document.getElementById('tide-data-hour-line'));
                myChart.setOption(option);
            }
        );
        bandTipStationWeatherData(data, station);

        //当前天气数据
        /*if (data.tideWeather != null) {
         $(".time-weather").show();
         $(".max-font").html(data.tideWeather.temp + "°");
         $("#tide-station-weather-temp-pic").attr("src", "/css/modules/tide/img/c-weather-ico/" + data.tideWeather.weather + ".png");
         $("#tide-station-weather-temp-pic").attr("title", data.tideWeather.weatherLable);
         }
         else {
         $(".time-weather").hide();
         }

         //一周天气数据
         var weatherDays = data.tideWeatherDays;
         var w_tr_title = "<tr>";
         var w_tr_date = "<tr>";
         var w_tr_weather = "<tr>";
         var w_tr_pic = "<tr>";
         if (weatherDays != null && weatherDays.length > 0) {
         for (var i = 0; i < weatherDays.length; i++) {
         var w = weatherDays[i];
         /!*if (i == 0) {
         w_tr_title += "<td>今天</td>"
         }
         else if (i == 1) {
         w_tr_title += "<td>明天</td>"
         }
         else {
         w_tr_title += "<td>" + w.titleLable + "</td>"
         }*!/
         w_tr_date += "<td>" + w.daykey + "</td>" // title='" + w.lowTemp + "°C~" + w.highTemp + "°C'
         w_tr_weather += "<td>" + w.weatherLable + "</td>"
         w_tr_pic += "<td><img src='/css/modules/tide/img/c-weather-ico/" + w.weather + ".png'></td>";
         }
         }
         else {
         w_tr_weather += "<td>暂不提供该时间内天气数据服务</td>"
         }

         w_tr_title += "</tr>";
         w_tr_date += "</tr>";
         w_tr_weather += "</tr>";
         w_tr_pic += "</tr>";
         weaterDayTable.html(w_tr_date + w_tr_weather + w_tr_pic);*/
    };
    //气象数据
    var bandTipStationWeatherData = function (data, station) {
        var ul = $("#tide-ul-weather-item").empty();
        $(".mete-data").find("#tide-nodata-msg").remove();
        var weathers = data.tideWeatherOneDay;
        if (weathers == null || weathers.length == 0) {
            $(".mete-cont").hide();
            var div = "<div id='tide-nodata-msg' style='width: 180px; margin: 0 auto;'>暂不提供该时间内天气数据服务</div>";
            $(".mete-data").append($(div));
            return;
        }
        $(".mete-cont").show();
        $("#tide-ul-weather-item").css("left", "0px");
        for (var i = 0; i < weathers.length; i++) {
            var weather = weathers[i];
            var content = "<li index='" + i + "'>"
            if (i == 0) {
                content = "<li index='" + i + "' current='true'>"
            }
            content += "<span class='on-arrow' style='display: none;'></span>"
            content += "<p>" + weather.timeLable + "</p>"
            content += "<p>" + weather.weatherLable + "</p>"
            content += "<p><img src='/css/modules/tide/img/c-weather-ico/" + weather.weather + ".png' width='30'></p>";
            content += "<p>" + weather.temp + "°C</p>"
            content += "<p>风速：" + weather.winS + "m/s</p>"
            content += "<p>风向：" + weather.winD + "°</p>"
            if (weather.dataType == 1) {
                content += "<p>实况</p>"
            }
            else {
                content += "<p>预报</p>"
            }
            content += "</li>"
            var $obj = $(content).data("weather", weather);

            $obj.mouseover(function () {
                $(this).find(".on-arrow").show();
                var d = $(this).data("weather");
                showTipStationWeatherDataMore(d);

            }).mouseout(function () {
                $(".mete-model").hide();
                $(".mete-cont").find(".on-arrow").hide();
            });
            $obj.hover(function () {
                $(this).find(".on-arrow").show();
                var d = $(this).data("weather");
                showTipStationWeatherDataMore(d);
            }, function () {
                $(".mete-model").hide();
                $(".mete-cont").find(".on-arrow").hide();
            });

            ul.append($obj);
        }
    };
    var showTipStationWeatherDataMore = function (data) {
        var kpi = data.kpiBeanList;
        var $div = $(".mete-model").show().find(".metemodel-table").empty();
        var content = "<table class='metemodel-table'>";
        content += "<tbody>"
        for (var i = 0; i < kpi.length; i = i + 2) {
            content += "<tr>";
            if (parseInt(kpi[i].kpiValue) >= 999998) {
                content += "<td class='t-b' style='text-align:right;padding-right: 5px;'>" + kpi[i].kpiName + ":</td><td>——</td>";
            }
            else {
                content += "<td class='t-b' style='text-align:right;padding-right: 5px;'>" + kpi[i].kpiName + ":</td><td>" + kpi[i].kpiValue + kpi[i].kpiUnit + "</td>";
            }

            if (i + 1 < kpi.length) {
                if (parseInt(kpi[i + 1].kpiValue) >= 999998) {
                    content += "<td class='t-b' style='text-align:right;padding-right: 5px;'>" + kpi[i + 1].kpiName + ":</td><td>——</td>";
                }
                else {
                    content += "<td class='t-b' style='text-align:right;padding-right: 5px;'>" + kpi[i + 1].kpiName + ":</td><td>" + kpi[i + 1].kpiValue + kpi[i + 1].kpiUnit + "</td>";
                }
            }
            content += "</tr>";
        }
        content += "</tbody>"
        content += "</table>";
        $div.append($(content));

        var h = ($(".mete-model-cont").height() + 28) * -1;
        $(".mete-model").css("top", h);
    }
    var moveTipStationSeatherItem = function (d, time) {
        if (time == undefined) {
            time = 400;
        }
        var ul = $("#tide-ul-weather-item");
        var lis = $("#tide-ul-weather-item").find("li");
        if (lis.length <= 0) {
            return;
        }
        var off_width = $(lis[0]).width() + 7;
        var currentIndex = 0; //lis.index("[current=true]");
        for (currentIndex = 0; currentIndex <= lis.length; currentIndex++) {
            var current = $(lis[currentIndex]).attr("current");
            if (current != undefined && current == "true") {
                break;
            }
        }
        if (d == "next") {
            var flat = lis.length - currentIndex > 4;
            var currentli = lis.get(currentIndex);
            currentIndex = currentIndex + 1;
            var left = off_width * currentIndex * -1;
            var li = lis.get(currentIndex);
            ul.animate({left: left - 7}, time, function () {
                if (!flat) {
                    ul.animate({left: left + off_width - 7}, 100);
                }
            });
            if (flat) {
                $(li).attr("current", "true");
                $(currentli).removeAttr("current");
            }
        }
        else {
            var flat = currentIndex > 0;
            var currentli = lis.get(currentIndex);
            currentIndex = currentIndex - 1;
            var left = off_width * currentIndex * -1;
            var li = lis.get(currentIndex);
            ul.animate({left: left - 7}, time, function () {
                if (!flat) {
                    ul.animate({left: left - off_width - 7}, 100);
                }
            });
            if (flat) {
                $(currentli).removeAttr("current");
                $(li).attr("current", "true");
            }

        }
    };
    //绑定港口提示弹出框
    var bandTipStationData = function (station, top, left) {
        //绑定内容
        $(".map-tip").show();
        $(".map-tip-title").empty();
        $(".map-tip-cont").empty();

        $(".map-tip-title").empty().html("<h3>" + station.stationName + "</h3>");
        var content = station.stationName + "的地理坐标为：" + station.ewNs + ",位于" + station.seaArea + station.address;
        if (!gis_common.stringIsNullOrWhiteSpace(station.desc)) {
            content += "," + station.desc;
        }
        $(".map-tip-cont").empty().html("<p>" + content + "</p>");

        //计算位置
        var width = window.screen.width;
        var height = window.screen.height;

        if (top == undefined || left == undefined) {
            top = height / 2;
            left = width / 2;
        }

        var tip = $(".map-tip");
        tip.css("top", top);
        tip.css("left", left);
        tip.css("width", 0);

        $(".map-tip").animate({top: 50, left: 300, width: 305}, 600);
    };
    //鼠标放上去提示
    var mouseOverLayerEvent = function (e) {
        gis_core.map.setMapCursor("pointer");
        var font = new esri.symbol.Font();
        font.setSize("10pt");
        font.setWeight(esri.symbol.Font.WEIGHT_BOLD);

        font.setFamily("微软雅黑");
        var cpoint = event.graphic.geometry;
        var text = new esri.symbol.TextSymbol(event.graphic.attributes.name);
        text.setFont(font);
        text.setColor(new dojo.Color([151, 194, 227, 255]));
        text.setOffset(10, 20);
        var labelGraphic = new esri.Graphic(cpoint, text);
        showTextLayer.add(labelGraphic);
    };
    var mouseOutLayerEvent = function () {
        showTextLayer.clear();
        gis_core.map.setMapCursor("default");
    };

    //模糊查询
    var tideTopQueryword = function () {
        $(".search-cont-list").empty();
        var queryword = $("#tide-top-queryword").val();
        if (queryword == "" || queryword == null) {
            $(".hide-search").hide();
            return;
        }

        $.ajax({
            url: "/tide/loadopttidesataion",
            data: {queryword: queryword},
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                //gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    $("#commonquery_content").show();

                    $(".search-cont-list").empty();
                    $(".search-cont-list").show();
                    $(".hide-search").show();

                    for (var i = 0; i < datas.length; i++) {
                        var rs = datas[i];
                        var $li = $("<li><div class='result-title' style='color: #82d1ff;'><i class='fa fa-location-arrow'/>" + rs.stationName + "</div></li>");
                        $(".search-cont-list").append($li);
                        $li.data("station", rs);
                        $li.click(function () {
                            var data = $(this).data("station");
                            var point = new esri.geometry.Point(data.ew, data.ns, gis_core.getSpatialReference());
                            gis_core.map.centerAndZoom(point, 7)

                            showStationGraphics(stations);
                            loadStationTideData(data);
                            bandTipStationData(data);

                            var stations = [];
                            stations.push(data);
                            showStationGraphics(stations);

                            $(".search-cont-list").show();
                            $(".left-window-open").find(".close").click();
                        });
                    }
                });
            },
            complete: function () {
                //gis_common.isLoadingSunCount();
            }
        });
    };

    var bindEvent = function () {
        //地图事件
        tide_ststion_layer.on("mouse-over", mouseOverLayerEvent);
        tide_ststion_layer.on("mouse-out", mouseOutLayerEvent);
        tide_ststion_layer.on("click", function (event) {
            stationGraphicsClickEvent(event.graphic);
            var station = event.graphic.attributes.station
            bandTipStationData(station, event.clientY, event.screenX);
        });

        //查询时间
        $("#tide-station-searchbtn").click(function () {
            eventLoadStations();
        });

        $(".right-scroll").click(function () {
            $(".map-tip").hide();
            var w = "-1" + ( $(".right-window-open").width() + 100) + "px";
            $(".right-window-open").animate({right: w}, 800, function () {
                $(".right-window-hidden").show();
            });
        });
        $(".right-window-hidden").click(function () {
            $(".right-window-hidden").hide();
            $(".right-window-open").animate({right: "0px"}, 800);
        });

        //左侧面板
        $(".left-window-open").find(".close").click(function () {
            var h = "-" + ($(".left-window-open").height() + 100) + "px";
            $(".left-window-open").animate({bottom: h}, 700, function () {
                $(".left-window-hidden").show();
            });
        });
        $(".left-window-hidden").find(".open").click(function () {
            $(".left-window-hidden").hide()
            $(".left-window-open").animate({bottom: "0px"}, 700);
        });
        $("a[type='select']").click(function () {
            if (start != null) {
                clearTimeout(start);
                start = null;
            }
            var a = $(this);
            bandTideStationMoldSelect(a.data("next"));
        });

        $("#downmenu-select-item").mouseout(function () {
            start = setInterval(function () {
                $("#downmenu-select-item").hide(200, function () {
                    $("#downmenu-select-item").empty();
                });
                clearTimeout(start);
                start = null;
            }, 600);
        });
        $("#downmenu-select-item").mouseover(function () {
            if (start != null) {
                clearTimeout(start);
            }
            start = null;
        });

        //绑定时间
        $(".left-window-open .date-tab").find("a").click(function () {
            $(".left-window-open .date-tab").find(".on").removeClass("on");
            var $a = $(this).addClass("on");
            var dd = new Date();

            var aData = $a.attr("data");
            if (aData == "yesterday") {
                dd.setDate(dd.getDate() - 1);
            }
            else if (aData == "nextday") {
                dd.setDate(dd.getDate() + 1);
            }
            else if (aData == "week") {
                var oneDayTime = 24 * 60 * 60 * 1000;
                var day = dd.getDay();
                var nowTime = dd.getTime();
                var MondayTime = nowTime - (day - 1) * oneDayTime;
                dd = new Date(MondayTime);
            }
            else if (aData == "month") {
                dd.setDate(1);
            }
            var sData = dd.Format("yyyy-MM-dd");
            $("#endtoend_20_query_time_input").val(sData);
        });

        //绑定模糊查询
        $("#tide-top-queryword").on('input', function () {
            tideTopQueryword();
        });

        //搜索查询
        $(".srh-inp").unbind("keyup");
        $(".srh-inp").bind('keyup', function (event) {
            event = document.all ? window.event : event;
            if ((event.keyCode || event.which) == 13) {
                queryLoadStations();
                $(".left-window-open").find(".close").click();
            }
        });
        $("#tide-top-btn-search").click(function () {
            queryLoadStations();
            $(".left-window-open").find(".close").click();
        });

        //时间左右移动
        $(".front-scroll-ico").click(function () {
            moveTipStationSeatherItem("back");
        });
        $(".back-scroll-ico").click(function () {
            moveTipStationSeatherItem("next");
        });
        $('#tide-ul-weather-item').bind('mousewheel', function (event, delta) {
            var dir = delta > 0 ? 'Up' : 'Down';
            if (dir == 'Up') {
                moveTipStationSeatherItem("back", 200);
            } else {
                moveTipStationSeatherItem("next", 200);
            }
            return false;
        });

        try {
            $(".hot-port").perfectScrollbar();
            $(".hot-port").perfectScrollbar('update');
        }
        catch (e) {
        }

        try {
            $(".tab-content").perfectScrollbar();
            //$(".tab-content").perfectScrollbar('update');
        }
        catch (e) {
        }
    };
    /**
     * 初始化
     */
    this.init = function () {
        gis_core.addPlugin(this);
        gis_core.mapLoaded(function () {
            $("#map_zoom_slider").css("display", "none");
        });
        if (gis_common.commonQuery != null) {
            gis_common.commonQuery.destroy();
        }
        //时间初始化
        $("#endtoend_20_query_time_input").val((new Date()).Format("yyyy-MM-dd"));
        bindEvent();
    };
    /**
     * 销毁
     */
    this.destroy = function () {
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();
    };
    /**
     * 模块信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "01_00_01",
        name: "潮汐数据"
    };
    /**
     *
     */
    this.loadTide = function () {
        getTideStationMold();
        getHotStationInfo();
    };
    this.setDate = function (date) {
        $("#endtoend_20_query_time_input").val(date);
        if (currentStationInfo != null) {
            loadStationTideData(currentStationInfo);
        }
    };
}

var tide = null;

function initTide() {
    if (tide == null) {
        tide = new Tide();
        tide.init();
    }
}

function loadTide() {
    initTide();
    hideMapButton();
    tide.loadTide();
}

function hideMapButton() {
    $(".nav-slide").hide();
    $(".nav-slide-home").hide();
}

function timeChange(obj) {
    var time = obj.cal.getNewDateStr();
    if (tide != null) {
        tide.setDate(time);
    }
}