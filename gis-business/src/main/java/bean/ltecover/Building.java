package bean.ltecover;

import gis.algorithm.Extent;
import gis.algorithm.GeoPoint;
import gis.algorithm.PointTransform;
import gis.core.bean.KeyValue;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xiaobing on 2016/5/3.
 */
public class Building extends KeyValue {
    public Building() {
        this.ring = new ArrayList<GeoPoint>();
        this.extent = new Extent();
        this.center = new GeoPoint();
    }

    /**
     * 中心点
     */
    private GeoPoint center;
    /**
     * 区域
     */
    private Extent extent = new Extent();
    /**
     * 边界
     */
    private List<GeoPoint> ring;

    /**
     * 中心点
     *
     * @return
     */
    public GeoPoint getCenter() {
        return center;
    }

    /**
     * 中心点
     *
     * @param center
     */
    public void setCenter(GeoPoint center) {
        this.center = center;
    }

    /**
     * 区域
     *
     * @return
     */
    public Extent getExtent() {
        return extent;
    }

    /**
     * 区域
     *
     * @param extent
     */
    public void setExtent(Extent extent) {
        this.extent = extent;
    }

    /**
     * 边界
     *
     * @return
     */
    public List<GeoPoint> getRing() {
        return ring;
    }

    /**
     * 边界
     *
     * @param ring
     */
    public void setRing(List<GeoPoint> ring) {
        this.ring = ring;
    }

    public void setXmax(double xmax) {
        this.extent.setxMax(xmax);
    }

    public void setYmax(double ymax) {
        this.extent.setyMax(ymax);
    }

    public void setXmix(double xmix) {
        this.extent.setxMin(xmix);
    }

    public void setYmix(double ymix) {
        this.extent.setyMin(ymix);
    }

    public void setCenterX(double x) {
        this.center.setX(x);
    }

    public void setCenterY(double y) {
        this.center.setY(y);
    }

    public void setRing(String ring) {
        String[] xys = StringUtils.splitPreserveAllTokens(ring, ",");
        int length = xys.length;
        for (int i = 0; i < length; i = i + 2) {
            if (i >= length || i + 1 >= length) {
                continue;
            }
            this.ring.add(new GeoPoint(Double.parseDouble(xys[i]), Double.parseDouble(xys[i + 1])));
        }

    }
}
