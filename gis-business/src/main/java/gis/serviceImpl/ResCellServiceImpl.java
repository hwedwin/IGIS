package gis.serviceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.enums.ResType;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cache.DataCache;
import bean.common.*;
import gis.common.GisConfig;
import gis.service.ResCellService;
import gis.sqlbase.DatabaseManage;
import gis.sqlbase.DatabaseName;
import gis.sqlbase.SQLHelper;

public class ResCellServiceImpl extends ResCellService {
    private Logger logger = LoggerFactory.getLogger(ResCellServiceImpl.class);
    private static final SQLHelper db = DatabaseManage.Instance(DatabaseName.common);
    private static final SQLHelper oradb = DatabaseManage.Instance(DatabaseName.oracle);
    private GisConfig config = GisConfig.GetInstance();

    @SuppressWarnings("unchecked")
    @Override
    public List<Cell> GetGSMCells(RqCell rqCell) throws java.lang.Exception {

        List<Cell> list = null;

        if (rqCell.isGSM() == false) {
            return list;
        }

        String keyString = "ResCellServiceImpl_GetGSMCells" + GetKeyPara(rqCell);
        Object obj = DataCache.getData(keyString);
        if (obj != null) {
            list = (List<Cell>) obj;
        }
        String sql = config.getSQL("res_gsm");
        if (list == null && StringUtils.isNotEmpty(sql)) {
            String strSQL = "select * from (" + sql + " ) a where longitude is not null and latitude is not null  " + GetSqlPara(rqCell);
            ResultSet rs = db.getResultSet(strSQL);
            if (rs != null) {
                try {
                    list = GetGSMCellsEntity(rs);
                    DataCache.addData(keyString, list);
                } catch (SQLException e) {
                    logger.error("调用ResCellServiceImpl 的方法 GetGSMCells出错," + e);
                    e.printStackTrace();
                    throw new java.lang.Exception("调用ResCellServiceImpl 的方法 GetGSMCells出错");
                } finally {
                    db.close(rs);
                }
            }
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Cell> GetLTECells(RqCell rqCell) throws java.lang.Exception {

        List<Cell> list = null;
        if (rqCell.isLTE() == false) {
            return list;
        }

        String keyString = "ResCellServiceImpl_GetLTECells" + GetKeyPara(rqCell);
        Object obj = DataCache.getData(keyString);
        if (obj != null) {
            list = (List<Cell>) obj;
        }

        String sql = config.getSQL("res_lte");
        if (list == null && StringUtils.isNotEmpty(sql)) {
            String strSQL = "select * from (" + sql + " ) a where longitude is not null and latitude is not null  " + GetSqlPara(rqCell);
            ResultSet rs = db.getResultSet(strSQL);
            if (rs != null) {
                try {
                    list = GetLTECellsEntity(rs);
                    DataCache.addData(keyString, list);
                } catch (SQLException e) {
                    logger.error("调用ResCellServiceImpl 的方法 GetLTECells出错," + e);
                    e.printStackTrace();
                    throw new java.lang.Exception("调用ResCellServiceImpl 的方法 GetLTECells出错");
                } finally {
                    db.close(rs);
                }
            }
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Cell> GetTDCells(RqCell rqCell) throws java.lang.Exception {

        List<Cell> list = null;
        if (rqCell.isTD() == false) {
            return list;
        }

        String keyString = "ResCellServiceImpl_GetTDCells" + GetKeyPara(rqCell);
        Object obj = DataCache.getData(keyString);
        if (obj != null) {
            list = (List<Cell>) obj;
        }

        String sql = config.getSQL("res_td");
        if (list == null && StringUtils.isNotEmpty(sql)) {
            if (rqCell.isTD()) {
                String strSQL = "select * from (" + sql + " ) a where longitude is not null and latitude is not null  " + GetSqlPara(rqCell);
                ResultSet rs = db.getResultSet(strSQL);
                if (rs != null) {
                    try {
                        list = GetTDCellsEntity(rs);
                        DataCache.addData(keyString, list);
                    } catch (SQLException e) {
                        logger.error("调用ResCellServiceImpl 的方法 GetTDCells出错," + e);
                        e.printStackTrace();
                        throw new java.lang.Exception("调用ResCellServiceImpl 的方法 GetTDCells出错");
                    } finally {
                        db.close(rs);
                    }
                }
            }
        }
        return list;
    }
}
