package bean.scenicgrid;

/**
 * Created by hupeng on 16/6/15.
 * 景区指标数据
 */
public class ScenicSpotData {

    /**
     * 时间(yyyymm) int
     */
    private String event_month;

    /**
     * 景区标识 bigint
     */
    private String scenic_spot_id;

    /**
     * 地市标识 int
     */
    private String city_id;

    /**
     * 达标rsrp次数 bigint
     */
    private String good_rsrp_cnt;

    /**
     * 未达标rsrp次数 bigint
     */
    private String bad_rsrp_cnt;

    /**
     * 总rsrp次数 bigint
     */
    private String total_rsrp_cnt;

    /**
     * 栅格弱覆盖比例 decimal(8,6)
     */
    private String weak_rat;

    /**
     * 4g流量 bigint
     */
    private String lte_flow;

    /**
     * 4g用户数 bigint
     */
    private String lte_user_cnt;
    /**
     * 2g流量 bigint
     */
    private String gsm_flow;
    /**
     * 2g栅格用户数 bigint
     */
    private String gsm_user_cnt;
    /**
     * 4g页面响应成功次数 bigint
     */
    private String page_resp_succ_cnt;
    /**
     * 4g页面请求次数 bigint
     */
    private String page_req_cnt;
    /**
     * 4g页面响应成功率 decimal(8,6)
     */
    private String page_resp_succ_rat;
    /**
     * 4g页面响应成功总时延 bigint
     */
    private String page_resp_succ_tot_delay;
    /**
     * 4g页面响应时长 decimal(15,4)
     */
    private String page_resp_dur;
    /**
     * 4g页面显示成功次数 bigint
     */
    private String page_display_succ_cnt;
    /**
     * 4g页面显示成功率 decimal(8,6)
     */
    private String page_display_succ_rat;
    /**
     * 4g页面显示成功总时延 bigint
     */
    private String page_display_succ_tot_delay;
    /**
     * 4g页面显示时长 decimal(15,4)
     */
    private String page_display_dur;
    /**
     * 4g视频播放成功次数 bigint
     */
    private String video_play_succ_cnt;
    /**
     * 4g视频播放请求次数 bigint
     */
    private String video_play_req_cnt;
    /**
     * 4g视频播放成功率 decimal(8,6)
     */
    private String video_play_succ_rat;
    /**
     * 4g视频下行流量 bigint
     */
    private String video_dl_flow;
    /**
     * 4g视频下行传输时长 bigint
     */
    private String video_dl_dur;
    /**
     * 4g视频下载速率（kbps） decimal(15,5)
     */
    private String video_dl_rate;
    /**
     * 4gim业务登陆成功次数 bigint
     */
    private String im_load_succ_cnt;
    /**
     * 4gim业务登陆请求次数 bigint
     */
    private String im_load_req_cnt;
    /**
     * 4gim业务登陆成功率 decimal(8,6)
     */
    private String im_load_succ_rat;
    /**
     * 4g应用下载成功次数 bigint
     */
    private String app_dl_succ_cnt;
    /**
     * 4g应用下载请求次数 bigint
     */
    private String app_dl_req_cnt;
    /**
     * 4g应用下载成功率 decimal(8,6)
     */
    private String app_dl_succ_rat;
    /**
     * 4g应用下行流量 bigint
     */
    private String app_dl_flow;
    /**
     * 4g应用下行传输时长 bigint
     */
    private String app_dl_dur;
    /**
     * 4g应用下载速率 decimal(15,5)
     */
    private String app_dl_rate;

    public String getEvent_month() {
        return event_month;
    }

    public void setEvent_month(String event_month) {
        this.event_month = event_month;
    }

    public String getScenic_spot_id() {
        return scenic_spot_id;
    }

    public void setScenic_spot_id(String scenic_spot_id) {
        this.scenic_spot_id = scenic_spot_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getGood_rsrp_cnt() {
        return good_rsrp_cnt;
    }

    public void setGood_rsrp_cnt(String good_rsrp_cnt) {
        this.good_rsrp_cnt = good_rsrp_cnt;
    }

    public String getBad_rsrp_cnt() {
        return bad_rsrp_cnt;
    }

    public void setBad_rsrp_cnt(String bad_rsrp_cnt) {
        this.bad_rsrp_cnt = bad_rsrp_cnt;
    }

    public String getTotal_rsrp_cnt() {
        return total_rsrp_cnt;
    }

    public void setTotal_rsrp_cnt(String total_rsrp_cnt) {
        this.total_rsrp_cnt = total_rsrp_cnt;
    }

    public String getWeak_rat() {
        return weak_rat;
    }

    public void setWeak_rat(String weak_rat) {
        this.weak_rat = weak_rat;
    }

    public String getLte_flow() {
        return lte_flow;
    }

    public void setLte_flow(String lte_flow) {
        this.lte_flow = lte_flow;
    }

    public String getLte_user_cnt() {
        return lte_user_cnt;
    }

    public void setLte_user_cnt(String lte_user_cnt) {
        this.lte_user_cnt = lte_user_cnt;
    }

    public String getGsm_flow() {
        return gsm_flow;
    }

    public void setGsm_flow(String gsm_flow) {
        this.gsm_flow = gsm_flow;
    }

    public String getGsm_user_cnt() {
        return gsm_user_cnt;
    }

    public void setGsm_user_cnt(String gsm_user_cnt) {
        this.gsm_user_cnt = gsm_user_cnt;
    }

    public String getPage_resp_succ_cnt() {
        return page_resp_succ_cnt;
    }

    public void setPage_resp_succ_cnt(String page_resp_succ_cnt) {
        this.page_resp_succ_cnt = page_resp_succ_cnt;
    }

    public String getPage_req_cnt() {
        return page_req_cnt;
    }

    public void setPage_req_cnt(String page_req_cnt) {
        this.page_req_cnt = page_req_cnt;
    }

    public String getPage_resp_succ_rat() {
        return page_resp_succ_rat;
    }

    public void setPage_resp_succ_rat(String page_resp_succ_rat) {
        this.page_resp_succ_rat = page_resp_succ_rat;
    }

    public String getPage_resp_succ_tot_delay() {
        return page_resp_succ_tot_delay;
    }

    public void setPage_resp_succ_tot_delay(String page_resp_succ_tot_delay) {
        this.page_resp_succ_tot_delay = page_resp_succ_tot_delay;
    }

    public String getPage_resp_dur() {
        return page_resp_dur;
    }

    public void setPage_resp_dur(String page_resp_dur) {
        this.page_resp_dur = page_resp_dur;
    }

    public String getPage_display_succ_cnt() {
        return page_display_succ_cnt;
    }

    public void setPage_display_succ_cnt(String page_display_succ_cnt) {
        this.page_display_succ_cnt = page_display_succ_cnt;
    }

    public String getPage_display_succ_rat() {
        return page_display_succ_rat;
    }

    public void setPage_display_succ_rat(String page_display_succ_rat) {
        this.page_display_succ_rat = page_display_succ_rat;
    }

    public String getPage_display_succ_tot_delay() {
        return page_display_succ_tot_delay;
    }

    public void setPage_display_succ_tot_delay(String page_display_succ_tot_delay) {
        this.page_display_succ_tot_delay = page_display_succ_tot_delay;
    }

    public String getPage_display_dur() {
        return page_display_dur;
    }

    public void setPage_display_dur(String page_display_dur) {
        this.page_display_dur = page_display_dur;
    }

    public String getVideo_play_succ_cnt() {
        return video_play_succ_cnt;
    }

    public void setVideo_play_succ_cnt(String video_play_succ_cnt) {
        this.video_play_succ_cnt = video_play_succ_cnt;
    }

    public String getVideo_play_req_cnt() {
        return video_play_req_cnt;
    }

    public void setVideo_play_req_cnt(String video_play_req_cnt) {
        this.video_play_req_cnt = video_play_req_cnt;
    }

    public String getVideo_play_succ_rat() {
        return video_play_succ_rat;
    }

    public void setVideo_play_succ_rat(String video_play_succ_rat) {
        this.video_play_succ_rat = video_play_succ_rat;
    }

    public String getVideo_dl_flow() {
        return video_dl_flow;
    }

    public void setVideo_dl_flow(String video_dl_flow) {
        this.video_dl_flow = video_dl_flow;
    }

    public String getVideo_dl_dur() {
        return video_dl_dur;
    }

    public void setVideo_dl_dur(String video_dl_dur) {
        this.video_dl_dur = video_dl_dur;
    }

    public String getVideo_dl_rate() {
        return video_dl_rate;
    }

    public void setVideo_dl_rate(String video_dl_rate) {
        this.video_dl_rate = video_dl_rate;
    }

    public String getIm_load_succ_cnt() {
        return im_load_succ_cnt;
    }

    public void setIm_load_succ_cnt(String im_load_succ_cnt) {
        this.im_load_succ_cnt = im_load_succ_cnt;
    }

    public String getIm_load_req_cnt() {
        return im_load_req_cnt;
    }

    public void setIm_load_req_cnt(String im_load_req_cnt) {
        this.im_load_req_cnt = im_load_req_cnt;
    }

    public String getIm_load_succ_rat() {
        return im_load_succ_rat;
    }

    public void setIm_load_succ_rat(String im_load_succ_rat) {
        this.im_load_succ_rat = im_load_succ_rat;
    }

    public String getApp_dl_succ_cnt() {
        return app_dl_succ_cnt;
    }

    public void setApp_dl_succ_cnt(String app_dl_succ_cnt) {
        this.app_dl_succ_cnt = app_dl_succ_cnt;
    }

    public String getApp_dl_req_cnt() {
        return app_dl_req_cnt;
    }

    public void setApp_dl_req_cnt(String app_dl_req_cnt) {
        this.app_dl_req_cnt = app_dl_req_cnt;
    }

    public String getApp_dl_succ_rat() {
        return app_dl_succ_rat;
    }

    public void setApp_dl_succ_rat(String app_dl_succ_rat) {
        this.app_dl_succ_rat = app_dl_succ_rat;
    }

    public String getApp_dl_flow() {
        return app_dl_flow;
    }

    public void setApp_dl_flow(String app_dl_flow) {
        this.app_dl_flow = app_dl_flow;
    }

    public String getApp_dl_dur() {
        return app_dl_dur;
    }

    public void setApp_dl_dur(String app_dl_dur) {
        this.app_dl_dur = app_dl_dur;
    }

    public String getApp_dl_rate() {
        return app_dl_rate;
    }

    public void setApp_dl_rate(String app_dl_rate) {
        this.app_dl_rate = app_dl_rate;
    }
}
