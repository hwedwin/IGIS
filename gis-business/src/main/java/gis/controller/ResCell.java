package gis.controller;

import bean.enums.ErrorCode;
import gis.algorithm.cluster.RqCluster;
import gis.core.utils.CollUtils;
import gis.core.utils.Serializer;
import gis.dao.ResCellDao;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import gis.toolkit.CellToGrid;
import gis.toolkit.GaodoHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import bean.common.*;

@Controller("Res")
public class ResCell {
    private static Logger logger = LoggerFactory.getLogger(ResCell.class);

    @RequestMapping(value = "/getbts", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Bts> GetBtsWithCluster(HttpServletRequest request, HttpServletResponse response) throws IOException {

        //GaodoHelper h = new GaodoHelper();
        //h.jiCell();

        //CellToGrid cellToGrid = new CellToGrid();
        //cellToGrid.createCellGrid();
        //cellToGrid.createCellTo20Grid();

        List<Bts> list = null;
        try {
            String extent = request.getParameter("extent");
            RqCluster rqCluster = RqCluster.fromJson(extent);

            String rqcell = request.getParameter("rqcell");
            RqCell rqCell = Serializer.deserializeJson(rqcell, RqCell.class);

            if (rqCell.getRegion() == null) {
                Region region = new Region();
                region.setType(9000);
                region.setId("1");
                rqCell.setRegion(region);
            }
            ResCellDao dao = new ResCellDao();
            list = dao.GetBtsList(rqCell, rqCluster);
        } catch (Exception e) {
            response.getWriter().write(e.getMessage());
            logger.error("GetBtsWithCluster错误", e);
        }

        return list;
    }

    @RequestMapping(value = "/gecells", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Output GetCellWithCluster(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Output output = new Output();
        List<Cell> list = null;
        try {
            String extent = request.getParameter("extent");
            RqCluster rqCluster = RqCluster.fromJson(extent);

            String rqcell = request.getParameter("rqcell");
            RqCell rqCell = Serializer.deserializeJson(rqcell, RqCell.class);

            if (rqCell.getRegion() == null) {
                Region region = new Region();
                region.setType(9000);
                region.setId("1");
                rqCell.setRegion(region);
            }
            ResCellDao dao = new ResCellDao();
            list = dao.GetResCells(rqCell);
            list = dao.clusterCells(list, rqCluster);
            output.setData(list);

        } catch (Exception e) {
            response.getWriter().write(e.getMessage());
            logger.error("GetCellWithCluster错误", e);

            output.setData(-1);
            output.setErrorCode(ErrorCode.getErrorCode(e));
            output.setErrorMessage("查询小区明细出错：" + e.getMessage());
            output.setException(e);
            e.printStackTrace();
        }

        return output;
    }

    @RequestMapping(value = "/getbtstopn", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Bts> GetBtsTopN(HttpServletRequest request) {
        List<Bts> list = null;
        try {
            String key = request.getParameter("key");
            int topn = Integer.parseInt(request.getParameter("topn"));
            String rqcell = request.getParameter("rqcell");
            RqCell rqCell = Serializer.deserializeJson(rqcell, RqCell.class);

            if (rqCell.getRegion() == null) {
                Region region = new Region();
                region.setType(9000);
                region.setId("1");
                rqCell.setRegion(region);
            }
            ResCellDao dao = new ResCellDao();
            list = dao.GetBtsList(rqCell);

            list = CollUtils.topN(list, "name", key, topn);
        } catch (Exception e) {
            logger.error("获取GetBtsTopN出差:" + e.getMessage(), e);
        }
        return list;
    }


    @RequestMapping(value = "/gecellswidthextend", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Output GetCellWithExtend(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Output output = new Output();
        List<Cell> list = null;
        try {
            String extent = request.getParameter("extent");
            RqCluster rqCluster = RqCluster.fromJson(extent);

            String rqcell = request.getParameter("rqcell");
            RqCell rqCell = Serializer.deserializeJson(rqcell, RqCell.class);

            if (rqCell.getRegion() == null) {
                Region region = new Region();
                region.setType(9000);
                region.setId("1");
                rqCell.setRegion(region);
            }
            ResCellDao dao = new ResCellDao();
            list = dao.GetResCells(rqCell);
            list = dao.GetExtendCells(list, rqCluster);
            output.setData(list);

        } catch (Exception e) {
            response.getWriter().write(e.getMessage());
            logger.error("GetCellWithCluster错误", e);

            output.setData(-1);
            output.setErrorCode(ErrorCode.getErrorCode(e));
            output.setErrorMessage("查询小区明细出错：" + e.getMessage());
            output.setException(e);
            e.printStackTrace();
        }

        return output;
    }
}
