package gis.dao;

import gis.common.ServiceFactory;
import gis.service.CommonService;

import java.util.List;

import bean.common.Region;

public class CommonDao {

	/**
	 * 获取地市
	 * @return
	 * @throws Exception
	 */
	public List<Region> GetRegions() throws Exception {
		return ServiceFactory.getInstance(CommonService.class).GetRegions();
	}
}
