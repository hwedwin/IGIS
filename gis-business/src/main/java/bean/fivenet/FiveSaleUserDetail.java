package bean.fivenet;

import gis.core.bean.KeyValue;

import java.util.List;

/**
 * Created by Xiaobing on 2016/11/29.
 */
public class FiveSaleUserDetail {
    private List<KeyValue> detail;
    private FiveCellInfo cell1;
    private FiveCellInfo cell2;
    private FiveCellInfo cell3;

    public List<KeyValue> getDetail() {
        return detail;
    }

    public void setDetail(List<KeyValue> detail) {
        this.detail = detail;
    }

    public FiveCellInfo getCell1() {
        return cell1;
    }

    public void setCell1(FiveCellInfo cell1) {
        this.cell1 = cell1;
    }

    public FiveCellInfo getCell2() {
        return cell2;
    }

    public void setCell2(FiveCellInfo cell2) {
        this.cell2 = cell2;
    }

    public FiveCellInfo getCell3() {
        return cell3;
    }

    public void setCell3(FiveCellInfo cell3) {
        this.cell3 = cell3;
    }
}
