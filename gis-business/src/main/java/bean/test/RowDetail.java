package bean.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Xiaobing on 2016/9/13.
 */
public class RowDetail {
    private String key;
    private List<Map<String, String>> Cell = new ArrayList<>();

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public List<Map<String, String>> getCell() {
        return Cell;
    }

    public void setCell(List<Map<String, String>> cell) {
        Cell = cell;
    }
}
