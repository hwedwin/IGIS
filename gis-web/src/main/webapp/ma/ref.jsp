<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/css/font-awesome-4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/script/plugins/scrollbar/perfect-scrollbar-0.4.10.min.css">
<link rel="stylesheet" type="text/css" href="/ma/css/ma-public.css">
<link rel="stylesheet" type="text/css" href="/ma/css/ma-core.css">
<link rel="stylesheet" type="text/css" href="/script/base/artcloudui/skins/default/css/artcloudui.css">

<script type="text/javascript" src="/script/base/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/ma/js/base.js"></script>
<script type="text/javascript" src="/script/plugins/scrollbar/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/script/plugins/scrollbar/perfect-scrollbar.js"></script>
<script type="text/javascript" src="/script/base/artcloudui/artcloudui.dialog.min.js"></script>