<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
    <%@ include file="header.jsp" %>
    <section>
        <div class="container">
            <%@ include file="leftmenu.jsp" %>
            <input id="pageid" type="hidden" value="baseinfoxml">

            <div class="docs-content" style="height: 1200px;">
                <h3 id="welcome" class="welcome">基础信息配置</h3>
                <br/>

                <p>resources/gisconfig文件下存放的是配置信息，每个部署站点有单独的文件价存放。在resources/文件夹下有area.xml这文件，
                    用来配置系统启动时读取哪个文件夹信息。</p>

                <p>下面介绍一下baseinfo.*****.properties</p>
            <pre class="prettyprint">
title = 中国工程科技知识中心GIS平台
&lt;!--是否转包   --&gt;
istransfer = true
&lt;!--地图类型  arcgis(4326) tiandi(4326) google(4326) gaode purplishblue  --&gt;
gistype = gaode
&lt;!--坐标系  4326 102100  --&gt;
wkid=4326
#默认地图初始化位置
extent=0.47903736310876,-30.246665555064755,170.01059044576762,63.365727972380895;
#地图地址 转包用
#arcgis地图
arcgisurl = http://10.190.95.90:6080/arcgis/rest/services/cqmap/MapServer
#默认数据缓存时间 12小时  如果不需要设置缓存  设置成0
cachetimeout=43200000
#地图最大图层
maxmaplevel=18

#自定义参数
#是否测试
istest=false
#潮汐数据接口
tideserviceurl = http://120.27.24.107:9100/tide
            </pre>
                <p>部分参数说明</p>

                <p>istransfer:参数控制，在请求底图服务器是，是否需要经过本站点服务端中转，如果浏览器端无法访问底图服务，就可以通过这种方式中转。</p>

                <p>arcgisurl:只有在gistype=arcgis时，系统在初始化底图服务时读取此参数，其他类型底图，此参数无效，系统已完成封装。</p>

                <p>自定义参数部分定义的参数，在程序开发的时候，如下方式通过关键字获取：</p>
          <pre class="prettyprint">
              <br/>
 String tideserviceurl= GisConfig.GetInstance().getUtil().getProperty("tideserviceurl");
          </pre>
                <a href="igisdeveloper.jsp" type="button" class="btn-link">上一页</a>
                <a href="menuxml.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>
            </div>
        </div>
    </section>
</div>
</body>
</html>
