package gis.web.demo;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import gis.algorithm.Extent;
import gis.algorithm.GeoPie;
import gis.algorithm.GeoPoint;
import gis.algorithm.GeoUtils;
import gis.algorithm.Legend;
import gis.algorithm.RenderMethod;
import gis.algorithm.density.DensityArg;
import gis.algorithm.density.DensityCalculator;
import gis.algorithm.density.RqDensity;
import gis.algtest.BizCell;
import gis.algtest.BizService;


/**
 *
 */
@Controller("Service_Density")
@RequestMapping({"/service/density"})
public class Density {
    private static Logger logger = LoggerFactory.getLogger(Density.class);

    /**
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        RqDensity rq;

        try {
            int x = Integer.parseInt(request.getParameter("x"));
            int y = Integer.parseInt(request.getParameter("y"));
            int z = Integer.parseInt(request.getParameter("z"));
            int wkid = Integer.parseInt(request.getParameter("wkid"));
            double resolution = Double.parseDouble(request.getParameter("r"));
            double originX = Double.parseDouble(request.getParameter("ox"));
            double originY = Double.parseDouble(request.getParameter("oy"));

            rq = new RqDensity();
            rq.setX(x);
            rq.setY(y);
            rq.setZ(z);
            rq.setWkid(wkid);
            rq.setResolution(resolution);
            rq.setOriginX(originX);
            rq.setOriginY(originY);
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            Output(response, null);
            return;
        }

        // 构造请求图片的边界数据
        GeoPoint sPoint = GeoUtils.getLeftTopPoint(rq.getOriginX(),
                rq.getOriginY(), rq.getResolution(), rq.getX(), rq.getY());
        GeoPoint ePoint = GeoUtils.getRightBottomPoint(rq.getOriginX(),
                rq.getOriginY(), rq.getResolution(), rq.getX(), rq.getY());
        Extent extent = new Extent(sPoint.getX(), ePoint.getX(), ePoint.getY(),
                sPoint.getY());

        List<DensityArg> args = getArgs(rq, extent);
        List<Legend> legends = getLegends();

        DensityCalculator calc = DensityCalculator.getInstance();
        BufferedImage image = calc.createPointImage(rq, args, extent, legends);
        Output(response, image);

        return;
    }

    /**
     * 获取密度图算法基础数据
     *
     * @param rq
     * @param extent
     * @return
     */
    private List<DensityArg> getArgs(RqDensity rq, Extent extent) {
        List<DensityArg> args = new ArrayList<DensityArg>();
        BizCell[] cells = BizService.getInstance().getCells();
        int wkid = rq.getWkid();

        for (int i = 0; i < cells.length; i++) {
            BizCell cell = cells[i];
            double lon = cell.getLongitude();
            double lat = cell.getLatitude();

            GeoPoint point;
            if (wkid == 4326) {
                point = new GeoPoint(lon, lat);
            } else if (wkid == 102100) {
                point = GeoUtils.lonLatToMercator(lon, lat);
            } else {
                throw new NotImplementedException(String.valueOf(wkid));
            }

            // 构造扇形对象
            int radius = cell.getCoverRadius();
            double startAngle = cell.getAntennaAzimuth() - cell.getAntbw();
            double sweepAngle = cell.getAntbw() * 2;
            GeoPie pie = new GeoPie(point, radius, startAngle, sweepAngle, wkid);

            // 判断小区是否在请求图片的范围内
            boolean isNotIn = GeoUtils.extentIsNotInExtent(extent,
                    pie.getRingExtent());
            if (isNotIn) {
                continue;
            }

            DensityArg arg = new DensityArg();
            arg.setAntbw(cell.getAntbw());
            arg.setAntennaAzimuth(cell.getAntennaAzimuth());
            arg.setArea(pie.getArea());
            arg.setCoverRadius(cell.getCoverRadius());
            arg.setLatitude(point.getY());
            arg.setLongitude(point.getX());
            arg.setRenderMethod(RenderMethod.Sum);
            arg.setRingExtent(pie.getRingExtent());
            arg.setRingPoints(pie.getRingPoints());
            arg.setValue(cell.getTchTraf());
            args.add(arg);
        }

        return args;
    }

    /**
     * 获取当前图例设置
     *
     * @return
     */
    private List<Legend> getLegends() {
        List<Legend> legends = new ArrayList<Legend>();

        Legend legend1 = new Legend();
        legend1.setColor(Color.GREEN);
        legend1.setValue(0);
        legend1.setText(String.valueOf(legend1.getValue()));
        legends.add(legend1);

        Legend legend2 = new Legend();
        legend2.setColor(Color.RED);
        legend2.setValue(300);
        legend2.setText(String.valueOf(legend2.getValue()));
        legends.add(legend2);

        return legends;
    }

    /**
     * @param response
     * @param image
     */
    private void Output(HttpServletResponse response, BufferedImage image) {
        response.setContentType("image/png");
        response.setHeader("pragma", "no-cache");
        response.setHeader("cache-control", "no-cache");
        response.setDateHeader("expires", -1);

        if (image != null) {
            ServletOutputStream output;
            try {
                output = response.getOutputStream();
                ImageIO.write(image, "png", output);
                output.close();
            } catch (IOException e) {
            }
        }
    }
}
