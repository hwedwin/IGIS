<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <title>iGIS地理信息系统</title>
    <!-- style -->
    <link href="/portal/nx/css/style.css" rel="stylesheet" type="text/css">
    <link href="/portal/nx/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/portal/nx/css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="/portal/nx/css/responsive.css" rel="stylesheet" type="text/css">
    <link href="/portal/nx/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/portal/nx/css/animate.min.css" rel="stylesheet" type="text/css">
    <link href="/portal/nx/css/popup.css" rel="stylesheet" type="text/css">

</head>

<body class="module-home" data-spy="scroll" data-target=".navbar">

<!-- header -->
<header role="header" class="header-top" id="headere-top">


    <div class="header-fixed-wrapper" role="header-fixed">
        <div class="container">
            <!-- hgroup -->
            <hgroup class="row">
                <!-- logo -->
                <div class="col-xs-5 col-sm-2 col-md-2 col-lg-2">
                    <h1>
                        <a href="#headere-top" title="Rooky"><img src="/portal/nx/images/logo.png" alt="Rooky" title="Rooky"/></a>
                    </h1>
                </div>
                <!-- logo -->

                <!-- nav -->
                <nav role="navigation" class="col-xs-12 col-sm-10 col-md-10 col-lg-10 navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="/" title="Home">首页</a></li>
                            <li><a target="_blank" href="/default" title="Overview">菜单墙</a></li>
                            <li><a target="_blank" href="/api/index.jsp" title="Features">开发文档</a></li>
                            <li><a href="#" title="Pricing">关于我们</a></li>
                        </ul>
                    </div>
                </nav>
                <!-- nav -->
            </hgroup>
            <!-- hgroup -->
        </div>
    </div>

    <!-- banner Text -->

    <section class="text-center">
        <h2>统一GIS开发平台</h2>

        <p>专业GIS开发框架 提供更好的应用开发平台</p>
    </section>
    <!-- banner Text -->
    <!-- banner image -->
    <figure>

        <div class="parallax-window item tp-banner-container" data-parallax="scroll"
             data-image-src="/portal/nx/images/bg.jpg"></div>

    </figure>
    <!-- banner image -->

</header>

<!-- header -->


<!-- main -->

<main role="main" id=" main-wrapper">

    <!-- section-one -->

    <section class="section-one text-center" id="section-one">

        <div class="container">

            <header role="title-page">

                <h4>What we do</h4>

                <h2>We do more work, and gain more.</h2>

            </header>

            <article>

                <p>灵活的部署方法，插件式的开发模式，使平台以更灵活的方式存在，更利于扩展、二次开发和集成。<br/>

                    平台集成了ArcGis、高德地图、天地图、深色大屏背景等主流底图服务，无论是在内网还是外网，都能完成部署。<br/>

                    前端提供完善的JS开发框架，后端框架还提供了大量的算法支持。</p>

            </article>


            <!-- four boxes  -->

            <div class="row four-box-pan" role="four-box">


                <section class="col-xs-12 col-sm-6 col-md-3">

                    <figure><i class="fa fa-rocket" aria-hidden="true"></i></figure>

                    <h5>算法支持</h5>

                    <p>平台提供大量算法，为业务提供很好支撑。</p>

                </section>

                <section class="col-xs-12 col-sm-6 col-md-3">

                    <figure><i class="fa fa-magic" aria-hidden="true"></i></figure>

                    <h5>前端融合</h5>

                    <p>融合多家地图服务，更灵活的适配不同的业务场景</p>

                </section>

                <section class="col-xs-12 col-sm-6 col-md-3">

                    <figure><i class="fa fa-television" aria-hidden="true"></i></figure>

                    <h5>快速开发</h5>

                    <p>基于完整的前端API和后端算法支撑，能进行快速开发，快速部署。</p>

                </section>

                <section class="col-xs-12 col-sm-6 col-md-3">

                    <figure><i class="fa fa-paper-plane" aria-hidden="true"></i></figure>

                    <h5>快速集成</h5>

                    <p>采用模块化开发，可以和外部系统快速集成。</p>

                </section>


            </div>

            <!-- four boxes -->


        </div>

    </section>

    <!-- section-one -->


    <div class="copyrights">Collect from <a href="http://www.cssmoban.com/">手机网站模板</a></div>
    <!-- section-two -->

    <section class="section-two" id="section-two">


        <!-- image-content -->

        <section>

            <div class="col-xs-12 col-sm-6 col-md-6">

                <article>

                    <h2>客户感知-用户轨迹分析.</h2>

                    <p>提供客户轨迹地图呈现功能，精确定位客户投诉时间点所处小区位置，以及协助客服人员查看客户投诉时间段内的地理位置轨迹变化详情。</p>

                    <ul>

                        <li>更快速定位问题小区</li>

                        <li>更精准分析问题原因</li>

                        <li>更直观展示用户轨迹</li>

                        <li>我们所有的努力 都是为了给用户提供更好的服务</li>

                    </ul>

                </article>

            </div>

            <div class="col-xs-12 col-sm-6 col-md-6">

                <figure class="row" style="background-image:url('/portal/nx/images/bg2.jpg')"></figure>

            </div>

        </section>

        <!-- image-content -->

        <div class="clearfix"></div>

        <!-- image-content -->

        <section>

            <div class="col-xs-12 col-sm-6 col-md-6">

                <figure class="row" style="background-image:url('/portal/nx/images/bg3.jpg')"></figure>

            </div>

            <div class="col-xs-12 col-sm-6 col-md-6">

                <article>

                    <h2>精准定位</h2>

                    <p>根据用户描述，精准定位到用户周边可能出现的问题小区，更方便快捷。 </p>

                    <p>将小区、POI点等关键点信息结合起来搜索，快速定位用户可能的位置，再将选中的经纬度信息输出到系统中。</p>

                </article>

            </div>

        </section>

        <!-- image-content -->

        <div class="clearfix"></div>

    </section>

    <!-- section-two -->


    <!-- section-four -->

    <section class="section-three" id="section-three">

        <div class="container">

            <header role="title-page" class="text-center">

                <h4>Making Effort</h4>

                <h2>新版GIS提供基础工具和特色功能。

            </header>

            <!-- Team -->

            <div class="team-pan row">


                <section class="col-xs-12 col-sm-6 col-md-3">

                    <figure><img src="/portal/nx/images/01.png" alt="" class=" img-responsive"/></figure>

                    <header>

                        <h5>深色背景</h5>

                        <h6>大屏 迁徙图</h6>

                    </header>

                </section>

                <section class="col-xs-12 col-sm-6 col-md-3">

                    <figure><img src="/portal/nx/images/02.png" alt="" class="img-responsive"/></figure>

                    <header>

                        <h5>经纬度信息获取</h5>

                        <h6>根据经纬度定位 或者在地图上获取点线面的经纬度</h6>

                    </header>

                </section>

                <section class="col-xs-12 col-sm-6 col-md-3">

                    <figure><img src="/portal/nx/images/03.png" alt="" class=" img-responsive"/></figure>

                    <header>

                        <h5>基站展示</h5>

                        <h6>将基站（小区）在地图上集中展示</h6>

                    </header>

                </section>

                <section class="col-xs-12 col-sm-6 col-md-3">

                    <!--<figure><img src="images/04.png" alt="" class=" img-responsive"/></figure>-->
                    <figure><img src="http://placehold.it/249x263" alt="" class=" img-responsive"/></figure>

                    <header>

                        <h5>测距</h5>

                        <h6>计算地图上点到点的距离</h6>

                    </header>

                </section>


            </div>

            <!-- Team -->

        </div>

    </section>

    <!-- section-fore -->


    <!-- footer -->

    <footer role="footer" class="footer text-center">

        <div class="container">

            <!-- socil-icons

            <section role="socil-icons" class="socil-icons">

                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>

                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>

                <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>

                <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>

                <a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a>

                <a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a>

            </section> -->

            <!-- socil-icons -->

            <!-- nav -->

            <nav role="footer-nav">

                <a href="#">Terms of Use </a>

                <a href="#">Privacy Policy</a>

            </nav>

            <!-- nav -->

            <p class="copy">&copy; 2016 Sartr. All rights reserved. 浪潮软件集团</p>

        </div>

    </footer>

    <!-- footer -->


</main>

<!-- main -->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="/portal/nx/js/jquery.min.js" type="text/javascript"></script>
<script src="/portal/nx/js/parallax.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $('.parallax-window').parallax({});
</script>
<script src="/portal/nx/js/main.js" type="text/javascript"></script>
<script src="/portal/nx/js/owl.carousel.js" type="text/javascript"></script>
<script src="/portal/nx/js/custom.js" type="text/javascript"></script>
<script src="/portal/nx/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="/portal/nx/js/jquery.contact.js" type="text/javascript"></script>
<script src="/portal/nx/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/portal/nx/js/html5shiv.min.js" type="text/javascript"></script>
</body>

</html>