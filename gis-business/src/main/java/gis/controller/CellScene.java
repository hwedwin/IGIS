package gis.controller;

import bean.cityfun.AreaDimSceneType;
import bean.cityfun.UserConfig;
import bean.common.*;
import bean.enums.ErrorCode;
import gis.core.bean.KeyValue;
import gis.core.utils.CSVUtils;
import gis.core.utils.Serializer;
import gis.core.utils.TimeUtils;
import gis.dao.CellSceneDao;
import gis.dao.CityFunDao;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by hupeng on 16/6/8.
 */
@Controller("CellScene")
public class CellScene {
    private static Logger logger = LoggerFactory.getLogger(CellScene.class);

    @RequestMapping(value = "/getscellcenetypelist", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<AreaDimSceneType> GetSceneTypeList(HttpServletRequest request) {

        List<AreaDimSceneType> list = null;
        try {
            list = new CellSceneDao().GetAreaDimSceneTypeList();
        } catch (Exception e) {
            logger.error("获取场景分类出错:" + e.getMessage(), e);
        }
        return list;
    }

    @RequestMapping(value = "/etgsceneapptopics", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Output GetSceneAppTopics(HttpServletRequest request, HttpServletResponse response) {
        Output output = new Output();
        try {
            List<KeyValue> list = new CellSceneDao().GetSceneAppTopics();
            output.setData(list);
        } catch (Exception e) {
            logger.error("获取场景分类出错:" + e.getMessage(), e);
            output.setData(-1);
            output.setErrorCode(ErrorCode.getErrorCode(e));
            output.setErrorMessage(e.getMessage());
            output.setException(e);
            e.printStackTrace();
        }
        return output;
    }

    @RequestMapping(value = "/getcellscenelist", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Output GetCellSceneList(HttpServletRequest request, HttpServletResponse response) {
        Output output = new Output();
        try {
            String sceneTypeId = request.getParameter("sceneid");
            String cityId = request.getParameter("cityid");
            String apptopicsid = request.getParameter("apptopicsid");
            CellSceneDao dao = new CellSceneDao();
            List<CellDimScene> list = dao.GetSceneList(sceneTypeId, cityId, apptopicsid);
            output.setData(list);
        } catch (Exception e) {
            logger.error("获取用户自定义场景出错" + e.getMessage(), e);
            output.setData(-1);
            output.setErrorCode(ErrorCode.getErrorCode(e));
            output.setErrorMessage("获取用户自定义场景出错：" + e.getMessage());
            output.setException(e);
            e.printStackTrace();
        }
        return output;
    }

    @RequestMapping(value = "/delcellscene", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Output DelCellScene(HttpServletRequest request, HttpServletResponse response) {
        Output output = new Output();
        try {
            String id = request.getParameter("id");
            CellSceneDao dao = new CellSceneDao();
            int i = dao.DeleteCellDimScene(id);
            output.setData(i);
        } catch (Exception e) {
            logger.error("删除用户自定义场景出错" + e.getMessage(), e);
            output.setData(-1);
            output.setErrorCode(ErrorCode.getErrorCode(e));
            output.setErrorMessage("删除用户自定义场景出错：" + e.getMessage());
            output.setException(e);
            e.printStackTrace();
        }
        return output;
    }

    @RequestMapping(value = "/addcellscene", produces = "application/json")
    @ResponseBody
    public Output AddCellScene(HttpServletRequest request, HttpServletResponse response) {
        Output output = new Output();
        try {
            String ringName = java.net.URLDecoder.decode(request.getParameter("ringName"), "UTF-8");
            String rings = request.getParameter("rings");
            int ringType = NumberUtils.toInt(request.getParameter("ringType"));
            String sceneTypeId = request.getParameter("sceneTypeId");
            String sceneTypeName = java.net.URLDecoder.decode(request.getParameter("sceneTypeName"), "UTF-8");
            String area_extend = request.getParameter("area_extend");
            String cells = request.getParameter("cells");
            String cellType = request.getParameter("cellType");
            String appTopicsId = request.getParameter("apptopicsid");
            String appTopicsName = request.getParameter("apptopicsname");

            Region region = Serializer.deserializeJson(request.getParameter("region"), Region.class);

            CellSceneDao dao = new CellSceneDao();
            long sceneId = dao.AddCellDimScene(ringName, rings, ringType, sceneTypeId, sceneTypeName, area_extend, cells, cellType, region, appTopicsId, appTopicsName);
            output.setData(String.valueOf(sceneId));
        } catch (Exception e) {
            logger.error("添加用户自定义场景出错" + e.getMessage(), e);
            output.setData(-1);
            output.setErrorCode(ErrorCode.getErrorCode(e));
            output.setErrorMessage("添加用户自定义场景出错：" + e.getMessage());
            output.setException(e);
            e.printStackTrace();
        }
        return output;
    }

    @RequestMapping(value = "/loadscenedetailcsv", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Output loadSceneDetailCSV(HttpServletRequest request, HttpServletResponse response) {
        Output output = new Output();
        try {
            String sceneTypeId = request.getParameter("scenetypetd");
            String cityId = request.getParameter("cityid");
            String appTopicsId = request.getParameter("apptopicsid");
            List<CellRefDimSceneExpot> list = new CellSceneDao().GetLExpotData(sceneTypeId, cityId, appTopicsId);

            List<KeyValue> cols = new ArrayList<>();
            cols.add(new KeyValue("appTopicsId", "应用专题ID"));
            cols.add(new KeyValue("appTopicsName", "应用专题名称"));
            cols.add(new KeyValue("scene_id", "场景ID"));
            cols.add(new KeyValue("scene_name", "场景名称"));
            cols.add(new KeyValue("scene_type_id", "场景类型ID"));
            cols.add(new KeyValue("scene_type_name", "场景类型名称"));
            cols.add(new KeyValue("id", "小区ID"));
            cols.add(new KeyValue("name", "小区名称"));
            cols.add(new KeyValue("cell_type", "小区类型"));
            cols.add(new KeyValue("tac", "TAC"));
            cols.add(new KeyValue("eci", "CI/ECI"));
            cols.add(new KeyValue("longitude", "经度"));
            cols.add(new KeyValue("latitude", "纬度"));
            cols.add(new KeyValue("azimuth", "方向角"));
            cols.add(new KeyValue("cityId", "场景所属地市ID"));
            cols.add(new KeyValue("cityName", "场景所属地市名称"));

            String id = "SceneDetail_" + TimeUtils.dateConvertString(new Date(), "HHmmss");
            String fid = new CSVUtils().exportCsvMap(cols, list, id);
            if (StringUtils.isNotEmpty(fid)) {
                output.setData(fid);
            } else {
                output.setErrorCode(ErrorCode.BusError);
                output.setErrorMessage("导出场景数据出错");
            }

        } catch (Exception e) {
            logger.error("导出场景数据出错" + e.getMessage(), e);
            output.setErrorCode(ErrorCode.getErrorCode(e));
            output.setErrorMessage("导出场景数据出错：" + e.getMessage());
            output.setException(e);
            e.printStackTrace();
        }
        return output;
    }

    @RequestMapping(value = "/getcellrefscenelist", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Output GetCellRefSceneList(HttpServletRequest request, HttpServletResponse response) {
        Output output = new Output();
        try {
            String sceneid = request.getParameter("sceneid");
            CellSceneDao dao = new CellSceneDao();
            List<CellRefDimScene> list = dao.GetCellRefSceneList(sceneid);
            output.setData(list);
        } catch (Exception e) {
            logger.error("获取场景小区出错" + e.getMessage(), e);
            output.setData(-1);
            output.setErrorCode(ErrorCode.getErrorCode(e));
            output.setErrorMessage("获取场景小区出错：" + e.getMessage());
            output.setException(e);
            e.printStackTrace();
        }
        return output;
    }

    /**
     * 文件方式导入portal
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/imporscenefile", method = RequestMethod.POST)
    @ResponseBody
    public String importPortalAsFile(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String msg = "导入成功";
        try {
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
            for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
                MultipartFile mf = entity.getValue();
                String sceneStr = new String(mf.getBytes(), "utf-8");

                logger.info("导入场景数据：" + sceneStr);
                CellSceneDao dao = new CellSceneDao();
                dao.LoadImPortSceneData(sceneStr);
            }
        } catch (Exception e) {
            logger.error("导入场景失败：" + e.getMessage());
            throw new Exception("导入场景失败：" + e.getMessage());
            //msg = "导入失败：" + e.getMessage();
        }

        return msg;
    }

}
