<%@ page language="java" pageEncoding="UTF-8" %>
<meta charset="utf-8">
<meta name="description" content="">
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">
<meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>iGIS开发说明文档</title>
<script src="/api/js/jquery.min.js"></script>
<script type="text/javascript" src="/api/js/prettify/prettify.js"></script>
<script src="/api/js/layout.js"></script>
<%--<link rel="alternate" type="application/rss+xml" title="" href="/api/feed/index.html">--%>
<link rel="stylesheet" href="/api/css/style.css">
<link rel="stylesheet" href="/api/css/prettify.css">