package bean.enums;

public enum ResType {
    none, GSMBTS, TDBTS, LTEBTS, GSMCELL, TDCELL, LTECELL, ENODEB, BTS, OLT
}
