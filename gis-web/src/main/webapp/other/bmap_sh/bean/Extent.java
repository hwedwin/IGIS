﻿package gis.algorithm;

public class Extent {
    private double xMin;
    private double xMax;
    private double yMin;
    private double yMax;
    private int level;
    private double resolution;
    private int wkid;

    public Extent() {

    }

    /**
     * @param xMin
     * @param xMax
     * @param yMin
     * @param yMax
     */
    public Extent(double xMin, double xMax, double yMin, double yMax) {
        this.xMin = xMin;
        this.xMax = xMax;
        this.yMin = yMin;
        this.yMax = yMax;
    }

    /**
     * 获取中心点
     */
    public GeoPoint getCenter() {
        return new GeoPoint(xMax - (xMax - xMin) / 2, yMax - (yMax - yMin) / 2);
    }

    /**
     * 判断点是否在区域内
     *
     * @return
     */
    public boolean contain(double x, double y) {
        if (x > xMax || x < xMin || y > yMax || y < yMin) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{xMin:" + xMin + ",xMax:" + xMax + ",yMin:" + yMin + ",yMax:"
                + yMax + "}";
    }

    /**
     * @return the xMin
     */
    public double getxMin() {
        return xMin;
    }

    /**
     * @param xMin the xMin to set
     */
    public void setxMin(double xMin) {
        this.xMin = xMin;
    }

    /**
     * @return the xMax
     */
    public double getxMax() {
        return xMax;
    }

    /**
     * @param xMax the xMax to set
     */
    public void setxMax(double xMax) {
        this.xMax = xMax;
    }

    /**
     * @return the yMin
     */
    public double getyMin() {
        return yMin;
    }

    /**
     * @param yMin the yMin to set
     */
    public void setyMin(double yMin) {
        this.yMin = yMin;
    }

    /**
     * @return the yMax
     */
    public double getyMax() {
        return yMax;
    }

    /**
     * @param yMax the yMax to set
     */
    public void setyMax(double yMax) {
        this.yMax = yMax;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public double getResolution() {
        return resolution;
    }

    public void setResolution(double resolution) {
        this.resolution = resolution;
    }

    public int getWkid() {
        return wkid;
    }

    public void setWkid(int wkid) {
        this.wkid = wkid;
    }

    /**
     * 稀疏列表数据
     *
     * @param list
     * @param count 返回数量
     * @return
     */
    public static <T> List<T> Spares(List<T> list, int count) {
        if (list.size() > count) {
            List<T> temp = new ArrayList<T>();
            for (int i = 0; i < list.size(); i = i + 2) {
                if (i >= list.size()) {
                    break;
                }
                temp.add(list.get(i));

                int f = temp.size() + list.size() - i;
                if (f <= count && (list.size() - i) > 0 && i > 0) {
                    temp.addAll(CollUtils.page(list, list.size() - i, i));
                    break;
                }
            }
            if (temp.size() > count) {
                return Spares(temp, count);
            } else {
                return temp;
            }
        } else {
            return list;
        }
    }
}
