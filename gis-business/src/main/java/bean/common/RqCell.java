package bean.common;

/**
 * 查询小区信息 输入参数
 * 
 * @author Xiaobing
 *
 */
public class RqCell {

	private boolean GSM;
	private boolean TD;
	private boolean LTE;
	/**
	 * 0:全部 1：室外 2：室内 默认0:全部
	 */
	private int alonecell = 0;
	private Region region;

	public boolean isGSM() {
		return GSM;
	}

	public void setGSM(boolean gSM) {
		GSM = gSM;
	}

	public boolean isTD() {
		return TD;
	}

	public void setTD(boolean tD) {
		TD = tD;
	}

	public boolean isLTE() {
		return LTE;
	}

	public void setLTE(boolean lTE) {
		LTE = lTE;
	}

	/**
	 * 0:全部 1：室外 2：室内 默认0:全部
	 * 
	 * @return
	 */
	public int getAlonecell() {
		return alonecell;
	}

	/**
	 * 0:全部 1：室外 2：室内 默认0:全部
	 * 
	 * @param alonecell
	 */
	public void setAlonecell(int alonecell) {
		this.alonecell = alonecell;
	}

	/**
	 * 地市信息
	 * 
	 * @return
	 */
	public Region getRegion() {
		return region;
	}

	/**
	 * 地市信息
	 * 
	 * @param region
	 */
	public void setRegion(Region region) {
		this.region = region;
	}

}
