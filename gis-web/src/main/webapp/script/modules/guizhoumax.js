/**
 * Created by Xiaobing on 2016/5/9.
 */
function guizhoumax() {
    var scene_list = null;
    var scene_name = "全部场景";
    var scene_type = gis_common.getQueryString("type");
    var wmsLayer = null; //渲染图层
    var  wmsBgLayer = null; //渲染图层
    //添加渲染图层
    var loadWmsLayer = function (url) {
        if (wmsLayer != null || wmsLayer != undefined) {
            gis_core.removeLayer("guizhoumaxLayer")
        }
        if (url == null || url == undefined) {
            return;
        }

        require(['esri/layers/WMSLayer', 'esri/layers/WMSLayerInfo', 'dojo/domReady!'],
            function (WMSLayer, WMSLayerInfo) {
                wmsLayer = new WMSLayer(url, {
                    resourceInfo: {
                        extent: new esri.geometry.Extent(73.2672757, 18.37574, -109.66552734375, 133.29657, {
                            wkid: 4326
                        }),
                        layerInfos: []
                    },
                    visibleLayers: ['guizhoumaxgrid']
                });
                wmsLayer.id = "guizhoumaxLayer";
                gis_core.insertLayer("guizhoumaxLayer", "场景热力图", 2, wmsLayer);

            });

    };
    //添加渲染图层
    var loadWmsBgLayer = function (url) {
        if (wmsBgLayer != null || wmsBgLayer != undefined) {
            gis_core.removeLayer("guizhoumaxbgLayer")
        }
        var url = "/dynamicmap/blackbg/0000/1111";
        require(['esri/layers/WMSLayer', 'esri/layers/WMSLayerInfo', 'dojo/domReady!'],
            function (WMSLayer, WMSLayerInfo) {
                wmsBgLayer = new WMSLayer(url, {
                    resourceInfo: {
                        extent: new esri.geometry.Extent(73.2672757, 18.37574, -109.66552734375, 133.29657, {
                            wkid: 4326
                        }),
                        layerInfos: []
                    },
                    visibleLayers: ['guizhoumaxbg']
                });
                wmsBgLayer.id = "guizhoumaxbgLayer";
                gis_core.insertLayer("guizhoumaxbgLayer", "背景", 0, wmsBgLayer);

            });

    };

    var laodSceneNameExtent = function () {
        $.ajax({
            url: "/gethotcellscenenameextent",
            data: {
                scenetype: scene_type,
                scenename: scene_name
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                //gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var extent = eval(data);
                    if (extent != null && extent != undefined) {
                        var xSub = extent.xMax - extent.xMin;
                        var ySub = extent.yMax - extent.yMin;
                        var xMin = extent.xMin - xSub;
                        var xMax = extent.xMax + xSub;
                        var yMin = extent.yMin - ySub;
                        var yMax = extent.yMax + ySub;
                        gis_common.setExtent(xMax, yMax, xMin, yMin);
                        //gis_common.setExtent(extent.xMax, extent.yMax, extent.xMin, extent.yMin);
                    }
                    var url = "/dynamicmap/guizhoumax/" + scene_type + "/" + scene_name;
                    loadWmsLayer(url);
                });
            },
            complete: function () {
                // gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    var loadSceneName = function () {
        if (gis_common.stringIsNullOrWhiteSpace(scene_type) || gis_common.stringIsNullOrWhiteSpace(scene_type)) {
            scene_type = "jt";
        }
        $.ajax({
            url: "/gethotcellscenename",
            data: {
                scenetype: scene_type
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                //gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    scene_list = eval(data);
                    createSceneNameWidget();
                    laodSceneNameExtent();
                });
            },
            complete: function () {
                // gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    var createSceneNameHtml = function (callback) {
        var content = "<div class='select-box'>";
        content += "     <div class='select-rel'>";
        content += "         <a href='#' class='select-ed'><span id='guizhoumax_scene_name_select_title'>全部场景</span><i class='seldown'></i> </a>";
        content += "         <div class='select-drpd' style='display: none'>"
        content += "             <ul class='select-list'>";
        content += "                 <li>全部场景</li>";
        for (var i = 0; i < scene_list.length; i++) {
            content += "                 <li>" + scene_list[i] + "</li>";
        }
        content += "             </ul>"
        content += "         </div>";
        content += "     </div>";
        content += "</div>";

        var div = $(content);
        div.find(".select-ed").bind("click", function () {
            div.find(".select-drpd").show();
        });
        div.find(".select-list li").bind("click", function () {
            scene_name = $(this).html();
            div.find(".select-drpd").hide();
            div.find("#guizhoumax_scene_name_select_title").html(scene_name);
            laodSceneNameExtent();
        })
        callback(div);
    };

    var createSceneNameWidget = function () {
        gis_layout.createOtherWidget("guizhoumax_scene_name_widget", createSceneNameHtml);
    };

    this.loadMaxData = function () {
        loadSceneName();
        //var url = "/dynamicmap/guizhoumax/kpi/20160501";
        //loadWmsLayer(url);
    };
    /**
     * 初始化
     */
    this.init = function () {
        //加载库文件
        dojo.require("esri.layers.WMSLayerInfo");
        dojo.require("esri.layers.WMSLayer");
        loadWmsBgLayer();
        //注册模块
        gis_core.addPlugin(this);
    };
    /**
     * 销毁
     */
    this.destroy = function () {
        gis_core.removeLayer("endtoendwmsLayer");
        gis_core.removeExtentChange("EndToEnd");
    };
    /**
     * 模块信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "00_02_00",
        name: "贵州大屏"
    };
}
//功能类 全局类
var guiZhouMax = null;

function initGuiZhouMax() {
    if (guiZhouMax == null || guiZhouMax == undefined) {
        guiZhouMax = new guizhoumax();
        guiZhouMax.init();
    }
}

function guiZhouMaxLoad() {
    initGuiZhouMax();
    guiZhouMax.loadMaxData();
}




