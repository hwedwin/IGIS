package bean.ltecover;


import gis.core.bean.ResValue;

/**
 * Created by Xiaobing on 2017/3/16.
 */
public class GridScene extends ResValue {
    private double weakRat;
    private double lteFlow;
    private int lteUserCnt;
    private double gsmFlow;
    private int gsmUserCnt;

    public double getWeakRat() {
        return weakRat;
    }

    public void setWeakRat(double weakRat) {
        this.weakRat = weakRat;
    }

    public double getLteFlow() {
        return lteFlow;
    }

    public void setLteFlow(double lteFlow) {
        this.lteFlow = lteFlow;
    }

    public int getLteUserCnt() {
        return lteUserCnt;
    }

    public void setLteUserCnt(int lteUserCnt) {
        this.lteUserCnt = lteUserCnt;
    }

    public double getGsmFlow() {
        return gsmFlow;
    }

    public void setGsmFlow(double gsmFlow) {
        this.gsmFlow = gsmFlow;
    }

    public int getGsmUserCnt() {
        return gsmUserCnt;
    }

    public void setGsmUserCnt(int gsmUserCnt) {
        this.gsmUserCnt = gsmUserCnt;
    }
}
