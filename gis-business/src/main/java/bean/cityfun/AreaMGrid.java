package bean.cityfun;

/**
 * Created by hupeng on 16/5/23.
 * ads_f_area_m_grid
 * 栅格性能
 */
public class AreaMGrid {

    /**
     * 栅格标识
     */
    private String grid_id;

    /**
     * 最小经度
     */
    private String min_longitude;

    /**
     * 最大经度
     */
    private String max_longitude;

    /**
     * 最小纬度
     */
    private String min_latitude;

    /**
     * 最大纬度
     */
    private String max_latitude;

    /**
     * 流量(mb)
     */
    private String flow;

    /**
     * 用户数
     */
    private String usr_cnt;

    /**
     * 4g用户数
     */
    private String lte_usr_cnt;

    /**
     * 常驻人口数
     */
    private String permanent_usr_cnt;

    /**
     * 流动人口
     */
    private String flowing_usr_cnt;

    /**
     * 人均日通话分钟数
     */
    private String per_traffic;

    /**
     * 人均日流量（mb)
     */
    private String per_flow;

    /**
     * 人均月消费(元）
     */
    private String per_consumer;

    /**
     * 高消费用户数
     */
    private String high_value_usr_cnt;

    /**
     * 普通用户数
     */
    private String normal_value_usr_cnt;

    /**
     * 月标识
     */
    private int event_month;

    public int getEvent_month() {
        return event_month;
    }

    public void setEvent_month(int event_month) {
        this.event_month = event_month;
    }

    public String getGrid_id() {
        return grid_id;
    }

    public void setGrid_id(String grid_id) {
        this.grid_id = grid_id;
    }

    public String getMin_longitude() {
        return min_longitude;
    }

    public void setMin_longitude(String min_longitude) {
        this.min_longitude = min_longitude;
    }

    public String getMax_longitude() {
        return max_longitude;
    }

    public void setMax_longitude(String max_longitude) {
        this.max_longitude = max_longitude;
    }

    public String getMin_latitude() {
        return min_latitude;
    }

    public void setMin_latitude(String min_latitude) {
        this.min_latitude = min_latitude;
    }

    public String getMax_latitude() {
        return max_latitude;
    }

    public void setMax_latitude(String max_latitude) {
        this.max_latitude = max_latitude;
    }

    public String getFlow() {
        return flow;
    }

    public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getUsr_cnt() {
        return usr_cnt;
    }

    public void setUsr_cnt(String usr_cnt) {
        this.usr_cnt = usr_cnt;
    }

    public String getLte_usr_cnt() {
        return lte_usr_cnt;
    }

    public void setLte_usr_cnt(String lte_usr_cnt) {
        this.lte_usr_cnt = lte_usr_cnt;
    }

    public String getPermanent_usr_cnt() {
        return permanent_usr_cnt;
    }

    public void setPermanent_usr_cnt(String permanent_usr_cnt) {
        this.permanent_usr_cnt = permanent_usr_cnt;
    }

    public String getFlowing_usr_cnt() {
        return flowing_usr_cnt;
    }

    public void setFlowing_usr_cnt(String flowing_usr_cnt) {
        this.flowing_usr_cnt = flowing_usr_cnt;
    }

    public String getPer_traffic() {
        return per_traffic;
    }

    public void setPer_traffic(String per_traffic) {
        this.per_traffic = per_traffic;
    }

    public String getPer_flow() {
        return per_flow;
    }

    public void setPer_flow(String per_flow) {
        this.per_flow = per_flow;
    }

    public String getPer_consumer() {
        return per_consumer;
    }

    public void setPer_consumer(String per_consumer) {
        this.per_consumer = per_consumer;
    }

    public String getHigh_value_usr_cnt() {
        return high_value_usr_cnt;
    }

    public void setHigh_value_usr_cnt(String high_value_usr_cnt) {
        this.high_value_usr_cnt = high_value_usr_cnt;
    }

    public String getNormal_value_usr_cnt() {
        return normal_value_usr_cnt;
    }

    public void setNormal_value_usr_cnt(String normal_value_usr_cnt) {
        this.normal_value_usr_cnt = normal_value_usr_cnt;
    }
}
