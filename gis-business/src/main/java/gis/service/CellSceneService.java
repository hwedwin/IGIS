package gis.service;

import bean.cityfun.AreaDimSceneType;
import bean.common.CellDimScene;
import bean.common.CellRefDimScene;
import bean.common.CellRefDimSceneExpot;
import gis.core.bean.KeyValue;

import java.util.Collection;
import java.util.List;

/**
 * Created by hupeng on 16/6/7.
 */
public abstract class CellSceneService {

    /**
     * 获取场景分类
     *
     * @return
     * @throws Exception
     */
    public abstract List<AreaDimSceneType> GetSceneTypeList() throws Exception;

    /**
     * 获取场景的应用对象
     *
     * @return
     * @throws Exception
     */
    public abstract List<KeyValue> GetSceneAppTopics() throws Exception;

    /**
     * 入库场景类型
     *
     * @param sceneTypes
     * @return
     * @throws Exception
     */
    public abstract int AddSceneTypes(List<AreaDimSceneType> sceneTypes) throws Exception;

    /**
     * 新增小区自定义场景
     *
     * @param item 场景实体类
     */
    public abstract int AddCellDimScene(CellDimScene item) throws Exception;

    /**
     * 批量入库场景主表
     *
     * @param scenes
     * @return
     * @throws Exception
     */
    public abstract int AddCellDimScenes(Collection<CellDimScene> scenes) throws Exception;

    /**
     * 删除小区自定义场景
     *
     * @param id 场景id
     */
    public abstract int DeleteCellDimScene(String id) throws Exception;

    /**
     * 查询场景列表
     * @param sceneTypeId 场景类型ID  如果为空 则查询全部
     * @param cityId 地市ID  如果为空 则查询全部
     * @param apptopicsid 应用ID  如果为空 则查询全部
     * @return
     * @throws Exception
     */
    public abstract List<CellDimScene> GetCellSceneList(String sceneTypeId, String cityId, String apptopicsid) throws Exception;

    /**
     * 新增小区自定义场景关联小区
     *
     * @param list 关联小区实体类列表
     */
    public abstract int AddCellRefSceneList(List<CellRefDimScene> list) throws Exception;

    /**
     * 获取用户自定义场景关联小区列表
     *
     * @param scene_id 自定义场景id
     */
    public abstract List<CellRefDimScene> GetCellRefSceneList(String scene_id) throws Exception;

    /**
     * 删除用户自定义场景关联小区
     *
     * @param scene_id 场景id
     */
    public abstract int DeleteCellRefScene(String scene_id) throws Exception;

    /**
     * 获取导出数据
     *
     * @param sceneTypeId
     * @param cityId
     * @return
     * @throws Exception
     */
    public abstract List<CellRefDimSceneExpot> GetLExpotData(String sceneTypeId, String cityId, String appTopicsId) throws Exception;
}
