package bean.end2end;

import bean.common.Cell;
import bean.common.Res;

/**
 * Created by hupeng on 16/6/17.
 * 2G小区月粒度性能
 */
public class GCell2G extends Cell {
    /**
     * 小区标识 bigint
     */
    private String cell_id;
    /**
     * 小区名称 varchar(256)
     */
    private String gcell_name;
    /**
     * 业务时间 int
     */
    private String event_month;
    /**
     * 小区标识lac bigint
     */
    private String lac;
    /**
     * 小区标识ci bigint
     */
    private String cid;
    /**
     * 地市标识 bigint
     */
    private String city_id;
    /**
     * 上行流量 bigint
     */
    private String up_flow;
    /**
     * 下行流量 bigint
     */
    private String dl_flow;
    /**
     * 总流量 bigint
     */
    private String tot_flow;
    /**
     * 用户数 bigint
     */
    private String user_cnt;


    public String getCell_id() {
        return cell_id;
    }

    public void setCell_id(String cell_id) {
        this.cell_id = cell_id;
    }

    public String getGcell_name() {
        return gcell_name;
    }

    public void setGcell_name(String gcell_name) {
        this.gcell_name = gcell_name;
    }

    public String getEvent_month() {
        return event_month;
    }

    public void setEvent_month(String event_month) {
        this.event_month = event_month;
    }

    public String getLac() {
        return lac;
    }

    public void setLac(String lac) {
        this.lac = lac;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getUp_flow() {
        return up_flow;
    }

    public void setUp_flow(String up_flow) {
        this.up_flow = up_flow;
    }

    public String getDl_flow() {
        return dl_flow;
    }

    public void setDl_flow(String dl_flow) {
        this.dl_flow = dl_flow;
    }

    public String getTot_flow() {
        return tot_flow;
    }

    public void setTot_flow(String tot_flow) {
        this.tot_flow = tot_flow;
    }

    public String getUser_cnt() {
        return user_cnt;
    }

    public void setUser_cnt(String user_cnt) {
        this.user_cnt = user_cnt;
    }

    public void clone(Cell cell){
        if (cell == null) {
            return;
        }


        this.setLongitude(cell.getLongitude());
        this.setLatitude(cell.getLatitude());
        this.setValue(cell.getValue());

        this.setOriginalLongitude(cell.getOriginalLongitude());
        this.setOriginalLatitude(cell.getOriginalLatitude());
        this.setCityId(cell.getCityId());
        this.setCityName(cell.getCityName());
        this.setCountryId(cell.getCountryId());
        this.setCountyName(cell.getCountyName());
        this.setResType(cell.getResType());

        this.setBtsId(cell.getBtsId());
        this.setBtsName(cell.getBtsName());
        this.setLac(cell.getLac());
        this.setCi(cell.getCi());
        this.setEci(cell.getEci());
        this.setRadius(cell.getRadius());
        this.setAzimuth(cell.getAzimuth());
        this.setAntbw(cell.getAntbw());
        this.setCoverSceneId(cell.getCoverSceneId());
        this.setCoverSceneName(cell.getCoverSceneName());
        this.setIfAlonecell(cell.getIfAlonecell());
        this.setIfAloneCellId(cell.getIfAloneCellId());
        this.setTac(cell.getTac());
        this.setCell_order_id(cell.getCell_order_id());

    }

    public static GCell2G read(Cell cell) {
        if (cell == null) {
            return null;
        }

        GCell2G item = new GCell2G();

        item.setLongitude(cell.getLongitude());
        item.setLatitude(cell.getLatitude());
        item.setValue(cell.getValue());

        item.setOriginalLongitude(cell.getOriginalLongitude());
        item.setOriginalLatitude(cell.getOriginalLatitude());
        item.setCityId(cell.getCityId());
        item.setCityName(cell.getCityName());
        item.setCountryId(cell.getCountryId());
        item.setCountyName(cell.getCountyName());
        item.setResType(cell.getResType());

        item.setBtsId(cell.getBtsId());
        item.setBtsName(cell.getBtsName());
        item.setLac(cell.getLac());
        item.setCi(cell.getCi());
        item.setEci(cell.getEci());
        item.setRadius(cell.getRadius());
        item.setAzimuth(cell.getAzimuth());
        item.setAntbw(cell.getAntbw());
        item.setCoverSceneId(cell.getCoverSceneId());
        item.setCoverSceneName(cell.getCoverSceneName());
        item.setIfAlonecell(cell.getIfAlonecell());
        item.setIfAloneCellId(cell.getIfAloneCellId());
        item.setTac(cell.getTac());
        item.setCell_order_id(cell.getCell_order_id());

        return item;
    }

}
