package bean.highspeedrail;

import gis.algorithm.Extent;
import gis.algorithm.GeoPoint;

/**
 * Created by Xiaobing on 2016/5/25.
 * 铁路栅格信息
 */
public class TrackGrid {
    /**
     * 栅格标识
     */
    private String gridId;
    /**
     * 栅格范围
     */
    private Extent extent;
    /**
     * 开始点
     */
    private GeoPoint startPoint;
    /**
     * 结束点
     */
    private GeoPoint endPoint;
    /**
     * 高铁线路标识
     */
    private String trackId;
    /**
     * 高铁线路名称
     */
    private String trackName;
    /**
     * 高铁线路区间标识
     */
    private String stationInterId;
    /**
     * 高铁线路区间名称
     */
    private String stationInterName;
    /**
     * 栅格路段标识
     */
    private String roadId;
    /**
     * 栅格路段名称
     */
    private String roadName;

    /**
     * 栅格标识
     * @return
     */
    public String getGridId() {
        return gridId;
    }

    /**
     * 栅格标识
     * @param gridId
     */
    public void setGridId(String gridId) {
        this.gridId = gridId;
    }

    /**
     * 栅格范围
     * @return
     */
    public Extent getExtent() {
        return extent;
    }

    /**
     * 栅格范围
     * @param extent
     */
    public void setExtent(Extent extent) {
        this.extent = extent;
    }

    /**
     * 开始点
     * @return
     */
    public GeoPoint getStartPoint() {
        return startPoint;
    }

    /**
     * 开始点
     * @param startPoint
     */
    public void setStartPoint(GeoPoint startPoint) {
        this.startPoint = startPoint;
    }

    /**
     * 结束点
     * @return
     */
    public GeoPoint getEndPoint() {
        return endPoint;
    }

    /**
     * 结束点
     * @param endPoint
     */
    public void setEndPoint(GeoPoint endPoint) {
        this.endPoint = endPoint;
    }

    /**
     * 高铁线路标识
     * @return
     */
    public String getTrackId() {
        return trackId;
    }

    /**
     * 高铁线路标识
     * @param trackId
     */
    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    /**
     * 高铁线路名称
     * @return
     */
    public String getTrackName() {
        return trackName;
    }

    /**
     * 高铁线路名称
     * @param trackName
     */
    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    /**
     * 高铁线路区间标识
     * @return
     */
    public String getStationInterId() {
        return stationInterId;
    }

    /**
     * 高铁线路区间标识
     * @param stationInterId
     */
    public void setStationInterId(String stationInterId) {
        this.stationInterId = stationInterId;
    }

    /**
     * 高铁线路区间名称
     * @return
     */
    public String getStationInterName() {
        return stationInterName;
    }

    /**
     * 高铁线路区间名称
     * @param stationInterName
     */
    public void setStationInterName(String stationInterName) {
        this.stationInterName = stationInterName;
    }

    /**
     * 栅格路段标识
     * @return
     */
    public String getRoadId() {
        return roadId;
    }

    /**
     * 栅格路段标识
     * @param roadId
     */
    public void setRoadId(String roadId) {
        this.roadId = roadId;
    }

    /**
     * 栅格路段名称
     * @return
     */
    public String getRoadName() {
        return roadName;
    }

    /**
     * 栅格路段名称
     * @param roadName
     */
    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }
}
