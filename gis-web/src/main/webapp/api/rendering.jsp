<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
    <%@ include file="header.jsp" %>
    <section>
        <div class="container">
            <%@ include file="leftmenu.jsp" %>
            <input id="pageid" type="hidden" value="rendering">

            <div class="docs-content" style="height: 1400px;">
                <h3 class="welcome">渲染算法</h3>

                <p>通过GIS的OGC标准，通过算法将数据渲染成图片，下面对部分渲染进行介绍</p>
                <br/>

                <h3 class="welcome">线段渲染</h3>

                <p>数据模型：gis.core.bean.ResValue</p>
        <pre class="prettyprint">

List&lt;ResValue&gt; res = new ArrayList&lt;&gt;();
Color e = new Color(0, 255, 0, 255);
GridMap gridmap = new GridMap();
gridmap.initGridMap(imageWidth, imageHeight, extent, res);
BufferedImage image = gridmap.reader(e);
        </pre>

                <h3 class="welcome">点渲染</h3>

                <p>数据模型：gis.core.bean.ResValue 根据不同的指标值，渲染成不同的颜色</p>
                <pre class="prettyprint">

List&lt;ResValue&gt; list = new ArrayList&lt;&gt;();
IColorReader color = ...;//接口，根据模型的value值确定颜色值
PointMap pointMap = new PointMap(imageWidth, imageHeight, extent, list);
BufferedImage image = pointMap.reader(color);
                </pre>

                <h3 class="welcome">封闭多边形（面）渲染：</h3>

                <p>数据模型：gis.algorithm.polygon.RqPolygonMap 根据不同的指标值，渲染成不同的颜色</p>
                <pre class="prettyprint">

List&lt;RqPolygonMap&gt; list = new ArrayList&lt;&gt;();
IColorReader color = ...;//接口，根据模型的value值确定颜色值
PolygonMap map = new PolygonMap(imageWidth, imageHeight, extent, list);
BufferedImage image = map.reader(color);
                </pre>

                <h3 class="welcome">热力图渲染：</h3>

                <p>数据模型：gis.core.bean.ResValue</p>
                <pre class="prettyprint">

List&lt;ResValue&gt; list = new ArrayList&lt;&gt;();
HeatMapRender&lt;ResValue&gt; render = new HeatMapRender&lt;ResValue&gt;();
BufferedImage image = render.reader(imageWidth, imageHeight, extent, list)

                </pre>

                <h3 class="welcome">栅格渲染：</h3>

                <p>数据模型：gis.algorithm.grid.RqGridMap</p>
                <pre class="prettyprint">
List&lt;RqGridMap&gt; list = new ArrayList&lt;&gt;();
GridMap gridmap = new GridMap();
IColorReader color = ...;//图例接口
BufferedImage image = gridmap.reader(imageWidth, imageHeight, extent, list)
                </pre>

                <a href="/api/operation.jsp" type="button" class="btn-link">上一页</a>
                <a href="/api/release.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>
            </div>
        </div>
    </section>
</div>
</body>
</html>
