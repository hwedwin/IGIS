package bean.topology;

import gis.core.bean.KeyValue;

/**
 * Created by Xiaobing on 2016/6/28.
 */
public class TopoCityPre extends KeyValue {
    private String currentValue;
    private String contrastValue;
    private String rate = "-";

    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    public String getContrastValue() {
        return contrastValue;
    }

    public void setContrastValue(String contrastValue) {
        this.contrastValue = contrastValue;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }
}
