package gis.service;

import bean.scenicgrid.*;

import java.util.List;

/**
 * Created by hupeng on 16/6/13.
 * 场景栅格化服务层 如景区、高校、地铁等栅格化专题均使用该服务层
 */
public abstract class ScenicGridService {

    /**
     * 获取所有的景区列表
     */
    public abstract List<ScenicSpot> GetScenicSpotList() throws Exception;

    /**
     * 获取所有的学校列表
     */
    public abstract List<SchoolScenic> GetSchoolScenicList() throws Exception;

    /**
     * 获取景区指标数据
     */
    public abstract ScenicSpotData GetScenicSpotData(long spotId, int time) throws Exception;

    /**
     * 获取高校指标数据
     */
    public abstract SchoolScenicData GetSchoolScenicData(long schoolId, int time) throws Exception;

    /**
     * 获取景区对应的栅格数据
     *
     * @param scenicId 景区id
     * @param time     时间 yyyyMM
     */
    public abstract List<ScenicGridData> GetScenicSpotGridDataList(long scenicId, int time) throws Exception;

    /**
     * 获取高校对应的栅格数据
     *
     * @param scenicId 高校id
     * @param time     时间 yyyyMM
     */
    public abstract List<ScenicGridData> GetSchoolScenicGridDataList(long scenicId, int time) throws Exception;

    /**
     * 获取栅格数据
     *
     * @param scenicId 场景id
     * @param time     时间 yyyyMM
     * @param gridId   栅格id
     * @param type     1 景区 2 高校
     */
    public abstract ScenicGridData GetScenicGridData(long scenicId, int time, long gridId, int type) throws Exception;
}
