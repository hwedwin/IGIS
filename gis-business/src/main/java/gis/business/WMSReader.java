package gis.business;

import bean.guizhoumax.HotCell;
import bean.highspeedrail.TrackGrid;
import bean.ltecover.BuildingEx;
import bean.ltecover.LoadGridInfo;
import bean.ltecover.RqBigGridMap;
import bean.scenicgrid.ScenicGridData;
import bean.technicalinve.RqGridMapInve;
import cache.DataCache;
import gis.algorithm.*;
import gis.algorithm.cluster.RqCluster;
import gis.algorithm.grid.GridMap;
import gis.algorithm.grid.GridUtils;
import gis.algorithm.grid.RqGridMap;
import gis.algorithm.heatMap.HeatMapRender;
import gis.algorithm.polygon.LineMap;
import gis.algorithm.polygon.PointMap;
import gis.algorithm.polygon.PolygonMap;
import gis.algorithm.polygon.RqPolygonMap;
import gis.algorithm.tide.TideArg;
import gis.algorithm.tide.TideCalculator;
import gis.algorithm.tide.TileGrid;
import gis.common.ServiceFactory;
import gis.core.bean.ResValue;
import gis.core.utils.CollUtils;
import gis.dao.*;
import gis.service.ScenicGridService;
import gis.toolkit.GaoTieHelper;
import ogc.wms.WMSGetMapParam;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class WMSReader {
    private static Logger logger = LoggerFactory.getLogger(WMSReader.class);
    private String fun, kpiName, param;
    private WMSGetMapParam p;
    private HttpServletRequest request;

    public WMSReader(HttpServletRequest request, WMSGetMapParam p) {
        this.request = request;
        this.p = p;

        String url[] = StringUtils.splitPreserveAllTokens(request.getRequestURI(), "/");
        this.fun = url[2];
        this.kpiName = url[3];
        this.param = url[4];
    }

    public BufferedImage reader() {
        BufferedImage image = null;
        switch (fun) {
            case "ltegridcover"://端到端 普通指标渲染
                image = lteGridCover();
                break;

            default:
                logger.error("无法找到网格渲染适配:" + fun);
                break;
        }
        return image;
    }

    /**
     * LTE网格覆盖
     *
     * @return
     */
    private BufferedImage lteGridCover() {
        BufferedImage image = new BufferedImage(p.getWidth(), p.getHeight(), BufferedImage.TYPE_INT_ARGB);
        GeoPoint p1;
        GeoPoint p2;
        p1 = GeoUtils.lonLatToMercator(p.getBbox().getxMin(), p.getBbox().getyMin());
        p2 = GeoUtils.lonLatToMercator(p.getBbox().getxMax(), p.getBbox().getyMax());

        String time = StringUtils.splitPreserveAllTokens(param, "_")[0];
        String id = StringUtils.splitPreserveAllTokens(param, "_")[1];
        String where = "";
        if (StringUtils.isNotEmpty(id) && !id.equals("null") && !id.equals("NULL")) {
            where = DataCache.getData(id) + "";
        }

        double w = p2.getX() - p1.getX();
        double h = p2.getY() - p1.getY();
        int count = (int) (w * h / (55 * 55));
        if (count <= 80000) {
            GridMap gridmap = new GridMap();
            Extent extent = new Extent();
            extent.setxMax(p.getBbox().getxMax() + 0.0005);
            extent.setxMin(p.getBbox().getxMin() - 0.0005);
            extent.setyMax(p.getBbox().getyMax() + 0.0005);
            extent.setyMin(p.getBbox().getyMin() - 0.0005);

            List<RqGridMap> list = new ArrayList<>();
            IColorReader color = new LteGridCoverColor(kpiName);

            gridmap.initGridMap(p.getWidth(), p.getHeight(), p.getBbox(), list);
            if (count < 4000) {
                gridmap.checkFillGridData();
            }
            image = gridmap.reader(color);
        } else {

            double d = 0.0005;
            double f = 1;
            //LTECoverDao dao = new LTECoverDao();
            GridMap gridmap = new GridMap();
            Extent extent = new Extent();

            String c_kpiName = kpiName;
            c_kpiName = "big_" + kpiName;

            List<RqBigGridMap> list = new ArrayList<>();

            if (count < 400000) {
                f = d * 10;
                extent.setxMax(p.getBbox().getxMax() + f);
                extent.setxMin(p.getBbox().getxMin() - f);
                extent.setyMax(p.getBbox().getyMax() + f);
                extent.setyMin(p.getBbox().getyMin() - f);
               // list = dao.getCoverMidGrid(time, kpiName, extent, where);
            } else {
                f = d * 100;
                extent.setxMax(p.getBbox().getxMax() + f);
                extent.setxMin(p.getBbox().getxMin() - f);
                extent.setyMax(p.getBbox().getyMax() + f);
                extent.setyMin(p.getBbox().getyMin() - f);
               // list = dao.getCoverBigGrid(time, kpiName, extent, where);
            }
            IColorReader color = new LteGridCoverColor(c_kpiName);
            gridmap.initGridMap(p.getWidth(), p.getHeight(), p.getBbox(), list);

            image = gridmap.reader(color);

        }
        return image;
    }

}
