package bean.highspeedrail;

import bean.common.Legend;
import bean.common.LegendItem;
import gis.algorithm.ColorHelper;
import gis.algorithm.IColorReader;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xiaobing on 2016/5/26.
 */
public class TrackLegendColor extends IColorReader {
    private List<LegendItem> legendItems = new ArrayList<LegendItem>();// 图例
    private Legend legend = new Legend();
    String kpiName = "";

    Color r = new Color(255, 0, 0, 255);// 红
    Color p = new Color(248, 134, 142, 255);// 粉
    Color y = new Color(248, 248, 132, 255);// 黄
    Color b = new Color(0, 0, 255, 255);// 蓝
    Color g = new Color(0, 255, 0, 255);// 绿

    int r1 = ColorHelper.getColorInt(r);
    int p1 = ColorHelper.getColorInt(p);
    int y1 = ColorHelper.getColorInt(y);
    int b1 = ColorHelper.getColorInt(b);
    int g1 = ColorHelper.getColorInt(g);

    /**
     * 图例
     *
     * @return
     */
    public Legend getLegend() {
        return legend;
    }

    public TrackLegendColor(String kpiName) {

        this.kpiName = kpiName;
        if (kpiName.equals("accept_avg_level")) {
            legend.setName("接收平均电平");//-141  -44
            legendItems.add(CreateLegend(255, 0, 0, 255, r1, "-141", "-100"));// 红
            legendItems.add(CreateLegend(248, 248, 132, 255, y1, "-100", "-90"));// 黄
            legendItems.add(CreateLegend(0, 0, 255, 255, b1, "-90", "-80"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 255, g1, "-80", "-44"));// 绿
        } else if (kpiName.equals("accept_avg_quality")) {
            legend.setName("接收平均质量");
            legendItems.add(CreateLegend(255, 0, 0, 255, r1, "MIN", "-12"));// 红
            legendItems.add(CreateLegend(248, 248, 132, 255, y1, "-12", "-9"));// 黄
            legendItems.add(CreateLegend(0, 0, 255, 255, b1, "-9", "-6"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 255, g1, "-6", "MAX"));// 绿
        } else if (kpiName.equals("accept_avg_sina")) {//sinr
            legend.setName("接收平均干扰功率");
            legendItems.add(CreateLegend(255, 0, 0, 255, r1, "MIN", "0"));// 红
            legendItems.add(CreateLegend(248, 248, 132, 255, y1, "0", "5"));// 黄
            legendItems.add(CreateLegend(0, 0, 255, 255, b1, "5", "10"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 255, g1, "10", "MAX"));// 绿
        } else if (kpiName.equals("weak_rat")) {
            legend.setName("弱覆盖比例");
            legendItems.add(CreateLegend(255, 0, 0, 255, r1, "MAX", "20%"));// 红
            legendItems.add(CreateLegend(248, 248, 132, 255, y1, "10%", "15%"));// 黄
            legendItems.add(CreateLegend(0, 0, 255, 255, b1, "5%", "10%"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 255, g1, "10%", "0"));// 绿
        } else if (kpiName.equals("high_sina_rat")) {
            legend.setName("高干扰占比");
            legendItems.add(CreateLegend(255, 0, 0, 255, r1, "100%", "15%"));// 红
            legendItems.add(CreateLegend(248, 248, 132, 255, y1, "15%", "10%"));// 黄
            legendItems.add(CreateLegend(0, 0, 255, 255, b1, "10%", "5%"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 255, g1, "5%", "0%"));// 绿
        } else if (kpiName.equals("flow")) {
            legend.setName("流量");
        } else if (kpiName.equals("offline_cnt")) {
            legend.setName("脱网次数");
            legendItems.add(CreateLegend(255, 0, 0, 255, r1, "MAX", "20"));// 红
            legendItems.add(CreateLegend(248, 248, 132, 255, y1, "20", "5"));// 黄
            legendItems.add(CreateLegend(0, 0, 255, 255, b1, "1", "5"));// 蓝
            legendItems.add(CreateLegend(0, 255, 0, 255, g1, "0", "0"));// 绿
        }
        legend.setLegendItems(legendItems);
    }

    /**
     * @param r        红
     * @param g        黄
     * @param b        蓝
     * @param a        透明度
     * @param colorInt 颜色值
     * @param valueMin 指标最大值
     * @param valueMax 指标最小值
     * @return
     */
    private LegendItem CreateLegend(int r, int g, int b, int a, int colorInt, String valueMin, String valueMax) {
        LegendItem legend = new LegendItem();
        legend.setValueMin(valueMin);
        legend.setValueMax(valueMax);
        legend.setR(r);
        legend.setG(g);
        legend.setB(b);
        legend.setA(a);
        legend.setColorInt(colorInt);
        return legend;
    }

    @Override
    public Integer GetColor(Double vales) {
        Integer intValue = null;
        if (kpiName.equals("accept_avg_level")) {//接收平均电平
            if (vales < -100) {
                intValue = r1;
            } else if (vales >= -100 && vales < -90) {
                intValue = y1;
            } else if (vales >= -90 && vales <= -80) {
                intValue = b1;
            } else {
                intValue = g1;
            }
        } else if (kpiName.equals("accept_avg_quality")) {//接收平均质量
            if (vales < -12) {
                intValue = r1;
            } else if (vales >= -12 && vales < -9) {
                intValue = y1;
            } else if (vales >= -9 && vales <= -6) {
                intValue = b1;
            } else {
                intValue = g1;
            }

        } else if (kpiName.equals("accept_avg_sina")) {//接收平均干扰功率
            if (vales < 0) {
                intValue = r1;
            } else if (vales >= 0 && vales < 5) {
                intValue = y1;
            } else if (vales >= 5 && vales <= 10) {
                intValue = b1;
            } else {
                intValue = g1;
            }

        } else if (kpiName.equals("high_sina_rat")) {//高干扰占比
            if (vales >= 0.15) {
                intValue = r1;
            } else if (vales >= 0.1 && vales < 0.15) {
                intValue = y1;
            } else if (vales >= 0.05 && vales <= 0.1) {
                intValue = b1;
            } else {
                intValue = g1;
            }

        } else if (kpiName.equals("offline_cnt")) {//脱网次数
            if (vales >= 20) {
                intValue = r1;
            } else if (vales >= 5 && vales < 20) {
                intValue = y1;
            } else if (vales >= 5 && vales <= 5) {
                intValue = b1;
            } else {
                intValue = g1;
            }

        } else if (kpiName.equals("weak_rat")) {//弱覆盖比例
            if (vales >= 0.15) {
                intValue = r1;
            } else if (vales >= 0.1 && vales < 0.15) {
                intValue = y1;
            } else if (vales >= 0.05 && vales <= 0.1) {
                intValue = b1;
            } else {
                intValue = g1;
            }
        }

        return intValue;
    }


    public String GetColorString(Double vales) {
        int v = GetColor(vales);
        if (v == r1) {
            return "255,0,0";
        } else if (v == y1) {
            return "248,248,132";
        } else if (v == b1) {
            return "0,0,255";
        }
        if (v == g1) {
            return "0,255,0";
        }
        return "148,148,148";
    }

}
