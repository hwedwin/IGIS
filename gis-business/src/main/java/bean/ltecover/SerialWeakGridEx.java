package bean.ltecover;

/**
 * Created by Xiaobing on 2016/8/23.
 */
public class SerialWeakGridEx extends SerialWeakGrid {
    private String gridIds="";

    public String getGridIds() {
        return gridIds;
    }

    public void setGridIds(String gridIds) {
        this.gridIds = gridIds;
    }
}
