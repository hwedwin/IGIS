package bean.common;

import gis.core.utils.CollUtils;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * Created by Xiaobing on 2016/4/19.
 * jqgrid 返回对象
 */
public class JQGridDataResult {
    /**
     * 当前页
     */
    private int page;
    /**
     * 总页数
     */
    private int total;
    /**
     * 查询出的记录数
     */
    private int records;
    /**
     * 包含实际数据的数组
     */
    private Object rows;

    /**
     * 当前页
     *
     * @return
     */
    public int getPage() {
        return page;
    }

    /**
     * 当前页
     *
     * @param page
     */
    public void setPage(int page) {
        this.page = page;
    }

    /**
     * 总页数
     *
     * @return
     */
    public int getTotal() {
        return total;
    }

    /**
     * 总页数
     *
     * @param total
     */
    public void setTotal(int total) {
        this.total = total;
    }

    /**
     * 查询出的记录数
     *
     * @return
     */
    public int getRecords() {
        return records;
    }

    /**
     * 查询出的记录数
     *
     * @param records
     */
    public void setRecords(int records) {
        this.records = records;
    }

    /**
     * 包含实际数据的数组
     *
     * @return
     */
    public Object getRows() {
        return rows;
    }

    /**
     * 包含实际数据的数组
     *
     * @param rows
     */
    public void setRows(Object rows) {
        this.rows = rows;
    }

    /**
     *设置数据源
     * @param list 数据源
     * @param jQGridParam
     * @param <E>
     */
    public <E> void setDataSource(List<E> list, JQGridParam jQGridParam) {
        if(StringUtils.isNotEmpty(jQGridParam.getSordField()) && StringUtils.isNotEmpty(jQGridParam.getSordType()) )
        {
            if(jQGridParam.getSordType().toLowerCase().equals("desc"))
            {
                list = CollUtils.sortByField(list,jQGridParam.getSordField(),false);
            }
            else {
                list = CollUtils.sortByField(list,jQGridParam.getSordField(),true);}
        }

        List<E> temp = CollUtils.page(list, jQGridParam.getPageSize(), (jQGridParam.getPageIndex() - 1) * jQGridParam.getPageSize());
        setPage(jQGridParam.getPageIndex());
        setRecords(list.size());
        setRows(temp);
        if (getRecords() % jQGridParam.getPageSize() == 0) {
            setTotal(getRecords() /  jQGridParam.getPageSize());
        } else {
            setTotal((getRecords() / jQGridParam.getPageSize()) + 1);
        }
    }
}
