package gis.serviceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.poi.PoiCategory;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cache.DataCache;
import bean.poi.PoiInfo;
import gis.common.GisConfig;
import gis.service.PoiService;
import gis.sqlbase.DatabaseManage;
import gis.sqlbase.DatabaseName;
import gis.sqlbase.SQLHelper;

public class PoiServiceImpl extends PoiService {

    private Logger logger = LoggerFactory.getLogger(PoiServiceImpl.class);
    private static final SQLHelper db = DatabaseManage.Instance(DatabaseName.gbase);
    private GisConfig config = GisConfig.GetInstance();

    @SuppressWarnings("unchecked")
    @Override
    public List<PoiInfo> GetPois() throws SQLException {
        String keyString = "PoiServiceImpl_GetPois";
        List<PoiInfo> list = null;
        Object obj = DataCache.getData(keyString);
        if (obj != null) {
            list = (List<PoiInfo>) obj;
        }

        String strSQL = config.getSQL("poi_select");
        if (list == null && StringUtils.isNotEmpty(strSQL)) {
            ResultSet rs = db.getResultSet(strSQL);
            if (rs != null) {
                try {
                    list = GetPoiEntity(rs);
                    DataCache.addData(keyString, list);
                } catch (SQLException e) {
                    logger.error("调用PoiServiceImpl 的方法 GetPois()出错," + e);
                    throw e;
                } finally {
                    db.close(rs);
                }
            }
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PoiCategory> GetPoiCategory() throws Exception {
        List<PoiCategory> list = new ArrayList<PoiCategory>();

        String sql = "SELECT id,level_f_id,level_f_name,level_s_id,level_s_name FROM t_poi_category ORDER BY id DESC";
        ResultSet rs = db.getResultSet(sql);

        try {
            while (rs.next()) {
                PoiCategory item = new PoiCategory();
                item.setId(rs.getString("id"));
                item.setLevel_f_id(rs.getString("level_f_id"));
                item.setLevel_f_name(rs.getString("level_f_name"));
                item.setLevel_s_id(rs.getString("level_s_id"));
                item.setLevel_s_name(rs.getString("level_s_name"));
                list.add(item);
            }
        } catch (Exception e) {
            logger.error("查询poi分类出错：" + e.getMessage(), e);
        } finally {
            db.close(rs);
        }

        return list;
    }
}
