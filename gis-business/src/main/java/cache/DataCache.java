package cache;

import gis.common.GisConfig;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;


public class DataCache {
    protected static final HashMap<String, DataCacheItem> MAP = new HashMap<String, DataCacheItem>();
    private static final Object lock = new Object();
    private static final long cachetimeout = GisConfig.GetInstance().getCachetimeout();

    static {
        // 初始化定时器
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                DataCache.ClearExpire();
            }
        }, 20000, 20000);// 设定指定的时间time,此处为2000毫秒

    }

    /**
     * 获取缓存项
     *
     * @param key
     * @return
     */
    public static Object getData(String key) {
        synchronized (lock) {
            DataCacheItem value = MAP.get(key);
            if (value == null) {
                value = MAP.get(key);
                if (value == null) {
                    return null;
                } else {
                    return value.value;
                }
            } else {
                return value.value;
            }
        }
    }

    /**
     * 添加缓存项
     *
     * @param key
     * @param value
     */
    public static void addData(String key, Object value) {
        addData(key, value, cachetimeout);
    }

    /**
     * 添加缓存项
     *
     * @param key
     * @param value
     * @param timeout 毫秒
     */
    public static void addData(String key, Object value, Long timeout) {
        synchronized (lock) {
            Date now = new Date();
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(getMillis(now) + timeout);
            Date expireDate = c.getTime();

            DateFormat format = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
            String dateStr = format.format(expireDate);
            long dateLong = Long.parseLong(dateStr);

            DataCacheItem item = new DataCacheItem();
            item.value = value;
            item.timeout = timeout;
            item.expire = dateLong;

            MAP.put(key, item);
        }
    }

    /**
     * 判断KEY是否存在
     *
     * @param key
     * @return
     */
    public static boolean containsKey(String key) {
        return MAP.containsKey(key);
    }

    /**
     * 清楚过期缓存
     */
    public static void ClearExpire() {
        Date now = new Date();

        DateFormat format = new java.text.SimpleDateFormat("yyyyMMddHHmmss");
        String dateStr = format.format(now);
        long dateLong = Long.parseLong(dateStr);

        synchronized (lock) {
            List<String> removeKeys = new ArrayList<String>();
            Set<String> keySet = MAP.keySet();
            for (String key : keySet) {
                if (MAP.get(key).expire < dateLong) {
                    removeKeys.add(key);
                }
            }

            for (String key : removeKeys) {
                MAP.remove(key);
            }
        }
    }

    public static void Clear() {
        synchronized (lock) {
            MAP.clear();
        }
    }

    public static void ClearItem(String keyPara) {
        synchronized (lock) {
            List<String> removeKeys = new ArrayList<String>();
            Set<String> keySet = MAP.keySet();
            for (String key : keySet) {
                if (key.equals(keyPara)) {
                    removeKeys.add(key);
                }
            }

            for (String key : removeKeys) {
                MAP.remove(key);
            }
        }
    }

    private static long getMillis(Date date) {

        Calendar c = Calendar.getInstance();
        c.setTime(date);

        return c.getTimeInMillis();
    }
}
