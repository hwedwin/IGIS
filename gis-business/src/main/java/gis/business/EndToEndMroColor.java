package gis.business;

import gis.algorithm.ColorHelper;
import gis.algorithm.IColorReader;

import java.awt.*;

/**
 * Created by Xiaobing on 2016/8/17.
 */
public class EndToEndMroColor extends IColorReader {

    public Color r = new Color(255, 0, 0, 125);// 红
    public Color g = new Color(0, 255, 0, 125);// 黄
    public Color b = new Color(0, 0, 255, 125);// 蓝

    int r1 = ColorHelper.getColorInt(r);
    int g1 = ColorHelper.getColorInt(g);
    int b1 = ColorHelper.getColorInt(b);

    @Override
    public Integer GetColor(Double vales) {
        if (vales <= 31) {
            return r1;
        } else {
            return b1;
        }
    }
}
