<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">

    <%String root_gisapp = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + StringUtils.replace(request.getServletPath(), "default", "")+ "/";%>
    <%String root_arcgis = request.getServerName() + ":" + request.getServerPort() + StringUtils.replace(request.getServletPath(), "default", "") + "/";%>

    <title><c:out value="${module.name}"/></title>
    <link rel="stylesheet" href="${baseurl}script/arcgis_js_api/library/3.9/3.9/js/dojo/dijit/themes/dijit.css">
    <link rel="stylesheet" href="${baseurl}script/arcgis_js_api/library/3.9/3.9/js/esri/css/esri.css">
    <link rel="stylesheet" type="text/css" media="screen" href="${baseurl}script/base/jqueryui/jquery-ui.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="${baseurl}script/base/jqgrid/ui.jqgrid.css"/>

    <link rel="stylesheet" type="text/css" href="${baseurl}bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="${baseurl}css/font-awesome-4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="${baseurl}css/public.css">
    <link rel="stylesheet" type="text/css" href="${baseurl}css/topic.css">
    <link rel="stylesheet" type="text/css" href="${baseurl}script/base/artcloudui/skins/default/css/artcloudui.css">

    <script type="text/javascript" src="${baseurl}script/base/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="${baseurl}script/base/jquery-1.11.0.js"></script>
    <script type="text/javascript" src="${baseurl}script/base/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="${baseurl}bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="${baseurl}script/base/json-js-master/json2.js"></script>
    <script type="text/javascript" src="${baseurl}script/base/artcloudui/artcloudui.dialog.min.js"></script>
    <script type="text/javascript" src="${baseurl}script/base/jqgrid/js/i18n/grid.locale-cn.js"></script>
    <script type="text/javascript" src="${baseurl}script/base/jqgrid/js/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="${baseurl}script/base/uploadify/jquery.uploadify-3.1.js"></script>
    <script type="text/javascript" src="${baseurl}script/base/html2canvas.js"></script>

    <script type="text/javascript">
        var root_gisapp = '${baseurl}';
        var root_arcgis = '${arcgis}';
        var dojoConfig = {
            parseOnLoad: true,
            packages: [{
                name: 'plugin',
                location: "/script/arcgis_js_api/plugin"
            }, {
                name: 'modules',
                location: "/script/modules"
            }]
        };
    </script>
    <script type="text/javascript" src="${baseurl}script/arcgis_js_api/library/3.9/3.9/init.js"></script>
    <script type="text/javascript" src="${baseurl}script/base.js"></script>
    <script type="text/javascript" src="${baseurl}script/modules/common.js"></script>
    <c:forEach items="${module.jsPath}" var="item">
    <script type="text/javascript" src="${baseurl}script/<c:out value="${item}"></c:out>"></script>
    </c:forEach>
    <script type="text/javascript">
        function initgis() {
            <c:forEach items="${module.init}" var="item">
            <c:out value="${item}"></c:out>
            </c:forEach>
        }
    </script>
</head>
<body>
<div class="map-main">
    <div id="map" class="map-wrap"></div>

    <!--搜索框-->
    <div class="search-panel">
    <c:if test="${module.showSearch == 'true'}">
        <div class="srh-l">
            <input type="text" class="srh-inp" placeholder="关键字搜索">
        </div>
        <div class="srh-r">
            <button type="submit" class="btn btn-primary btn-search" onClick="gis_common.query();">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </c:if>
    </div>
    <!--end搜索框-->

    <!--搜索弹出面板-->
    <div id='commonquery_content' class="modal-search">
        <div class="panel">
            <div class="panel-body" style="padding:0;">
                <div class="search-cont">
                    <div class="hide-search">鼠标移入 展开</div>
                    <ul class="search-cont-list">

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--end搜索弹出面板-->

    <!--end搜索弹出面板-->
    <!--导航-->
    <div class="nav-slide">
        <div class="nav-btn"><i class="fa fa-navicon"></i></div>
    </div>
    <ul class="nav-list">
    <c:forEach items="${module.menus}" var="item">
        <li><a href="javascript:void(0);" onClick="<c:out value="${item.fun}"></c:out>;"><i class="fa fa-subway"></i><br><c:out value="${item.name}"></c:out></a></li>
    </c:forEach>
    </ul>
    <!--end导航-->

    <%--返回菜单页面--%>
    <div class="nav-slide-home">
        <a href="/default"><div class="nav-btn"><img style="margin-top: 9px" src="/images/nav/home.png" alt="返回菜单页面"/></div></a>
    </div>

    <!--导航二级弹出-->
    <div id='div_menu'>
    </div>
    <!--end导航二级弹出-->

    <!--工具栏-->
    <div class="toolbox" style="display:block;">
        <div class="toolbox-wrap">
            <ul class="toolbar">
            <c:forEach items="${module.toolBars}" var="item">
                <li did="<c:out value="${item.fun}"></c:out>" ><a href="javascript:void(0);" did="<c:out value="${item.fun}"></c:out>" onclick="<c:out value="${item.fun}"></c:out>;"><i class="fa fa-files-o"></i><c:out value="${item.name}"></c:out></a></li>
            </c:forEach>
            </ul>
            <div class="tabs-modal" id="mymodal">
            </div>
        </div>
    </div>

    <div id="div_toolbox_content">
    </div>
    <!--end工具栏-->
</div>
</body>
</html>