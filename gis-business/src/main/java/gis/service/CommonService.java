package gis.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.common.Region;
import gis.core.bean.KeyValue;

public abstract class CommonService {

    /**
     * 获取GSM小区 接口
     *
     * @return
     * @throws SQLException
     * @throws Exception
     */
    public abstract List<Region> GetRegions() throws Exception;

    /**
     * 创建Bean
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    public static List<Region> GetRegionEntity(ResultSet rs) throws SQLException {
        List<Region> list = new ArrayList<Region>();

        Region province = null;
        Region city = null;
        while (rs.next()) {
            if (province == null) {
                province = new Region();
                province.setId(rs.getString("province_key"));
                province.setName(rs.getString("province_name"));
                province.setType(9000);
                list.add(province);
            }
            city = new Region();
            city.setId(rs.getString("city_key"));
            city.setName(rs.getString("city_name"));
            city.setType(9001);
            list.add(city);
        }

        return list;
    }

    public static List<KeyValue> getKeyValueEntity(ResultSet rs) throws SQLException {
        List<KeyValue> list = new ArrayList<>();
        while (rs.next()) {
            KeyValue kv = new KeyValue();
            kv.setId(rs.getString("id"));
            kv.setName(rs.getString("name"));
            list.add(kv);
        }
        return list;
    }
}
