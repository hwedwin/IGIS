<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">

    <%String root_gisapp = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>
    <%String root_arcgis = request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>

    <title>GIS</title>
    <link rel="stylesheet" href="/script/arcgis_js_api/library/3.9/3.9/js/dojo/dijit/themes/dijit.css">
    <link rel="stylesheet" href="/script/arcgis_js_api/library/3.9/3.9/js/esri/css/esri.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/script/base/jqueryui/jquery-ui.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/script/base/jqgrid/ui.jqgrid.css"/>

    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome-4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/public.css">
    <link rel="stylesheet" type="text/css" href="/css/topic.css">
    <link rel="stylesheet" type="text/css" href="/script/base/artcloudui/skins/default/css/artcloudui.css">

    <script type="text/javascript" src="/script/base/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="/script/base/jquery-1.11.0.js"></script>
    <script type="text/javascript" src="/script/base/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/script/base/json-js-master/json2.js"></script>
    <script type="text/javascript" src="/script/base/artcloudui/artcloudui.dialog.min.js"></script>
    <script type="text/javascript" src="/script/base/jqgrid/js/i18n/grid.locale-cn.js"></script>
    <script type="text/javascript" src="/script/base/jqgrid/js/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/script/base/html2canvas.js"></script>

    <script type="text/javascript">
        var root_gisapp = '<%=root_gisapp%>';
        var root_arcgis = '<%=root_arcgis%>';
        var dojoConfig = {
            parseOnLoad: true,
            packages: [{
                name: 'plugin',
                location: "/script/arcgis_js_api/plugin"
            }, {
                name: 'modules',
                location: "/script/modules"
            }]
        };
    </script>
    <script type="text/javascript" src="/script/arcgis_js_api/library/3.9/3.9/init.js"></script>
    <script type="text/javascript" src="/script/base.js"></script>
    <script type="text/javascript" src="/script/modules/common.js"></script>
    <script type="text/javascript" src="/script/modules/cityfunction.js"></script>
    <script type="text/javascript">
        function initgis() {
            cityFunction_Init();
        }
    </script>
</head>
<body>
<div class="map-main">
    <div id="map" class="map-wrap"></div>

    <!--搜索框-->
    <div class="search-panel" style="display: block;">
        <div class="srh-l">
            <input type="text" class="srh-inp" placeholder="关键字搜索">
        </div>
        <div class="srh-r">
            <button type="submit" class="btn btn-primary btn-search" onClick="gis_common.query();">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </div>
    <!--end搜索框-->

    <!--搜索弹出面板-->
    <div id='commonquery_content' class="modal-search">
        <div class="panel">
            <div class="panel-body" style="padding:0;">
                <div class="search-cont">
                    <div class="hide-search">鼠标移入 展开</div>
                    <ul class="search-cont-list">

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--end搜索弹出面板-->

    <!--end搜索弹出面板-->
    <!--导航-->
    <div class="nav-slide">
        <div class="nav-btn"><i class="fa fa-navicon"></i></div>
    </div>
    <ul class="nav-list">
        <li><a href="javascript:void(0);" onClick="cityFun_createQueryParamHtml();"><i class="fa fa-subway"></i><br>城市功能区分析</a></li>

    </ul>
    <!--end导航-->

    <!--导航二级弹出-->
    <div id='div_menu'>
    </div>
    <!--end导航二级弹出-->

    <!--工具栏-->
    <div class="toolbox" style="display:block;">
        <div class="toolbox-wrap">
            <ul class="toolbar">
                <li>
                    <a href="javascript:void(0);" onclick="createDrawToolbar(this);"><i class="fa fa-suitcase"></i>
                        工具</a>
                </li>
                <li>
                    <a href="javascript:void(0);" onclick="gis_common.resLoad(this);"><i
                            class="fa fa-files-o"></i>资源</a>
                </li>
                <li>
                    <a href="javascript:void(0);" onclick="loadPoiCategory(this);"><i class="fa fa-files-o"></i>POI</a>
                </li>
            </ul>
            <div class="tabs-modal" id="mymodal">
                <div class="panel">
                    <div class="list-group">
                        <button type="button" class="list-group-item"><i class="fa fa-file-o"></i>
                        </button>
                        <button type="button" class="list-group-item"><i class="fa fa-map"></i> Dapibus ac facilisis in
                        </button>
                        <button type="button" class="list-group-item"><i class="fa fa-ruble"></i> Morbi leo risus
                        </button>
                        <button type="button" class="list-group-item"><i class="fa fa-won"></i> Porta ac consectetur ac
                        </button>
                        <button type="button" class="list-group-item"><i class="fa fa-android"></i> Vestibulum at eros
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="div_toolbox_content">
    </div>
    <!--end工具栏-->
</div>
</body>
</html>