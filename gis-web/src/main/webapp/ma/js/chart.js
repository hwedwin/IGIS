$(function(){

    require.config({
        paths: {
            echarts: 'plugins/echarts/'
        }
    });

    // 基于准备好的dom，初始化echarts实例
    //var myChart = echarts.init(document.getElementById('main'));

    var base = +new Date(2015, 9, 3);
    var oneDay = 24 * 3600 * 1000;
    var date = [];

    var data = [Math.random() * 150];

    for (var i = 1; i < 100; i++) {
        var now = new Date(base += oneDay);
        date.push([now.getFullYear(), now.getMonth() + 1, now.getDate()].join('-'));
        data.push((Math.random() - 0.4) * 20 + data[i - 1]);
    }



    chart1();
    chart2();
    chart3();
    chart4();
    chart5();





});


function chart1(){
    var option = {
        backgroundColor:'#be180e',
        tooltip : {
            trigger: 'axis'
        },
        toolbox: {
            show : true,
            feature : {
                mark : {show: true},
                dataView : {show: true, readOnly: false},
                magicType: {show: true, type: ['line', 'bar']},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        calculable : true,
        legend: {
            data:['当日返乡人数','累计返乡人数'],
            y:'bottom'
        },
        xAxis : [
            {
                type : 'category',
                data : ['2016/1/1','2016/1/2','2016/1/3','2016/1/4','2016/1/5','2016/1/6','2016/1/7','2016/1/8','2016/1/9','2016/1/10','2016/1/11','2016/1/12','2016/1/13','2016/1/14','2016/1/15','2016/1/16','2016/1/17','2016/1/18','2016/1/19','2016/1/20','2016/1/21','2016/1/22','2016/1/23','2016/1/24','2016/1/25','2016/1/26','2016/1/27','2016/1/28','2016/1/29','2016/1/30','2016/1/31','2016/2/1','2016/2/2','2016/2/3','2016/2/4','2016/2/5','2016/2/6']
            }
        ],
        yAxis : [
            {
                type : 'value',
                name : '用户数',
                axisLabel : {
                    formatter: '{value}'
                }
            },
            {
                type : 'value',
                name : '用户数',
                axisLabel : {
                    formatter: '{value}'
                }
            }
        ],
        series : [

            {
                name:'当日返乡人数',
                type:'line',
                data:[41243,48311,275740,118204,40112,25999,4022,33211,12607,6115,5426,63803,52257,17738,72291,40543,31602,27940,70121,50208,91391,54126,74115,64895,213567,203978,202170,156397,177448,357683,353272,430056,290096,268276,263578,3807,233203]
            },
            {
                name:'累计返乡人数',
                type:'bar',
                yAxisIndex: 1,
                data:[41243,89554,365294,483498,443386,469385,473407,506618,519225,525340,530766,594569,646826,664564,736855,777398,809000,836940,907061,957269,1048660,1102786,1176901,1241796,1455363,1659341,1861511,2017908,2195356,2553039,2906311,3336367,3626463,3894739,4158317,4154510,4387713]
            }
        ]
    };



    require(
        [
            'echarts',
            'echarts/theme/dark',
            'echarts/chart/line',
            'echarts/chart/bar'
        ],
        function (ec, theme) {
            var myChart1 = ec.init(document.getElementById('main'), theme);
            myChart1.setOption(option);
        }
    ); //end
};


function chart2(){


    var option = {
        backgroundColor:'#be180e',
        color: ['gold','aqua','lime'],
        title : {
            //text: '回家过年的江西人',
            subtext:'',
            x:'right',
            textStyle : {
                color: '#fff'
            }
        },
        tooltip : {
            trigger: 'item',
            formatter: function (v) {
                return v[1].replace(':', ' > ');
            }
        },
        toolbox: {
            show : true,
            orient : 'vertical',
            x: 'right',
            y: 'center',
            feature : {
                mark : {show: true},
                dataView : {show: true, readOnly: false},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },

        series : [
            {
                name:'北京',
                type:'map',
                roam: true,
                hoverable: false,
                mapType: 'china',
                itemStyle:{
                    normal:{
                        borderColor:'rgba(100,149,237,1)',
                        borderWidth:0.5,
                        areaStyle:{
                            color: '#1b1b1b'
                        }
                    }
                },
                data:[],
                geoCoord: {
                    '上海': [121.4648,31.2891],
                    '东莞': [113.8953,22.901],
                    '东营': [118.7073,37.5513],
                    '中山': [113.4229,22.478],
                    '临汾': [111.4783,36.1615],
                    '临沂': [118.3118,35.2936],
                    '丹东': [124.541,40.4242],
                    '丽水': [119.5642,28.1854],
                    '乌鲁木齐': [87.9236,43.5883],
                    '佛山': [112.8955,23.1097],
                    '保定': [115.0488,39.0948],
                    '兰州': [103.5901,36.3043],
                    '包头': [110.3467,41.4899],
                    '北京': [116.4551,40.2539],
                    '北海': [109.314,21.6211],
                    '南京': [118.8062,31.9208],
                    '南宁': [108.479,23.1152],
                    '南昌': [116.0046,28.6633],
                    '南通': [121.1023,32.1625],
                    '厦门': [118.1689,24.6478],
                    '台州': [121.1353,28.6688],
                    '合肥': [117.29,32.0581],
                    '呼和浩特': [111.4124,40.4901],
                    '咸阳': [108.4131,34.8706],
                    '哈尔滨': [127.9688,45.368],
                    '唐山': [118.4766,39.6826],
                    '嘉兴': [120.9155,30.6354],
                    '大同': [113.7854,39.8035],
                    '大连': [122.2229,39.4409],
                    '天津': [117.4219,39.4189],
                    '太原': [112.3352,37.9413],
                    '威海': [121.9482,37.1393],
                    '宁波': [121.5967,29.6466],
                    '宝鸡': [107.1826,34.3433],
                    '宿迁': [118.5535,33.7775],
                    '常州': [119.4543,31.5582],
                    '广州': [113.5107,23.2196],
                    '廊坊': [116.521,39.0509],
                    '延安': [109.1052,36.4252],
                    '张家口': [115.1477,40.8527],
                    '徐州': [117.5208,34.3268],
                    '德州': [116.6858,37.2107],
                    '惠州': [114.6204,23.1647],
                    '成都': [103.9526,30.7617],
                    '扬州': [119.4653,32.8162],
                    '承德': [117.5757,41.4075],
                    '拉萨': [91.1865,30.1465],
                    '无锡': [120.3442,31.5527],
                    '日照': [119.2786,35.5023],
                    '昆明': [102.9199,25.4663],
                    '杭州': [119.5313,29.8773],
                    '枣庄': [117.323,34.8926],
                    '柳州': [109.3799,24.9774],
                    '株洲': [113.5327,27.0319],
                    '武汉': [114.3896,30.6628],
                    '汕头': [117.1692,23.3405],
                    '江门': [112.6318,22.1484],
                    '沈阳': [123.1238,42.1216],
                    '沧州': [116.8286,38.2104],
                    '河源': [114.917,23.9722],
                    '泉州': [118.3228,25.1147],
                    '泰安': [117.0264,36.0516],
                    '泰州': [120.0586,32.5525],
                    '济南': [117.1582,36.8701],
                    '济宁': [116.8286,35.3375],
                    '海口': [110.3893,19.8516],
                    '淄博': [118.0371,36.6064],
                    '淮安': [118.927,33.4039],
                    '深圳': [114.5435,22.5439],
                    '清远': [112.9175,24.3292],
                    '温州': [120.498,27.8119],
                    '渭南': [109.7864,35.0299],
                    '湖州': [119.8608,30.7782],
                    '湘潭': [112.5439,27.7075],
                    '滨州': [117.8174,37.4963],
                    '潍坊': [119.0918,36.524],
                    '烟台': [120.7397,37.5128],
                    '玉溪': [101.9312,23.8898],
                    '珠海': [113.7305,22.1155],
                    '盐城': [120.2234,33.5577],
                    '盘锦': [121.9482,41.0449],
                    '石家庄': [114.4995,38.1006],
                    '福州': [119.4543,25.9222],
                    '秦皇岛': [119.2126,40.0232],
                    '绍兴': [120.564,29.7565],
                    '聊城': [115.9167,36.4032],
                    '肇庆': [112.1265,23.5822],
                    '舟山': [122.2559,30.2234],
                    '苏州': [120.6519,31.3989],
                    '莱芜': [117.6526,36.2714],
                    '菏泽': [115.6201,35.2057],
                    '营口': [122.4316,40.4297],
                    '葫芦岛': [120.1575,40.578],
                    '衡水': [115.8838,37.7161],
                    '衢州': [118.6853,28.8666],
                    '西宁': [101.4038,36.8207],
                    '西安': [109.1162,34.2004],
                    '贵阳': [106.6992,26.7682],
                    '连云港': [119.1248,34.552],
                    '邢台': [114.8071,37.2821],
                    '邯郸': [114.4775,36.535],
                    '郑州': [113.4668,34.6234],
                    '鄂尔多斯': [108.9734,39.2487],
                    '重庆': [107.7539,30.1904],
                    '金华': [120.0037,29.1028],
                    '铜川': [109.0393,35.1947],
                    '银川': [106.3586,38.1775],
                    '镇江': [119.4763,31.9702],
                    '长春': [125.8154,44.2584],
                    '长沙': [113.0823,28.2568],
                    '长治': [112.8625,36.4746],
                    '阳泉': [113.4778,38.0951],
                    '青岛': [120.4651,36.3373],
                    '韶关': [113.7964,24.7028]
                },

                markLine : {
                    smooth:true,
                    effect : {
                        show: true,
                        scaleSize: 1,
                        period: 30,
                        color: '#fff',
                        shadowBlur: 10
                    },
                    itemStyle : {
                        normal: {
                            borderWidth:1,
                            lineStyle: {
                                type: 'solid',
                                shadowBlur: 10
                            }
                        }
                    },
                    data : [
                        [{name:'上海'},{name:'南昌',value:18.9}],
                        [{name:'北京'},{name:'南昌',value:15.2}],
                        [{name:'广州'},{name:'南昌',value:10.3}],
                        [{name:'西安'},{name:'南昌',value:7.5}],
                        [{name:'包头'},{name:'南昌',value:4.3}]
                    ]
                },
                markPoint : {
                    symbol:'emptyCircle',
                    symbolSize : function (v){
                        return 10 + v/10
                    },
                    effect : {
                        show: true,
                        shadowBlur : 0
                    },
                    itemStyle:{
                        normal:{
                            label:{show:false}
                        }
                    },
                    data : [
                        {name:'上海',value:18.9},
                        {name:'北京',value:15.2},
                        {name:'广州',value:10.3},
                        {name:'西安',value:7.5},
                        {name:'包头',value:4.3}
                    ]
                }

            },
        ]
    };


    require(
        [
            'echarts',
            'echarts/theme/dark',
            'echarts/chart/map'
        ],
        function (ec, theme) {
            var myChart2 = ec.init(document.getElementById('main2'), theme);
            myChart2.setOption(option);
        }
    ); //end




};

function chart3(){
    var option = {
        //backgroundColor:'#be180e',
        title : {
            //text: '返乡最多的地市',
            subtext: '',
            x:'center'
        },
        tooltip : {
            trigger: 'item'
        },
        dataRange: {
            min: 0,
            max: 100,
            x: 'left',
            y: 'bottom',
            text:['高','低'],           // 文本，默认为数值文本
            calculable : true
        },
        toolbox: {
            show: true,
            orient : 'vertical',
            x: 'right',
            y: 'center',
            feature : {
                mark : {show: true},
                dataView : {show: true, readOnly: false},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        roamController: {
            show: true,
            x: 'right',
            mapTypeControl: {
                'china': true
            }
        },
        series : [
            {
                name: 'iphone3',
                type: 'map',
                mapType: '江西',
                roam: false,
                itemStyle:{
                    normal:{label:{show:true}},
                    emphasis:{label:{show:true}}
                },
                data:[
                    {name:'上饶市',value:15.01},
                    {name:'赣州市',value:19.00},
                    {name:'九江市',value:12.86},
                    {name:'吉安市',value:13.41},
                    {name:'宜春市  ',value:12.88},
                    {name:'抚州市 ',value:7.53},
                    {name:'南昌市 ',value:9.82},
                    {name:'鹰潭市 ',value:2.16},
                    {name:'萍乡市 ',value:3.44},
                    {name:'景德镇市',value:1.92},
                    {name:'新余市 ',value:1.97},
                ]
            }
        ]
    };

    require(
        [
            'echarts',
            'echarts/theme/dark',
            'echarts/chart/map'
        ],
        function (ec, theme) {
            var myChart2 = ec.init(document.getElementById('main3'), theme);
            myChart2.setOption(option);
        }
    ); //end
};

function chart4(){
    var option = {
        title: {
            x: 'center',
            text: '线上购物也已深入人心',
            subtext: 'Rainbow bar example',
            link: 'http://echarts.baidu.com/doc/example.html'
        },
        tooltip: {
            trigger: 'item'
        },
        toolbox: {
            show: true,
            feature: {
                dataView: {show: true, readOnly: false},
                restore: {show: true},
                saveAsImage: {show: true}
            }
        },
        calculable: true,
        grid: {
            borderWidth: 0,
            y: 80,
            y2: 60
        },
        xAxis: [
            {
                type: 'category',
                show: false,
                data : ['淘宝','京东','亚马逊','1号店','苏宁易购','当当','凡客','国美商城','聚美优品']
            }
        ],
        yAxis: [
            {
                type: 'value',
                show: false
            }
        ],
        series: [
            {
                name: '线上购物也已深入人心',
                type: 'bar',
                itemStyle: {
                    normal: {
                        color: function(params) {
                            // build a color map as your need.
                            var colorList = [
                                '#C1232B','#B5C334','#FCCE10','#E87C25','#27727B',
                                '#FE8463','#9BCA63','#FAD860','#F3A43B','#60C0DD',
                                '#D7504B','#C6E579','#F4E001','#F0805A','#26C0C0'
                            ];
                            return colorList[params.dataIndex]
                        },
                        label: {
                            show: true,
                            position: 'top',
                            formatter: '{b}\n{c}'
                        }
                    }
                },
                data : [52.28,34.92,1.06,3.51,0.18,2.94,5,0.04,0.07]
            }
        ]
    };

    require(
        [
            'echarts',
            'echarts/theme/dark',
            'echarts/chart/bar'
        ],
        function (ec, theme) {
            var myChart2 = ec.init(document.getElementById('main4'), theme);
            myChart2.setOption(option);
        }
    ); //end
};

function chart5(){
    var option = {
        backgroundColor:'#be180e',
        title : {
            //text: '线上购物最疯狂',
            subtext: '',
            x:'center'
        },
        tooltip : {
            trigger: 'item'
        },
        dataRange: {
            min: 0,
            max: 100,
            x: 'left',
            y: 'bottom',
            text:['高','低'],           // 文本，默认为数值文本
            calculable : true
        },
        toolbox: {
            show: true,
            orient : 'vertical',
            x: 'right',
            y: 'center',
            feature : {
                mark : {show: true},
                dataView : {show: true, readOnly: false},
                restore : {show: true},
                saveAsImage : {show: true}
            }
        },
        roamController: {
            show: true,
            x: 'right',
            mapTypeControl: {
                'china': true
            }
        },
        series : [
            {
                name: 'iphone3',
                type: 'map',
                mapType: '江西',
                roam: false,
                itemStyle:{
                    normal:{label:{show:true}},
                    emphasis:{label:{show:true}}
                },
                data:[
                    {name:'南昌市',value:10.29},
                    {name:'九江市',value:12.58},
                    {name:'赣州市',value:17.42},
                    {name:'吉安市',value:11.09},
                    {name:'萍乡市',value:3.57},
                    {name:'鹰潭市',value:4.22},
                    {name:'新余市',value:2.83},
                    {name:'宜春市',value:10.81},
                    {name:'上饶市',value:16.11},
                    {name:'景德镇市',value:2.39},
                    {name:'抚州市',value:8.69},
                ]
            }
        ]
    };

    require(
        [
            'echarts',
            'echarts/theme/dark',
            'echarts/chart/map'
        ],
        function (ec, theme) {
            var myChart2 = ec.init(document.getElementById('main5'), theme);
            myChart2.setOption(option);
        }
    ); //end
};