package bean.ltecover;


import bean.common.Cell;

/**
 * Created by Xiaobing on 2016/12/23.
 */
public class GridCellRel extends Cell {
    private String time;
    private String gridId;
    private int index;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getGridId() {
        return gridId;
    }

    public void setGridId(String gridId) {
        this.gridId = gridId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
