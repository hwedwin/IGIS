/**
 * Created by Xiaobing on 2016/5/23.
 */

function HighSpeedRail() {
    var hightLineLayer = gis_core.addGraphicsLayer("HighSpeedRail_hightLineLayer", "建议路线");
    var cellLayer = gis_core.addGraphicsLayer("HighSpeedRail_celllayer", "高铁小区");
    var mroLayer = gis_core.addGraphicsLayer("HighSpeedRail_mroLayer", "MRO分布");
    var map_click_handle = null;
    var hightLineLayer_click_handle = null;
    var hightLineLayer_click_over = null;
    var hightLineLayer_click_out = null;
    var cellLayer_click_handle = null;
    var wmsLayer = null; //渲染图层
    //查询参数
    var queryType = "";
    var timetype = "";
    var time = "";
    var mro_time = "";
    var query = "";
    var kpi = "";
    var lsLoadCell = false;

    //地图改变事件
    var extentchange = function (delta) {
        if (lsLoadCell == true) {
            loadTrackCell();
        }
    };

    //测试方法
    var loadLinePoint = function () {
        $.ajax({
            url: "/gethighspeedrailallpoints",
            data: {
                time: "time"
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    //createHightSpeedRailTestData(data);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var createHightSpeedRailTestData = function (points) {
        /*var polygon = new esri.geometry.Polygon(gis_core.map.spatialReference);
         var rings = [];
         for (var j = 0; j < points.length; j++) {
         var p = points[j];
         rings.push([p.longitude, p.latitude]);
         }
         polygon.addRing(rings);
         var symbol = new esri.symbol.SimpleLineSymbol().setColor(new esri.Color([255, 0, 0])).setWidth(2);
         var graphic = new esri.Graphic(polygon, symbol);
         hightLineLayer.add(graphic);*/

        require(["esri/graphic", "esri/geometry/Point", "esri/symbols/PictureMarkerSymbol", "esri/symbols/TextSymbol", "dojo/domReady!"],
            function (Graphic, Point, PictureMarkerSymbol, TextSymbol) {
                for (var j = 0; j < points.length; j++) {
                    var p = points[j];

                    var point = new Point(p.longitude, p.latitude, gis_core.map.spatialReference);

                    var textSymbol = new TextSymbol(p.name);
                    textSymbol.setAlign(TextSymbol.ALIGN_START);
                    textSymbol.setOffset(5, -5);
                    var textgraphic = new Graphic(point, textSymbol);
                    hightLineLayer.add(textgraphic);

                    var symbol = new PictureMarkerSymbol('/images/poi.png', 10, 10);
                    var graphic = new Graphic(point, symbol);

                    hightLineLayer.add(graphic);
                }
            });

    };
    var loadLineGrid = function () {
        $.ajax({
            url: "/gethighspeedraillinerrid",
            data: {
                time: "time"
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    createHightSpeedRailGridTestData(data);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var createHightSpeedRailGridTestData = function (grids) {
        for (var j = 0; j < grids.length; j++) {
            var rings = [];
            var g = grids[j];
            rings.push([g.extent.xMin, g.extent.yMin]);
            rings.push([g.extent.xMax, g.extent.yMin]);
            rings.push([g.extent.xMax, g.extent.yMax]);
            rings.push([g.extent.xMin, g.extent.yMax]);
            rings.push([g.extent.xMin, g.extent.yMin]);

            var polygon = new esri.geometry.Polygon(gis_core.map.spatialReference);
            polygon.addRing(rings);
            var symbol = new esri.symbol.SimpleLineSymbol().setColor(new esri.Color([0, 255, 0])).setWidth(1);
            var graphic = new esri.Graphic(polygon, symbol);
            hightLineLayer.add(graphic);
        }

        var index = 0;
        var id = "";
        var c1 = new esri.Color([0, 0, 255]);
        var c2 = new esri.Color([0, 255, 0]);
        var c3 = new esri.Color([255, 0, 0]);
        var rings_line = [];
        for (var j = 0; j < grids.length; j++) {
            var p = grids[j];
            if (rings_line.length == 0) {
                rings_line.push([p.startPoint.x, p.startPoint.y]);
            }
            else {
                rings_line.push([p.endPoint.x, p.endPoint.y]);
            }
            if (gis_common.stringIsNullOrWhiteSpace(id)) {
                id = p.stationToStationId;
            } else if (id != p.stationToStationId) {
                readerTestLine(rings_line, index);
                index = index + 1;
                if (index >= 3) {
                    index = 0;
                }
                rings_line = [];
                id = p.stationToStationId
            }
        }
        readerTestLine(rings_line, index);
    };
    var readerTestLine = function (rings_line, index) {
        var c1 = new esri.Color([0, 0, 255]);
        var c2 = new esri.Color([0, 255, 0]);
        var c3 = new esri.Color([255, 0, 0]);

        var symbol = new esri.symbol.SimpleLineSymbol().setColor(c1).setWidth(1);
        if (index == 0) {
            symbol = new esri.symbol.SimpleLineSymbol().setColor(c1).setWidth(1)
        }
        else if (index == 1) {
            symbol = new esri.symbol.SimpleLineSymbol().setColor(c2).setWidth(1)
        }
        else if (index == 2) {
            symbol = new esri.symbol.SimpleLineSymbol().setColor(c3).setWidth(1)
        }

        var polygon_line = new esri.geometry.Polygon(gis_core.map.spatialReference);
        polygon_line.addRing(rings_line);
        var graphic = new esri.Graphic(polygon_line, symbol);
        hightLineLayer.add(graphic);
    };
    var loadLineGridEx = function () {
        $.ajax({
            url: "/gethighspeedraillinerridex",
            data: {
                time: "time"
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (grids) {
                    for (var j = 0; j < grids.length; j++) {
                        var rings = [];
                        var g = grids[j];
                        rings.push([g.extent.xMin, g.extent.yMin]);
                        rings.push([g.extent.xMax, g.extent.yMin]);
                        rings.push([g.extent.xMax, g.extent.yMax]);
                        rings.push([g.extent.xMin, g.extent.yMax]);
                        rings.push([g.extent.xMin, g.extent.yMin]);

                        var polygon = new esri.geometry.Polygon(gis_core.map.spatialReference);
                        polygon.addRing(rings);
                        var symbol = new esri.symbol.SimpleLineSymbol().setColor(new esri.Color([0, 255, 0])).setWidth(1);
                        var graphic = new esri.Graphic(polygon, symbol);
                        hightLineLayer.add(graphic);
                    }
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };


    //线路查询  车次查询 用户查询  功能块
    var loadTrackListQueryParam = function (callback, _queryType) {
        $.ajax({
            url: "/gettrackList",
            data: {
                querytype: queryType
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var div = createQueryParamHtml(data, _queryType);
                    callback(div);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var createQueryParamHtml = function (data, _queryType) {
        var content = "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>时间粒度:</label>";
        content += "	<div id='highSpeedRail_" + _queryType + "_timetype' class='col-sm-9'>";
        content += "		<select id='highSpeedRail_" + _queryType + "_timetype_select' class='form-control'>";
        content += "			<option selected='true' va='2'>天</option>";
        content += "			<option va='1'>小时</option>";
        content += "		</select>";
        content += "	</div>";
        content += " </div > "

        content += "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
        content += "	<div id='highSpeedRail_" + _queryType + "_div' class='col-sm-9'>";
        //content += "			<input id='highSpeedRail_" + _queryType + "_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd'});\"/>";
        content += "			<input id='highSpeedRail_" + _queryType + "_time_2' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd'});\"/>";
        content += "			<input id='highSpeedRail_" + _queryType + "_time_1' style='display: none' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH'});\"/>";
        content += " 	</div > ";
        content += " </div > "

        content += "<div class=\"form-group\">";
        if (_queryType == "station") {
            content += "<label class='col-sm-3 control-label'>线路名称:</label>";
        }
        else if (_queryType == "trip") {
            content += "<label class='col-sm-3 control-label'>车次名称:</label>";
        }
        else if (_queryType == "user") {
            content += "<label class='col-sm-3 control-label'>用户号码:</label>";
        }
        content += "<div id='highSpeedRail_" + _queryType + "_Lines' class='col-sm-9'>";
        if (_queryType == "station" || _queryType == "trip") {
            content += "	<select id='highSpeedRail_" + _queryType + "_Lines_select' class='form-control'>";
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                content += " <option va='" + d.id + "'>" + d.name + "</option>"
            }
            content += "    </select>";
        }
        else if (_queryType == "user") {
            content += "<input id='highSpeedRail_" + _queryType + "_no' type='text' class='form-control'\"/>";
        }
        content += "	</div>";
        content += " </div > "

        content += "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>指标:</label>";
        content += "	<div id='highSpeedRail_kpi_div' class='col-sm-9'>";
        content += "		<select id='highSpeedRail_kpi_select' class='form-control'>";
        content += "			<option selected='true' va='accept_avg_level'>接收平均电平</option>";
        content += "			<option  va='accept_avg_quality'>接收平均质量</option>";
        content += "			<option  va='accept_avg_sina'>接收平均干扰功率</option>";
        content += "			<option  va='weak_rat'>弱覆盖比例</option>";
        content += "			<option  va='high_sina_rat'>高干扰占比</option>";
        //content += "			<option  va='flow'>流量</option>";
        content += "			<option  va='offline_cnt'>脱网次数</option>";
        content += "		</select>";
        content += "	</div>";
        content += " </div > "

        content += "<div class='form-group'>";
        content += " 	<div class='col-sm-9 col-sm-offset-3'>";
        content += "		<button id='highSpeedRail_" + _queryType + "_btn_load' class='btn btn-primary'>确 定</button>";
        content += "	</div>";
        content += "</div>";


        var div = $(content);

        div.find("#highSpeedRail_" + _queryType + "_timetype_select").bind("change", function () {
            //时间粒度
            var selects = $("#highSpeedRail_" + _queryType + "_timetype option:selected");
            var timetype = selects.attr("va");
            div.find("#highSpeedRail_" + _queryType + "_time_" + timetype).addClass("show").removeClass("hidden").siblings().removeClass('show').addClass("hidden");
        });

        div.find("#highSpeedRail_" + _queryType + "_btn_load").bind("click", function () {
            //时间粒度
            var selects = $("#highSpeedRail_" + _queryType + "_timetype option:selected");
            timetype = selects.attr("va");

            //时间
            time = div.find("#highSpeedRail_" + _queryType + "_time_" + timetype + "").val().replace('-', '').replace('-', '').replace(' ', '');
            if (gis_common.stringIsNullOrWhiteSpace(time)) {
                gis_common.showMessage("请选择时间");
                div.find("#highSpeedRail_" + _queryType + "_time_" + timetype + "").focus();
                return;
            }
            //渲染内容
            if (_queryType == "station" || _queryType == "trip") {
                var s = div.find("#highSpeedRail_" + _queryType + "_Lines_select option:selected");
                query = s.attr("va");
            }
            else if (_queryType == "user") {
                query = div.find("#highSpeedRail_" + _queryType + "_no").val()
            }
            if (gis_common.stringIsNullOrWhiteSpace(query)) {
                gis_common.showMessage("请选择目标");
                return;
            }
            //kpi
            var kpiselect = $("#highSpeedRail_kpi_select option:selected");
            kpi = kpiselect.attr("va");
            lsLoadCell = false;
            queryType = _queryType;
            loadTrackLine(timetype, time, queryType, query, kpi);
            loadLegend();
        });
        return div;
    }
    var loadTrackLine = function (timetype, time, queryType, query, kpi) {
        hightLineLayer.clear();
        cellLayer.clear();
        mroLayer.clear();
        loadWmsLayer(null);
        $.ajax({
            url: "/gettracline",
            data: {
                querytype: queryType,
                query: query,
                time: time,
                timetype: timetype,
                fieldname: kpi
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    readerTrackLine(data);
                    getSerialWeakGridTable();
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var readerTrackLine = function (data) {
        if (data != null && data != undefined) {
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                var colors = d.color.split(",");
                var color = new esri.Color([colors[0], colors[1], colors[2]]);
                var rings_line = [];
                var rings = d.ring.split(",");
                var length = rings.length
                for (var j = 0; j < length; j = j + 2) {
                    if (j >= length || j + 1 > length) {
                        continue;
                    }
                    rings_line.push([rings[j], rings[j + 1]]);
                }
                var polygon_line = new esri.geometry.Polygon(gis_core.map.spatialReference);
                polygon_line.addRing(rings_line);

                var symbol = new esri.symbol.SimpleLineSymbol().setColor(color).setWidth(8);

                var attr = {
                    "data": d
                };
                var graphic = new esri.Graphic(polygon_line, symbol, attr);
                hightLineLayer.add(graphic);
            }
        }
    };
    var loadTrackKpiDetails = function (id) {
        $.ajax({
            url: "/gettrackkpidetails",
            data: {
                querytype: queryType,
                query: query,
                time: time,
                timetype: timetype,
                fieldname: kpi,
                id: id
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var div = gis_common.createKeyValueList(data);
                    gis_layout.createRightWidget("highSpeedRail_leftwidget_gettrackkpidetails", "指标明细", div);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var loadLegend = function () {
        if (gis_common.stringIsNullOrWhiteSpace(kpi)) {
            gis_common.showMessage("请选择查询参数");
            return;
        }
        $.ajax({
            url: "/gettracklenged",
            data: {
                kpi: kpi
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    createLengedHtml(data);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var createLengedHtml = function (legends) {//加载图例
        if (legends == null || legends == undefined || legends.length == 0) {
            gis_common.showMessage("初始化图例失败");
            return;
        }
        var content = "";
        for (var i = 0; i < legends.legendItems.length; i++) {
            var legend = legends.legendItems[i];
            var sRgb = "RGB(" + legend.r + ", " + legend.g + ", " + legend.b + ")"; //sRgb.colorHex()
            content += "<div class=\"legend-box\" style=\"background:rgba(" + legend.r + ", " + legend.g + ", " + legend.b + ",0.5);color:#fff;\"> " + legend.valueMin + " — " + legend.valueMax + "</div>";
        }
        var div = $(content);
        gis_layout.createLegend("track_line_legend", legends.name + "图例", div);
    };
    var getSerialWeakGridTable = function () {//加载路段 表格显示
        var content = "<div>";
        content += "<table id='HighSpeedRail_lines_Table'></table>";
        content += "<div id='HighSpeedRail_lines_Pager'></div>"
        content += "<div>";
        var div = $(content);
        gis_layout.createLeftWidget("HighSpeedRail_lines_JQGrid_Div", "路段", div);

        div.find("#HighSpeedRail_lines_Table").jqGrid({
            url: '/gettraclinelistgrid?time=' + time + "&querytype=" + queryType + "&query=" + query + "&timetype=" + timetype + "&fieldname=" + kpi,
            datatype: "json",
            width: 300,
            colNames: ['ID', '路段', '区间', 'maxLongitude', 'maxLatitude', 'minLongitude', 'minLatitude'],
            colModel: [{
                name: 'id',
                index: 'id',
                width: 1,
                hidden: true
            }, {
                name: 'index',
                index: 'index',
                width: 20
            }, {
                name: 'tag',
                index: 'tag',
                width: 60
            }, {
                name: 'maxLongitude',
                index: 'maxLongitude',
                width: 80,
                hidden: true
            }, {
                name: 'maxLatitude',
                index: 'maxLatitude',
                width: 80,
                hidden: true
            }, {
                name: 'minLongitude',
                index: 'minLongitude',
                width: 80,
                hidden: true
            }, {
                name: 'minLatitude',
                index: 'minLatitude',
                width: 80,
                hidden: true
            }
            ],
            rowNum: 10,
            rowList: [10, 20, 30, 50],
            pager: '#HighSpeedRail_lines_Pager',
            mtype: "get",
            viewrecords: true,
            caption: "",
            onSelectRow: function (id) {
                var data = div.find("#HighSpeedRail_lines_Table").jqGrid('getRowData', id);
                gis_common.setExtent(parseFloat(data.maxLongitude) + 0.0005, parseFloat(data.maxLatitude) + 0.0005, parseFloat(data.minLongitude) - 0.0005, parseFloat(data.minLatitude) - 0.0005);
                loadTrackKpiDetails(data.id);
                //var url = "/dynamicmap/serialweakgrid/weak_rat/" + data.time + "_" + data.id;
                //loadWmsLayer(url);
                //grid_id = data.id;
                //time = data.time;
            }
        });
    };

    //网格渲染 铁路渲染 高铁小区 MRO点显示
    var loadTrackId = function (callback) {
        $.ajax({
            url: "/gettrackList",
            data: {
                querytype: "station"
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var div = createCellMroHtml(data);
                    callback(div);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var createCellMroHtml = function (data) {
        var content = "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
        content += "	<div id='highSpeedRail_cellmro_div' class='col-sm-9'>";
        content += "			<input id='highSpeedRail_cellmro_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd'});\"/>";
        content += " 	</div > ";
        content += " </div > "

        content += "<div class=\"form-group\">";
        content += "<label class='col-sm-3 control-label'>线路名称:</label>";
        content += "    <div id='highSpeedRail_cellmro_Lines' class='col-sm-9'>";
        content += "	<select id='highSpeedRail_cellmro_Lines_select' class='form-control'>";
        for (var i = 0; i < data.length; i++) {
            var d = data[i];
            content += " <option va='" + d.id + "'>" + d.name + "</option>"
        }
        content += "    </select>";
        content += "	</div>";
        content += "</div >"

        content += "<div class='form-group'>";
        content += " 	<div class='col-sm-9 col-sm-offset-3'>";
        content += "		<button id='highSpeedRail_cellmro_btn_load' class='btn btn-primary'>确 定</button>";
        content += "	</div>";
        content += "</div>";

        var div = $(content);
        div.find("#highSpeedRail_cellmro_btn_load").bind("click", function () {
            mro_time = div.find("#highSpeedRail_cellmro_time").val().replace(/-/g, '');
            if (gis_common.stringIsNullOrWhiteSpace(mro_time)) {
                gis_common.showMessage("选择时间");
                div.find("#highSpeedRail_cellmro_time").focus();
                return;
            }
            var lineselect = div.find("#highSpeedRail_cellmro_Lines_select option:selected");
            var line = lineselect.attr("va");
            var url = "/dynamicmap/trackcridexpend/kpi/" + line;
            lsLoadCell = true;
            loadWmsLayer(url);
            loadTrackCell();
        });
        return div;
    };
    var loadWmsLayer = function (url) {//添加渲染图层
        if (wmsLayer != null || wmsLayer != undefined) {
            gis_core.removeLayer("highspeedrailwmsLayer")
        }
        if (url == null || url == undefined) {
            return;
        }
        wmsLayer = new esri.layers.WMSLayer(url, {
            resourceInfo: {
                extent: new esri.geometry.Extent(73.2672757, 18.37574, -109.66552734375, 133.29657, {
                    wkid: 4326
                }),
                layerInfos: []
            },
            visibleLayers: ['highspeedrailgrid']
        });
        wmsLayer.id = "highspeedrailwmsLayer";
        gis_core.insertLayer("highspeedrailwmsLayer", "网格覆盖", 2, wmsLayer);
    };
    var loadTrackCell = function () {
        cellLayer.clear();
        $.ajax({
            url: "/gettrackcell",
            data: {
                extent: JSON.stringify(gis_common.getMapExtent())
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (btses) {
                    showCell(btses);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var showCell = function (btses) {
        var graphics = gis_common.createCellCover(btses, 300);
        for (var j = 0; j < graphics.length; j++) {
            cellLayer.add(graphics[j]);
        }
    };
    var loadMROPoint = function (eci) {
        $.ajax({
            url: "/gettrackcellmro",
            data: {
                eci: eci,
                time: mro_time
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    // 标记边框
                    var outSymbol = new esri.symbol.SimpleLineSymbol();
                    outSymbol.setStyle(esri.symbol.SimpleLineSymbol.STYLE_NULL);

                    var symbolblue = new esri.symbol.SimpleMarkerSymbol();
                    symbolblue.setOutline(outSymbol);
                    symbolblue.setSize(3);
                    symbolblue.setColor(new esri.Color([0, 0, 255, 1]));

                    for (var i = 0; i < datas.length; i++) {
                        var b = datas[i];
                        var point = new esri.geometry.Point(b.longitude, b.latitude, gis_core.map.spatialReference);
                        var graphic = new esri.Graphic(point, symbolblue);
                        mroLayer.add(graphic);
                    }
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var loadCellDetail = function (eci) {
        $.ajax({
            url: "/getonetrackcell",
            data: {
                eci: eci
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var div = gis_common.createKeyValueList(data);
                    gis_layout.createRightWidget("highSpeedRail_leftwidget_oneTrackCelldetails", "小区信息", div);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    }


    this.loadLine = function () {
        hightLineLayer.clear();
        loadLinePoint();
        loadLineGrid();
    };

    this.createCellMroShow = function (callback) {
        loadTrackId(callback);
    };

    this.createQuerystationParam = function (callback) {
        queryType = "station";
        loadTrackListQueryParam(callback, queryType);
    };

    this.createQuerytripParam = function (callback) {
        queryType = "trip";
        loadTrackListQueryParam(callback, queryType);
    };

    this.createQueryuserParam = function (callback) {
        queryType = "user";
        loadTrackListQueryParam(callback, queryType);
    };
    /**
     * 初始化
     */
    this.init = function () {
        //加载库文件
        dojo.require("esri.layers.WMSLayerInfo");
        dojo.require("esri.layers.WMSLayer");
        gis_core.addExtentChange("highspeedrail", extentchange);
        gis_core.addPlugin(this);

        if (hightLineLayer_click_handle != null) {
            dojo.disconnect(hightLineLayer_click_handle);
        }
        hightLineLayer_click_handle = hightLineLayer.on("click", function (evt) {
            var data = evt.graphic.attributes.data
            if (data.color != "148,148,148") {
                loadTrackKpiDetails(data.id);
            }
        });
        if (hightLineLayer_click_over != null) {
            dojo.disconnect(hightLineLayer_click_over);
        }
        hightLineLayer_click_over = hightLineLayer.on("mouse-over", function (evt) {
            var color = evt.graphic.attributes.data.color
            if (color != "148,148,148") {
                gis_core.map.setMapCursor("pointer");
            }
        });

        if (hightLineLayer_click_out != null) {
            dojo.disconnect(hightLineLayer_click_out);
        }
        hightLineLayer_click_out = hightLineLayer.on("mouse-out", function (evt) {
            gis_core.map.setMapCursor("default");
        });

        if (cellLayer_click_handle != null) {
            dojo.disconnect(cellLayer_click_handle);
        }
        cellLayer_click_handle = cellLayer.on("click", function (evt) {
            var data = evt.graphic.attributes.data;
            if (data != null && data.tag != "cluster") {
                var eci = data.ecigci;
                mroLayer.clear();
                loadMROPoint(eci);
                loadCellDetail(eci);
            }
        });
    };
    /**
     * 销毁
     */
    this.destroy = function () {
        gis_core.removeLayer("HighSpeedRail_hightLineLayer");
        gis_core.removeLayer("HighSpeedRail_celllayer");
        gis_core.removeLayer("HighSpeedRail_mroLayer");
        gis_core.removeLayer("highspeedrailwmsLayer");
        gis_core.removeExtentChange("highspeedrail");
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();
        highSpeedRail = null;
    };
    /**
     * 模块信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "00_02_00",
        name: "高铁分析"
    };
}

var highSpeedRail = null;
function initHightSpeedRail() {
    if (highSpeedRail == null) {
        highSpeedRail = new HighSpeedRail();
        highSpeedRail.init();
    }
}

function highSpeedRail_loadTrackLine() {
    initHightSpeedRail();
    gis_layout.createMenubarWithFun("highSpeedRail_loadTrackLine_a", obj, "高铁线路", highSpeedRail.createQuerystationParam)
}
function highSpeedRail_loadTracktrip() {
    initHightSpeedRail();
    gis_layout.createMenubarWithFun("highSpeedRail_loadTracktrip_b", obj, "高铁车次", highSpeedRail.createQuerytripParam)
}
function highSpeedRail_loadTrackuser() {
    initHightSpeedRail();
    gis_layout.createMenubarWithFun("highSpeedRail_loadTracktrip_c", obj, "用户号码", highSpeedRail.createQueryuserParam)
}
function highSpeedRail_cellMroShow() {
    initHightSpeedRail();
    gis_layout.createMenubarWithFun("highSpeedRail_cellMroShow", obj, "MRO点分布", highSpeedRail.createCellMroShow)
}