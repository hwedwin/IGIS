package bean.highspeedrail;

/**
 * Created by Xiaobing on 2016/5/27.
 */
public class UserGridValue extends TripGridValue {
    private String mdn;
    private String imsi;
    private String teId;
    private String teName;

    public String getMdn() {
        return mdn;
    }

    public void setMdn(String mdn) {
        this.mdn = mdn;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public String getTeId() {
        return teId;
    }

    public void setTeId(String teId) {
        this.teId = teId;
    }

    public String getTeName() {
        return teName;
    }

    public void setTeName(String teName) {
        this.teName = teName;
    }
}
