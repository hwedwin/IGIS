package bean.knowledge.tide;

import gis.core.bean.KeyValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xiexb on 2017/8/2.
 */
public class TideDataDayBean {
    private String id_;
    private String stationNo;
    private String stationName;
    private double ew;
    private double ns;
    private String timeZone;
    private int daykey;
    private String tideTime1;
    private Integer timeHeight1;
    private String tideTime2;
    private Integer timeHeight2;
    private String tideTime3;
    private Integer timeHeight3;
    private String tideTime4;
    private Integer timeHeight4;
    private String tideTime5;
    private Integer timeHeight5;
    private String tideDatum;
    private List<KeyValue> tideLst = new ArrayList<>();

    public String getId_() {
        return id_;
    }

    public void setId_(String id_) {
        this.id_ = id_;
    }

    public String getStationNo() {
        return stationNo;
    }

    public void setStationNo(String stationNo) {
        this.stationNo = stationNo;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public double getEw() {
        return ew;
    }

    public void setEw(double ew) {
        this.ew = ew;
    }

    public double getNs() {
        return ns;
    }

    public void setNs(double ns) {
        this.ns = ns;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public int getDaykey() {
        return daykey;
    }

    public void setDaykey(int daykey) {
        this.daykey = daykey;
    }

    public String getTideTime1() {
        return tideTime1;
    }

    public void setTideTime1(String tideTime1) {
        this.tideTime1 = tideTime1;
    }

    public Integer getTimeHeight1() {
        return timeHeight1;
    }

    public void setTimeHeight1(Integer timeHeight1) {
        this.timeHeight1 = timeHeight1;
    }

    public String getTideTime2() {
        return tideTime2;
    }

    public void setTideTime2(String tideTime2) {
        this.tideTime2 = tideTime2;
    }

    public Integer getTimeHeight2() {
        return timeHeight2;
    }

    public void setTimeHeight2(Integer timeHeight2) {
        this.timeHeight2 = timeHeight2;
    }

    public String getTideTime3() {
        return tideTime3;
    }

    public void setTideTime3(String tideTime3) {
        this.tideTime3 = tideTime3;
    }

    public Integer getTimeHeight3() {
        return timeHeight3;
    }

    public void setTimeHeight3(Integer timeHeight3) {
        this.timeHeight3 = timeHeight3;
    }

    public String getTideTime4() {
        return tideTime4;
    }

    public void setTideTime4(String tideTime4) {
        this.tideTime4 = tideTime4;
    }

    public Integer getTimeHeight4() {
        return timeHeight4;
    }

    public void setTimeHeight4(Integer timeHeight4) {
        this.timeHeight4 = timeHeight4;
    }

    public String getTideTime5() {
        return tideTime5;
    }

    public void setTideTime5(String tideTime5) {
        this.tideTime5 = tideTime5;
    }

    public Integer getTimeHeight5() {
        return timeHeight5;
    }

    public void setTimeHeight5(Integer timeHeight5) {
        this.timeHeight5 = timeHeight5;
    }

    public String getTideDatum() {
        return tideDatum;
    }

    public void setTideDatum(String tideDatum) {
        this.tideDatum = tideDatum;
    }

    public List<KeyValue> getTideLst() {
        return tideLst;
    }

    public void setTideLst(List<KeyValue> tideLst) {
        this.tideLst = tideLst;
    }
}