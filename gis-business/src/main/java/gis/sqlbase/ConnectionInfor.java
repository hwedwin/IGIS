package gis.sqlbase;

/**
 * SQL JDBC 参数
 * 
 * @author
 */
public class ConnectionInfor {

	private String driver;

	private String user;

	private String password;

	private String url;

	/**
	 * 得到驱动
	 * 
	 * @return
	 */
	public String getDriver() {
		return driver;
	}

	/**
	 * 设置驱动
	 * 
	 * @param driver
	 */
	public void setDriver(String driver) {
		this.driver = driver;
	}

	/**
	 * 用户名
	 * 
	 * @return
	 */
	public String getUser() {
		return user;
	}

	/**
	 * 设置用户名
	 * 
	 * @param user
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * 用户名
	 * 
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 设置用户名
	 * 
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * 连接地址 带端口
	 * @return
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * 连接地址 带端口
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}
