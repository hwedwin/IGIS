package bean.common;

import gis.core.bean.KeyValue;

/**
 * Created by Xiaobing on 2016/8/18.
 */
public class SceneInfor extends KeyValue {
    private String sceneTypeId;
    private String sceneTypeName;

    public String getSceneTypeId() {
        return sceneTypeId;
    }

    public void setSceneTypeId(String sceneTypeId) {
        this.sceneTypeId = sceneTypeId;
    }

    public String getSceneTypeName() {
        return sceneTypeName;
    }

    public void setSceneTypeName(String sceneTypeName) {
        this.sceneTypeName = sceneTypeName;
    }
}
