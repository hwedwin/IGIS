<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@ include file="reference.jsp" %>
    <title></title>
    <script type="text/javascript">
        var dojoConfig = {
            parseOnLoad: true,
            packages: [{
                name: 'plugin',
                location: root + "script/arcgis_js_api/plugin"
            }]
        };
    </script>
    <link rel="stylesheet" type="text/css"
          href="<%=root%>script/arcgis_js_api/library/3.9/3.9/js/dojo/dijit/themes/tundra/tundra.css"/>
    <link rel="stylesheet" type="text/css" href="<%=root%>script/arcgis_js_api/library/3.9/3.9/js/esri/css/esri.css"/>
    <script type="text/javascript" src="<%=root%>script/arcgis_js_api/library/3.9/3.9/init.js"></script>
    <script type="text/javascript">
        // 功能插件
        var features = [];
        features.push("esri/map");
        features.push("esri/geometry/Point");
        features.push("esri/geometry/webMercatorUtils");
        features.push("esri/SpatialReference");
        features.push("esri/symbols/SimpleMarkerSymbol");
        features.push("esri/symbols/SimpleLineSymbol");
        features.push("esri/graphic");
        features.push("esri/layers/GraphicsLayer");
        features.push("dijit/form/Button");
        features.push("dojo/_base/Color");
        features.push("plugin/Voronoi4326Layer");
        features.push("plugin/Delaunay4326Layer");
        features.push("plugin/TianDiTuVecLayer");
        features.push("plugin/TianDiTuCvaLayer");
        features.push("dojo/domReady!");

        require(features, function (Map, Point, webMercatorUtils, SpatialReference, SimpleMarkerSymbol, SimpleLineSymbol, Graphic, GraphicsLayer, Button, Color, Voronoi4326Layer, Delaunay4326Layer, TianDiTuVecLayer, TianDiTuCvaLayer) {
            var map = new Map("map", {
                logo: false
            });

            var vec = new TianDiTuVecLayer();
            map.addLayer(vec);
            var delaunay = new Delaunay4326Layer();
            //map.addLayer(delaunay);
            var voronoi = new Voronoi4326Layer();
            map.addLayer(voronoi);
            var cva = new TianDiTuCvaLayer();
            map.addLayer(cva);

            // 添加画板
            var resLayer = new GraphicsLayer();
            map.addLayer(resLayer);

            // 标记边框
            var outSymbol = new SimpleLineSymbol();
            outSymbol.setStyle(SimpleLineSymbol.STYLE_NULL);

            // 地图点击事件
            map.on("click", function (evt) {
                // 右下角呈现基本信息
                var obj = {};
                obj.wkid = map.spatialReference.wkid;
                obj.level = map.getLevel();
                obj.x = evt.mapPoint.x;
                obj.y = evt.mapPoint.y;
                dojo.byId("labelR").innerHTML = $.toJSON(obj);

                var point = new Point(evt.mapPoint.x, evt.mapPoint.y, new SpatialReference({
                    wkid: map.spatialReference.wkid
                }));

                // 在地图上标记点
                var symbol = new SimpleMarkerSymbol();
                symbol.setOutline(outSymbol);
                symbol.setSize(10);
                symbol.setColor(new Color([255, 0, 0, 1]));
                var graphic = new Graphic(point, symbol);
                map.graphics.clear();
                map.graphics.add(graphic);

                // 左下角呈现业务信息
                var resolution = null;
                for (var i = 0; i < voronoi.tileInfo.lods.length; i++) {
                    if (voronoi.tileInfo.lods[i].level == map.getLevel()) {
                        resolution = voronoi.tileInfo.lods[i].resolution;
                        break;
                    }
                }
                dojo.byId("labelL").innerHTML = "";
                resLayer.clear();
                $.ajax({
                    url: root + "service/voronoi4326",
                    type: "post",
                    data: {
                        x: point.x,
                        y: point.y
                    },
                    success: function () {
                        dojo.byId("labelL").innerHTML = arguments[0];
                        var obj = eval("[" + arguments[0] + "][0]");
                        if (obj && obj.x && obj.y) {
                            // 标记资源位置
                            var s = new SimpleMarkerSymbol();
                            s.setOutline(outSymbol);
                            s.setSize(6);
                            s.setColor(new Color([0, 0, 255, 1]));
                            var p = new Point(obj.x, obj.y, new SpatialReference({
                                wkid: map.spatialReference.wkid
                            }));
                            var g = new Graphic(p, s);
                            resLayer.add(g);
                        }
                    },
                    error: function () {
                        var error = arguments[0].responseText;
                        if (error)
                            alert(error);
                    }
                });
            });

            // 初始定位
            map.centerAndZoom(new Point({
                x: 106.55548282458164,
                y: 29.559427429311895,
                spatialReference: {
                    wkid: 4326
                }
            }), 12);

            // 点击定位
            var button = new Button({
                label: "Locate",
                onClick: function () {
                    var point = new Point(106.5185541708646, 29.621794153326178, new SpatialReference({
                        wkid: map.spatialReference.wkid
                    }));

                    var symbol = new SimpleMarkerSymbol();
                    symbol.setOutline(outSymbol);
                    symbol.setSize(14);
                    symbol.setColor(new Color([255, 0, 0, 1]));

                    map.centerAndZoom(point, 15);

                    var graphic = new Graphic(point, symbol);
                    map.graphics.clear();
                    map.graphics.add(graphic);
                }
            }, "locateHome");
        });
    </script>
</head>
<body>
<div class="tundra">
    <div>泰森多边形:TianDiTuVecLayer,TianDiTuCvaLayer:4326</div>
    <button id="locateHome" type="button"></button>
    <div id="map"></div>
</div>
<div id="labelL" style="position: fixed; bottom: 20px; left: 0px"></div>
<div id="labelR" style="position: fixed; bottom: 0px; right: 0px"></div>
</body>
</html>
