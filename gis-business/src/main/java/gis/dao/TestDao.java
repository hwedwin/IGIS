package gis.dao;

import java.util.List;

import bean.common.Bts;
import gis.common.ServiceFactory;
import gis.service.TestService;

public class TestDao {

	public void Test() {

		try {
			TestService service = ServiceFactory.getInstance(TestService.class);
			service.Test();
		}
		catch (Exception e) {

			System.out.print(e);
		}
	}
	
	public List<Bts> ShowTestPoint()
	{
		TestService service = ServiceFactory.getInstance(TestService.class);
		return service.ShowTestPoint();
		
	}
}
