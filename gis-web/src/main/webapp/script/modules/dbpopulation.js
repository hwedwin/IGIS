﻿
function DbPopulation() {
	var grid_url = "/dynamicmap/dbpopulation";
	var kpi = "weak_rat";
	var time = 201602;
	var wmsLayer = null;
	var timer = false;
	var timename = null;

	var timeLoad = function () {
		if (timer == true) {
			$(".time-axis-box").find(".time-axis > li").removeClass();
			time = time + 1;
			if (time > 201604) {
				time = 201602;
			}
			$("#time_box_" + time).addClass("active");
			loadLayer();
		}
	}

	var loadLayer = function () {
		if (gis_common.stringIsNullOrWhiteSpace(kpi) || gis_common.stringIsNullOrWhiteSpace(time)) {
			//gis_common.showMessage("请选择查询参数");
			return;
		}

		if (wmsLayer != null || wmsLayer != undefined) {
			gis_core.removeLayer("dbpopulationLayer")
		}
		wmsLayer = new esri.layers.WMSLayer(grid_url + "/" + kpi + "/" + time, {
				resourceInfo : {
					extent : new esri.geometry.Extent(73.2672757, 18.37574, -109.66552734375, 133.29657, {
						wkid : 4326
					}),
					layerInfos : []
				},
				visibleLayers : ['dbpopulationgrid']
			});
		wmsLayer.id = "dbpopulationLayer";
		gis_core.insertLayer("dbpopulationLayer", "人口密度覆盖", 2, wmsLayer);
	};

	//初始化数据
	var initData = function () {
		$.ajax({
			url : "/getltecovercells",
			data : {
				time : time + "",
				kpi : kpi
			},
			type : "GET",
			beforeSend : function () {
				gis_common.isLoadingAddCount();
			},
			success : function (req) {
				var legends = eval(req);
				loadLayer();
			},
			complete : function () {
				gis_common.isLoadingSunCount();
			},
			error : function (XMLHttpRequest, textStatus, errorThrown) {
				gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
			}
		});
	};

	this.createTimeBox = function (callback) {
		var content = "";
		content += "<div class=\"time-axis-box\">";
		content += "  <div class=\"pos-rel\">";
		content += " 	<div class=\"time-paly-stop\">";
		content += "			<a id='time_box_play' href=\"javascript: void(0);\"><i class=\"fa fa-play\"></i></a>"
		content += "			<a id='time_box_pause' href=\"javascript: void(0);\"><i class=\"fa fa-pause\"></i></a>"
		content += "	 	</div>";
		content += "	 </div>";
		content += "	 <ul class=\"time-axis\">";
		content += "	 	<li id='time_box_201602' index='0' time='201602' class=\"active\" >2016-02</li>";
		content += "	 	<li id='time_box_201603' index='1' time='201603'>2016-03</li>";
		content += "	 	<li id='time_box_201604' index='2' time='201604'>2016-04</li>";
		content += "	 </ul>";
		content += "</div>";

		var div = $(content);
		div.find("#time_box_play").bind("click", function () {
			timer = true;
		});
		div.find("#time_box_pause").bind("click", function () {
			timer = false;
		});
		div.find(".time-axis > li").bind("click", function () {
			time = parseInt($(this).attr("time"));
			div.find(".time-axis > li").removeClass();
			$(this).addClass("active");
			loadLayer();
		});
		callback(div);
	};
	
	//初始化
	this.init = function () {
		//加载库文件
		dojo.require("esri.layers.WMSLayerInfo");
		dojo.require("esri.layers.WMSLayer");
		//注册模块
		gis_core.addPlugin(this);
		//初始化数据
		//initData();
		timename = setInterval(timeLoad, 3000);
	};

	//销毁  13415731471 CLnWsN83
	this.destroy = function () {
		$("#dbPopulation_createTimeBox").remove();
		clearTimeout(timename);
		gis_core.removeLayer("dbpopulationLayer");
		dbPopulation = null;
	};

	//模块信息
	this.title = {
		id : "00_02_00",
		name : "人口密度分析"
	};

}  

//功能类 全局类
var dbPopulation = null;
//初始化人口渲染
function initDbPopulation() {
	if (dbPopulation == null) {
		dbPopulation = new DbPopulation();
		dbPopulation.init();
	}
}

function dbPopulation_createTimeBox(obj) {
	initDbPopulation();	
	gis_core.pluginDestroyExceptId(dbPopulation.title.id);
	gis_layout.createOtherWidget("dbPopulation_createTimeBox", dbPopulation.createTimeBox);
	var extent = new esri.geometry.Extent(106.435030, 29.46694, 106.669519, 29.600882, new esri.SpatialReference({
				wkid : 4326
			}));
	gis_core.map.setExtent(extent);
}
