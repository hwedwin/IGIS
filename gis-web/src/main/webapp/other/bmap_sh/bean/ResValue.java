package gis.core.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * 资源类
 * 
 * @author Xiaobing
 *
 */
public class ResValue extends KeyValue {
	
	public  ResValue() {
		roperties = new HashMap<String, Object>();
	}
	
	private double longitude;
	private double latitude;
	private double value;
	private Map<String, Object> roperties;
	

	/**
	 * 经度
	 * 
	 * @return
	 */
	public double getLongitude() {
		return this.longitude;
	}

	/**
	 * 经度
	 * 
	 * @param longitude
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	/**
	 * 纬度
	 * 
	 * @return
	 */
	public double getLatitude() {
		return this.latitude;
	}

	/**
	 * 纬度
	 * 
	 * @param latitude
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	/**
	 * 值
	 * 
	 * @return
	 */
	public double getValue() {
		return this.value;
	}

	/**
	 * 值
	 * 
	 * @param value
	 */
	public void setValue(double value) {
		this.value = value;
	}

	/**
	 * 资源
	 * @return
	 */
	public Map<String, Object> getRoperties() {
		return roperties;
	}

	
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	


}
