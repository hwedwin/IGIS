package bean.bd;

import java.util.List;

import gis.algorithm.GeoPoint;
import gis.core.bean.KeyValue;

public class ExpressWayLine extends KeyValue {
	private String startPlace;
	private String endPlace;
	private double charging;
	private double distance;
	private List<GeoPoint> ring;

	/**
	 * 开始位置
	 * 
	 * @return
	 */
	public String getStartPlace() {
		return startPlace;
	}

	/**
	 * 开始位置
	 * 
	 * @param startPlace
	 */
	public void setStartPlace(String startPlace) {
		this.startPlace = startPlace;
	}

	/**
	 * 结束位置
	 * 
	 * @return
	 */
	public String getEndPlace() {
		return endPlace;
	}

	/**
	 * 结束位置
	 * 
	 * @param endPlace
	 */
	public void setEndPlace(String endPlace) {
		this.endPlace = endPlace;
	}

	/**
	 * 价格
	 * 
	 * @return
	 */
	public double getCharging() {
		return charging;
	}

	/**
	 * 价格
	 * 
	 * @param charging
	 */
	public void setCharging(double charging) {
		this.charging = charging;
	}

	/**
	 * 距离
	 * @return
	 */
	public double getDistance() {
		return distance;
	}

	/**
	 * 距离
	 * @param distance
	 */
	public void setDistance(double distance) {
		this.distance = distance;
	}

	/**
	 * 关键点
	 * @return
	 */
	public List<GeoPoint> getRing() {
		return ring;
	}

	/**
	 * 关键点
	 * @param ring
	 */
	public void setRing(List<GeoPoint> ring) {
		this.ring = ring;
	}

}
