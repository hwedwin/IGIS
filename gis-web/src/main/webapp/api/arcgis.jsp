<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
    <%@ include file="header.jsp" %>
    <section>
        <div class="container">
            <%@ include file="leftmenu.jsp" %>
            <input id="pageid" type="hidden" value="arcgis">

            <div class="docs-content" style="height: 850px;">
                <h3 id="welcome" class="welcome"> ArcGis简介</h3>
                <br/>

                <p>ArcGis是一套完整的地理信息系统平台，iGIS前端开发框架是基于ArcGis js api深度封装而来。</p>

                <p><a target="_blank" href="https://developers.arcgis.com/javascript/3/">ArcGis原生api说明</a> :
                    https://developers.arcgis.com/javascript/3/</p>

                <p><a target="_blank" href="http://www.cnblogs.com/imihiroblog/p/3636782.html">ArcGis开发笔记</a> ：
                    http://www.cnblogs.com/imihiroblog/p/3636782.html</p>

                <p><a target="_blank" href="/api/ArcGIS_API_for_JavaScript.pdf">开发教程</a></p>

                <a href="/api/igis.jsp" type="button" class="btn-link">上一页</a>
                <a href="/api/igisdeveloper.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>

            </div>
        </div>
    </section>
</div>
</body>
</html>
