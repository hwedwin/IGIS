package bean.common;

import gis.core.bean.KeyValue;

import java.util.List;

/**
 * Created by Xiaobing on 2016/4/26.
 * 图例
 */
public class Legend<T> extends KeyValue {
    /**
     * 图例明细
     */
    private List<T> legendItems;

    /**
     * 图例明细
     * @return
     */
    public List<T> getLegendItems() {
        return legendItems;
    }

    /**
     * 图例明细
     * @param legendItems
     */
    public void setLegendItems(List<T> legendItems) {
        this.legendItems = legendItems;
    }
}
