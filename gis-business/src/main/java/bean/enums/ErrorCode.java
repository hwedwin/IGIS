package bean.enums;

import java.sql.SQLException;

/**
 * Created by Xiaobing on 2016/4/20.
 */
public class ErrorCode {
    /**
     * 无错误信息
     */
    public static final int None = 0;
    /**
     * 数据库  SQL异常
     */
    public static final int SQLError = 100;
    /**
     * 逻辑错误
     */
    public static final int BusError = 200;

    /**
     * 获取错误骂
     * @param e
     * @return
     */
    public static int getErrorCode(Exception e) {
        if (e instanceof SQLException) {
            return SQLError;
        } else {
            return BusError;
        }
    }
}
