package gis.core.bean;

public class KeyValue {

	private String id;
	private String name;
	private Object tag;

	public KeyValue(){};

	public KeyValue(String id,String name){
		this.id = id;
		this.name = name;
	};
	public KeyValue(String id,String name,Object tag){
		this.id = id;
		this.name = name;
		this.tag = tag;
	};
	/**
	 * id
	 * @return
	 */
	public String getId() {
		return id;
	}
	/**
	 * id
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * 名称
	 * @return
	 */
	public String getName() {
		return name;
	}
	/**
	 * 名称
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}	
	/**
	 * 扩展属性
	 * @return
	 */
	public Object getTag() {
		return tag;
	}
	/**
	 *  扩展属性
	 * @param tag
	 */
	public void setTag(Object tag) {
		this.tag = tag;
	}
}
