<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%String root = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>
<%String root_arcgis = request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>
<link rel="stylesheet" type="text/css" href="<%=root%>theme/core.css"/>
<script type="text/javascript" src="<%=root%>script/base/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<%=root%>script/base/jquery.json-2.5.1.min.js"></script>
<script type="text/javascript">
    var root = '<%=root%>';
    var root_arcgis = '<%=root_arcgis%>';
</script>
