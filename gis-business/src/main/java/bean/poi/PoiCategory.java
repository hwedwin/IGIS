package bean.poi;

/**
 * Created by hupeng on 16/5/24.
 * poi 分类
 */
public class PoiCategory {

    /**
     * id
     */
    private String id;

    /**
     * 一级分类id
     */
    private String level_f_id;

    /**
     * 一级分类名称
     */
    private String level_f_name;

    /**
     * 二级分类id
     */
    private String level_s_id;

    /**
     * 二级分类名称
     */
    private String level_s_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLevel_f_id() {
        return level_f_id;
    }

    public void setLevel_f_id(String level_f_id) {
        this.level_f_id = level_f_id;
    }

    public String getLevel_f_name() {
        return level_f_name;
    }

    public void setLevel_f_name(String level_f_name) {
        this.level_f_name = level_f_name;
    }

    public String getLevel_s_id() {
        return level_s_id;
    }

    public void setLevel_s_id(String level_s_id) {
        this.level_s_id = level_s_id;
    }

    public String getLevel_s_name() {
        return level_s_name;
    }

    public void setLevel_s_name(String level_s_name) {
        this.level_s_name = level_s_name;
    }
}
