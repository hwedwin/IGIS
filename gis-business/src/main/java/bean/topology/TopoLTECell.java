package bean.topology;

import bean.common.Cell;

/**
 * Created by Xiaobing on 2016/6/29.
 */
public class TopoLTECell extends Cell implements Cloneable {
    private String time;
    private String roomId;
    private String roomName;
    private String equipId;
    private String equipName;
    private String quality;
    private String isAlarm;
    private String isColse;
    private String isAttach;
    private String isEps;

    public String getIsAttach() {
        return isAttach;
    }

    public String getIsEps() {
        return isEps;
    }

    public void setIsAttach(String isAttach) {
        this.isAttach = isAttach;
    }

    public void setIsEps(String isEps) {
        this.isEps = isEps;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getEquipId() {
        return equipId;
    }

    public void setEquipId(String equipId) {
        this.equipId = equipId;
    }

    public String getEquipName() {
        return equipName;
    }

    public void setEquipName(String equipName) {
        this.equipName = equipName;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getIsAlarm() {
        return isAlarm;
    }

    public void setIsAlarm(String isAlarm) {
        this.isAlarm = isAlarm;
    }

    public String getIsColse() {
        return isColse;
    }

    public void setIsColse(String isColse) {
        this.isColse = isColse;
    }

    @Override
    public  TopoLTECell clone() {
        return (TopoLTECell) super.clone();
    }

}
