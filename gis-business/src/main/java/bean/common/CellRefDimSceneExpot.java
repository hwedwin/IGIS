package bean.common;

/**
 * Created by Xiaobing on 2016/10/11.
 */
public class CellRefDimSceneExpot extends CellRefDimScene {
    private String cityId;
    private String cityName;
    /**
     * 应用专题ID
     */
    private String appTopicsId;
    /**
     * 应用专题名称
     */
    private String appTopicsName;

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAppTopicsId() {
        return appTopicsId;
    }

    public void setAppTopicsId(String appTopicsId) {
        this.appTopicsId = appTopicsId;
    }

    public String getAppTopicsName() {
        return appTopicsName;
    }

    public void setAppTopicsName(String appTopicsName) {
        this.appTopicsName = appTopicsName;
    }
}
