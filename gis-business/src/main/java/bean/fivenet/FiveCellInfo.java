package bean.fivenet;


import bean.common.Res;
import gis.algorithm.Extent;
import gis.algorithm.GeoPoint;
import gis.core.bean.ResValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xiaobing on 2016/11/11.
 */
public class FiveCellInfo extends Res {
    private int type;
    private String rings;
    private String extent;
    private String address;
    private String time;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getRings() {
        return rings;
    }

    public void setRings(String rings) {
        this.rings = rings;
    }

    public String getExtent() {
        return extent;
    }

    public void setExtent(String extent) {
        this.extent = extent;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
