package bean.test;

import javax.naming.ldap.PagedResultsControl;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Xiaobing on 2016/9/13.
 */
public class RowResult {
    private List<RowDetail> Row = new ArrayList<>();

    public List<RowDetail> getRow() {
        return Row;
    }

    public void setRow(List<RowDetail> row) {
        Row = row;
    }
}

