<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
  <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
  <%@ include file="header.jsp" %>
  <section>
    <div class="container">
      <%@ include file="leftmenu.jsp" %>
      <input id="pageid" type="hidden" value="otherxml">

      <div class="docs-content" style="height: 1200px;">
        <h3 id="welcome" class="welcome">其他配置</h3>
        <br/>
        <p>connection.*****.properties配置，配置数据库链接信息，使用方法如下：</p>
            <pre class="prettyprint">
              <br/>
private static final SQLHelper db = DatabaseManage.Instance(DatabaseName.common);
            </pre>

        <p>sql.*****.properties配置，SQL语句配置,获取方式如下：</p>
          <pre class="prettyprint">
              <br/>
 String sql= GisConfig.GetInstance().getSQL("key");
          </pre>
        <a href="/api/menuxml.jsp" type="button" class="btn-link">上一页</a>
        <a href="/api/mapapi.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>
      </div>
    </div>
  </section>
</div>
</body>
</html>
