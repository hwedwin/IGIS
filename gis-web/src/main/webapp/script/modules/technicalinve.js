/**
 * Created by Xiaobing on 2016/8/27.
 */

function TechnicalInve() {
    var wmsLayer = null; //渲染图层
    var wmsLayer_type = "";//渲染方式
    //添加渲染图层
    var table_name = "";
    var time = "";
    var timespan = "";
    var loadWmsLayer = function () {
        var url = "";
        if (wmsLayer_type == "") {
            gis_core.removeLayer("technicalInvewmsLayer");
            return;
        }
        else if (wmsLayer_type == "grid") {
            url = "/dynamicmap/techinvetestgrid/weak_rat/" + table_name + "|" + time + "|" + timespan;
        }
        else if (wmsLayer_type == "hot") {
            url = "/dynamicmap/techinvehotmap/weak_rat/" + table_name + "|" + time + "|" + timespan;
        }
        else if (wmsLayer_type == "tide") {
            url = "/dynamicmap/techinvetidemap/weak_rat/" + table_name + "|" + time + "|" + timespan;
        }

        if (wmsLayer != null || wmsLayer != undefined) {
            gis_core.removeLayer("technicalInvewmsLayer")
        }
        if (url == null || url == undefined) {
            return;
        }
        wmsLayer = new esri.layers.WMSLayer(url, {
            resourceInfo: {
                extent: new esri.geometry.Extent(73.2672757, 18.37574, -109.66552734375, 133.29657, {
                    wkid: 4326
                }),
                layerInfos: []
            },
            visibleLayers: ['lteconvergrid']
        });
        wmsLayer.id = "technicalInvewmsLayer";
        gis_core.insertLayer("technicalInvewmsLayer", "网格覆盖", 2, wmsLayer);
    };
    //获取图例
    var loadInitData = function () {
        $.ajax({
            url: "/gettechnicalinveinitdata?flag=" + Math.random(),
            data: {
                tablename: table_name,
                time: time,
                timespan: timespan
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    if (data == undefined || gis_common.stringIsNullOrWhiteSpace(data)) {
                        gis_common.showMessage("无数据");
                    }
                    else {
                        loadWmsLayer();
                    }
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    }
    //获取图例
    var loadLenged = function () {
        if (wmsLayer_type == "" || wmsLayer_type == "hot") {
            gis_layout.removeLegend();
        }
        $.ajax({
            url: "/gettechnicalinvelendeg?time=" + Math.random(),
            type: "GET",
            data: {
                lon: "000",
                tablename: table_name
            },
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    initLegend(data);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    }
    //生成图例
    var initLegend = function (legends) {
        if (legends == null || legends == undefined || legends.legendItems.length == 0) {
            //gis_common.showMessage("初始化图例失败");
            return;
        }
        var content = "";
        for (var i = 0; i < legends.legendItems.length; i++) {
            var legend = legends.legendItems[i];
            var sRgb = "RGB(" + legend.r + ", " + legend.g + ", " + legend.b + ")";
            content += "<div class=\"legend-box\" style=\"background:rgba(" + legend.r + ", " + legend.g + ", " + legend.b + ",0.5);color:#fff;\"> " + legend.valueMin + "-" + legend.valueMax + "</div>";
        }
        var div = $(content);
        gis_layout.createLegend("TechnicalInve_grid_legend", legends.name + "图例", div);
    };

    var getGridKpiDetail = function (lon, lat) {
        $.ajax({
            url: "/technicalinve_getGridKpiDetail",
            data: {
                tablename: table_name,
                time: time,
                timespan: timespan,
                lon: lon,
                lat: lat
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var div = gis_common.createKeyValueList(data);
                    gis_layout.createRightWidget("TechnicalInve_getGridKpiDetail", "栅格信息明细", div);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    //获取时间段
    var getTimeSpan = function (tableName, time, callback) {
        $.ajax({
            url: "/gettechnicalinvetimespan",
            data: {
                tablename: tableName,
                time: time
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    callback(data);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    /**
     * 初始化
     */
    this.init = function () {
        //加载库文件
        dojo.require("esri.layers.WMSLayerInfo");
        dojo.require("esri.layers.WMSLayer");
        gis_core.addPlugin(this);
        //加载库文件
        //gis_core.addExtentChange("broadbandcus_extentchange", extentchange);
        //注册事件
        /*if (cellLayer_click_handle != null) {
         dojo.disconnect(cellLayer_click_handle);
         }
         cellLayer_click_handle = cell_layer.on("click", function (evt) {
         var cell = evt.graphic.attributes.data;
         getBadCellKpiDetail(cell.id);
         });*/

        gis_core.addMapClick("technicalinve_map_click", function (evt) {
            if (wmsLayer_type == "grid") {
                getGridKpiDetail(evt.mapPoint.x, evt.mapPoint.y);
            }
        });

    };
    /**
     * 销毁
     */
    this.destroy = function () {
        gis_core.removeExtentChange("realrimemonitoring_extentchange");
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();
    };
    /**
     * 模块信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "00_07_00",
        name: "技侦"
    };
    /**
     * 清理
     */
    this.clear = function () {
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();
        //marker_layer.clear();
        //cell_layer.clear();
    };

    this.initQueryHtml = function (callback) {
        var content = "";
        content += " <div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>省市:</label>";
        content += "	<div id='technicalInve_table_name' class='col-sm-9'>";
        content += "		<select id='technicalInve_table_name_select' class='form-control'>";
        content += "			<option kpi='p_beijing_new'>北京市</option>";
        content += "			<option kpi='p_shanghai_new'>上海</option>";
        content += "			<option kpi='p_tianjin_new''>天津</option>";
        content += "			<option kpi='p_chongqing_new'>重庆</option>";
        content += "			<option kpi='p_heinan_new'>河南省</option>";
        content += "			<option kpi='p_guangdong_new'>广东省</option>";
        content += "			<option kpi='p_heilongjiang_new'>黑龙江省</option>";
        content += "			<option kpi='p_gansu_new'>甘肃省</option>";
        content += "		</select>";
        content += "	</div>";
        content += "</div>"

        content += "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
        content += "	<div id='res_type' class='col-sm-9'>";
        content += "			<input id='technicalInve_table_name_time' type='text' class='form-control' onClick=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd'});\"/>";
        content += " 	</div > ";
        content += " </div > "

        content += "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>区间:</label>";
        content += "	<div id='technicalInve_table_name_span' class='col-sm-9'>";
        content += "		<select id='technicalInve_table_name_span_select' class='form-control'>";
        content += "		</select>";
        content += "	</div>";
        content += " </div > "

        content += " <div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>渲染方式:</label>";
        content += "	<div id='technicalinve_reader_type' class='col-sm-9'>";
        content += "		<select id='technicalinve_reader_type_select' class='form-control'>";
        content += "			<option typ='grid'>栅 格</option>";
        content += "			<option typ='hot'>热力图</option>";
        content += "		</select>";
        content += "	</div>";
        content += "</div>";

        content += "<div class='form-group'>";
        content += " 	<div class='col-sm-9 col-sm-offset-3'>";
        content += "		<button id='technicalinve_btn_load' class='btn btn-primary'>渲 染</button>";
        content += "	</div>";
        content += "</div>";

        var div = $(content);
        //时间联动
        /*div.find("#technicalInve_table_name_span").click(function(){
         alert("1111");
         });*/
        div.find("#technicalInve_table_name_span").bind("click", function (e) {
            table_name = div.find("#technicalInve_table_name_select  option:selected").attr("kpi");
            var time_temp = div.find("#technicalInve_table_name_time").val();
            if (time_temp == null || time_temp == time) {
                return
            }

            time = time_temp;
            getTimeSpan(table_name, time, function (data) {
                var selects = $("#technicalInve_table_name_span_select");
                selects.html("");
                for (var i = 0; i < data.length; i++) {
                    var r = data[i];
                    selects.append("<option value'" + r + "'>" + r + "</option>");
                }
            });
        });

        div.find("#technicalinve_btn_load").bind("click", function () {
            table_name = div.find("#technicalInve_table_name_select  option:selected").attr("kpi");
            time = div.find("#technicalInve_table_name_time").val();
            timespan = div.find("#technicalInve_table_name_span_select option:selected").val();
            //渲染方式
            var selects = $("#technicalinve_reader_type_select option:selected");
            if (selects != null && selects != undefined) {
                wmsLayer_type = selects.attr("typ");
            }

            if (wmsLayer_type == "grid") {
                loadLenged();
            }
            //loadWmsLayer();
            loadInitData();
        });
        callback(div)
    };

    this.initQuery = function () {
    };
}

var technicalInve = null;

//初始化
function initBroadbandCus() {
    if (technicalInve == null) {
        technicalInve = new TechnicalInve();
        technicalInve.init();
    }
}

function TechnicalInve_initQueryHtml(obj) {
    initBroadbandCus();
    gis_layout.createMenubarWithFun("TechnicalInve_initQueryHtml", obj, "渲染类型", technicalInve.initQueryHtml)
}

function TechnicalInve_Init_Qyery() {
    initBroadbandCus();
    technicalInve.clear();
    technicalInve.initQuery();
}