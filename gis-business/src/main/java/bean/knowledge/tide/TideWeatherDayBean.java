package bean.knowledge.tide;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by xiexb on 2017/8/4.
 */
public class TideWeatherDayBean {
    private String id_;
    private String stationNo;
    private String stationName;
    private String ew;
    private String ns;
    private String timeZone;
    private String daykey;
    private String weather;
    private double highTemp;
    private double lowTemp;
    private String winD;
    private String winS;
    private String weatherLable;
    private String titleLable;


    public String getId_() {
        return id_;
    }

    public void setId_(String id_) {
        this.id_ = id_;
    }

    public String getStationNo() {
        return stationNo;
    }

    public void setStationNo(String stationNo) {
        this.stationNo = stationNo;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getEw() {
        return ew;
    }

    public void setEw(String ew) {
        this.ew = ew;
    }

    public String getNs() {
        return ns;
    }

    public void setNs(String ns) {
        this.ns = ns;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getDaykey() {
        return daykey;
    }

    public void setDaykey(String daykey) {
        this.daykey = daykey;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public double getHighTemp() {
        return highTemp;
    }

    public void setHighTemp(double highTemp) {
        this.highTemp = highTemp;
    }

    public double getLowTemp() {
        return lowTemp;
    }

    public void setLowTemp(double lowTemp) {
        this.lowTemp = lowTemp;
    }

    public String getWinD() {
        return winD;
    }

    public void setWinD(String winD) {
        this.winD = winD;
    }

    public String getWinS() {
        return winS;
    }

    public void setWinS(String winS) {
        this.winS = winS;
    }

    public String getWeatherLable() {
        return weatherLable;
    }

    public void setWeatherLable(String weatherLable) {
        this.weatherLable = weatherLable;
    }

    public String getTitleLable() {
        return titleLable;
    }

    public void setTitleLable(String titleLable) {
        this.titleLable = titleLable;
    }
}
