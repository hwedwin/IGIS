<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">
<%String root_gisapp=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";%>
<%String root_arcgis=request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";%>

<title>Insert title here</title>

<style>
 #mapDiv { height: 98%; width: 99%; position:absolute; }
</style>

<script type="text/javascript">
	var root_gisapp='<%=root_gisapp%>';
	var root_arcgis='<%=root_arcgis%>';
	
	var dojoConfig = {
			parseOnLoad : true,
			packages : [ {
				name : 'plugin',
				location : root_gisapp + "scripts/arcgis_js_api/plugin"
			} ]
		};
</script>

<link rel="stylesheet" href="<%=root_gisapp%>scripts/arcgis_js_api/library/3.9/3.9/js/esri/css/esri.css">
<script src="<%=root_gisapp%>scripts/arcgis_js_api/library/3.9/3.9/js/dojo/dojo/dojo.js"></script>
<script src="<%=root_gisapp%>scripts/arcgis_js_api/library/3.9/3.9/js/esri/jsapi.js"></script>

<script type="text/javascript">
	dojo.require("esri.map");
	function init() {
	   var myMap = new esri.Map("mapDiv");
	   
	   //var myTiledMapServiceLayerServiceLayer = new esri.layers.ArcGISTiledMapServiceLayer("http://server.arcgisonline.com/ArcGIS/rest/services/NGS_Topo_US_2D/MapServer");
	   var myTiledMapServiceLayer = new esri.layers.ArcGISTiledMapServiceLayer("http://localhost:5898/mapaction");
	   myMap.addLayer(myTiledMapServiceLayer);
	}
	dojo.addOnLoad(init);
</script>
</head>
<body>
<div id="mapDiv"></div>
</body>
</html>