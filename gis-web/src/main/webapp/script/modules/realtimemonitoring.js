/**
 * Created by Xiaobing on 2016/8/12.
 */

function RealTimeMonitoring() {
    var cell_layer = gis_core.addGraphicsLayer("RealTimeMonitoring_cell_layer", "小区图层");
    var marker_layer = gis_core.addGraphicsLayer("RealTimeMonitoring_marker_layer", "标记图层");

    var fun = "";
    var selectid = "";
    var initcellid = "";
    var isFirst = true;
    // 劣化小区 查询参数
    var time = "";//时间
    var isdegradecell = "-1";//是否劣化小区  -1 全部小区  1 劣化小区  0 非劣化小区
    var region = {
        id: 1,
        type: 9000
    };//地市
    //相关事件
    var cellLayer_click_handle = null;
    //场景 查询参数
    var scene_type_id = "";
    var scene_id = "";


    //地图改变事件
    var extentchange = function (delta) {
        if (fun == "monit") {
            getMonitoringCells();
        }
    };

    //加载表格数据
    var getMonitoringCellsTable = function () {
        var content = "<div>";
        content += "<table id='RealTimeMonitoring_MonitoringCells_Table'></table>";
        content += "<div id='RealTimeMonitoring_MonitoringCells_Pager'></div>"
        content += "<div>";
        var div = $(content);

        gis_layout.createLeftWidget("RealTimeMonitoring_MonitoringCells_JQGrid_Div", "劣化小区明细", div);

        div.find("#RealTimeMonitoring_MonitoringCells_Table").jqGrid({
            url: '/getdetericelllist?isdegradecell=1&time=' + time + "&region=" + JSON.stringify(region) + "&cellid=" + initcellid,
            datatype: "json",
            width: 400,
            colNames: ['ID', '时间', '小区名称', '地市', '经度', '纬度'],
            colModel: [{
                name: 'id',
                index: 'id',
                width: 50,
                hidden: true
            }, {
                name: 'tag',
                index: 'tag',
                width: 30
            }, {
                name: 'name',
                index: 'name',
                width: 80
            }, {
                name: 'cityName',
                index: 'cityName',
                width: 20,
                align: "right"
            }, {
                name: 'longitude',
                index: 'longitude',
                width: 30,
                align: "right"
            }, {
                name: 'latitude',
                index: 'latitude',
                width: 30,
                align: "right"
            }
            ],
            rowNum: 10,
            rowList: [10, 20, 30, 50],
            pager: '#RealTimeMonitoring_MonitoringCells_Pager',
            mtype: "get",
            viewrecords: true,
            caption: "",
            onSelectRow: function (id) {
                var data = div.find("#RealTimeMonitoring_MonitoringCells_Table").jqGrid('getRowData', id);
                gis_common.setExtent(parseFloat(data.longitude) + 0.005, parseFloat(data.latitude) + 0.005, parseFloat(data.longitude) - 0.005, parseFloat(data.latitude) - 0.005);
                selectid = data.id;
                getMonitoringCellKpiDetail(data.id);
            }
        });
    };
    //加载地图小区数据
    var getMonitoringCells = function () {
        cell_layer.clear();
        if (time == null || time == "") {
            //gis_common.queryRemoveFun("endToEnd_getmroltebtstopn");
            return;
        }
        $.ajax({
            url: "/getdetericell",
            data: {
                extent: JSON.stringify(gis_common.getMapExtent()),
                region: JSON.stringify(region),
                isdegradecell: "1",
                time: time,
                cellid: initcellid
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    var btses = eval(datas);
                    createCellCover(btses);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var createCellCover = function (btses) {
        var xmax = 0.0, ymax = 0.0, xmin = 0.0, ymin = 0.0;

        for (var i = 0; i < btses.length; i++) {
            var bts = btses[i];

            if (xmax == 0.0) {
                xmax = bts.longitude;
                xmin = bts.longitude;
                ymax = bts.latitude;
                ymin = bts.latitude;
            }
            else {
                xmax = bts.longitude > xmax ? bts.longitude : xmax;
                xmin = bts.longitude < xmin ? bts.longitude : xmin;
                ymax = bts.latitude > ymax ? bts.latitude : ymax;
                ymin = bts.latitude < ymin ? bts.latitude : ymin;
            }

            if (bts.tag == "cluster") {
                var point = new esri.geometry.Point(bts.longitude, bts.latitude, gis_core.map.spatialReference);

                var symbol = new esri.symbol.PictureMarkerSymbol('/images/cluster.png', 48, 48);
                var graphic = new esri.Graphic(point, symbol);
                cell_layer.add(graphic);

                var text = bts.value + "";
                if (bts.value >= 1000) {
                    text = (bts.value / 1000).toFixed(2) + "万"
                }
                var textSymbol = new esri.symbol.TextSymbol(text);
                textSymbol.setAlign(esri.symbol.TextSymbol.ALIGN_MIDDLE);
                textSymbol.setColor(new esri.Color([255, 255, 255, 1]));
                textSymbol.setOffset(0, -5);
                var textgraphic = new esri.Graphic(point, textSymbol);
                cell_layer.add(textgraphic);
            }
            else {
                var antbw = 30;
                var azimuth = 0;
                var off_azimuth = 0;
                if (bts.cells.length > 1) {
                    antbw = 180.0 / bts.cells.length;
                    off_azimuth = 360.0 / bts.cells.length;
                }
                if (antbw > 30) {
                    antbw = 30;
                }

                for (var j = 0; j < bts.cells.length; j++) {
                    var cell = bts.cells[j];
                    cell.radius = 200;
                    cell.antbw = antbw;
                    if (cell.azimuth == 0) {
                        cell.azimuth = azimuth;
                    }
                    var g = gis_common.createCellGraphic(cell);
                    cell_layer.add(g);
                    azimuth = azimuth + off_azimuth;

                    if (!gis_common.stringIsNullOrWhiteSpace(selectid) && selectid == cell.id) {
                        marker_layer.clear();
                        var rings = gis_common.createCellCoverRing(cell.longitude, cell.latitude, cell.radius, cell.azimuth, cell.antbw);
                        var p = rings[parseInt(rings.length / 2)];
                        var x = cell.longitude - (cell.longitude - p[0]) / 2;
                        var y = cell.latitude - (cell.latitude - p[1]) / 2;

                        marker_layer.clear();
                        var point = new esri.geometry.Point(x, y, gis_core.map.spatialReference);
                        var symbol = new esri.symbol.PictureMarkerSymbol('/images/point.png', 28, 28);
                        var graphic = new esri.Graphic(point, symbol);
                        marker_layer.add(graphic);
                    }
                }
            }
        }
        if (xmax > 0.0 && isFirst == true) {
            gis_common.setExtent(xmax + 0.005, ymax + 0.005, xmin - 0.005, ymin - 0.005);
            isFirst = false;
        }
    };
    //加载小区指标明细
    var getMonitoringCellKpiDetail = function (cellId) {
        $.ajax({
            url: "/getdetericellkpidetail",
            data: {
                region: JSON.stringify(region),
                isdegradecell: isdegradecell,
                time: time,
                cellid: cellId
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var datalist = [];
                    //湖南
                    if (!(data == null || data == undefined || data.length == 0)) {
                        for (var i = 0; i < data.length; i++) {
                            var d = data[i];
                            if (d.id != "Attach成功率" && d.id != "Attach平均时延(ms)" && d.id != "TCP上行乱序率" && d.id != "TCP下行乱序率"
                                && d.id != "EPS缺省承载建立成功率" && d.id != "EPS缺省承载建立平均时延(ms)") {
                                datalist.push(d);
                            }
                        }
                    }
                    var div = gis_common.createKeyValueList(datalist);
                    gis_layout.createRightWidget("RealTimeMonitoring_getMonitoringCellKpiDetailHtml", "小区指标明细", div);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    //加载场景分类
    var getSceneTypeList = function (callback) {
        var key = "RealTimeMonitoring_getSceneTypeList";
        var dataCache = gis_resources.getCache(key);
        if (dataCache != null) {
            callback(dataCache);
        } else {
            $.ajax({
                url: "/getmonitscenetype",
                dataType: "json",
                async: true,
                type: "GET",
                beforeSend: function () {
                    gis_common.isLoadingAddCount();
                },
                success: function (req) {
                    gis_common.outPut(req, function (data) {
                        gis_resources.addCache(key, data);
                        callback(data);
                    });
                },
                complete: function () {
                    gis_common.isLoadingSunCount();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
                }
            });
        }
    };
    //加载场景列表
    var getSceneList = function (scenetypeid) {
        var content = "<div>";
        content += "<table id='RealTimeMonitoring_Scenelist_Table'></table>";
        content += "<div id='RealTimeMonitoring_Scenelist_Pager'></div>"
        content += "<div>";
        var div = $(content);

        gis_layout.createLeftWidget("RealTimeMonitoring_Scenelist_JQGrid_Div", "场景明细", div);

        div.find("#RealTimeMonitoring_Scenelist_Table").jqGrid({
            url: '/getmonitsceneinfolistjqgrid?scenetypeid=' + scenetypeid,
            datatype: "json",
            width: 400,
            colNames: ['ID', '场景名称', '场景分类'],
            colModel: [{
                name: 'id',
                index: 'id',
                width: 50,
                hidden: true
            }, {
                name: 'name',
                index: 'name',
                align: "left"
            }, {
                name: 'sceneTypeName',
                index: 'sceneTypeName',
                align: "left"
            }
            ],
            rowNum: 10,
            rowList: [10, 20, 30, 50],
            pager: '#RealTimeMonitoring_Scenelist_Pager',
            mtype: "get",
            viewrecords: true,
            caption: "",
            onSelectRow: function (id) {
                var data = div.find("#RealTimeMonitoring_Scenelist_Table").jqGrid('getRowData', id);
                /*gis_common.setExtent(parseFloat(data.longitude) + 0.005, parseFloat(data.latitude) + 0.005, parseFloat(data.longitude) - 0.005, parseFloat(data.latitude) - 0.005);
                 selectid = data.id;
                 getMonitoringCellKpiDetail(data.id);*/
            }
        });
    };
    //加载场景数据
    var getScene = function (scenetypeid, callback) {
        var key = "RealTimeMonitoring_getScene_" + scenetypeid;
        var dataCache = gis_resources.getCache(key);
        if (dataCache != null) {
            callback(dataCache);
        } else {
            $.ajax({
                url: "/getmonitsceneinfolist?scenetypeid=" + scenetypeid,
                dataType: "json",
                async: true,
                type: "GET",
                beforeSend: function () {
                    gis_common.isLoadingAddCount();
                },
                success: function (req) {
                    gis_common.outPut(req, function (data) {
                        gis_resources.addCache(key, data);
                        callback(data);
                    });
                },
                complete: function () {
                    gis_common.isLoadingSunCount();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
                }
            });
        }
    };
    //加载场景小区
    var getSceneCell = function () {
        cell_layer.clear();
        isFirst = true;
        if (gis_common.stringIsNullOrWhiteSpace(time) || gis_common.stringIsNullOrWhiteSpace(scene_id)) {
            return;
        }
        $.ajax({
            url: "/getscenecelllist",
            dataType: "json",
            data: {
                sceneid: scene_id,
                time: time,
                isdegradecell: isdegradecell
            },
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    createCellCover(data);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    //加载场景小区列表
    var getSceneCellTable = function () {
        if (gis_common.stringIsNullOrWhiteSpace(time) || gis_common.stringIsNullOrWhiteSpace(scene_id)) {
            return;
        }
        var content = "<div>";
        content += "<table id='RealTimeMonitoring_Scene_Cells_Table'></table>";
        content += "<div id='RealTimeMonitoring_Scene_Cells_Pager'></div>"
        content += "<div>";
        var div = $(content);

        gis_layout.createLeftWidget("RealTimeMonitoring_Scene_Cells_JQGrid_Div", "场景小区明细", div);

        div.find("#RealTimeMonitoring_Scene_Cells_Table").jqGrid({
            url: '/getscenecelllistjqgrid?isdegradecell=' + isdegradecell + '&time=' + time + "&sceneid=" + scene_id,
            datatype: "json",
            width: 400,
            height: 230,
            colNames: ['ID', '时间', '小区名称', '地市', '经度', '纬度'],
            colModel: [{
                name: 'id',
                index: 'id',
                width: 50,
                hidden: true
            }, {
                name: 'tag',
                index: 'tag',
                width: 30
            }, {
                name: 'name',
                index: 'name',
                width: 80
            }, {
                name: 'cityName',
                index: 'cityName',
                width: 20,
                align: "right"
            }, {
                name: 'longitude',
                index: 'longitude',
                width: 30,
                align: "right"
            }, {
                name: 'latitude',
                index: 'latitude',
                width: 30,
                align: "right"
            }
            ],
            rowNum: 10,
            rowList: [10, 20, 30, 50],
            pager: '#RealTimeMonitoring_Scene_Cells_Pager',
            mtype: "get",
            viewrecords: true,
            caption: "",
            onSelectRow: function (id) {
                var data = div.find("#RealTimeMonitoring_Scene_Cells_Table").jqGrid('getRowData', id);
                gis_common.setExtent(parseFloat(data.longitude) + 0.005, parseFloat(data.latitude) + 0.005, parseFloat(data.longitude) - 0.005, parseFloat(data.latitude) - 0.005);
                selectid = data.id;
                getMonitoringCellKpiDetail(data.id);
                getSceneCell();
            }
        });
    };
    //加载场景指标明细
    var getSceneKpiDetail = function () {
        $.ajax({
            url: "/getscenekpidetail",
            data: {
                sceneid: scene_id,
                time: time,
                scenetypeid: scene_type_id
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var datalist = [];
                    //湖南
                    if (!(data == null || data == undefined || data.length == 0)) {
                        for (var i = 0; i < data.length; i++) {
                            var d = data[i];
                            if (d.id != "Attach成功率" && d.id != "Attach平均时延(ms)" && d.id != "TCP上行乱序率" && d.id != "TCP下行乱序率"
                                && d.id != "EPS缺省承载建立成功率" && d.id != "EPS缺省承载建立平均时延(ms)") {
                                datalist.push(d);
                            }
                        }
                    }
                    var div = gis_common.createKeyValueList(datalist);
                    gis_layout.createRightWidget("RealTimeMonitoring_getSceneKpiDetailHtml", "场景指标指标明细", div);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    /**
     * 初始化
     */
    this.init = function () {
        //加载库文件
        gis_core.addExtentChange("realrimemonitoring_extentchange", extentchange);
        gis_core.addPlugin(this);
        //注册事件
        if (cellLayer_click_handle != null) {
            dojo.disconnect(cellLayer_click_handle);
        }
        cellLayer_click_handle = cell_layer.on("click", function (evt) {
            var cell = evt.graphic.attributes.data;
            getMonitoringCellKpiDetail(cell.id);
        });

    };
    /**
     * 销毁
     */
    this.destroy = function () {
        gis_core.removeExtentChange("realrimemonitoring_extentchange");
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();
    };
    /**
     * 模块信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "00_05_00",
        name: "4G实时业务监控"
    };
    /**
     * 清理
     */
    this.clear = function (_fun) {
        if (fun != _fun) {
            gis_layout.removeLeftWidget();
            gis_layout.removeRightWidget();
            marker_layer.clear();
            cell_layer.clear();
        }
    };
    //化劣化小区查询条件面板
    this.createQueryHtml = function (callback) {
        fun = "monit";
        isdegradecell = "1";
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();

        gis_resources.getRegions(function (data) {
            var content = "<div class=\"form-group\">";
            content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
            content += "	<div id='res_type' class='col-sm-9'>";
            content += "			<input id='realtimemonitoring_query_param_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm'});\"/>";
            content += " 	</div > ";
            content += " </div > "

            content += "<div class='form-group'>";
            content += "	<label for='' class='col-sm-3 control-label'>地市:</label>";
            content += " 	<div class='col-sm-9'>";
            content += "		<select id='realtimemonitoring_query_param_region_select' class='form-control'>";
            if (data != null && data != undefined && data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    var r = data[i];
                    content += "<option neid='" + r.id + "' netype='" + r.type + "'>" + r.name + "</option>";
                }
            } else {
                content += "<option neid='1' netype='9000'>全部</option>";
            }
            content += "		</select>";
            content += "	</div>";
            content += "</div>";

            content += "<div class='form-group'>";
            content += " 	<div class='col-sm-9 col-sm-offset-3'>";
            content += "		<button id='realtimemonitoring_query_param_btn_load' class='btn btn-primary'>确 定</button>";
            content += "	</div>";
            content += "</div>";

            var div = $(content);
            div.find("#realtimemonitoring_query_param_btn_load").bind("click", function (e) {
                fun = "monit";
                isdegradecell = "1";

                time = div.find("#realtimemonitoring_query_param_time").val().replace(/-/g, '').replace(/:/g, '').replace(/ /g, '');
                if (gis_common.stringIsNullOrWhiteSpace(time)) {
                    gis_common.showMessage("请选择时间");
                    div.find("#endtoend_Serial_Weak_param_time").focus();
                    return;
                }
                //地市
                var selects = $("#realtimemonitoring_query_param_region_select option:selected");
                if (selects != null && selects != undefined) {
                    region.id = selects.attr("neid");
                    region.type = selects.attr("netype");
                } else {
                    gis_common.showMessage("请选择地市");
                    div.find("#realtimemonitoring_query_param_region_select").focus();
                    return;
                }

                isFirst = true;
                marker_layer.clear()
                getMonitoringCellsTable();
                getMonitoringCells();
            });
            var d = new Date().Format("yyyy-MM-dd hh") + ":00";
            div.find("#realtimemonitoring_query_param_time").val(d);
            callback(div);
        });
    };
    //场景查询条件面板
    this.createSceneQueryHtml = function (callback) {
        fun = "scene";
        isdegradecell = "-1";
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();

        getSceneTypeList(function (data) {
            var content = "<div class=\"form-group\">";
            content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
            content += "	<div id='res_type' class='col-sm-9'>";
            content += "			<input id='realtimemonitoring_scene_param_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm'});\"/>";
            content += " 	</div > ";
            content += " </div > "

            content += "<div class='form-group'>";
            content += "	<label for='' class='col-sm-3 control-label'>场景类型:</label>";
            content += " 	<div class='col-sm-9'>";
            content += "		<select id='realtimemonitoring_scene_type_param_select' class='form-control'>";
            if (data != null && data != undefined && data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    var r = data[i];
                    content += "<option id='" + r.id + "'>" + r.name + "</option>";
                }
            } else {
                content += "<option id='-1' >全部</option>";
            }
            content += "		</select>";
            content += "	</div>";
            content += "</div>";

            content += "<div class='form-group'>";
            content += "	<label for='' class='col-sm-3 control-label'>场景:</label>";
            content += " 	<div class='col-sm-9'>";
            content += "		<select id='realtimemonitoring_scene_param_select' class='form-control'>";
            content += "		</select>";
            content += "	</div>";
            content += "</div>";

            content += "<div class='form-group'>";
            content += "	<label for='' class='col-sm-3 control-label'>类型:</label>";
            content += " 	<div class='col-sm-9 radio'>";
            content += "     <label class='checkbox-inline'><input type='radio' checked='checked' name='realtime_scene_cell_type' value='-1' />全部小区</label>";
            content += "     <label class='checkbox-inline'><input type='radio' name='realtime_scene_cell_type' value='1' />劣化小区</label>";
            content += "	</div>";
            content += "</div>";

            content += "<div class='form-group'>";
            content += " 	<div class='col-sm-9 col-sm-offset-3'>";
            content += "		<button id='realtimemonitoring_scene_param_btn_load' class='btn btn-primary'>确 定</button>";
            content += "	</div>";
            content += "</div>";

            var div = $(content);
            //场景类型与场景  联动
            div.find("#realtimemonitoring_scene_type_param_select").bind("change", function (e) {
                scene_type_id = $(this).children('option:selected').attr("id");
                getScene(scene_type_id, function (data) {
                    var selects = $("#realtimemonitoring_scene_param_select");
                    selects.html("");
                    for (var i = 0; i < data.length; i++) {
                        var r = data[i];
                        selects.append("<option id='" + r.id + "'>" + r.name + "</option>");
                    }
                });
            });
            //绑定确认按钮
            div.find("#realtimemonitoring_scene_param_btn_load").bind("click", function (e) {
                fun = "scene";
                isdegradecell = "-1";

                time = div.find("#realtimemonitoring_scene_param_time").val().replace(/-/g, '').replace(/:/g, '').replace(/ /g, '');
                if (gis_common.stringIsNullOrWhiteSpace(time)) {
                    gis_common.showMessage("请选择时间");
                    div.find("#realtimemonitoring_scene_param_time").focus();
                    return;
                }
                //场景分类
                var selects_scene_type = $("#realtimemonitoring_scene_type_param_select option:selected");
                if (selects_scene_type != null && selects_scene_type != undefined) {
                    scene_type_id = selects_scene_type.attr("id");
                } else {
                    gis_common.showMessage("请场景类型");
                    div.find("#realtimemonitoring_scene_type_param_select").focus();
                    return;
                }

                //场景
                var selects_scene = $("#realtimemonitoring_scene_param_select option:selected");
                if (selects_scene != null && selects_scene != undefined) {
                    scene_id = selects_scene.attr("id");
                } else {
                    gis_common.showMessage("请场景");
                    div.find("#realtimemonitoring_scene_param_select").focus();
                    return;
                }

                //小区类型
                isdegradecell = div.find("input[name='realtime_scene_cell_type']:checked").val();

                selectid = "";
                gis_layout.removeLeftWidget();
                gis_layout.removeRightWidget();

                getSceneCell();
                getSceneCellTable();
                getSceneKpiDetail();

            });

            callback(div);

            //绑定默认时间
            var d = new Date().Format("yyyy-MM-dd hh") + ":00";
            div.find("#realtimemonitoring_scene_param_time").val(d);

            //绑定场景下拉框
            getScene("-1", function (data) {
                var selects = $("#realtimemonitoring_scene_param_select");
                selects.html("");
                for (var i = 0; i < data.length; i++) {
                    var r = data[i];
                    selects.append("<option id='" + r.id + "'>" + r.name + "</option>");
                }
            });
        });
    };
    //初始化劣化小区查询
    this.initDegQuery = function (cityid, tim, cellid) {
        fun = "monit";
        isdegradecell = "1";
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();

        gis_resources.getRegions(function (data) {
            fun = "monit";
            isdegradecell = "1";

            var f = false;
            for (var i = 0; i < data.length; i++) {
                var city = data[i];
                if (city.id == cityid) {
                    region.id = city.id;
                    region.type = city.type;
                    f = true;
                    break;
                }
            }
            if (f == false) {
                gis_common.showMessage("地市匹配错误，请检查地市参数!");
                return;
            }
            time = tim;
            if (cellid != null && !gis_common.stringIsNullOrWhiteSpace(cellid)) {
                initcellid = cellid;
            }
            isFirst = true;
            marker_layer.clear()
            getMonitoringCellsTable();
            getMonitoringCells();
        });
    };
    this.initSceneQuery = function (stypeid, sid, tim, isdeg) {
        fun = "scene";
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();
        scene_type_id = stypeid;
        scene_id = sid;
        time = tim;
        isdegradecell = isdeg;

        getSceneCell();
        getSceneCellTable();
        getSceneKpiDetail();
    }
}

var realTimeMonitoring = null;
function initRealTimeMonitoring() {
    if (realTimeMonitoring == null) {
        realTimeMonitoring = new RealTimeMonitoring();
        realTimeMonitoring.init();
    }
}

//劣化小区查询
function RealTimeMonitoring_createQueryHtml(obj) {
    initRealTimeMonitoring();
    realTimeMonitoring.clear("monit");
    gis_layout.createMenubarWithFun("RealTimeMonitoring_createQueryHtml", obj, "劣化小区", realTimeMonitoring.createQueryHtml);
}
//场景查询条件
function RealTime_Scene_CreateQueryHtml(obj) {
    initRealTimeMonitoring();
    realTimeMonitoring.clear("scene");
    gis_layout.createMenubarWithFun("RealTime_Scene_CreateQueryHtml", obj, "场景", realTimeMonitoring.createSceneQueryHtml);
}
//默认查询
function RealTimeMonitoring_init_query() {
    initRealTimeMonitoring();
    realTimeMonitoring.clear();

    var fun = gis_common.getQueryString("fun");
    if (fun == "deg") {
        var cityid = gis_common.getQueryString("cityid");
        var time = gis_common.getQueryString("time");
        var cellid = gis_common.getQueryString("cellid");
        if (cityid == null || time == null) {
            return;
        }
        realTimeMonitoring.initDegQuery(cityid, time, cellid);
    }
    else if (fun == "scene") {
        var scenetypeid = gis_common.getQueryString("scenetypeid");
        var sceneid = gis_common.getQueryString("sceneid");
        var time = gis_common.getQueryString("time");
        var isdegradecell = gis_common.getQueryString("isdegradecell");
        realTimeMonitoring.initSceneQuery(scenetypeid, sceneid, time, isdegradecell);
    }
    else {

    }
}
