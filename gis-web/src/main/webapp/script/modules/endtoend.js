//引入checkBox 的样式
dynamicLoading.css("/css/checkboxes.css");
//var EndToEnd = function(){
function EndToEnd() {
    //事件相关
    var map_mouse_move = null;
    var map_mouse_out = null;
    var map_click_handle = null;
    var cellLayer_click_handle = null;
    //图层相关
    var grid_high_layer = gis_core.addGraphicsLayer("endtoend_grid_high_layer", "高亮图层");
    var cell_layer = gis_core.addGraphicsLayer("endtoEnd_cell_layer", "小区图层");
    var gridCellRel_layer = gis_core.addGraphicsLayer("endtoend_gridCellRel_layer", "栅格小区弱覆盖占比图层");
    var mro_layer = gis_core.addGraphicsLayer("endtoend_mro_layer", "MRO点图层");
    var mro_time = null;
    var wmsLayer_type = "";
    var wmsLayer = null; //渲染图层
    var wmsMroLayer = null; //渲染图层
    //指标渲染
    var kpi = null;
    var time = null;
    var delta_temp = null;
    var grid_id = "";
    var kpisinfo = null; //高级指标分析  指标信息
    var where_sql = "";
    var is_cell_layer_click = false;
    var legend_key = null;//图例关键字
    var iscompetitor = null;//是否竞争对手对比覆盖
    var RqCell = {  //查询小区的条件
        gsm: false,
        td: false,
        lte: false,
        alonecell: 0,
        region: {
            id: 1,
            type: 9000
        }
    };
    //连续弱覆盖栅格
    var serialWeakGrid_region = {
        id: 1,
        type: 9000
    };
    var gridCellRelLayer_onMouseOver_handle = null;
    var gridCellRelLayer_onMouseOut_handle = null;
    var dialog = null;
    //20*20 栅格渲染
    var Rq20GridParam = {
        time_dim: "m",
        time: ""
    };

    //添加渲染图层
    var loadWmsLayer = function (url) {
        if (wmsLayer != null || wmsLayer != undefined) {
            gis_core.removeLayer("endtoendwmsLayer")
        }
        if (url == null || url == undefined) {
            return;
        }
        wmsLayer = new esri.layers.WMSLayer(url, {
            resourceInfo: {
                extent: new esri.geometry.Extent(73.2672757, 18.37574, -109.66552734375, 133.29657, {
                    wkid: 4326
                }),
                layerInfos: []
            },
            visibleLayers: ['lteconvergrid']
        });
        wmsLayer.id = "endtoendwmsLayer";
        gis_core.insertLayer("endtoendwmsLayer", "网格覆盖", 2, wmsLayer);
    };
    //MRO渲染图层
    var loadMroWmsLayer = function (url) {
        if (wmsMroLayer != null || wmsMroLayer != undefined) {
            gis_core.removeLayer("endtoendwmsMroLayer")
        }
        if (url == null || url == undefined) {
            return;
        }
        wmsMroLayer = new esri.layers.WMSLayer(url, {
            resourceInfo: {
                extent: new esri.geometry.Extent(73.2672757, 18.37574, -109.66552734375, 133.29657, {
                    wkid: 4326
                }),
                layerInfos: []
            },
            visibleLayers: ['ltemroconvergrid']
        });
        wmsMroLayer.id = "endtoendwmsMroLayer";
        gis_core.insertLayer("endtoendwmsMroLayer", "MR点渲染", 4, wmsMroLayer);
        //gis_core.insertLayerNoIndex("endtoendwmsMroLayer", "MR点渲染", wmsMroLayer);
    };

    //初始化数据
    var initData = function () {
        if (gis_common.stringIsNullOrWhiteSpace(kpi) || gis_common.stringIsNullOrWhiteSpace(time)) {
            gis_common.showMessage("请选择查询参数");
            return;
        }
        $.ajax({
            url: "/getltecovercells",
            data: {
                time: time,
                kpi: kpi,
                wmsLayer_type: wmsLayer_type,
                where: where_sql
            },
            type: "POST",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var url = "/dynamicmap/" + wmsLayer_type + "/" + kpi + "/" + time + "_" + data.id;
                    loadWmsLayer(url);
                    legend_key = "";
                    loadLegendChange();
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    var loadLegend = function (key) {
        $.ajax({
            url: "/getltecoverlegend?time=" + Math.random(),
            data: {
                kpi: key,
                wmsLayer_type: wmsLayer_type
            },
            type: "POST",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    initLegend(data.tag);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var loadLegendChange = function () {
        if (gis_core.map.extent != null && wmsLayer_type != "serialweakgrid" && wmsLayer_type != "20gridcellscene" && wmsLayer_type != "" && wmsLayer_type != undefined) {
            var key = "";
            var xmax = gis_core.map.extent.xmax;
            var xmin = gis_core.map.extent.xmin;
            var ymax = gis_core.map.extent.ymax;
            var ymin = gis_core.map.extent.ymin;
            var p1 = GPS.mercator_encrypt(ymin, xmin);
            var p2 = GPS.mercator_encrypt(ymax, xmax);
            var w = p2.lon - p1.lon;
            var h = p2.lat - p1.lat;
            var count = w * h / (55 * 55);
            if (count > 100000) {
                key = "big_";
            }
            key = key + kpi;
            if (key != legend_key) {
                legend_key = key;
                $.ajax({
                    url: "/getltecoverlegend?time=" + Math.random(),
                    data: {
                        kpi: key,
                        wmsLayer_type: wmsLayer_type
                    },
                    type: "POST",
                    dataType: "json",
                    beforeSend: function () {
                        gis_common.isLoadingAddCount();
                    },
                    success: function (req) {
                        gis_common.outPut(req, function (data) {
                            initLegend(data.tag);
                        });
                    },
                    complete: function () {
                        gis_common.isLoadingSunCount();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
                    }
                });
            }

        }
    };
    //加载图例
    var initLegend = function (legends) {
        if (legends == null || legends == undefined || legends.legendItems.length == 0) {
            //gis_common.showMessage("初始化图例失败");
            return;
        }
        var content = "";
        for (var i = 0; i < legends.legendItems.length; i++) {
            var legend = legends.legendItems[i];
            var sRgb = "RGB(" + legend.r + ", " + legend.g + ", " + legend.b + ")";
            content += "<div class=\"legend-box\"";
            if (!gis_common.stringIsNullOrWhiteSpace(legend.title)) {
                content += " title='" + legend.title + "' ";
            }
            content += "style=\"background:rgba(" + legend.r + ", " + legend.g + ", " + legend.b + ",0.5);color:#fff;\">"
            if (!gis_common.stringIsNullOrWhiteSpace(legend.valueMin)) {
                content += legend.valueMin;
            }
            if (!gis_common.stringIsNullOrWhiteSpace(legend.valueMax)) {
                content += "-" + legend.valueMax;
            }
            content += "</div>";
            //content += "<div class=\"legend-box\" title='" + legend.title + "' style=\"background:rgba(" + legend.r + ", " + legend.g + ", " + legend.b + ",0.5);color:#fff;\"> " + legend.valueMin + "-" + legend.valueMax + "</div>";
        }
        var div = $(content);
        gis_layout.createLegend("endtoend_grid_legend", legends.name + "图例", div);

        var legend_load_data = "endtoend_legend_load";
        //添加导出按钮
        if (wmsLayer_type == "ltegridcover" || wmsLayer_type == "ltebuildcover") {
            var btn = "<div class=\"legend-box\" style='padding-left: 0px;padding-right: 0px;'><button id='legend_btn_load_data' class='btn btn-danger btn-xs'>数据导出</button></div>";
            var legend_btn = $(btn);
            gis_layout.createLegend(legend_load_data, "", legend_btn);
            legend_btn.find("#legend_btn_load_data").bind("click", function () {
                if (wmsLayer_type == "ltegridcover") {
                    loadWeakGridDataToCSV();
                }
                else if (wmsLayer_type == "ltebuildcover") {
                    loadWeakBuildingDataToCSV();
                }
            });
        }
        else {
            gis_layout.removeLegend(legend_load_data);
        }
    };

    //栅格点击 处理
    //查询栅格指标明细
    var loadGridKpiDetail = function () {
        if (gis_common.stringIsNullOrWhiteSpace(grid_id) || gis_common.stringIsNullOrWhiteSpace(time)) {
            //gis_common.showMessage("请选择查询参数");
            return;
        }
        $.ajax({
            url: "/getgriddetail",
            data: {
                time: time,
                gridid: grid_id
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var kpi = eval(data);
                    createGridKpiDetailHtml(data);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var createGridKpiDetailHtml = function (kpi) {
        var div = gis_common.createKeyValueList(kpi);
        gis_layout.createRightWidget("endtoend_createGridKpiDetailHtml", "栅格指标明细", div);
    };
    /**
     * 查询单个栅格 对应的小区 弱覆盖占比情况
     */
    var getGridCellRes = function (lon, lat) {
        gridCellRel_layer.clear();
        if (gis_common.stringIsNullOrWhiteSpace(grid_id) || gis_common.stringIsNullOrWhiteSpace(time)) {
            return;
        }
        $.ajax({
            url: "/getgridcellres",
            data: {
                time: time,
                gridid: grid_id
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    var point = new esri.geometry.Point(lon, lat);
                    var lineSymbol = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new esri.Color([200, 112, 181, 207]), 1);

                    //画小区
                    var btses = eval(datas);
                    var graphics = createCellCover(btses);
                    for (var i = 0; i < graphics.length; i++) {
                        var g = graphics[i];
                        var cell = g.attributes.data;
                        if (cell.index == 0) {
                            var symbol = new esri.symbol.SimpleFillSymbol();
                            symbol.setOutline(lineSymbol);
                            symbol.setColor(new esri.Color([0, 255, 102, 0.5]));
                            g.setSymbol(symbol);
                        }
                        else if (cell.index == 1) {
                            var symbol = new esri.symbol.SimpleFillSymbol();
                            symbol.setOutline(lineSymbol);
                            symbol.setColor(new esri.Color([255, 0, 0, 0.9]));
                            g.setSymbol(symbol);
                        }
                        else if (cell.index == 2) {
                            var symbol = new esri.symbol.SimpleFillSymbol();
                            symbol.setOutline(lineSymbol);
                            symbol.setColor(new esri.Color([255, 102, 102, 0.9]));
                            g.setSymbol(symbol);
                        }
                        else if (cell.index == 3) {
                            var symbol = new esri.symbol.SimpleFillSymbol();
                            symbol.setOutline(lineSymbol);
                            symbol.setColor(new esri.Color([255, 153, 204, 0.9]));
                            g.setSymbol(symbol);
                        }
                        else if (cell.index == 4) {
                            var symbol = new esri.symbol.SimpleFillSymbol();
                            symbol.setOutline(lineSymbol);
                            symbol.setColor(new esri.Color([250, 255, 152, 0.8]));
                            g.setSymbol(symbol);
                        }

                        gridCellRel_layer.add(g);
                    }

                    //画连线
                    var symbol = new esri.symbol.SimpleLineSymbol(
                        esri.symbol.SimpleLineSymbol.STYLE_SOLID,
                        new esri.Color([180, 168, 192]),
                        2
                    );

                    for (var i = 0; i < btses.length; i++) {
                        var bts = btses[i];
                        var line = new esri.geometry.Polyline(gis_core.getSpatialReference());
                        line.addPath([point, new esri.geometry.Point(bts.longitude, bts.latitude)]);
                        var g = new esri.Graphic(line, symbol);
                        gridCellRel_layer.add(g);
                    }
                    //开始点
                    var start_symbol = new esri.symbol.PictureMarkerSymbol('/images/point.png', 28, 28);
                    var start_graphic = new esri.Graphic(point, start_symbol);
                    gridCellRel_layer.add(start_graphic);

                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var tooltip = function (evt) {
        if (evt.graphic.attributes != undefined && evt.graphic.attributes.data == null) {
            return;
        }
        var data = evt.graphic.attributes.data;
        var t = "<b>" + evt.graphic.attributes.name + "</b>";
        t += "</br><b>MR点弱覆盖占比:" + data.value + "%</b>";
        dialog.setContent(t);
        dijit.popup.open({
            popup: dialog,
            x: evt.pageX,
            y: evt.pageY
        });
    };
    var closeDialog = function () {
        dijit.popup.close(dialog);
    };

    //高亮栅格块
    var createHigh50Grid = function (lon, lat) {
        if (delta_temp != null) {
            var xmax = delta_temp.extent.xmax;
            var xmin = delta_temp.extent.xmin;
            var ymax = delta_temp.extent.ymax;
            var ymin = delta_temp.extent.ymin;
            var p1 = GPS.mercator_encrypt(ymin, xmin);
            var p2 = GPS.mercator_encrypt(ymax, xmax);
            var w = p2.lon - p1.lon;
            var h = p2.lat - p1.lat;
            var count = w * h / (55 * 55);
            if (count <= 4000) {
                var point_xmin = get50GridId(lon);
                var point_ymin = get50GridId(lat);
                var _id = point_xmin.replace(".", "") + point_ymin.replace(".", "");
                if (grid_id != _id) {
                    grid_high_layer.clear();
                    grid_id = _id;
                    var rings = [];
                    var x = parseFloat(point_xmin);
                    var y = parseFloat(point_ymin);
                    rings.push([x, y]);
                    rings.push([x + 0.0005, y]);
                    rings.push([x + 0.0005, y + 0.0005]);
                    rings.push([x, y + 0.0005]);
                    rings.push([x, y]);
                    var polygon = new esri.geometry.Polygon(gis_core.map.spatialReference);
                    polygon.addRing(rings);
                    var symbol = new esri.symbol.SimpleLineSymbol().setColor(new esri.Color([255, 0, 0])).setWidth(2);
                    var graphic = new esri.Graphic(polygon, symbol);
                    grid_high_layer.add(graphic);
                }
            } else {
                grid_high_layer.clear();
                grid_id = null;
            }
        }
    };
    var get50GridId = function (l) {
        var va = 0;
        l = l + 0.000001;
        var ls = (l + "").split(".");
        var l_4 = parseInt(ls[1].substring(3, 4));
        if (l_4 > 4) {
            va = ls[0] + "." + ls[1].substring(0, 3) + "5";
        } else {
            va = ls[0] + "." + ls[1].substring(0, 3) + "0";
        }
        return va;
    };
    var createHighGrid = function (lon, lat, count) {
        if (delta_temp != null) {
            var xmax = delta_temp.extent.xmax;
            var xmin = delta_temp.extent.xmin;
            var ymax = delta_temp.extent.ymax;
            var ymin = delta_temp.extent.ymin;
            var p1 = GPS.mercator_encrypt(ymin, xmin);
            var p2 = GPS.mercator_encrypt(ymax, xmax);
            var w = p2.lon - p1.lon;
            var h = p2.lat - p1.lat;
            var max_grid = w * h / (count * count * 100);
            if (max_grid <= 8000) {
                var grid = getGrid(lon, lat, count);
                if (grid_id != grid.id) {
                    grid_high_layer.clear();
                    grid_id = grid.id;
                    var rings = [];
                    rings.push([grid.xmin, grid.ymin]);
                    rings.push([grid.xmax, grid.ymin]);
                    rings.push([grid.xmax, grid.ymax]);
                    rings.push([grid.xmin, grid.ymax]);
                    rings.push([grid.xmin, grid.ymin]);
                    var polygon = new esri.geometry.Polygon(gis_core.map.spatialReference);
                    polygon.addRing(rings);
                    var symbol = new esri.symbol.SimpleLineSymbol().setColor(new esri.Color([255, 0, 0])).setWidth(2);
                    var graphic = new esri.Graphic(polygon, symbol);
                    grid_high_layer.add(graphic);
                }
            } else {
                grid_high_layer.clear();
                grid_id = null;
            }
        }
    };
    var getGrid = function (x, y, count) {
        var m = 10000;
        var off = 0.0001 * count;
        var _x = x * m;
        var _y = y * m;
        var x_min = parseInt(_x / count) * 1.0 * count / m;
        var y_min = parseInt(_y / count) * 1.0 * count / m;
        var x_max = x_min + off;
        var y_max = y_min + off;
        var x_min_arr = (x_min + "").split(".");
        var y_min_arr = (y_min + "").split(".");
        var id = x_min_arr[0] + (x_min_arr[1] + "00000").substring(0, 4) + y_min_arr[0] + (y_min_arr[1] + "00000").substring(0, 4);
        var grid = {xmin: x_min, ymin: y_min, xmax: x_max, ymax: y_max, id: id};
        return grid;

    };

    //保存图片
    var saveImage = function () {
        html2canvas($("#map"), {
            allowTaint: true,
            taintTest: false,
            onrendered: function (canvas) {
                var dataUrl = canvas.toDataURL();
                $.ajax({
                    url: "/saveimage",
                    data: {
                        imagedata: dataUrl
                    },
                    type: "POST",
                    beforeSend: function () {
                        gis_common.isLoadingAddCount();
                    },
                    success: function (req) {
                        gis_common.outPut(req, function (url) {
                            gis_common.windowOpen("pages/export.jsp?typ=image/bmp&fid=" + url, "_blank")
                        });
                    },
                    complete: function () {
                        gis_common.isLoadingSunCount();
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
                    }
                });
            }
        });
    };

    //导出文件
    var loadSerialWeakGridDataToCSV = function () {
        $.ajax({
            url: "/loadserialweakgridcsv",
            data: {
                time: time,
                region: JSON.stringify(serialWeakGrid_region)
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.isLoadingSunCount();
                gis_common.outPut(req, function (url) {
                    gis_common.windowOpen("pages/export.jsp?typ=application/csv&fid=" + url, "_blank")
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.isLoadingSunCount();
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    }
    var loadWeakBuildingDataToCSV = function () {
        $.ajax({
            url: "/loadweakbuildingcsv",
            data: {
                time: time,
                where: where_sql
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.isLoadingSunCount();
                gis_common.outPut(req, function (url) {
                    gis_common.windowOpen("pages/export.jsp?typ=application/csv&fid=" + url, "_blank")
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.isLoadingSunCount();
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    }
    var loadWeakGridDataToCSV = function () {
        $.ajax({
            url: "/loadweakgridcsv",
            data: {
                time: time,
                where: where_sql
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.isLoadingSunCount();
                gis_common.outPut(req, function (url) {
                    gis_common.windowOpen("pages/export.jsp?typ=application/csv&fid=" + url, "_blank")
                });
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.isLoadingSunCount();
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    }
    //加载连续弱覆盖数据
    var loadSerialWeakGridData = function () {
        $.ajax({
            url: "/loadserialseakgriddata",
            data: {
                time: time,
                region: JSON.stringify(serialWeakGrid_region)
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    if (data > 0) {
                        getSerialWeakGridTable();
                    } else {
                        gis_common.showMessage("无数据");
                        gis_layout.removeLeftWidget("EndToEnd_SerialWeakGrid_JQGrid_Div");
                    }
                });
            },
            complete: function (lines) {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    //加载连续弱覆盖数据 表格显示
    var getSerialWeakGridTable = function () {
        var content = "<div>";
        content += "<div><span><button id='end_to_end_saveimage' class='btn btn-primary btn-xs'>保存图片</button>  <button id='end_to_end_loaddata' class='btn btn-primary btn-xs'>导 出</button></span></div>";
        content += "<table id='EndToEnd_SerialWeakGrid_Table'></table>";
        content += "<div id='EndToEnd_SerialWeakGrid_Pager'></div>"
        content += "<div>";
        var div = $(content);

        gis_layout.createLeftWidget("EndToEnd_SerialWeakGrid_JQGrid_Div", "连续弱覆盖", div);
        div.find("#end_to_end_saveimage").bind("click", function () {
            saveImage();
        });
        div.find("#end_to_end_loaddata").bind("click", function () {
            loadSerialWeakGridDataToCSV();
        });
        div.find("#EndToEnd_SerialWeakGrid_Table").jqGrid({
            url: '/serialweakgrid?time=' + time + "&region=" + JSON.stringify(serialWeakGrid_region),
            datatype: "json",
            width: 400,
            height: 230,
            colNames: ['ID', '时间', '地市', '弱覆盖栅格数', '平均弱覆盖比例', 'XMAX', 'YMAX', 'XMIN', 'YMIN'],
            colModel: [{
                name: 'id',
                index: 'id',
                width: 50,
                hidden: true
            }, {
                name: 'time',
                index: 'time',
                width: 50
            }, {
                name: 'cityName',
                index: 'cityId',
                width: 40
            }, {
                name: 'weakGridCount',
                index: 'weakGridCount',
                width: 40,
                align: "right"
            }, {
                name: 'avgWeakRat',
                index: 'avgWeakRat',
                width: 40,
                align: "right"
            }, {
                name: 'maxLongitude',
                index: 'maxLongitude',
                width: 80,
                align: "right"
            }, {
                name: 'maxLatitude',
                index: 'maxLatitude',
                width: 80,
                align: "right"
            }, {
                name: 'minLongitude',
                index: 'minLongitude',
                width: 80,
                align: "right"
            }, {
                name: 'minLatitude',
                index: 'minLatitude',
                width: 80,
                align: "right"
            }
            ],
            rowNum: 10,
            rowList: [10, 20, 30, 50],
            pager: '#EndToEnd_SerialWeakGrid_Pager',
            mtype: "get",
            viewrecords: true,
            caption: "",
            onSelectRow: function (id) {
                var data = div.find("#EndToEnd_SerialWeakGrid_Table").jqGrid('getRowData', id);
                gis_common.setExtent(parseFloat(data.maxLongitude) + 0.0005, parseFloat(data.maxLatitude) + 0.0005, parseFloat(data.minLongitude) - 0.0005, parseFloat(data.minLatitude) - 0.0005);
                var url = "/dynamicmap/serialweakgrid/weak_rat/" + data.time + "_" + data.id;
                loadWmsLayer(url);
                grid_id = data.id;
                time = data.time;
                loadLegend("weak_rat");
            }
        });
    };

    //地图改变事件
    var extentchange = function (delta) {
        delta_temp = delta;
        loadCells();
        loadLegendChange();
        loadCompetitorCoverCellKpi();
    };

    //指标高级分析主界面
    var kpiAdvancedAnalysisTable = function () {
        var rowIndex = 0;
        var content = "<ul class='panel-list'>";
        content += "<table id='endtoend_advanced_table' class='table'>";
        content += "</table>";
        content += "<div>";
        //content += "<span><button id='endtoend_advanced_btn_query' class='btn btn-primary'>查 询</button>    <button id='endtoend_advanced_btn_clear' class='btn btn-primary'>清除条件</button></span>";
        content += "<span><button id='endtoend_advanced_btn_add_param' class='btn btn-primary' style='display:none;'>新增查询条件</button>   <button id='endtoend_advanced_btn_query' class='btn btn-primary'>查 询</button>";
        content += "</div>";
        content += "</ul>";

        var div = $(content);
        kpiAdvancedAnalysisAddRow(0, div);
        div.find("#endtoend_advanced_btn_query").bind("click", function () {
            var trs = div.find("#endtoend_advanced_table tr");
            var where = "";
            for (var i = 0; i < trs.length; i++) {
                var tr = $(trs[i]);
                var index = tr.attr("index");
                var value = tr.find("#" + index + "_endtoend_advanced_value").val();
                //如果值为空，直接跳过
                if (gis_common.stringIsNullOrWhiteSpace(value)) {
                    continue;
                }

                //判断条件
                if (i > 0) {
                    var td0 = tr.find("#" + index + "_endtoend_advanced_where option:selected");
                    where += " " + td0.attr("where") + " ";
                }
                //指标名
                var td1 = tr.find("#" + index + "_endtoend_advanced_kpi option:selected");
                var tag = td1.attr("kpitag");
                where += td1.attr("kpiid") + " ";
                //运算符
                var td2 = tr.find("#" + index + "_endtoend_advanced_alg option:selected");
                where += td2.attr("where") + " ";
                //值
                //where += tr.find("#" + index + "_endtoend_advanced_value").val();
                where += value;

                if (tag != null && tag != "null" && tag != undefined && tag.length > 0) {
                    where += tag;
                }
            }
            where_sql = where;
            initData();
        });
        div.find("#endtoend_advanced_btn_add_param").bind("click", function () {
            kpiAdvancedAnalysisAddRow(0, div);
            $(this).css("display", "none");
        });
        return div;
    };

    var kpiAdvancedAnalysisAddRow = function (rowIndex, div) {
        var content = "<tr id='" + rowIndex + "_tr' index='" + rowIndex + "'>";
        content += "<td>";
        //第一列 and or
        if (rowIndex != 0) {
            content += "<select id='" + rowIndex + "_endtoend_advanced_where'>";
            content += "<option where='AND'>AND</option>";
            content += "<option where='OR'>OR</option>";
            content += "</select>";
        }
        content + "</td>";
        //第二列 指标下拉列表
        content += "<td>";
        content += "<select id='" + rowIndex + "_endtoend_advanced_kpi' style='width:120px;' >";
        for (var i = 0; i < kpisinfo.length; i++) {
            var kpi = kpisinfo[i];
            content += "<option kpiid='" + kpi.id + "' kpiname='" + kpi.name + "' kpitag='" + kpi.tag + "'>" + kpi.name + "</option>";
        }
        content += "</select>";
        content + "</td>";
        //第三列 运算符
        content += "<td>";
        content += "<select id='" + rowIndex + "_endtoend_advanced_alg'>";
        content += "<option where='>'>></option>";
        content += "<option where='>='>>=</option>";
        content += "<option where='='>=</option>";
        content += "<option where='<'><</option>";
        content += "<option where='<='><=</option>";
        content += "<option where='<>'>!=</option>";
        content += "</select>";
        content + "</td>";
        //第四列 值
        content += "<td>";
        content += "<input id='" + rowIndex + "_endtoend_advanced_value' type='text' style='width:50px' />";
        content + "</td>";
        //第五列  新增 删除
        content += "<td>";
        content += "<span>";
        content += "<button id='" + rowIndex + "_endtoend_advanced_btn_add' class='btn btn-primary btn-xs'>新增</button>  ";
        content += "<button id='" + rowIndex + "_endtoend_advanced_btn_del'  class='btn btn-primary btn-xs'>删除</button>";

        var tr = $(content);
        tr.find("#" + rowIndex + "_endtoend_advanced_btn_add").bind("click", function () {
            kpiAdvancedAnalysisAddRow(rowIndex, div);
        });
        tr.find("#" + rowIndex + "_endtoend_advanced_btn_del").bind("click", function () {
            var trs = div.find("#endtoend_advanced_table tr");
            tr.remove();
            var tr_count = div.find("#endtoend_advanced_table tr").length;
            if (tr_count == 0) {
                $("#endtoend_advanced_btn_add_param").show();
            }
        });

        div.find("#endtoend_advanced_table").append(tr);
        rowIndex = rowIndex + 1;
    };

    //加载小区
    var loadCells = function () {
        cell_layer.clear();
        if (mro_time == null || mro_time == "") {
            gis_common.queryRemoveFun("endToEnd_getmroltebtstopn");
            loadMroWmsLayer(null);
            return;
        }
        $.ajax({
            url: "/getmroltebts",
            data: {
                extent: JSON.stringify(gis_common.getMapExtent()),
                time: mro_time
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    var btses = eval(datas);
                    var graphics = createCellCover(btses);
                    for (var i = 0; i < graphics.length; i++) {
                        cell_layer.add(graphics[i]);
                    }
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
        gis_common.queryRegisterFun("endToEnd_getmroltebtstopn", loadTopCells);
    };
    var loadTopCells = function (keywords, callback) {
        if (mro_time == null) {
            return;
        }
        $.ajax({
            url: "/getmroltebtstopn",
            data: {
                key: keywords,
                time: mro_time
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    callback(datas);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    var createCellCover = function (btses) {
        var cells = [];
        for (var i = 0; i < btses.length; i++) {
            var bts = btses[i];
            if (bts.tag == "cluster") {
                var point = new esri.geometry.Point(bts.longitude, bts.latitude, gis_core.initconfig.wkid);

                var symbol = new esri.symbol.PictureMarkerSymbol('/images/cluster.png', 48, 48);
                var graphic = new esri.Graphic(point, symbol);
                //cell_layer.add(graphic);
                cells.push(graphic);
                var text = bts.value + "";
                if (bts.value >= 1000) {
                    text = (bts.value / 1000).toFixed(2) + "万"
                }
                var textSymbol = new esri.symbol.TextSymbol(text);
                textSymbol.setAlign(esri.symbol.TextSymbol.ALIGN_MIDDLE);
                textSymbol.setColor(new esri.Color([255, 255, 255, 1]));
                textSymbol.setOffset(0, -5);
                var textgraphic = new esri.Graphic(point, textSymbol);
                //cell_layer.add(textgraphic);
                cells.push(textgraphic);
            }
            else {
                var antbw = 30;
                var azimuth = 0;
                var off_azimuth = 0;
                if (bts.cells.length > 1) {
                    antbw = 180.0 / bts.cells.length;
                    off_azimuth = 360.0 / bts.cells.length;
                }
                if (antbw > 30) {
                    antbw = 30;
                }

                for (var j = 0; j < bts.cells.length; j++) {
                    var cell = bts.cells[j];
                    if (cell.radius < 30) {
                        cell.radius = 30;
                    }
                    cell.antbw = antbw;
                    if (cell.azimuth == 0) {
                        cell.azimuth = azimuth;
                    }
                    var g = gis_common.createCellGraphic(cell);
                    //cell_layer.add(g);
                    cells.push(g);
                    azimuth = azimuth + off_azimuth;
                }
            }
        }
        cells = cells.reverse();
        return cells;
    };

    var loadCellMroPoint = function (cellId) {
        mro_layer.clear();
        if (gis_common.stringIsNullOrWhiteSpace(mro_time)) {
            return;
        }
        $.ajax({
            url: "/loadcellmropoint",
            data: {
                time: mro_time,
                cellid: cellId
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    loadMroWmsLayer(null);
                    if (datas == "success") {
                        var url = "/dynamicmap/endtoendmro/cell/" + mro_time + "_" + cellId;
                        loadMroWmsLayer(url);
                    }
                    else {
                        // 标记边框
                        var outSymbol = new esri.symbol.SimpleLineSymbol();
                        outSymbol.setStyle(esri.symbol.SimpleLineSymbol.STYLE_NULL);

                        var symbolred = new esri.symbol.SimpleMarkerSymbol();
                        symbolred.setOutline(outSymbol);
                        symbolred.setSize(3);
                        symbolred.setColor(new esri.Color([255, 0, 0, 1]));

                        var symbolblue = new esri.symbol.SimpleMarkerSymbol();
                        symbolblue.setOutline(outSymbol);
                        symbolblue.setSize(3);
                        symbolblue.setColor(new esri.Color([0, 0, 255, 1]));

                        for (var i = 0; i < datas.length; i++) {
                            var b = datas[i];
                            var point = new esri.geometry.Point(b.longitude, b.latitude, gis_core.map.spatialReference);
                            if (b.value < 31) {
                                var graphic = new esri.Graphic(point, symbolred);
                                mro_layer.add(graphic);
                            }
                            else {
                                var graphic = new esri.Graphic(point, symbolblue);
                                mro_layer.add(graphic);
                            }
                        }
                    }
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    }
    var loadBuildMroPoint = function (buildId) {
        mro_layer.clear();
        $.ajax({
            url: "/loadbuildmropoint",
            data: {
                time: time,
                buildid: buildId
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    // 标记边框
                    var outSymbol = new esri.symbol.SimpleLineSymbol();
                    outSymbol.setStyle(esri.symbol.SimpleLineSymbol.STYLE_NULL);

                    var symbolred = new esri.symbol.SimpleMarkerSymbol();
                    symbolred.setOutline(outSymbol);
                    symbolred.setSize(3);
                    symbolred.setColor(new esri.Color([255, 0, 0, 1]));


                    var symbolblue = new esri.symbol.SimpleMarkerSymbol();
                    symbolblue.setOutline(outSymbol);
                    symbolblue.setSize(3);
                    symbolblue.setColor(new esri.Color([0, 0, 255, 1]));

                    for (var i = 0; i < datas.length; i++) {
                        var b = datas[i];
                        var point = new esri.geometry.Point(b.longitude, b.latitude, gis_core.map.spatialReference);
                        if (b.value < 31) {
                            var graphic = new esri.Graphic(point, symbolred);
                            mro_layer.add(graphic);
                        }
                        else {
                            var graphic = new esri.Graphic(point, symbolblue);
                            mro_layer.add(graphic);
                        }
                    }
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    }
    var checkPointBuild = function (lng, lat) {
        $.ajax({
            url: "/checkpoinbuild",
            data: {
                lng: lng,
                lat: lat
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    if (!gis_common.stringIsNullOrWhiteSpace(data)) {
                        loadBuildMroPoint(data);
                        loadBuildKpiDetail(data);
                    }
                    else {
                        gis_layout.removeLeftWidget("endtoend_createBuildKpiDetailHtml");
                    }
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    }
    var loadBuildKpiDetail = function (buildId) {
        if (gis_common.stringIsNullOrWhiteSpace(buildId) || gis_common.stringIsNullOrWhiteSpace(time)) {
            //gis_common.showMessage("请选择查询参数");
            return;
        }
        $.ajax({
            url: "/getbuilddetail",
            data: {
                time: time,
                buildid: buildId
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var div = gis_common.createKeyValueList(data);
                    gis_layout.createRightWidget("endtoend_createBuildKpiDetailHtml", "建筑物指标明细", div);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    //竞争对手覆盖 竞争对手黑点
    var loadCompetitorCover = function () {
        loadLegend(kpi);
        loadCompetitorCoverCellKpi();

    };
    var loadCompetitorCoverCellKpi = function () {
        var arr = wmsLayer_type.split("_");
        if (!(arr.length == 2 && (arr[0] == "competitorcover" || arr[0] == "competitorcoverspots") && arr[1] == "1")) {
            gis_common.queryRemoveFun("endToEnd_loadCompetitorCoverCellKpi");
            return;
        }
        cell_layer.clear();
        $.ajax({
            url: "/getgridcovercellkpi",
            data: {
                time: time,
                kpi: kpi,
                where: where_sql,
                wmsLayer_type: wmsLayer_type,
                extent: JSON.stringify(gis_common.getMapExtent())
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (btses) {
                    if (btses == null || btses == undefined) {
                        return;
                    }
                    for (var i = 0; i < btses.length; i++) {
                        var bts = btses[i];
                        var antbw = 30;
                        var azimuth = 0;
                        var off_azimuth = 0;
                        if (bts.cells.length > 1) {
                            antbw = 180.0 / bts.cells.length;
                            off_azimuth = 360.0 / bts.cells.length;
                        }
                        if (antbw > 30) {
                            antbw = 30;
                        }

                        for (var j = 0; j < bts.cells.length; j++) {
                            var cell = bts.cells[j];
                            cell.radius = 30;
                            cell.antbw = antbw;
                            if (cell.azimuth == 0) {
                                cell.azimuth = azimuth;
                            }
                            var g = gis_common.createCellGraphic(cell);
                            var symbol = new esri.symbol.SimpleFillSymbol();
                            var c = cell.tag.split(",");
                            symbol.setColor(new esri.Color([c[0], c[1], c[2], c[3]]));
                            symbol.setOutline(new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new esri.Color([200, 112, 181, 207]), 1));
                            g.setSymbol(symbol);
                            cell_layer.add(g);
                            azimuth = azimuth + off_azimuth;
                        }
                    }
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
        gis_common.queryRegisterFun("endToEnd_loadCompetitorCoverCellKpi", loadCompetitorCoverCellKpiTopn);
    };
    var loadCoverCellKpiDetail = function (eci) {
        if (gis_common.stringIsNullOrWhiteSpace(eci) || gis_common.stringIsNullOrWhiteSpace(time)) {
            //gis_common.showMessage("请选择查询参数");
            return;
        }
        $.ajax({
            url: "/getgridcovercellkpidetail",
            data: {
                time: time,
                eci: eci
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var div = gis_common.createKeyValueList(data);
                    gis_layout.createRightWidget("endtoend_createCompetitorCoverCellKpiHtml", "小区指标明细", div);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var loadCompetitorCoverCellKpiTopn = function (keywords, callback) {
        var arr = wmsLayer_type.split("_");
        if (time == null || !(arr.length == 2 && (arr[0] == "competitorcover" || arr[0] == "competitorcoverspots") && arr[1] == "1")) {
            gis_common.queryRemoveFun("endToEnd_loadCompetitorCoverCellKpi");
            return;
        }
        $.ajax({
            url: "/getcovercellkpitopn",
            data: {
                key: keywords,
                time: time,
                kpi: kpi,
                where: where_sql,
                wmsLayer_type: wmsLayer_type
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    callback(datas);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };

    //20*20栅格
    var createQueryParamHtml20 = function () {
        var content = "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>查询维度:</label>";
        content += "	<div class='col-sm-9' id='endtoend_20_query_time_dim_div'>";
        content += "          <div class='radio radio-info radio-inline'>";
        content += "            <input type='radio' id='inlineRadio1' value='m' name='radioInline' checked>";
        content += "            <label for='inlineRadio1'> 月 </label>";
        content += "          </div>"
        content += "          <div class='radio radio-info radio-inline'>";
        content += "            <input type='radio' id='inlineRadio2' value='d' name='radioInline'>";
        content += "            <label for='inlineRadio2'> 天 </label>";
        content += "          </div>"
        content += "          <div class='radio radio-info radio-inline'>";
        content += "            <input type='radio' id='inlineRadio3' value='h' name='radioInline'>";
        content += "            <label for='inlineRadio3'> 小时 </label>";
        content += "          </div>"
        content += "          <div class='radio radio-info radio-inline'>";
        content += "            <input type='radio' id='inlineRadio4' value='min' name='radioInline'>";
        content += "            <label for='inlineRadio4'> 15分钟 </label>";
        content += "          </div>"
        content += " 	</div > ";
        content += " </div > "

        content += "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
        content += "	<div class='col-sm-9'>";
        content += "        <div class='input-group' id='endtoend_20_query_time_input_group'>";
        content += "             <input id='endtoend_20_query_time_input' type='text' class='form-control' aria-label='...' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM'});\" >";
        content += "             <div class='input-group-btn dropup'>"
        content += "			    <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>";
        content += "                     <span id='select_show'>快速选取</span><span class='caret'></span>";
        content += "                 </button>";
        content += "                <ul class='dropdown-menu dropdown-menu-right' aria-labelledby='dLabel'>";
        content += "                </ul>";
        content += "             </div>";
        content += " 	    </div > ";
        content += " 	</div > ";
        content += " </div > "

        content += " <div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>渲染指标:</label>";
        content += "	<div id='endtoend_20_query_param_kpi' class='col-sm-9'>";
        content += "		<select id='endtoend_20_menu_kpi_select' class='form-control'>";
        content += "			<option value='weak_rat' kpi='weak_rat'>4G覆盖</option>";
        content += "			<option value='lte_flow' kpi='lte_flow'>4G流量</option>";
        content += "			<option value='lte_user_cnt' kpi='lte_user_cnt'>4G用户数</option>";
        content += "			<option value='gsm_flow' kpi='gsm_flow'>2G流量</option>";
        content += "			<option value='gsm_user_cnt' kpi='gsm_user_cnt'>2G用户</option>";
        content += "		</select>";
        content += "	</div>";
        content += "</div>"

        content += "<div class='form-group'>";
        content += " 	<div class='col-sm-9 col-sm-offset-3'>";
        content += "		<button id='endtoend_20_menu_btn_load' class='btn btn-primary'>查 询</button>";
        content += "	</div>";
        content += "</div>";

        var div = $(content);

        div.find('.dropdown-toggle').dropdown();
        div.find("#endtoend_20_query_time_dim_div input").click(function () {
            var dim = $(this).val();
            if (Rq20GridParam.time_dim != dim) {
                Rq20GridParam.time_dim = dim;
                div.find("#endtoend_20_query_time_input").remove();

                var con = "<input id='endtoend_20_query_time_input' type='text' class='form-control' aria-label='...' ";
                if (Rq20GridParam.time_dim == "m") {
                    con += "onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM'});\" >";
                }
                else if (Rq20GridParam.time_dim == "d") {
                    con += "onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd'});\" >";
                }
                else if (Rq20GridParam.time_dim == "h") {
                    con += "onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH'});\" >";
                }
                else if (Rq20GridParam.time_dim == "min") {
                    con += "onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM-dd HH:mm'});\" >";
                }
                div.find("#endtoend_20_query_time_input_group").prepend($(con));
                loadTimeKeyQueryGrid20(Rq20GridParam.time_dim, function (datas) {
                    div.find(".dropdown-menu li").remove();
                    if (datas != null || datas != "") {
                        var ul_li = "";
                        datas = datas.reverse();

                        for (var i = 0; i < datas.length; i++) {
                            var data = datas[i];
                            if (i == datas.length - 1) {
                                div.find("#endtoend_20_query_time_input").val(data.name);
                            }
                            ul_li += "<li value='" + data.id + "'><a href='javascript:void(0);'>" + data.name + "</a></li>";
                        }
                        var $ul_li = $(ul_li);
                        div.find(".dropdown-menu").append($ul_li);
                        $ul_li.click(function () {
                            var val = $(this).find('a').text();
                            div.find("#endtoend_20_query_time_input").val(val);
                            div.find('.dropdown-toggle').dropdown('toggle')
                        });
                    }
                });
            }
        });
        div.find("#inlineRadio4").click();
        div.find("#endtoend_20_menu_btn_load").bind("click", function () {
            var time = div.find("#endtoend_20_query_time_input").val();
            if (gis_common.stringIsNullOrWhiteSpace(time)) {
                gis_common.showMessage("请选择时间");
                return;
            }

            var timdim = div.find("#endtoend_20_query_time_dim_div input:checked").val()
            Rq20GridParam.time_dim = timdim;

            var _time = time.replace(/-/g, "").replace(/:/g, "").replace(" ", "");
            if (timdim == "min") {
                _time = _time.substring(0, 10) + parseInt(parseInt(_time.substring(10, 12)) / 15) * 15;
            }

            Rq20GridParam.time = _time; //time.replace(/-/g, "").replace(/:/g, "").replace(" ", "");
            var kpi_option = div.find("#endtoend_20_menu_kpi_select option:selected")
            if (kpi_option == null) {
                gis_common.showMessage("请选择渲染指标");
                return;
            }
            kpi = kpi_option.val();
            loadSceneDimTableGrid20();
            loadLegend(kpi);
        });
        return div;
    };
    var loadTimeKeyQueryGrid20 = function (timedim, callback) {
        $.ajax({
            url: "/get20gridparamtimekey",
            data: {
                timedim: timedim
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (datas) {
                    callback(datas);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var loadSceneDimTableGrid20 = function () {
        var content = "<div>";
        content += "<table id='EndToEnd_20GridCellScene_Table'></table>";
        content += "<div id='EndToEnd_20GridCellScene_Pager'></div>"
        content += "<div>";
        var div = $(content);

        gis_layout.createLeftWidget("EndToEnd_20GridCellScene_JQGrid_Div", "20*20场景栅格覆盖", div);

        div.find("#EndToEnd_20GridCellScene_Table").jqGrid({
            url: '/get20gridcellscenelist?time=' + Rq20GridParam.time + "&timdim=" + Rq20GridParam.time_dim,
            datatype: "json",
            width: 500,
            height: 230,
            colNames: ['ID', '名称', '弱覆盖率(%)', 'LTE流量(M)', 'LTE用户数', 'GSM流量(M)', 'GSM用户数', '经度', '纬度'],
            colModel: [{
                name: 'id',
                index: 'id',
                hidden: true
            }, {
                name: 'name',
                index: 'name'
            }, {
                name: 'weakRat',
                index: 'weakRat'
            }, {
                name: 'lteFlow',
                index: 'lteFlow',
                align: "right"
            }, {
                name: 'lteUserCnt',
                index: 'lteUserCnt',
                align: "right"
            }, {
                name: 'gsmFlow',
                index: 'gsmFlow',
                align: "right"
            }, {
                name: 'gsmUserCnt',
                index: 'gsmUserCnt',
                align: "right"
            }, {
                name: 'longitude',
                index: 'longitude',
                hidden: true
            }, {
                name: 'latitude',
                index: 'latitude',
                hidden: true
            }
            ],
            rowNum: 10,
            rowList: [10, 20, 30, 50],
            pager: '#EndToEnd_20GridCellScene_Pager',
            mtype: "get",
            viewrecords: true,
            caption: "",
            onSelectRow: function (id) {
                var data = div.find("#EndToEnd_20GridCellScene_Table").jqGrid('getRowData', id);
                gis_common.setExtent(parseFloat(data.longitude) + 0.005, parseFloat(data.latitude) + 0.005, parseFloat(data.longitude) - 0.005, parseFloat(data.latitude) - 0.005);
                wmsLayer_type = "20gridcellscene";
                var url = "/dynamicmap/" + wmsLayer_type + "/" + kpi + "/" + Rq20GridParam.time + "_" + Rq20GridParam.time_dim + "_" + data.id;
                loadWmsLayer(url);
                loadSceneKpiDetail20(data.id);
                /*grid_id = data.id;
                 time = data.time;
                 loadLegend("weak_rat");*/
            }
        });
    };
    var loadGridKpiDetail20 = function () {
        if (gis_common.stringIsNullOrWhiteSpace(grid_id) || gis_common.stringIsNullOrWhiteSpace(Rq20GridParam.time_dim) || gis_common.stringIsNullOrWhiteSpace(Rq20GridParam.time)) {
            //gis_common.showMessage("请选择查询参数");
            return;
        }
        $.ajax({
            url: "/get20griddetail",
            data: {
                timdim: Rq20GridParam.time_dim,
                time: Rq20GridParam.time,
                gridid: grid_id
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var kpi = eval(data);
                    createGridKpiDetailHtml(data);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    var loadSceneKpiDetail20 = function (sceneid) {
        if (gis_common.stringIsNullOrWhiteSpace(Rq20GridParam.time_dim) || gis_common.stringIsNullOrWhiteSpace(Rq20GridParam.time)
            || gis_common.stringIsNullOrWhiteSpace("sceneid")) {
            //gis_common.showMessage("请选择查询参数");
            return;
        }
        $.ajax({
            url: "/getcelldimscenedetail",
            data: {
                timdim: Rq20GridParam.time_dim,
                time: Rq20GridParam.time,
                sceneid: sceneid
            },
            type: "GET",
            dataType: "json",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    var kpi = eval(data);
                    var div = gis_common.createKeyValueList(kpi);
                    gis_layout.createRightWidget("endtoend_loadSceneKpiDetail20Html", "场景指标明细", div);
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };


    this.loadTianYuanDemo = function () {
        var url = "/dynamicmap/tianyuandemo/kpi/20160501";
        loadWmsLayer(url);
    };
    /**
     * 指标高级分析
     * @param callback
     */
    this.loadkpiAdvancedAnalysisKpiInfo = function (callback) {
        if (kpisinfo == null) {
            $.ajax({
                url: "/loadadvancedanalysiskpi",
                type: "GET",
                dataType: "json",
                beforeSend: function () {
                    gis_common.isLoadingAddCount();
                },
                success: function (req) {
                    gis_common.outPut(req, function (data) {
                        kpisinfo = data;
                        var div = kpiAdvancedAnalysisTable();
                        callback(div);
                    });
                },
                complete: function () {
                    gis_common.isLoadingSunCount();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
                }
            });
        } else {
            kpiAdvancedAnalysisTable();
        }
    };
    /**
     * MRO指标渲染
     * @param callback
     */
    this.loadCellMrPointHtml = function (callback) {
        var content = "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
        content += "	<div id='res_type' class='col-sm-9'>";
        content += "			<input id='endtoend_res_type_select_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM'});\"/>";
        content += " 	</div > ";
        content += " </div > "
        /*content += "<div class='form-group'>";
         content += "	<label class='col-sm-3 control-label'>小区类型:</label>";
         content += "	<div id='endtoend_res_param_div_type' class='col-sm-9'>";
         content += "		<div class='checkbox pull-left margin-right-20'>";
         content += "			<label> <input id='endtoend_res_type_select_lte' type='radio' name='endtoend_res_type_select' restype = 'lte'> 4G </label>";
         content += "		</div>";
         content += "		<div class='checkbox pull-left margin-right-20'>";
         content += "			<label> <input id='endtoend_res_type_select_td' type='radio' name='endtoend_res_type_select' restype = 'td'> 3G </label>";
         content += "		</div>";
         content += "		<div class='checkbox pull-left margin-right-20'>";
         content += "			<label><input id='endtoend_res_type_select_gsm' type='radio' name='endtoend_res_type_select' restype = 'gsm'> 2G </label>";
         content += "		</div>";
         content += "	</div>";
         content += "</div>";*/

        content += "<div class='form-group'>";
        content += " 	<div class='col-sm-9 col-sm-offset-3'>";
        content += "		<button id='endtoend_res_btn_load' class='btn btn-primary'>查 询</button>";
        content += "		<button id='endtoend_res_btn_clear' class='btn btn-default btn-xs'>清理地图上的MRO点</button>";
        content += "	</div>";
        content += "</div>";

        var div = $(content);
        div.find("#endtoend_res_btn_load").bind("click", function (e) {
            RqCell.gsm = false;
            RqCell.td = false;
            RqCell.lte = false;
            //时间
            mro_time = div.find("#endtoend_res_type_select_time").val().replace('-', '');
            if (gis_common.stringIsNullOrWhiteSpace(mro_time)) {
                gis_common.showMessage("请选择时间");
                div.find("#endtoend_res_type_select_time").focus();
            }
            //渲染方式
            /*var type = $("#endtoend_res_param_div_type input[name='endtoend_res_type_select']:checked").attr("restype");
             if (type == "lte") {
             RqCell.lte = true;
             }
             else if (type == "td") {
             RqCell.td = true;
             }
             else if (type == "gsm") {
             RqCell.gsm = true;
             }*/
            RqCell.lte = true;
            loadCells();
        });
        div.find("#endtoend_res_btn_clear").bind("click", function () {
            mro_layer.clear();
            cell_layer.clear();
            RqCell.gsm = false;
            RqCell.td = false;
            RqCell.lte = false;
            mro_time = "";
            gis_common.queryRemoveFun("endToEnd_getmroltebtstopn");
            loadMroWmsLayer(null);
        });
        callback(div);
    };
    /**
     * 室内弱覆盖分析
     * @param callback
     */
    this.createQueryBuildParamHtml = function (callback) {
        var content = "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
        content += "	<div id='res_type' class='col-sm-9'>";
        content += "			<input id='endtoend_query_Build_param_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM'});\"/>";
        content += " 	</div > ";
        content += " </div > "

        content += " <div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>渲染指标:</label>";
        content += "	<div id='endtoend_query_Build_param_kpi' class='col-sm-9'>";
        content += "		<select id='endtoend_menu_Build_kpi_select' class='form-control'>";
        content += "			<option kpi='weak_rat'>4G覆盖</option>";
        content += "			<option kpi='lte_flow'>4G流量</option>";
        content += "			<option kpi='lte_user_cnt'>4G用户数</option>";
        content += "			<option kpi='gsm_flow'>2G流量</option>";
        content += "			<option kpi='gsm_user_cnt'>2G用户数</option>";
        content += "		</select>";
        content += "	</div>";
        content += "</div>"

        content += "<div class='form-group'>";
        content += " 	<div class='col-sm-9 col-sm-offset-3'>";
        content += "		<button id='endtoend_menu_Build_btn_load' class='btn btn-primary'>查 询</button>";
        content += "	</div>";
        content += "</div>";

        var div = $(content);
        div.find("#endtoend_menu_Build_btn_load").bind("click", function (e) {
            mro_layer.clear();
            time = div.find("#endtoend_query_Build_param_time").val().replace('-', '');
            if (gis_common.stringIsNullOrWhiteSpace(time)) {
                gis_common.showMessage("请选择时间");
                div.find("#endtoend_query_Build_param_time").focus();
            }
            //渲染指标
            var selects = $("#endtoend_menu_Build_kpi_select option:selected");
            if (selects != null && selects != undefined) {
                kpi = selects.attr("kpi");
            }
            if (gis_common.stringIsNullOrWhiteSpace(kpi)) {
                gis_common.showMessage("请选择时间");
                div.find("#endtoend_menu_Build_kpi_select").focus();
            }
            //渲染方式
            wmsLayer_type = "ltebuildcover";
            //渲染栅格
            initData();
        });
        //默认时间
        var tim = Date.getDateOfPreMonth(new Date()).format("yyyy-MM");
        div.find("#endtoend_query_Build_param_time").val(tim);
        callback(div)
    };
    /**
     * 栅格弱覆盖分析
     * @param callback
     */
    this.createQueryParamHtml = function (callback) {
        //var d = new Date().Format("yyyy-MM-dd");
        var content = "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
        content += "	<div id='res_type' class='col-sm-9'>";
        content += "			<input id='endtoend_query_param_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM'});\"/>";
        content += " 	</div > ";
        content += " </div > "

        content += " <div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>渲染指标:</label>";
        content += "	<div id='endtoend_query_param_kpi' class='col-sm-9'>";
        content += "		<select id='endtoend_menu_kpi_select' class='form-control'>";
        content += "			<option kpi='weak_rat'>4G覆盖</option>";
        content += "			<option kpi='lte_flow'>4G流量</option>";
        content += "			<option kpi='lte_user_cnt'>4G用户数</option>";
        content += "			<option kpi='gsm_flow'>2G流量</option>";
        content += "			<option kpi='gsm_user_cnt'>2G用户</option>";
        content += "		</select>";
        content += "	</div>";
        content += "</div>"

        content += "<div class='form-group'>";
        content += " 	<div class='col-sm-9 col-sm-offset-3'>";
        content += "		<button id='endtoend_menu_btn_load' class='btn btn-primary'>查 询</button>";
        content += "	</div>";
        content += "</div>";

        var div = $(content);
        div.find("#endtoend_menu_btn_load").bind("click", function (e) {
            mro_layer.clear();
            time = div.find("#endtoend_query_param_time").val().replace('-', '');
            if (gis_common.stringIsNullOrWhiteSpace(time)) {
                gis_common.showMessage("请选择时间");
                div.find("#endtoend_query_param_time").focus();
            }
            //地市
            var selects = $("#endtoend_query_param_kpi option:selected");
            if (selects != null && selects != undefined) {
                kpi = selects.attr("kpi");
            }
            if (gis_common.stringIsNullOrWhiteSpace(kpi)) {
                gis_common.showMessage("请选择时间");
                div.find("#endtoend_query_param_kpi").focus();
            }

            wmsLayer_type = "ltegridcover";
            //渲染栅格
            initData();
        });
        //默认时间
        var tim = Date.getDateOfPreMonth(new Date()).format("yyyy-MM");
        div.find("#endtoend_query_param_time").val(tim);
        callback(div)
    };
    /**
     * 连续弱覆盖 查询参数
     * @param callback
     */
    this.createSerialWeakGridHtml = function (callback) {
        gis_resources.getRegions(function (data) {
            var content = "<div class=\"form-group\">";
            content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
            content += "	<div id='res_type' class='col-sm-9'>";
            content += "			<input id='endtoend_Serial_Weak_param_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM'});\"/>";
            content += " 	</div > ";
            content += " </div > ";

            content += "<div class='form-group'>";
            content += "	<label for='' class='col-sm-3 control-label'>地市:</label>";
            content += " 	<div class='col-sm-9'>";
            content += "		<select id='endtoend_Serial_Weak_param_region_select' class='form-control'>";
            if (data != null && data != undefined && data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    var r = data[i];
                    content += "<option neid='" + r.id + "' netype='" + r.type + "'>" + r.name + "</option>";
                }
            } else {
                content += "<option neid='1' netype='9000'>全部</option>";
            }
            content += "		</select>";
            content += "	</div>";
            content += "</div>";

            content += "<div class='form-group'>";
            content += " 	<div class='col-sm-9 col-sm-offset-3'>";
            content += "		<button id='endtoend_Serial_Weak_param_btn_load' class='btn btn-primary'>确 定</button>";
            content += "	</div>";
            content += "</div>";

            var div = $(content);
            div.find("#endtoend_Serial_Weak_param_btn_load").bind("click", function (e) {
                mro_layer.clear();
                time = div.find("#endtoend_Serial_Weak_param_time").val().replace('-', '');
                if (gis_common.stringIsNullOrWhiteSpace(time)) {
                    gis_common.showMessage("请选择时间");
                    div.find("#endtoend_Serial_Weak_param_time").focus();
                    return;
                }
                //地市
                var selects = $("#endtoend_Serial_Weak_param_region_select option:selected");
                if (selects != null && selects != undefined) {
                    serialWeakGrid_region.id = selects.attr("neid");
                    serialWeakGrid_region.type = selects.attr("netype");
                } else {
                    gis_common.showMessage("请选择地市");
                    div.find("#endtoend_Serial_Weak_param_region_select").focus();
                    return;
                }
                wmsLayer_type = "serialweakgrid";
                loadSerialWeakGridData();
            });
            //默认时间
            var tim = Date.getDateOfPreMonth(new Date()).format("yyyy-MM");
            div.find("#endtoend_Serial_Weak_param_time").val(tim);
            callback(div);
        });
    }
    /**
     * 竞争对手覆盖
     * @param callback
     */
    this.createCompetitorHtml = function (callback) {
        var content = "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
        content += "	<div id='res_type' class='col-sm-9'>";
        content += "			<input id='endtoend_competitor_param_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM'});\"/>";
        content += " 	</div > ";
        content += " </div > ";

        content += " <div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>运营商:</label>";
        content += "	<div id='endtoend_competitor_param_kpi' class='col-sm-9'>";
        content += "		<select id='endtoend_competitor_kpi_select' class='form-control'>";
        content += "			<option kpi='weak_rat_cu'>中国联通</option>";
        content += "			<option kpi='weak_rat_189'>中国电信</option>";
        content += "		</select>";
        content += "	</div>";
        content += "</div>";

        content += " <div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>渲染指标:</label>";
        content += "	<div id='endtoend_competitor_param_kpi_typ' class='col-sm-9'>";
        content += "		<select id='endtoend_competitor_kpi_select_typ' class='form-control'>";
        content += "			<option kpi='-110'>CRS RSRP>=-110dBm覆盖率</option>";
        content += "			<option kpi='-113'>CRS RSRP>=-113dBm覆盖率</option>";
        content += "		</select>";
        content += "	</div>";
        content += "</div>";

        content += "<div class='form-group'>";
        content += "	<label for='' class='col-sm-3 control-label'>渲染方式:</label>";
        content += " 	<div class='col-sm-9 radio'>";
        content += "     <label class='checkbox-inline'><input type='radio' checked='checked' name='endtoend_competitor_sender_type' value='0' />栅格</label>";
        content += "     <label class='checkbox-inline'><input type='radio' name='endtoend_competitor_sender_type' value='1' />小区</label>";
        content += "	</div>";
        content += "</div>";

        content += "<div class='form-group'>";
        content += " 	<div class='col-sm-9 col-sm-offset-3'>";
        content += "		<button id='endtoend_competitor_btn_load' class='btn btn-primary'>查 询</button>";
        content += "	</div>";
        content += "</div>";

        var div = $(content);
        div.find("#endtoend_competitor_btn_load").bind("click", function () {
            mro_layer.clear();
            time = div.find("#endtoend_competitor_param_time").val().replace('-', '');
            if (gis_common.stringIsNullOrWhiteSpace(time)) {
                gis_common.showMessage("请选择时间");
                div.find("#endtoend_competitor_param_time").focus();
            }
            //运营商
            var selects = $("#endtoend_competitor_kpi_select option:selected");
            if (selects != null && selects != undefined) {
                kpi = selects.attr("kpi");
            }
            //渲染指标
            var kpi_typ = "-110";
            var kpi_typ_selects = $("#endtoend_competitor_param_kpi_typ option:selected");
            if (kpi_typ_selects != null && kpi_typ_selects != undefined) {
                kpi_typ = kpi_typ_selects.attr("kpi");
            }
            if (kpi_typ == "-113") {
                kpi = kpi + "_113"
            }

            //渲染方式
            var sender_typ = div.find("input[name='endtoend_competitor_sender_type']:checked").val();
            //添加对比功能
            if (gis_common.stringIsNullOrWhiteSpace(kpi)) {
                gis_common.showMessage("请选择运营商");
                div.find("#endtoend_competitor_kpi_select").focus();
            }

            cell_layer.clear();
            loadWmsLayer(null);
            wmsLayer_type = "competitorcover_" + sender_typ;
            if (sender_typ == "" || sender_typ == "0") {//栅格
                //渲染栅格
                initData();
            }
            else {//小区
                loadCompetitorCover();
            }

        });
        var dtNow = new Date();
        var tim = Date.getDateOfPreMonth(dtNow).format("yyyy-MM");
        div.find("#endtoend_competitor_param_time").val(tim);
        callback(div)
    };
    /**
     * 竞争对手黑点
     * @param callback
     */
    this.createCompetitorSpotsHtml = function (callback) {
        var content = "<div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>查询时间:</label>";
        content += "	<div id='res_type' class='col-sm-9'>";
        content += "			<input id='endtoend_competitor_spots_param_time' type='text' class='form-control' onFocus=\"WdatePicker({lang:'zh-cn',dateFmt:'yyyy-MM'});\"/>";
        content += " 	</div > ";
        content += " </div > ";

        content += " <div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>运营商:</label>";
        content += "	<div id='endtoend_competitor_spots_param_kpi' class='col-sm-9'>";
        content += "		<select id='endtoend_competitor_spots_kpi_select' class='form-control'>";
        content += "			<option kpi='spots_weak_rat_cu'>中国联通</option>";
        content += "			<option kpi='spots_weak_rat_189'>中国电信</option>";
        content += "		</select>";
        content += "	</div>";
        content += "</div>";

        content += " <div class=\"form-group\">";
        content += "	<label class='col-sm-3 control-label'>渲染指标:</label>";
        content += "	<div id='endtoend_competitor_spots_param_kpi_typ' class='col-sm-9'>";
        content += "		<select id='endtoend_competitor_spots_kpi_select_typ' class='form-control'>";
        content += "			<option kpi='-110'>CRS RSRP>=-110dBm覆盖率</option>";
        content += "			<option kpi='-113'>CRS RSRP>=-113dBm覆盖率</option>";
        content += "		</select>";
        content += "	</div>";
        content += "</div>";

        content += "<div class='form-group'>";
        content += "	<label for='' class='col-sm-3 control-label'>渲染方式:</label>";
        content += " 	<div class='col-sm-9 radio'>";
        content += "     <label class='checkbox-inline'><input type='radio' checked='checked' name='endtoend_competitor_spots_sender_type' value='0' />栅格</label>";
        content += "     <label class='checkbox-inline'><input type='radio' name='endtoend_competitor_spots_sender_type' value='1' />小区</label>";
        content += "	</div>";
        content += "</div>";

        content += "<div class='form-group'>";
        content += " 	<div class='col-sm-9 col-sm-offset-3'>";
        content += "		<button id='endtoend_competitor_spots_btn_load' class='btn btn-primary'>查 询</button>";
        content += "	</div>";
        content += "</div>";

        var div = $(content);
        div.find("#endtoend_competitor_spots_btn_load").bind("click", function () {
            mro_layer.clear();
            time = div.find("#endtoend_competitor_spots_param_time").val().replace('-', '');
            if (gis_common.stringIsNullOrWhiteSpace(time)) {
                gis_common.showMessage("请选择时间");
                div.find("#endtoend_competitor_spots_param_time").focus();
            }
            //运营商
            var selects = $("#endtoend_competitor_spots_kpi_select option:selected");
            if (selects != null && selects != undefined) {
                kpi = selects.attr("kpi");
            }
            //渲染指标
            var kpi_typ = "-110";
            var kpi_typ_selects = $("#endtoend_competitor_spots_kpi_select_typ option:selected");
            if (kpi_typ_selects != null && kpi_typ_selects != undefined) {
                kpi_typ = kpi_typ_selects.attr("kpi");
            }
            if (kpi_typ == "-113") {
                kpi = kpi + "_113"
            }

            //渲染方式
            var sender_typ = div.find("input[name='endtoend_competitor_spots_sender_type']:checked").val();
            //添加对比功能
            if (gis_common.stringIsNullOrWhiteSpace(kpi)) {
                gis_common.showMessage("请选择运营商");
                div.find("#endtoend_competitor_spots_kpi_select").focus();
            }

            cell_layer.clear();
            loadWmsLayer(null);
            wmsLayer_type = "competitorcoverspots_" + sender_typ;
            if (sender_typ == "" || sender_typ == "0") {//栅格
                //渲染栅格
                initData();
            }
            else {//小区
                loadCompetitorCover();
            }

        });
        var dtNow = new Date();
        var tim = Date.getDateOfPreMonth(dtNow).format("yyyy-MM");
        div.find("#endtoend_competitor_spots_param_time").val(tim);
        callback(div)
    };
    /**
     * 20*20 栅格弱覆盖服分析
     * @param callback
     */
    this.createQueryParamHtml20 = function (callback) {
        var div = createQueryParamHtml20();
        callback(div);
    };
    /**
     * 清理
     */
    this.clear = function () {
        gis_core.removeLayer("endtoendwmsLayer");
        gis_common.queryRemoveFun("endToEnd_getmroltebtstopn");
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();
        gis_layout.removeLegend();
        grid_high_layer.clear();
        cell_layer.clear();
        mro_layer.clear();
        gridCellRel_layer.clear();
    };


    /**
     * 初始化
     */
    this.init = function () {
        //加载库文件
        dojo.require("esri.layers.WMSLayerInfo");
        dojo.require("esri.layers.WMSLayer");
        gis_core.addExtentChange("EndToEnd", extentchange);
        if (dialog == null) {
            dialog = new dijit.TooltipDialog({
                id: "gridCellRel_tooltipDialog",
                style: "position: absolute; width: auto; font: normal normal normal 10pt Helvetica;z-index:100"
            });
            dialog.startup();
        }

        //注册事件
        if (map_mouse_move != null) {
            dojo.disconnect(map_mouse_move);
        }
        map_mouse_move = gis_core.map.on("mouse-move", function (delta) {
            if (wmsLayer_type != "ltebuildcover" && wmsLayer_type != "competitorcover_1" && wmsLayer_type != "competitorcoverspots_1") {
                if (wmsLayer_type == "20gridcellscene") {
                    createHighGrid(delta.mapPoint.x, delta.mapPoint.y, 2);
                }
                else {
                    createHighGrid(delta.mapPoint.x, delta.mapPoint.y, 5);
                    //createHigh50Grid(delta.mapPoint.x, delta.mapPoint.y);
                }
            }
        });

        if (map_mouse_out != null) {
            dojo.disconnect(map_mouse_out);
        }
        map_mouse_out = gis_core.map.on("mouse-out", function (delta) {
            grid_high_layer.clear();
        });

        gis_core.addMapClick("endtoend_map_click", function (evt) {
            if (wmsLayer_type == "ltebuildcover") {
                checkPointBuild(evt.mapPoint.x, evt.mapPoint.y);
            }
            else if (wmsLayer_type == "competitorcover_1" || wmsLayer_type == "competitorcoverspots_1") { //spots
            }
            else if (wmsLayer_type == "20gridcellscene") {
                loadGridKpiDetail20();
            }
            else {
                loadGridKpiDetail();
                getGridCellRes(evt.mapPoint.x, evt.mapPoint.y);
            }
        });

        if (cellLayer_click_handle != null) {
            dojo.disconnect(cellLayer_click_handle);
        }
        cellLayer_click_handle = cell_layer.on("click", function (evt) {
            is_cell_layer_click = true;
            var cell = evt.graphic.attributes.data;
            if (wmsLayer_type == "competitorcover_1" || wmsLayer_type == "competitorcoverspots_1") {
                loadCoverCellKpiDetail(cell.eci);
            }
            else {
                loadCellMroPoint(cell.eci);
            }
        });

        //添加 tooltip
        if (gridCellRelLayer_onMouseOver_handle != null) {
            dojo.disconnect(gridCellRelLayer_onMouseOver_handle);
        }
        gridCellRelLayer_onMouseOver_handle = gridCellRel_layer.on("mouse-over", tooltip);
        if (gridCellRelLayer_onMouseOut_handle != null) {
            dojo.disconnect(gridCellRelLayer_onMouseOut_handle);
        }
        gridCellRelLayer_onMouseOut_handle = gridCellRel_layer.on("mouse-out", closeDialog);

        //注册模块
        //gis_core.addPlugin(this);
        gis_core.addPlugin(endToEnd);
    };
    /**
     * 销毁
     */
    this.destroy = function () {
        gis_core.removeLayer("endtoendwmsLayer");
        gis_core.removeLayer("endtoEnd_cell_layer");
        gis_core.removeLayer("endtoend_mro_layer");
        gis_core.removeLayer("endtoendwmsLayer");
        gis_core.removeLayer("endtoend_gridCellRel_layer");

        gis_core.removeExtentChange("EndToEnd");
        gis_core.removeClick("endtoend_map_click");

        dojo.disconnect(map_mouse_move);
        dojo.disconnect(map_mouse_out);
        dojo.disconnect(map_click_handle);

        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();
        gis_layout.removeLegend();

        kpi = null;
        time = null;
        wmsLayer = null;
        delta_temp = null;
    };
    /**
     * 模块信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "00_01_00",
        name: "端到端分析"
    };


    this.initQuery_serialweakgrid = function (tim, id) {
        mro_layer.clear();
        wmsLayer_type = "serialweakgrid";
        grid_id = id;
        time = tim;

        $.ajax({
            url: "/getserialweakgridbyid",
            data: {
                id: grid_id,
                time: time
            },
            dataType: "json",
            async: true,
            type: "GET",
            beforeSend: function () {
                gis_common.isLoadingAddCount();
            },
            success: function (req) {
                gis_common.outPut(req, function (data) {
                    if (data != null && data != undefined) {
                        gis_common.setExtent(parseFloat(data.maxLongitude) + 0.0005, parseFloat(data.maxLatitude) + 0.0005, parseFloat(data.minLongitude) - 0.0005, parseFloat(data.minLatitude) - 0.0005);
                        var url = "/dynamicmap/serialweakgrid/weak_rat/" + time + "_" + id;
                        loadWmsLayer(url);
                        loadLegend("weak_rat");
                    }
                    else {
                        gis_common.showMessage("无法查询连续弱覆盖区域");
                    }
                });
            },
            complete: function () {
                gis_common.isLoadingSunCount();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                gis_common.showError(XMLHttpRequest.status + " " + XMLHttpRequest.readyState + " " + textStatus);
            }
        });
    };
    this.initQuery_ltegridcover = function (tim, lat, lon) {
        gis_common.setPoint(lon, lat);
        time = tim;
        wmsLayer_type = "ltegridcover";
        kpi = "weak_rat";
        //渲染栅格
        initData();
    };
}

//功能类 全局类
var endToEnd = null;
//初始LTE覆盖化
function initEndToEnd() {
    if (endToEnd == null) {
        endToEnd = new EndToEnd();
    }
    endToEnd.init();
}
//菜单 查询条件 弱覆盖栅格分析
function endToEnd_createQueryParamHtml(obj) {
    initEndToEnd();
    gis_core.pluginDestroyExceptId(endToEnd.title.id);
    endToEnd.clear();
    gis_layout.createMenubarWithFun("endToEnd_createQueryParamHtml", obj, "弱覆盖栅格分析", endToEnd.createQueryParamHtml)
}
//菜单 查询条件 弱覆盖区域分析
function endToEnd_createQueryBuildParamHtml(obj) {
    initEndToEnd();
    gis_core.pluginDestroyExceptId(endToEnd.title.id);
    endToEnd.clear();
    gis_layout.createMenubarWithFun("endToEnd_createQueryBuildParamHtml", obj, "室内网络质量分析", endToEnd.createQueryBuildParamHtml)
}
//菜单 连续弱覆盖
function endToEnd_createSerialWeakGridHtml(obj) {
    initEndToEnd();
    gis_core.pluginDestroyExceptId(endToEnd.title.id);
    endToEnd.clear();
    gis_layout.createMenubarWithFun("endToEnd_createSerialWeakGridHtml", obj, "连续弱覆盖", endToEnd.createSerialWeakGridHtml);
}
// 菜单 竞争对手覆盖
function endToEnd_createCompetitorHtml(obj) {
    initEndToEnd();
    gis_core.pluginDestroyExceptId(endToEnd.title.id);
    endToEnd.clear();
    gis_layout.createMenubarWithFun("endToEnd_createCompetitorHtml", obj, "竞争对手覆盖", endToEnd.createCompetitorHtml);
}
// 菜单 竞争对手覆盖
function endToEnd_createCompetitorSpotsHtml(obj) {
    initEndToEnd();
    gis_core.pluginDestroyExceptId(endToEnd.title.id);
    endToEnd.clear();
    gis_layout.createMenubarWithFun("endToEnd_createCompetitorSpotsHtml", obj, "竞争对手黑店覆盖", endToEnd.createCompetitorSpotsHtml);
}
//菜单  栅格渲染  20*20 规格
function endToEnd_createQueryParamHtml20(obj) {
    initEndToEnd();
    gis_core.pluginDestroyExceptId(endToEnd.title.id);
    endToEnd.clear();
    gis_layout.createMenubarWithFun("endToEnd_createQueryParamHtml20", obj, "20*20栅格分析", endToEnd.createQueryParamHtml20);
}
// 工具  高级指标分析
function endToEnd_createTootbar_advancedAnalysis(obj) {
    initEndToEnd();
    gis_layout.createToolbarWithFun("endtoend_advanced", obj, 420, endToEnd.loadkpiAdvancedAnalysisKpiInfo);
}
// 工具 查询小区下的MR点
function endToEnd_createTootbar_cells(obj) {
    initEndToEnd();
    gis_layout.createToolbarWithFun("endtoend_res_mrpoint", obj, 320, endToEnd.loadCellMrPointHtml);
    //gis_layout.createMenubarWithFun("endtoend_res_mrpoint", obj, "MRO分布", endToEnd.loadCellMrPointHtml);
}

function endtoend_init_query() {
    initEndToEnd();
    var typ = gis_common.getQueryString("wmstyp");
    if (typ == "" || typ == undefined) {
        return;
    }
    if (typ == "serialweakgrid") {
        var tim = gis_common.getQueryString("time");
        var id = gis_common.getQueryString("id");
        endToEnd.initQuery_serialweakgrid(tim, id);
    }
    else if (typ == "ltegridcover") {
        var tim = gis_common.getQueryString("time");
        var lat = parseFloat(gis_common.getQueryString("lat"));
        var lon = parseFloat(gis_common.getQueryString("lon"));
        endToEnd.initQuery_ltegridcover(tim, lat, lon);
    }
}

function endToEnd_createTootbar_tianyuandemo(obj) {
    initEndToEnd();
    gis_layout.createMenubar("endToEnd_createTootbar_tianyuandemo", obj, endToEnd.loadTianYuanDemo);
}

