package bean.enums;

/**
 * Created by Xiaobing on 2016/5/26.
 */
public enum TimeType {
    /**
     * 无
     */None,
    /**
     * 小时
     */Hour,
    /**
     * 天
     */Day,
    /**
     * 周
     */Week,
    /**
     * 月
     */Mouth,
    /**
     * 年
     */Year
}
