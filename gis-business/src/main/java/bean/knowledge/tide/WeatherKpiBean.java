package bean.knowledge.tide;

/**
 * Created by xiexb on 2017/8/17.
 */
public class WeatherKpiBean {

    private String kpiId;
    private String kpiName;
    private String kpiUnit;
    private String kpiValue;

    public WeatherKpiBean() {
    }

    public WeatherKpiBean(String kpiId, String kpiName, String kpiUnit, String kpiValue) {
        this.kpiId = kpiId;
        this.kpiName = kpiName;
        this.kpiUnit = kpiUnit;
        this.kpiValue = kpiValue;
    }

    public String getKpiId() {
        return kpiId;
    }

    public void setKpiId(String kpiId) {
        this.kpiId = kpiId;
    }

    public String getKpiName() {
        return kpiName;
    }

    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public String getKpiUnit() {
        return kpiUnit;
    }

    public void setKpiUnit(String kpiUnit) {
        this.kpiUnit = kpiUnit;
    }

    public String getKpiValue() {
        return kpiValue;
    }

    public void setKpiValue(String kpiValue) {
        this.kpiValue = kpiValue;
    }
}