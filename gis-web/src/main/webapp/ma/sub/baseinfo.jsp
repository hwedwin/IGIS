<%--
  Created by IntelliJ IDEA.
  User: Xiaobing
  Date: 2017/2/21
  Time: 15:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">
<html>
<head>
    <title>基础信息配置</title>
    <%@ include file="../common/baseref.jsp" %>
    <style type="text/css">
        a:hover, a:focus {
            outline: none;
            text-decoration: none;
        }

        #accordion .panel {
            box-shadow: none;
            border: none;
            border-radius: 0;
            margin-bottom: 6px;
            /*border-left: 5px solid #944f95;*/
        }

        #accordion .panel-heading {
            padding: 0;
            background-color: #fff;
        }

        #accordion .panel-title a {
            display: block;
            color: #333;
            font-size: 15px;
            font-weight: bold;
            padding: 14px 40px;
            background: #f5f5f5;
            text-transform: uppercase;
            border-bottom: 1px solid #d3d3d3;
            position: relative;
        }

        #accordion .panel-title a.collapsed {
            border-top: 0;
            color: #333;
            background: #eee;
            border-bottom: 1px solid transparent;
        }

        #accordion .panel-title a:before,
        #accordion .panel-title a.collapsed:before {
            content: "\f068";
            font-family: FontAwesome;
            position: absolute;
            top: 10px;
            left: 21px;
            font-size: 12px;
            line-height: 24px;
            color: #868686;
        }

        #accordion .panel-title a.collapsed:before {
            content: "\f067";
        }

        #accordion .panel-body {
            color: #666464;
            font-size: 14px;
            line-height: 20px;
            border-top: 0 none;
            background: #eee;
            padding: 15px 27px;
        }
    </style>

    <script type="text/javascript">

        function init() {
            $("#div-radio-map-type-list  input[type='radio']").bind("click", function () {
                if (this.checked && this.value == "arcgis") {
                    $("#div-input-arcgis-url").show();
                }
                else {
                    $("#div-input-arcgis-url").hide();
                }
            });
        }

        $(function () {

            init();
        })

    </script>
</head>
<body>
<jsp:include page="../common/header.jsp"/>

<div id="page-wrapper" class="container-fluid" header-menu="baseinfo.jsp">

    <div class="header">
        <h3 class="page-header">
            基础信息管理
            <small>Basic Information</small>
        </h3>
    </div>

    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a class="" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                       aria-expanded="true" aria-controls="collapseOne">
                        基础配置
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="input-menu-title" class="col-sm-2 control-label">导航页标题：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="input-menu-title"
                                       placeholder="iGIS地理信息系统" data-toggle="tooltip"
                                       title="定制化导航页标题 默认值为：iGIS地理信息系统">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">保 存</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                       href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        地图配置
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group" id="div-radio-map-type-list">
                            <label class="col-sm-2 control-label">地图类型：</label>

                            <div class="col-sm-6">
                                <label class="radio-inline">
                                    <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="tiandi">
                                    天地图
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="gaode">
                                    高德
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="inlineRadioOptions" id="inlineRadio4" value="google">
                                    谷歌
                                </label>
                                <label class="radio-inline" data-toggle="tooltip" data-placement="bottom"
                                       title="勾选此项后，必须填写ArcGIS地图服务地址。">
                                    <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="arcgis"
                                           checked="checked">ArcGIS地图服务
                                </label>
                            </div>
                        </div>

                        <div class="form-group" id="div-input-arcgis-url">
                            <label for="input-arcgis-url" class="col-sm-2 control-label">ArcGIS地图服务地址：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="input-arcgis-url" placeholder=""
                                       data-toggle="tooltip" data-placement="bottom" title="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox col-sm-offset-2 col-sm-10">
                                <label data-toggle="tooltip" title="勾选此项后，服务端将请求地图服务地址，将获取到的资源提供给客户端使用，适用于内网ArcGIS服务。">
                                    <input id="checkbox-arcgis-istransfer" type="checkbox" value=""
                                           checked="checked">
                                    是否在服务器端进行转发
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">保 存</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                       href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        缓存设置
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="input-menu-title" class="col-sm-2 control-label">缓存时间(小时)：</label>

                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="input-arcgis-cachetime" placeholder=""
                                       value="12"
                                       data-toggle="tooltip" title="设置服务器缓存时间,默认时长为12小时,如果不需要缓存请设置成0">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary" title="保存缓存时间">保 存</button>
                                <button type="submit" class="btn btn-warning" data-toggle="tooltip"
                                        data-placement="bottom" title="清理服务端数据缓存，清理完成后部分应用加载速度会变慢，请谨慎操作！">清理服务端缓存
                                </button>
                            </div>
                        </div>
                        <%--<div class="form-group">
                            <label for="input-menu-title" class="col-sm-2 control-label">清理缓存：</label>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary">清 理</button>
                            </div>
                        </div>--%>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
</body>
</html>
