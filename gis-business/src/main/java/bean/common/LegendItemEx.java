package bean.common;

/**
 * Created by Xiaobing on 2016/9/6.
 */
public class LegendItemEx extends LegendItem {
    /**
     * 显示名称
     */
    private String displayName;

    /**
     * 显示名称
     * @return
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * 显示名称
     * @param displayName
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
