package bean.bd;

import gis.core.bean.ResValue;

/**
 * Created by chengzhong on 2016-04-04.
 */
public class SchoolInfo extends ResValue {
    private String uid;
    private String area;
    private String name;
    private String address;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
