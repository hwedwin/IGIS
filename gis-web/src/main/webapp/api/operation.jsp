<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
    <%@ include file="header.jsp" %>
    <section>
        <div class="container">
            <%@ include file="leftmenu.jsp" %>
            <input id="pageid" type="hidden" value="operation">

            <div class="docs-content" style="height: 2250px;">
                <h3 class="welcome">运算算法</h3>

                <p>gis-algorithm.jar封装了大量算法，这些算法包括了基本的点线面运算、坐标系运算和图像渲染算法。由于算法过多，在此不一一介绍，本章节主要介绍常用运算算法。</p>

                <h3 class="welcome">gis.algorithm.GeoPie</h3>

                <p>小区扇形区域边界计算，返回边界数据点集合</p>
                    <pre class="prettyprint">
    /**
     * @param centerPoint 圆心坐标
     * @param radio 覆盖半径
     * @param startAngle 开始角（正北0°）
     * @param sweepAngle 接受角（正北0°）
     * @param wkid
     */

     Cell cell = cellList.get(i);
     Double y0 = cell.getLatitude();
     Double x0 = cell.getLongitude();
     GeoPoint centerPoint = new GeoPoint();
     centerPoint.setX(x0);
     centerPoint.setY(y0);
     double radio = r * 100000;
     double startAngle = i * angle;
     double sweepAngle = (i + 1) * angle;
     int wkid = 4326;

     GeoPie pie = new GeoPie(centerPoint, radio, startAngle, sweepAngle, wkid);
     List<GeoPoint> points = pie.getRingPoints();
                        Extent ex = pie.getRingExtent();
                    </pre>

                <h3 class="welcome">gis.algorithm.GeoUtils</h3>

                <p>点面运算工具类，提供点-面位置关系，面-面位置关系等运算。</p>
                    <pre class="prettyprint">
    /**
     * @param centerPoint 圆心坐标
     * @param radio 覆盖半径
     * @param startAngle 开始角（正北0°）
     * @param sweepAngle 接受角（正北0°）
     * @param wkid
     */

     Cell cell = cellList.get(i);
     Double y0 = cell.getLatitude();
     Double x0 = cell.getLongitude();
     GeoPoint centerPoint = new GeoPoint();
     centerPoint.setX(x0);
     centerPoint.setY(y0);
     double radio = r * 100000;
     double startAngle = i * angle;
     double sweepAngle = (i + 1) * angle;
     int wkid = 4326;

     GeoPie pie = new GeoPie(centerPoint, radio, startAngle, sweepAngle, wkid);
     List<GeoPoint> points = pie.getRingPoints();
                        Extent ex = pie.getRingExtent();
                        /**
                        * 地理坐标系(4326)转墨卡托投影(102100)
                        *
                        * @param lon
                        * @param lat
                        * @return
                        */
                        GeoPoint p1 = GeoUtils.lonLatToMercator(lon, lat);

                        /**
                        * 判断两个矩形是否有交集
                        *
                        * @param extent1
                        * @param extent2
                        * @return
                        */
                        boolean isNotIn = GeoUtils.extentIsNotInExtent(extent1, extent2);

                        /**
                        * 判断点是否在凹凸多边形内
                        *
                        * @param points
                        * @param x
                        * @param y
                        * @return
                        */
                        boolean isNotIn = isPointInPolygon(points, x, y);

                        ......算法很多，多研究
                    </pre>

                <h3 class="welcome">gis.algorithm.PointTransform</h3>

                <p>坐标系转换 完成火星坐标、GPS（WGS-84）、百度坐标、墨卡托坐标之间的转换</p>

                <h3 class="welcome">gis.algorithm.cluster</h3>

                <p>聚合算法，对大数据量进行聚合运算，返回少量数据集</p>
                    <pre class="prettyprint">
        RqCluster rqCluster = ...
        List&lt;ClusterArg&lt;Cell&gt;&gt; clusterArgs = new ArrayList&lt;ClusterArg&lt;Cell&gt;&gt;();
        for (Cell b : cells) {
            ClusterArg&lt;Cell&gt; arg = new ClusterArg&lt;Cell&gt;();
            arg.setX(b.getLongitude());
            arg.setY(b.getLatitude());
            arg.setObj(b);
            clusterArgs.add(arg);
        }

        ClusterCalculator c = ClusterCalculator.getInstance();
        List&lt;ClusterPoint&lt;Cell&gt;&gt; result = c.calc(clusterArgs, rqCluster);
                    </pre>
                <a href="/api/commresapi.jsp" type="button" class="btn-link">上一页</a>
                <a href="/api/rendering.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>

            </div>
        </div>
    </section>
</div>
</div>
</body>
</html>
