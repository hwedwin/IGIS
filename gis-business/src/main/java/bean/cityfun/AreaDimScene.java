package bean.cityfun;

/**
 * Created by hupeng on 16/5/18.
 * 用户圈定的场景维度
 */
public class AreaDimScene {

    /**
     * 场景标识
     */
    private String scene_id;
    /**
     * 场景名称
     */
    private String scene_name;
    /**
     * 场景类型标识
     */
    private String scene_type_id;
    /**
     * 场景类型名称
     */
    private String scene_type_name;
    /**
     * 场景边界
     */
    private String range;


    public String getScene_id() {
        return scene_id;
    }

    public void setScene_id(String scene_id) {
        this.scene_id = scene_id;
    }

    public String getScene_name() {
        return scene_name;
    }

    public void setScene_name(String scene_name) {
        this.scene_name = scene_name;
    }

    public String getScene_type_id() {
        return scene_type_id;
    }

    public void setScene_type_id(String scene_type_id) {
        this.scene_type_id = scene_type_id;
    }

    public String getScene_type_name() {
        return scene_type_name;
    }

    public void setScene_type_name(String scene_type_name) {
        this.scene_type_name = scene_type_name;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

}