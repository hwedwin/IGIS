package gis.web.demo;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller("demoClass")
public class DemoClass {

	@RequestMapping("/demo")
	public String Demo() {
		return "/demo/index.jsp";
	}
	/**
     * @param request
     * @return
     */
    @RequestMapping({"/demo01"})
    public String demo1(HttpServletRequest request) {
        return "/demo/demo01.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo02"})
    public String demo2(HttpServletRequest request) {
        return "/demo/demo02.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo03"})
    public String demo3(HttpServletRequest request) {
        return "/demo/demo03.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo04"})
    public String demo4(HttpServletRequest request) {
        return "/demo/demo04.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo05"})
    public String demo5(HttpServletRequest request) {
        return "/demo/demo05.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo06"})
    public String demo6(HttpServletRequest request) {
        return "/demo/demo06.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo07"})
    public String demo7(HttpServletRequest request) {
        return "/demo/demo07.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo08"})
    public String demo8(HttpServletRequest request) {
        return "/demo/demo08.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo09"})
    public String demo9(HttpServletRequest request) {
        return "/demo/demo09.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo10"})
    public String demo10(HttpServletRequest request) {
        return "/demo/demo10.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo11"})
    public String demo11(HttpServletRequest request) {
        return "/demo/demo11.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo12"})
    public String demo12(HttpServletRequest request) {
        return "/demo/demo12.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo13"})
    public String demo13(HttpServletRequest request) {
        return "/demo/demo13.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo14"})
    public String demo14(HttpServletRequest request) {
        return "/demo/demo14.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo15"})
    public String demo15(HttpServletRequest request) {
        return "/demo/demo15.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo16"})
    public String demo16(HttpServletRequest request) {
        return "/demo/demo16.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo17"})
    public String demo17(HttpServletRequest request) {
        return "/demo/demo17.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo18"})
    public String demo18(HttpServletRequest request) {
        return "/demo/demo18.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo19"})
    public String demo19(HttpServletRequest request) {
        return "/demo/demo19.jsp";
    }

    /**
     * @param request
     * @return
     */
    @RequestMapping({"/demo20"})
    public String demo20(HttpServletRequest request) {
        return "/demo/demo20.jsp";
    }
}
