package bean.common;

/**
 * Created by Xiaobing on 2016/6/8.
 */
public class ToolBar {
    private String fun;
    private String name;

    public String getFun() {
        return fun;
    }

    public void setFun(String fun) {
        this.fun = fun;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
