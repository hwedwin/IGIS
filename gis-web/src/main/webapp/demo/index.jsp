<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <%@ include file="reference.jsp" %>
    <title></title>
</head>
<body>
<div class="navigate">
    <div>
        <a href="<%=root%>demo01" target="_blank">Demo01</a><label>ArcGisOfficialStreets:102100</label>
    </div>
    <div>
        <a href="<%=root%>demo02" target="_blank">Demo02</a><label>ArcGisOpenStreetMapLayer:102100</label>
    </div>
    <div>
        <a href="<%=root%>demo03" target="_blank">Demo03</a><label>TianDiTuImgLayer,TianDiTuCvaLayer:4326</label>
    </div>
    <div>
        <a href="<%=root%>demo04" target="_blank">Demo04</a><label>ArcGisOpenStreetMapLayer:102100(聚合算法)</label>
    </div>
    <div>
        <a href="<%=root%>demo05" target="_blank">Demo05</a><label>TianDiTuVecLayer,TianDiTuCvaLayer:4326(聚合算法)</label>
    </div>
    <div>
        <a href="<%=root%>demo06" target="_blank">Demo06</a><label>ArcGisOfficialStreets:102100(指标渲染图)</label>
    </div>
    <div>
        <a href="<%=root%>demo07" target="_blank">Demo07</a><label>TianDiTuVecLayer,TianDiTuCvaLayer:4326(指标渲染图)</label>
    </div>
    <div>
        <a href="<%=root%>demo08" target="_blank">Demo08</a><label>ArcGisOpenStreetMapLayer:102100(高级指标渲染图)</label>
    </div>
    <div>
        <a href="<%=root%>demo09"
           target="_blank">Demo09</a><label>TianDiTuVecLayer,TianDiTuCvaLayer:4326(高级指标渲染图)</label>
    </div>
    <div>
        <a href="<%=root%>demo10" target="_blank">Demo10</a><label></label>
    </div>
    <div>
        <a href="<%=root%>demo11" target="_blank">Demo11</a><label>TianDiTuVecLayer,TianDiTuCvaLayer:4326(泰森多边形)</label>
    </div>
    <div>
        <a href="<%=root%>demo12" target="_blank">Demo12</a><label></label>
    </div>
    <div>
        <a href="<%=root%>demo13" target="_blank">Demo13</a><label></label>
    </div>
    <div>
        <a href="<%=root%>demo14" target="_blank">Demo14</a><label></label>
    </div>
    <div>
        <a href="<%=root%>demo15" target="_blank">Demo15</a><label></label>
    </div>
    <div>
        <a href="<%=root%>demo16" target="_blank">Demo16</a><label></label>
    </div>
    <div>
        <a href="<%=root%>demo17" target="_blank">Demo17</a><label></label>
    </div>
    <div>
        <a href="<%=root%>demo18" target="_blank">Demo18</a><label></label>
    </div>
    <div>
        <a href="<%=root%>demo19" target="_blank">Demo19</a><label></label>
    </div>
    <div>
        <a href="<%=root%>demo20" target="_blank">Demo20</a><label>重庆移动ArcGIS集成示例</label>
    </div>
</div>
</body>
</html>