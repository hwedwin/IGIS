package bean.bd;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import gis.core.bean.KeyValue;

/**
 * 建议线路
 * 
 * @author Xiaobing
 *
 */
public class AdviseWayLine extends KeyValue {
	private String startPlace;
	private String endPlace;
	private double charging;
	private double distance;
	private List<ExpressWayLine> expressWayLines;
	
	/**
	 * 开始地点
	 * @return
	 */
	public String getStartPlace() {
		return startPlace;
	}
	/**
	 * 开始地点
	 * @param startPlace
	 */
	public void setStartPlace(String startPlace) {
		this.startPlace = startPlace;
	}
	/**
	 * 结束地点
	 * @return
	 */
	public String getEndPlace() {
		return endPlace;
	}
	/**
	 * 结束地点
	 * @param endPlace
	 */
	public void setEndPlace(String endPlace) {
		this.endPlace = endPlace;
	}
	/**
	 * 费用
	 * @return
	 */
	public double getCharging() {
		return charging;
	}
	/**
	 * 费用
	 * @param charging
	 */
	public void setCharging(double charging) {
		this.charging = charging;
	}
	/**
	 * 距离
	 * @return
	 */
	public double getDistance() {
		return distance;
	}
	/**
	 * 距离
	 * @param distance
	 */
	public void setDistance(double distance) {
		this.distance = distance;
	}
	/**
	 * 高速公路
	 * @return
	 */
	public List<ExpressWayLine> getExpressWayLines() {
		return expressWayLines;
	}
	/**
	 * 高速公路
	 * @param expressWayLines
	 */
	public void setExpressWayLines(List<ExpressWayLine> expressWayLines) {
		this.expressWayLines = expressWayLines;
	}
	
	@Override
	public AdviseWayLine clone() {
		// TODO Auto-generated method stub
		AdviseWayLine adviseWayLine = new AdviseWayLine();
		adviseWayLine.setId(UUID.randomUUID().toString());
		adviseWayLine.setName(getName());
		adviseWayLine.setTag(getTag());
		adviseWayLine.setStartPlace(getStartPlace());
		adviseWayLine.setEndPlace(getEndPlace());
		adviseWayLine.setCharging(getCharging());
		adviseWayLine.setDistance(getDistance());
		adviseWayLine.setExpressWayLines(new ArrayList<ExpressWayLine>());
		for (ExpressWayLine expressWayLine : getExpressWayLines()) {
			adviseWayLine.getExpressWayLines().add(expressWayLine);			
		}
		return adviseWayLine;
	}
	
	
}
