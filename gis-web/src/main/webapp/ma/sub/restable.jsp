<%--
  Created by IntelliJ IDEA.
  User: Xiaobing
  Date: 2017/6/14
  Time: 10:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">
<html>
<head>
    <title>资源配置</title>
    <%@ include file="../common/baseref.jsp" %>
</head>
<body>
<jsp:include page="../common/header.jsp"/>

<div id="page-wrapper" header-menu="restable.jsp">
    <div class="header">
        <h3 class="page-header">
            资源配置管理
            <small>Resource Management</small>
        </h3>
    </div>
    <div id="page-inner">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="databoard">
                    <div class="panel panel-primary">
                        <div>标题：静态资源展示静态资源展示</div>
                        <div>状态：已启用</div>
                        <div>创建时间：2017-06-20</div>
                        <div class="icon">
                            <i class="fa fa-eye fa-5x red"></i>
                        </div>
                        <div>说明：高倒流小区,高话务量小区等等</div>
                        <div>删除</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="databoard">
                    <div class="panel panel-primary">
                        <div>标题：静态资源展示静态资源展示</div>
                        <div>状态：已启用</div>
                        <div>创建时间：2017-06-20</div>
                        <div class="icon">
                            <i class="fa fa-eye fa-5x red"></i>
                        </div>
                        <div>说明：高倒流小区,高话务量小区等等</div>
                        <div>删除</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="databoard">
                    <div class="panel panel-primary">
                        <div>标题：静态资源展示静态资源展示</div>
                        <div>状态：已启用</div>
                        <div>创建时间：2017-06-20</div>
                        <div class="icon">
                            <i class="fa fa-eye fa-5x red"></i>
                        </div>
                        <div>说明：高倒流小区,高话务量小区等等</div>
                        <div>删除</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="databoard">
                    <div class="panel panel-primary">
                        <div>标题：静态资源展示静态资源展示</div>
                        <div>状态：已启用</div>
                        <div>创建时间：2017-06-20</div>
                        <div class="icon">
                            <i class="fa fa-eye fa-5x red"></i>
                        </div>
                        <div>说明：高倒流小区,高话务量小区等等</div>
                        <div>删除</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="databoard">
                    <div class="panel panel-primary">
                        <div>标题：静态资源展示静态资源展示</div>
                        <div>状态：已启用</div>
                        <div>创建时间：2017-06-20</div>
                        <div class="icon">
                            <i class="fa fa-eye fa-5x red"></i>
                        </div>
                        <div>说明：高倒流小区,高话务量小区等等</div>
                        <div>删除</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="databoard">
                    <div class="panel panel-primary">
                        <div>标题：静态资源展示静态资源展示</div>
                        <div>状态：已启用</div>
                        <div>创建时间：2017-06-20</div>
                        <div class="icon">
                            <i class="fa fa-eye fa-5x red"></i>
                        </div>
                        <div>说明：高倒流小区,高话务量小区等等</div>
                        <div>删除</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="databoard">
                    <div class="panel panel-primary">
                        <div>标题：静态资源展示静态资源展示</div>
                        <div>状态：已启用</div>
                        <div>创建时间：2017-06-20</div>
                        <div class="icon">
                            <i class="fa fa-eye fa-5x red"></i>
                        </div>
                        <div>说明：高倒流小区,高话务量小区等等</div>
                        <div>删除</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="databoard">
                <div class="panel panel-primary">
                    <a href="resdetail.jsp" class="btn btn-primary" title="新建资源">新建资源</a>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>

</body>
</html>
