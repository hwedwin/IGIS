package gis.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.poi.PoiCategory;
import bean.poi.PoiInfo;
import gis.common.GisConfig;
import gis.common.ServiceFactory;
import gis.core.utils.CollUtils;
import gis.service.PoiService;
import org.omg.CORBA.PUBLIC_MEMBER;

public class PoiDao {

    /**
     * 获取当前可视范围内的poi点
     *
     * @param xmax
     * @param ymax
     * @param xmin
     * @param ymin
     * @param level 地图层级
     * @return
     * @throws SQLException
     */
    public List<PoiInfo> GetPois(double xmax, double ymax, double xmin, double ymin, int level) throws SQLException {
        GisConfig c = GisConfig.GetInstance();
        List<PoiInfo> list = new ArrayList<PoiInfo>();
        PoiService service = ServiceFactory.getInstance(PoiService.class);
        List<PoiInfo> temp = service.GetPois();
        for (PoiInfo poi : temp) {
            if (poi.getLongitude() >= xmax || poi.getLongitude() <= xmin || poi.getLatitude() >= ymax || poi.getLatitude() <= ymin) {
                continue;
            }
            poi.setShow(true);
            list.add(poi);
        }
        List<PoiInfo> tempInfos = null;
        // 最后2级全部显示
        if (c.getMapmaxlevel() == level || c.getMapmaxlevel() == level + 1 || c.getMapmaxlevel() == level + 2) {
            tempInfos = list;
        } else {
            tempInfos = CollUtils.Spares(list, 50);
        }

        for (PoiInfo poi : tempInfos) {
            // 乡、镇级地名 村级地名 市区地名 省地二级政府 区县级行政机关 地级行政机关
            char[] ids = poi.getId().toCharArray();
            int id_f = (int) ids[0] * 10 + (int) ids[1];

            if (poi.getGroupID() == "FF0" || poi.getGroupID() == "FF1" || poi.getGroupID() == "F31" || poi.getGroupID() == "CE0" || poi.getGroupID() == "CE1" || poi.getGroupID() == "CE2"
                    || id_f % 50 == 0) {
                poi.setShow(true);
            } else {
                poi.setShow(false);
            }
        }
        return tempInfos;
    }

    /**
     * @param topN
     * @param key
     * @return
     * @throws SQLException
     */
    public List<PoiInfo> GetPoisTop(int topN, String key) throws SQLException {
        PoiService service = ServiceFactory.getInstance(PoiService.class);
        List<PoiInfo> temp = service.GetPois();
        return CollUtils.topN(temp, "query", key, topN);
    }

    /**
     * 获取poi 分类列表
     * @return poi 分类列表
     * */
    public List<PoiCategory> GetPoiCategory() throws Exception {
        PoiService service = ServiceFactory.getInstance(PoiService.class);
        List<PoiCategory> categoryList = service.GetPoiCategory();
        return categoryList;
    }
}
