package gis.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import bean.poi.PoiCategory;
import gis.common.ServiceFactory;
import gis.dao.PoiDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import bean.poi.PoiInfo;

@Controller("poi")
public class Poi {

	private static Logger logger = LoggerFactory.getLogger(ServiceFactory.class);

	@RequestMapping(value = "/getpois", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<PoiInfo> GetPois(HttpServletRequest request) {

		List<PoiInfo> list = new ArrayList<>();
		try {
			double xmax = Double.parseDouble(request.getParameter("xmax"));
			double ymax = Double.parseDouble(request.getParameter("ymax"));
			double xmin = Double.parseDouble(request.getParameter("xmin"));
			double ymin = Double.parseDouble(request.getParameter("ymin"));
			int level = Integer.parseInt(request.getParameter("level"));

			list = new PoiDao().GetPois(xmax, ymax, xmin, ymin, level);

		}
		catch (Exception e) {
			logger.error("获取当前可视范围内的POI信息出错:" + e.getMessage(), e);
		}
		return list;
	}

	@RequestMapping(value = "/getpoistopn", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<PoiInfo> GetPoisTopN(HttpServletRequest request) {
		List<PoiInfo> list = new ArrayList<>();
		try {
			String key = request.getParameter("key");
			int topn = Integer.parseInt(request.getParameter("topn"));
			list = new PoiDao().GetPoisTop(topn, key);
		}
		catch (Exception e) {
			logger.error("获取GetPoisTopN出差:" + e.getMessage(), e);
		}
		return list;
	}

	@RequestMapping(value = "/getpoicategory", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<PoiCategory> GetPoiCategory(HttpServletRequest request) {
		List<PoiCategory> list = new ArrayList<>();
		try {
			list = new PoiDao().GetPoiCategory();
		}
		catch (Exception e) {
			logger.error("获取GetPoisTopN出差:" + e.getMessage(), e);
		}
		return list;
	}
}
