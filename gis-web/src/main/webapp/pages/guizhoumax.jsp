<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title></title><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">

  <%String root_gisapp = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>
  <%String root_arcgis = request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>

  <title>GIS</title>
  <link rel="stylesheet" href="/script/arcgis_js_api/library/3.9/3.9/js/dojo/dijit/themes/dijit.css">
  <link rel="stylesheet" href="/script/arcgis_js_api/library/3.9/3.9/js/esri/css/esri.css">
  <link rel="stylesheet" type="text/css" media="screen" href="/script/base/jqueryui/jquery-ui.min.css"/>
  <link rel="stylesheet" type="text/css" media="screen" href="/script/base/jqgrid/ui.jqgrid.css"/>

  <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="/css/font-awesome-4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="/css/public.css">
  <link rel="stylesheet" type="text/css" href="/css/guizhoumax.css">
  <link rel="stylesheet" type="text/css" href="/css/topic.css">
  <link rel="stylesheet" type="text/css" href="/script/base/artcloudui/skins/default/css/artcloudui.css">

  <script type="text/javascript" src="/script/base/My97DatePicker/WdatePicker.js"></script>
  <script type="text/javascript" src="/script/base/jquery-1.11.0.js"></script>
  <script type="text/javascript" src="/script/base/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="/script/base/json-js-master/json2.js"></script>
  <script type="text/javascript" src="/script/base/artcloudui/artcloudui.dialog.min.js"></script>
  <script type="text/javascript" src="/script/base/jqgrid/js/i18n/grid.locale-cn.js"></script>
  <script type="text/javascript" src="/script/base/jqgrid/js/jquery.jqGrid.min.js"></script>
  <script type="text/javascript" src="/script/base/html2canvas.js"></script>

  <script type="text/javascript">
    var root_gisapp = '<%=root_gisapp%>';
    var root_arcgis = '<%=root_arcgis%>';
    var dojoConfig = {
      parseOnLoad: true,
      packages: [{
        name: 'plugin',
        location: "/script/arcgis_js_api/plugin"
      }, {
        name: 'modules',
        location: "/script/modules"
      }]
    };
  </script>
  <script type="text/javascript" src="/script/arcgis_js_api/library/3.9/3.9/init.js"></script>
  <script type="text/javascript" src="/script/base.js"></script>
  <script type="text/javascript" src="/script/modules/common.js"></script>
  <script type="text/javascript" src="/script/modules/guizhoumax.js"></script>
  <script type="text/javascript">
    //var scene_type = '<%=request.getParameter("type")%>';
    //var abc=location.search;
    //var scene_type =GetQueryString("type");
    function initgis(){
      guiZhouMaxLoad();
    }

    function GetQueryString(name) {
      var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)","i");
      var r = window.location.search.substr(1).match(reg);
      if (r!=null) return (r[2]); return null;
    }
  </script>
</head>
<body>
<div class="map-main">
  <div id="map" class="map-wrap"></div>

  <!--搜索弹出面板-->
  <div id='commonquery_content' class="modal-search">
    <div class="panel">
      <div class="panel-body" style="padding:0;">
        <div class="search-cont">
          <div class="hide-search">鼠标移入 展开</div>
          <ul class="search-cont-list">

          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--end搜索弹出面板-->

  <!--end搜索弹出面板-->

  <!--导航二级弹出-->
  <div id='div_menu'>
  </div>
  <!--end导航二级弹出-->

  <!--工具栏-->
  <div class="toolbox">
    <div class="toolbox-wrap">
      <ul class="toolbar">
      </ul>
    </div>
  </div>

  <div id="div_toolbox_content">
  </div>
  <!--end工具栏-->
</div>
</body>
</html>
