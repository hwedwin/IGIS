package gis.business;

import bean.common.Legend;
import bean.common.LegendItem;
import bean.common.LegendItemEx;
import gis.algorithm.ColorHelper;
import gis.algorithm.IColorReader;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class LteGridCoverColor extends IColorReader {
    private List<LegendItem> legendItems = new ArrayList<LegendItem>();// 图例
    private Legend legend = new Legend();
    String kpiName = "";
    Color r = new Color(255, 40, 75, 125);// 红
    Color g = new Color(245, 160, 22, 125);// 黄
    Color b = new Color(31, 151, 243, 125);// 蓝

    int r1 = ColorHelper.getColorInt(r);
    int g1 = ColorHelper.getColorInt(g);
    int b1 = ColorHelper.getColorInt(b);


    /**
     * 图例
     *
     * @return
     */
    public Legend getLegend() {
        return legend;
    }

    public LteGridCoverColor(String kpiName) {

        this.kpiName = kpiName;
        if (kpiName.equals("weak_rat"))// 4G覆盖
        {
            legend.setName("4G弱覆盖");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "0", "30%"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "30%", "50%"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "50%", "100%"));// 红
        } else if (kpiName.equals("lte_flow"))// 4G流量
        {
            legend.setName("4G流量");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "0", "1G"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "1G", "10G"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "10G", "MAX"));// 红
        } else if (kpiName.equals("lte_user_cnt"))// 4G用户
        {
            legend.setName("4G用户数");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "0", "5000"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "5000", "10000"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "10000", "MAX"));// 红
        } else if (kpiName.equals("gsm_flow")) // 2G流量
        {
            legend.setName("2G流量");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "0", "1G"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "1G", "10G"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "10G", "MAX"));// 红
        } else if (kpiName.equals("gsm_user_cnt")) // 2G用户
        {
            legend.setName("2G用户数");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "0", "5000"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "5000", "10000"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "10000", "MAX"));// 红
        } else if (kpiName.equals("weaklist_cnt"))//4G 覆盖 大栅格
        {
            legend.setName("4G连续弱覆盖栅格数量");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "0", "2"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "3", "10"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "10", "MAX"));// 红
        } else if (kpiName.equals("big_lte_flow"))//4G 流量 大栅格
        {
            legend.setName("4G流量(大栅格)");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "0", "1T"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "1T", "10T"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "10t", "MAX"));// 红
        } else if (kpiName.equals("big_gsm_flow"))//4G 流量 大栅格
        {
            legend.setName("2G流量(大栅格)");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "0", "1T"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "1T", "10T"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "10T", "MAX"));// 红
        } else if (kpiName.equals("big_lte_user_cnt"))//4G 用户数 大栅格
        {
            legend.setName("4G用户数(大栅格)");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "0", "50万"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "50万", "100万"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "100万", "MAX"));// 红
        } else if (kpiName.equals("big_gsm_user_cnt"))//4G 用户数 大栅格
        {
            legend.setName("2G用户数(大栅格)");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "0", "50万"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "50万", "100万"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "100万", "MAX"));// 红
        } else if (kpiName.equals("big_weak_rat"))// 4G覆盖
        {
            legend.setName("4G弱覆盖栅格占比");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "0", "30%"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "30%", "50%"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "50%", "100%"));// 红
        } else if (kpiName.equals("weak_rat_cu") || kpiName.equals("big_weak_rat_cu") || kpiName.equals("weak_rat_cu_113") || kpiName.equals("big_weak_rat_cu_113"))//竞争对手覆盖 联通
        {
            if (kpiName.equals("weak_rat_cu") || kpiName.equals("big_weak_rat_cu")) {
                legend.setName("联通CRS RSRP>=-110dBm覆盖率");
            } else {
                legend.setName("联通CRS RSRP>=-113dBm覆盖率");
            }
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "70", "100%"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "50%", "70%"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "0%", "50%"));// 红
        } else if (kpiName.equals("weak_rat_189") || kpiName.equals("big_weak_rat_189") || kpiName.equals("weak_rat_189_113") || kpiName.equals("big_weak_rat_189_113"))//竞争对手覆盖 电信
        {
            if (kpiName.equals("weak_rat_189") || kpiName.equals("big_weak_rat_189")) {
                legend.setName("电信CRS RSRP>=-110dBm覆盖率");
            } else {
                legend.setName("电信CRS RSRP>=-113dBm覆盖率");
            }
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "70", "100%"));// 蓝
            legendItems.add(CreateLegend(245, 160, 22, 125, g1, "50%", "70%"));// 黄
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "0%", "50%"));// 红
        } else if (kpiName.equals("weak_rat_cu-0") || kpiName.equals("weak_rat_cu-1") || kpiName.equals("big_weak_rat_cu-0") || kpiName.equals("big_weak_rat_cu-1"))//竞争对手覆盖 联通
        {
            legend.setName("对比联通覆盖");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "比联通覆盖好", ""));// 蓝
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "比联通覆盖差", ""));// 红
        } else if (kpiName.equals("weak_rat_189-0") || kpiName.equals("weak_rat_189-1") || kpiName.equals("big_weak_rat_189-0") || kpiName.equals("big_weak_rat_189-1"))//竞争对手覆盖 电信
        {
            legend.setName("对比电信覆盖");
            legendItems.add(CreateLegend(31, 151, 243, 125, b1, "比电信覆盖好", ""));// 蓝
            legendItems.add(CreateLegend(255, 40, 75, 125, r1, "比电信覆盖差", ""));// 红
        } else if (kpiName.equals("spots_weak_rat_cu") || kpiName.equals("big_spots_weak_rat_cu") || kpiName.equals("spots_weak_rat_cu_113") || kpiName.equals("big_spots_weak_rat_cu_113"))//竞争对手覆盖 联通
        {
            if (kpiName.equals("spots_weak_rat_cu") || kpiName.equals("big_spots_weak_rat_cu")) {
                legend.setName("联通RSRP>=-110dBm黑点");
            } else {
                legend.setName("联通RSRP>=-113dBm黑点");
            }
            String name = "联通";
            legendItems.add(CreateLegendTitle(255, 40, 75, 125, r1, "级别一", "", "移动MR覆盖率低于80%且低于" + name + "覆盖率"));// 蓝
            legendItems.add(CreateLegendTitle(245, 160, 22, 125, g1, "级别二", "", "移动MR覆盖率低于80%与" + name + "覆盖率相当或略优"));// 黄
            legendItems.add(CreateLegendTitle(31, 151, 243, 125, b1, "级别三", "", "移动MR覆盖率高于80%但弱于" + name + "覆盖5个百分点以上"));// 红
        } else if (kpiName.equals("spots_weak_rat_189") || kpiName.equals("big_spots_weak_rat_189") || kpiName.equals("spots_weak_rat_189_113") || kpiName.equals("big_spots_weak_rat_189_113"))//竞争对手覆盖 电信
        {
            if (kpiName.equals("spots_weak_rat_189") || kpiName.equals("big_spots_weak_rat_189")) {
                legend.setName("电信RSRP>=-110dBm黑点");
            } else {
                legend.setName("电信RSRP>=-113dBm黑点");
            }
            String name = "电信";
            legendItems.add(CreateLegendTitle(255, 40, 75, 125, r1, "级别一", "", "移动MR覆盖率低于80%且低于" + name + "覆盖率"));// 蓝
            legendItems.add(CreateLegendTitle(245, 160, 22, 125, g1, "级别二", "", "移动MR覆盖率低于80%与" + name + "覆盖率相当或略优"));// 黄
            legendItems.add(CreateLegendTitle(31, 151, 243, 125, b1, "级别三", "", "移动MR覆盖率高于80%但弱于" + name + "覆盖5个百分点以上"));// 红
        }

        legend.setLegendItems(legendItems);
    }

    /**
     * @param r        红
     * @param g        黄
     * @param b        蓝
     * @param a        透明度
     * @param colorInt 颜色值
     * @param valueMin 指标最大值
     * @param valueMax 指标最小值
     * @return
     */
    private LegendItem CreateLegend(int r, int g, int b, int a, int colorInt, String valueMin, String valueMax) {
        LegendItem legend = new LegendItem();
        legend.setValueMin(valueMin);
        legend.setValueMax(valueMax);
        legend.setR(r);
        legend.setG(g);
        legend.setB(b);
        legend.setA(a);
        legend.setColorInt(colorInt);
        return legend;
    }

    private LegendItem CreateLegendTitle(int r, int g, int b, int a, int colorInt, String valueMin, String valueMax, String title) {
        LegendItem legend = new LegendItem();
        legend.setValueMin(valueMin);
        legend.setValueMax(valueMax);
        legend.setR(r);
        legend.setG(g);
        legend.setB(b);
        legend.setA(a);
        legend.setTitle(title);
        legend.setColorInt(colorInt);
        return legend;
    }

    private LegendItemEx CreateLegend(int r, int g, int b, int a, int colorInt, String valueMin, String valueMax, String displayName) {
        LegendItemEx legend = new LegendItemEx();
        legend.setValueMin(valueMin);
        legend.setValueMax(valueMax);
        legend.setR(r);
        legend.setG(g);
        legend.setB(b);
        legend.setA(a);
        legend.setColorInt(colorInt);
        legend.setDisplayName(displayName);
        return legend;
    }

    @Override
    public Integer GetColor(Double vales) {
        Integer intValue = null;

        if (kpiName.equals("weak_rat"))// 4G覆盖
        {
            if (vales >= 0 && vales < 0.3) {
                intValue = b1;
            } else if (vales >= 0.3 && vales < 0.5) {
                intValue = g1;
            } else if (vales >= 0.5 && vales <= 1) {
                intValue = r1;
            }
        } else if (kpiName.equals("lte_flow"))// 4G流量
        {
            //100*1024*1024
            //1024*1024*1024 = 1073741824  107374182400
            if (vales >= 0 && vales < 1073741824) {
                intValue = b1;
            }//200*1024*1024
            else if (vales >= 1073741824 && vales < Long.parseLong("10737418240")) {
                intValue = g1;
            } else if (vales >= Long.parseLong("10737418240")) {
                intValue = r1;
            }
        } else if (kpiName.equals("lte_user_cnt"))// 4G用户
        {
            if (vales >= 0 && vales < 5000) {
                intValue = b1;
            }//200*1024*1024
            else if (vales >= 5000 && vales < 10000) {
                intValue = g1;
            } else if (vales >= 10000) {
                intValue = r1;
            }
        } else if (kpiName.equals("fivenet_cell"))// 4G用户
        {
            if (vales >= 0 && vales < 60000) {
                intValue = b1;
            }//200*1024*1024
            else if (vales >=60000 && vales < 100000) {
                intValue = g1;
            } else if (vales >= 100000) {
                intValue = r1;
            }
        } else if (kpiName.equals("gsm_flow")) // 2G流量
        {
            //100*1024*1024
            //1024*1024*1024 = 1073741824  107374182400
            if (vales >= 0 && vales < 1073741824) {
                intValue = b1;
            }//200*1024*1024
            else if (vales >= 1073741824 && vales < Long.parseLong("10737418240")) {
                intValue = g1;
            } else if (vales >= Long.parseLong("10737418240")) {
                intValue = r1;
            }

        } else if (kpiName.equals("gsm_user_cnt")) // 2G用户
        {
            //200*1024*1024
            if (vales >= 0 && vales < 5000) {
                intValue = b1;
            } else if (vales >= 5000 && vales < 10000) {
                intValue = g1;
            } else if (vales >= 10000) {
                intValue = r1;
            }
        } else if (kpiName.equals("weaklist_cnt")) {//4G 覆盖 大栅格
            if (vales >= 0 && vales <= 2) {
                intValue = b1;
            } else if (vales >= 3 && vales < 10) {
                intValue = g1;
            } else if (vales >= 10) {
                intValue = r1;
            }

        } else if (kpiName.equals("big_lte_flow"))// 4G流量 大栅格
        {
            //1024*1024*1024 = 1073741824  107374182400
            if (vales >= 0 && vales < Long.parseLong("1099511627776")) {//0~1T
                intValue = b1;
            }//200*1024*1024
            else if (vales >= Long.parseLong("1099511627776") && vales < Long.parseLong("10995116277760")) {//1T~10T
                intValue = g1;
            } else if (vales >= Long.parseLong("10995116277760")) {//10T~MAX
                intValue = r1;
            }
        } else if (kpiName.equals("big_lte_user_cnt"))// 4G用户 大栅格
        {
            if (vales >= 0 && vales < 500000.0) {
                intValue = b1;
            }//200*1024*1024
            else if (vales >= 500000.0 && vales < 1000000.0) {
                intValue = g1;
            } else if (vales >= 1000000.0) {
                intValue = r1;
            }
        } else if (kpiName.equals("big_gsm_flow")) // 2G流量 大栅格
        {
            //1024*1024*1024 = 1073741824  107374182400
            if (vales >= 0 && vales < Long.parseLong("1099511627776")) {
                intValue = b1;
            }//200*1024*1024
            else if (vales >= Long.parseLong("1099511627776") && vales < Long.parseLong("10995116277760")) {
                intValue = g1;
            } else if (vales >= Long.parseLong("10995116277760")) {
                intValue = r1;
            }
        } else if (kpiName.equals("big_gsm_user_cnt")) // 2G用户 大栅格
        {
            if (vales >= 0 && vales < 500000.0) {
                intValue = b1;
            }//200*1024*1024
            else if (vales >= 500000.0 && vales < 1000000.0) {
                intValue = g1;
            } else if (vales >= 1000000.0) {
                intValue = r1;
            }
        } else if (kpiName.equals("big_weak_rat"))// 4G覆盖  大栅格
        {
            if (vales >= 0 && vales < 0.3) {
                intValue = b1;
            } else if (vales >= 0.3 && vales < 0.5) {
                intValue = g1;
            } else if (vales >= 0.5 && vales <= 1) {
                intValue = r1;
            }
        } else if (kpiName.equals("weak_rat_189") || kpiName.equals("big_weak_rat_189") || kpiName.equals("weak_rat_189_113") || kpiName.equals("big_weak_rat_189_113"))// 电信覆盖
        {
            if (vales >= 0.7) {
                intValue = b1;
            } else if (vales >= 0.5 && vales < 0.7) {
                intValue = g1;
            } else if (vales < 0.5) {
                intValue = r1;
            }
        } else if (kpiName.equals("weak_rat_cu") || kpiName.equals("big_weak_rat_cu") || kpiName.equals("weak_rat_cu_113") || kpiName.equals("big_weak_rat_cu_113"))// 联通覆盖
        {
            if (vales >= 0.7) {
                intValue = b1;
            } else if (vales >= 0.5 && vales < 0.7) {
                intValue = g1;
            } else if (vales < 0.5) {
                intValue = r1;
            }
        } else if (kpiName.equals("weak_rat_cu||0") || kpiName.equals("weak_rat_cu-1") || kpiName.equals("big_weak_rat_cu-0") || kpiName.equals("big_weak_rat_cu-1"))// 联通覆盖
        {
            if (vales == 1) {
                intValue = b1;
            } else if (vales == 0) {
                intValue = r1;
            }
        } else if (kpiName.equals("weak_rat_189||0") || kpiName.equals("weak_rat_189-1") || kpiName.equals("big_weak_rat_189-0") || kpiName.equals("big_weak_rat_189-1"))// 电信覆盖
        {
            if (vales == 1) {
                intValue = b1;
            } else if (vales == 0) {
                intValue = r1;
            }
        } else if (kpiName.equals("spots_weak_rat_189") || kpiName.equals("big_spots_weak_rat_189") || kpiName.equals("spots_weak_rat_189_113") || kpiName.equals("big_spots_weak_rat_189_113"))// 电信覆盖
        {
            if (vales == 1) {
                intValue = r1;
            } else if (vales == 2) {
                intValue = g1;
            } else if (vales == 3) {
                intValue = b1;
            }
        } else if (kpiName.equals("spots_weak_rat_cu") || kpiName.equals("big_spots_weak_rat_cu") || kpiName.equals("spots_weak_rat_cu_113") || kpiName.equals("big_spots_weak_rat_cu_113"))// 联通覆盖
        {
            if (vales == 1) {
                intValue = r1;
            } else if (vales == 2) {
                intValue = g1;
            } else if (vales == 3) {
                intValue = b1;
            }
        }

        return intValue;
    }

    public Color GetOrgColor(Double vales) {
        Color color = null;
        if (kpiName.equals("weak_rat_189") || kpiName.equals("big_weak_rat_189") || kpiName.equals("weak_rat_189_113") || kpiName.equals("big_weak_rat_189_113"))// 电信覆盖
        {
            if (vales >= 0.7) {
                color = b;
            } else if (vales >= 0.5 && vales < 0.7) {
                color = g;
            } else if (vales < 0.5) {
                color = r;
            }
        } else if (kpiName.equals("weak_rat_cu") || kpiName.equals("big_weak_rat_cu") || kpiName.equals("weak_rat_cu_113") || kpiName.equals("big_weak_rat_cu_113"))// 联通覆盖
        {
            if (vales >= 0.7) {
                color = b;
            } else if (vales >= 0.5 && vales < 0.7) {
                color = g;
            } else if (vales < 0.5) {
                color = r;
            }
        } else if (kpiName.equals("spots_weak_rat_cu") || kpiName.equals("spots_weak_rat_189")) {
            if (vales == 1) {
                color = r;
            } else if (vales == 2) {
                color = g;
            } else if (vales == 3) {
                color = b;
            }
        } else if (kpiName.equals("spots_weak_rat_cu_113") || kpiName.equals("spots_weak_rat_189_113")) {
            if (vales == 1) {
                color = r;
            } else if (vales == 2) {
                color = g;
            } else if (vales == 3) {
                color = b;
            }
        }
        return color;
    }

}
