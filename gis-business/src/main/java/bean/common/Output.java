package bean.common;


import bean.enums.ErrorCode;

/**
 * Created by Xiaobing on 2016/4/20.
 * 统一返回对象 包含错误码  错误信息 等
 */
public class Output {
    /**
     * 错误码 0：无错误信息 -1001：授权过期
     */
    private  int errorCode=0;
    /**
     * 错误信息
     */
    private  String errorMessage;
    /**
     * 异常信息
     */
    private  Exception exception;
    /**
     * 数据对象
     */
    private Object data;
    /**
     * 其他信息
     */
    private Object tag;

    /**
     * 错误码
     * @return
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * 错误码
     * @param errorCode
     */
    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * 错误信息
     * @return
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * 错误信息
     * @param errorMessage
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * 异常信息
     * @return
     */
    public Exception getException() {
        return exception;
    }

    /**
     * 异常信息
     * @param exception
     */
    public void setException(Exception exception) {
        this.exception = exception;
    }

    /**
     * 数据对象
     * @return
     */
    public Object getData() {
        return data;
    }

    /**
     * 数据对象
     * @param data
     */
    public void setData(Object data) {
        this.data = data;
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }
}
