package bean.ltecover;

import gis.core.bean.KeyValue;

/**
 * Created by Xiaobing on 2016/8/9.
 */
public class LoadGridInfo extends KeyValue {
    private String time;
    private String weakRat;
    private String lteFlow;
    private String lteUserCnt;
    private String gsmFlow;
    private String gsmUserCnt;

    private double xMax;
    private double yMax;
    private double xMin;
    private double yMin;

    private double centerX;
    private double centerY;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWeakRat() {
        return weakRat;
    }

    public void setWeakRat(String weakRat) {
        this.weakRat = weakRat;
    }

    public String getLteFlow() {
        return lteFlow;
    }

    public void setLteFlow(String lteFlow) {
        this.lteFlow = lteFlow;
    }

    public String getLteUserCnt() {
        return lteUserCnt;
    }

    public void setLteUserCnt(String lteUserCnt) {
        this.lteUserCnt = lteUserCnt;
    }

    public String getGsmFlow() {
        return gsmFlow;
    }

    public void setGsmFlow(String gsmFlow) {
        this.gsmFlow = gsmFlow;
    }

    public String getGsmUserCnt() {
        return gsmUserCnt;
    }

    public void setGsmUserCnt(String gsmUserCnt) {
        this.gsmUserCnt = gsmUserCnt;
    }

    public double getxMax() {
        return xMax;
    }

    public void setxMax(double xMax) {
        this.xMax = xMax;
    }

    public double getyMax() {
        return yMax;
    }

    public void setyMax(double yMax) {
        this.yMax = yMax;
    }

    public double getxMin() {
        return xMin;
    }

    public void setxMin(double xMin) {
        this.xMin = xMin;
    }

    public double getyMin() {
        return yMin;
    }

    public void setyMin(double yMin) {
        this.yMin = yMin;
    }

    public double getCenterX() {
        return centerX;
    }

    public void setCenterX(double centerX) {
        this.centerX = centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public void setCenterY(double centerY) {
        this.centerY = centerY;
    }
}
