package bean.cityfun;

/**
 * Created by hupeng on 16/5/18.
 * 用户场景与栅格对应关系
 */
public class AreaRefScene {
    /**
     * 场景标识
     */
    private String scene_id;
    /**
     * 场景名称
     */
    private String scene_name;
    /**
     * 场景类型标识
     */
    private String scene_type_id;
    /**
     * 场景类型名称
     */
    private String scene_type_name;
    /**
     * 栅格标识
     */
    private String grid_id;
    /**
     * 最小经度
     */
    private String min_longitude;
    /**
     * 最大经度
     */
    private String max_longitude;
    /**
     * 最小纬度
     */
    private String min_latitude;
    /**
     * 最大纬度
     */
    private String max_latitude;

    public String getScene_id() {
        return scene_id;
    }

    public void setScene_id(String scene_id) {
        this.scene_id = scene_id;
    }

    public String getScene_name() {
        return scene_name;
    }

    public void setScene_name(String scene_name) {
        this.scene_name = scene_name;
    }

    public String getScene_type_id() {
        return scene_type_id;
    }

    public void setScene_type_id(String scene_type_id) {
        this.scene_type_id = scene_type_id;
    }

    public String getScene_type_name() {
        return scene_type_name;
    }

    public void setScene_type_name(String scene_type_name) {
        this.scene_type_name = scene_type_name;
    }

    public String getGrid_id() {
        return grid_id;
    }

    public void setGrid_id(String grid_id) {
        this.grid_id = grid_id;
    }

    public String getMin_longitude() {
        return min_longitude;
    }

    public void setMin_longitude(String min_longitude) {
        this.min_longitude = min_longitude;
    }

    public String getMax_longitude() {
        return max_longitude;
    }

    public void setMax_longitude(String max_longitude) {
        this.max_longitude = max_longitude;
    }

    public String getMin_latitude() {
        return min_latitude;
    }

    public void setMin_latitude(String min_latitude) {
        this.min_latitude = min_latitude;
    }

    public String getMax_latitude() {
        return max_latitude;
    }

    public void setMax_latitude(String max_latitude) {
        this.max_latitude = max_latitude;
    }
}
