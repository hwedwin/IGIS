package bean.knowledge.tide;

/**
 * Created by xiexb on 2017/8/3.
 */
public class TideDataHourBean {
    private String id_;
    private String stationNo;
    private String stationName;
    private String ew;
    private String ns;
    //	private String timeZone;
    private int daykey;
    private String hourKey;
    private int timeHeight;

    public String getId_() {
        return id_;
    }

    public void setId_(String id_) {
        this.id_ = id_;
    }

    public String getStationNo() {
        return stationNo;
    }

    public void setStationNo(String stationNo) {
        this.stationNo = stationNo;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getEw() {
        return ew;
    }

    public void setEw(String ew) {
        this.ew = ew;
    }

    public String getNs() {
        return ns;
    }

    public void setNs(String ns) {
        this.ns = ns;
    }

    public int getDaykey() {
        return daykey;
    }

    public void setDaykey(int daykey) {
        this.daykey = daykey;
    }

    public String getHourKey() {
        return hourKey;
    }

    public void setHourKey(String hourKey) {
        this.hourKey = hourKey;
    }

    public int getTimeHeight() {
        return timeHeight;
    }

    public void setTimeHeight(int timeHeight) {
        this.timeHeight = timeHeight;
    }
}