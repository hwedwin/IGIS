function Demo() {

    /**
     * 外部调用方法
     */
    this.fristFun = function () {
        alert("hello word");
    };

    /**
     * 接口 初始化  插件入口
     */
    this.init = function () {
        //注册组件
        gis_core.addPlugin(this);
    };
    /**
     * 接口 销毁
     */
    this.destroy = function () {

    };
    /**
     * 接口  模块管理  插件信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "01_00_01",
        name: "测试Demo"
    };
}

/**
 * 插件对象  可以销毁
 * @type {null}
 */
var demo = null;

/**
 *实例化对象
 */
function initDemo() {
    if (demo == null) {
        demo = new Demo();
        demo.init();
    }
    demo.fristFun();
}
