<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
    <%@ include file="header.jsp" %>
    <section>
        <div class="container">
            <%@ include file="leftmenu.jsp" %>
            <input id="pageid" type="hidden" value="igisdeveloper">

            <div class="docs-content" style="height: 2000px;">
                <h3 id="welcome" class="welcome"> iGIS 插件开发</h3>
                <br/>

                <p>下面开始我们的第一个iGIS插件开发</p>
            <pre class="prettyprint">
function Demo() {

    /**
     * 外部调用方法
     */
    this.fristFun = function () {
        alert("hello word");
    };

    /**
     * 接口 初始化  插件入口
     */
    this.init = function () {
        //注册组件
        gis_core.addPlugin(this);
    };
    /**
     * 接口 销毁
     */
    this.destroy = function () {

    };
    /**
     * 接口  模块管理  插件信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "01_00_01",
        name: "测试Demo"
    };
}

/**
 * 插件对象  可以销毁
 * @type {null}
 */
var demo = null;

/**
 *实例化对象
 */
function initDemo() {
    if (demo == null) {
        demo = new Demo();
        demo.init();
    }
    demo.fristFun();
}

            </pre>
                <p>以上为一个标准的iGIS组件模板。其中：init 、destroy 、title 为固定接口，前端框架在注册，模块管理，销毁模块等操作是会调用对象的这几个方法。
                    title为固定格式json对象，id: 插件唯一编号   name:插件名称 </p>
                <p>接下来只需要在菜单配置文件中，将菜单配置上去，插件就可以运行起来。在项目中找到resources\gisconfig对应的文件夹下的menu.****.xml配置文件，完成菜单配置即可</p>

                <pre class="prettyprint">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;menuConfigs&gt;
    &lt;common&gt;
        &lt;defaultPage&gt;pages/gisdefault.jsp&lt;/defaultPage&gt;
        &lt;navPage&gt;pages/nav.jsp&lt;/navPage&gt;
        &lt;showSearch&gt;true&lt;/showSearch&gt;
        &lt;toolBars&gt;
            &lt;toolBar fun="gis_common.queryPointFun(this)" name="经纬度查询" isShow="true"&gt;&lt;/toolBar&gt;
            &lt;toolBar fun="gis_common.clearDataCache()" name="清理缓存" isShow="true"&gt;&lt;/toolBar&gt;
        &lt;/toolBars&gt;
    &lt;/common&gt;

    &lt;modules&gt;
        &lt;module name="测试程序" m="fristdemo" isShow="true" gistype="gaode" showsearch="false" istransfer="false" showToolBars = "true"&gt;
            &lt;jsPaths&gt;
                &lt;jsPath path="modules/demo.js"&gt;&lt;/jsPath&gt;
            &lt;/jsPaths&gt;
            &lt;menus&gt;
                &lt;menu fun="initDemo()" name="测试"&gt;&lt;/menu&gt;
            &lt;/menus&gt;
        &lt;/module&gt;
    &lt;/modules&gt;
&lt;/menuConfigs&gt;
                </pre>
                <a href="/api/arcgis.jsp" type="button" class="btn-link">上一页</a>
                <a href="/api/baseinfoxml.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>
            </div>
        </div>
    </section>
</div>
</body>
</html>
