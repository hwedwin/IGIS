package bean.knowledge.tide;

import gis.core.bean.KeyValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xiexb on 2017/8/4.
 */
public class WeatherEnum {
    private List<KeyValue> weather = null;

    private static WeatherEnum instance = null;

    private WeatherEnum() {
        weather = new ArrayList<>();
        weather.add(new KeyValue("0", "晴"));
        weather.add(new KeyValue("2", "阴"));
        weather.add(new KeyValue("4", "雷阵雨"));
        weather.add(new KeyValue("6", "雨夹雪"));
        weather.add(new KeyValue("8", "中雨"));
        weather.add(new KeyValue("10", "暴雨"));
        weather.add(new KeyValue("12", "特大暴雨'"));
        weather.add(new KeyValue("14", "小雪"));
        weather.add(new KeyValue("16", "大雪"));
        weather.add(new KeyValue("18", "雾"));
        weather.add(new KeyValue("20", "沙尘暴"));
        weather.add(new KeyValue("22", "中到大雨"));
        weather.add(new KeyValue("24", "暴雨到大暴雨"));
        weather.add(new KeyValue("26", "小到中雪"));
        weather.add(new KeyValue("28", "大到暴雪"));
        weather.add(new KeyValue("30", "扬沙"));
        weather.add(new KeyValue("53", "霾"));
        weather.add(new KeyValue("1", "多云"));
        weather.add(new KeyValue("3", "阵雨"));
        weather.add(new KeyValue("5", "雷阵雨伴有冰雹"));
        weather.add(new KeyValue("7", "小雨"));
        weather.add(new KeyValue("9", "大雨"));
        weather.add(new KeyValue("11", "大暴雨"));
        weather.add(new KeyValue("13", "阵雪"));
        weather.add(new KeyValue("15", "中雪"));
        weather.add(new KeyValue("17", "暴雪"));
        weather.add(new KeyValue("19", "冻雨"));
        weather.add(new KeyValue("21", "小到中雨"));
        weather.add(new KeyValue("23", "大到暴雨"));
        weather.add(new KeyValue("25", "大暴雨到特大暴雨"));
        weather.add(new KeyValue("27", "中到大雪"));
        weather.add(new KeyValue("29", "浮尘"));
        weather.add(new KeyValue("31", "强沙尘暴"));
    }

    public static synchronized WeatherEnum getInstance() {
        if (instance == null) {
            instance = new WeatherEnum();
        }
        return instance;
    }

    public KeyValue getWeather(String id) {
        KeyValue keyValue = null;
        for (KeyValue kv : weather) {
            if (kv.getId().equals(id))
                keyValue = kv;
        }
        return keyValue;
    }
}
