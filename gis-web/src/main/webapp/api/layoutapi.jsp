<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
  <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
  <%@ include file="header.jsp" %>
  <section>
    <div class="container">
      <%@ include file="leftmenu.jsp" %>
      <input id="pageid" type="hidden" value="layoutapi">

      <div class="docs-content" style="height: 2900px;">
        <h3 class="welcome">通用页面布局</h3>
        <p>common.js文件中gis_layout类，封装了关于页面布局的操作，下面对部分接口进行介绍</p>
        <br/>

        <h3 class="welcome">相关事件</h3>

        <li>gis_layout.createToolbar 工具栏创建布局</li>
        <pre class="prettyprint">
      输入参数:
           @param id 全局ID
           @param obj 被点击的标签
           @param width 面板宽度
           @param divContent jquery 对象 (html代码)
      样例:
            function endToEnd_createTootbar_cells(obj) {
                initEndToEnd();
                gis_layout.createToolbar("endtoend_res_mrpoint", obj, 320, endToEnd.loadCellMrPointHtml);
            }
                </pre>

        <li>gis_layout.createToolbarWithFun 工具栏创建布局</li>
        <pre class="prettyprint">
      输入参数:
           @param id 全局ID
           @param obj 被点击的标签
           @param width 面板宽度
           @param fun 方法  获取需要添加的jquery 对象 (html代码)
      样例:
            function createDrawToolbar(obj) {
                gis_layout.createToolbarWithFun("_draw_toolbar_holder", obj, 194, cityFunction.createDrawToolbarHtml);
            }
            <!----
            cityFunction.createDrawToolbarHtml = function (callback) {
            var html = [];
            html.push('<div class="list-group">');
            html.push('    <button type="button" class="list-group-item _rectangle_btn"><i class="fa fa-square-o"></i> 矩形选择区域');
            html.push('    </button>');
            html.push('    <button type="button" class="list-group-item _polygon_btn"><i class="fa fa-star-o"></i> 不规则区域（打点）');
            html.push('    </button>');
            html.push('    <button type="button" class="list-group-item _free_polygon_btn"><i class="fa fa-star-o"></i> 不规则区域（划线）');
            html.push('    </button>');
            html.push('</div>');
            var $div = $(html.join(''));
        $div.find('._rectangle_btn').on('click', function () {
            if (checkGridCount()) {
                toolbar.activate(esri.toolbars.Draw.RECTANGLE);
                ringType = 1;
            } else {
                gis_common.showMessage('当前栅格数过多');
            }

        });
        $div.find('._polygon_btn').on('click', function () {
            if (checkGridCount()) {
                toolbar.activate(esri.toolbars.Draw.POLYGON);
                ringType = 2;
            } else {
                gis_common.showMessage('当前栅格数过多');
            }
        });
        $div.find('._free_polygon_btn').on('click', function () {
                if (checkGridCount()) {
                   toolbar.activate(esri.toolbars.Draw.FREEHAND_POLYGON);
                   ringType = 2;
                } else {
                    gis_common.showMessage('当前栅格数过多');
                }
            });
            callback($div);
        };
          --->
                </pre>

        <li>gis_layout.createMenubarWithFun 创建菜单（有弹出面板 fun获取jquery对象）</li>
        <pre class="prettyprint">
      输入参数:
           @param id 全局ID
           @param obj 被点击的标签
           @param title 菜单标题
           @param fun 方法  获取需要添加的jquery 对象 (html代码)
      样例:
            gis_layout.createToolbarWithFun("endtoend_res_mrpoint", obj, 320, endToEnd.loadCellMrPointHtml);
                </pre>

        <li>gis_layout.createMenubar 创建菜单（无弹出面板）</li>
        <pre class="prettyprint">
      输入参数:
           @param id 全局ID
           @param obj 被点击的标签
           @param fun 方法  获取需要添加的jquery 对象 (html代码)
      样例:
            gis_layout.createMenubar("highSpeedRailbj_loadGrid", obj, highSpeedRailBJ.loadGrid);
                </pre>

        <li>gis_layout.hideMenubar  隐藏菜单</li>
        <pre class="prettyprint">
      输入参数:
            无
      样例:
            function scenicSchool_Init() {
                initScenicSchool();
                gis_core.pluginDestroyExceptId(scenicSchool.title.id);
                gis_layout.hideMenubar();
            }

                </pre>
        <li>gis_layout.createLegend 创建图例</li>
        <pre class="prettyprint">
      输入参数:
           @param id 全局ID
           @param title 图例标题
           @param divContent  jquery 对象 (html代码)
      样例:
            gis_layout.createLegend("endtoend_grid_legend", legends.name + "图例", div);
                </pre>
        <li>gis_layout.removeLegend 移除图例</li>
        <pre class="prettyprint">
      输入参数:
           @param id 全局ID
      样例:
            gis_layout.removeLegend(legend_load_data);
                </pre>
        <li>gis_layout.createLeftWidget 添加左边面板 如果ID相同，将移除之前面板</li>
        <pre class="prettyprint">
      输入参数:
           @param id 全局ID
           @param title 标题
           @param divContent jquery 对象 (html代码)
           @param forbidClose 禁止关闭
      样例:
        gis_layout.createLeftWidget("EndToEnd_SerialWeakGrid_JQGrid_Div", "连续弱覆盖", div);
        gis_layout.createLeftWidget('_scenic_spot_list', '高校', divContent, true);
                </pre>
          <li>gis_layout.removeLeftWidget 根据ID移除左边面板tab，如果面板的tab都被移除了，就关闭左边侧面板</li>
        <pre class="prettyprint">
      输入参数:
           @param id 全局ID
      样例:
            gis_layout.removeLeftWidget('_scenic_spot_list');
                </pre>
          <li>gis_layout.createRightWidget 添加右边面板 如果ID相同，将移除之前面板</li>
        <pre class="prettyprint">
      输入参数:
           @param id 全局ID
           @param title 标题
           @param divContent jquery 对象 (html代码)
           @param forbidClose 禁止关闭
      样例:
            gis_layout.createRightWidget("endtoend_createBuildKpiDetailHtml", "建筑物指标明细", div);
            gis_layout.createRightWidget("endtoend_createBuildKpiDetailHtml", "建筑物指标明细", div,true);

                </pre>
          <li>gis_layout.removeRightWidget 根据ID移除右边面板tab，如果面板的tab都被移除了，就关闭右边侧面板</li>
        <pre class="prettyprint">
      输入参数:
           @param id 全局ID
      样例:
             gis_layout.removeRightWidget('_scenic_spot_list');

                </pre>
          <li>gis_layout.createOtherWidget 添加其他面板 框架不做任何操作 如果ID相同，将移除之前面板</li>
        <pre class="prettyprint">
      输入参数:
           @param id 全局ID
           @param fun 方法  获取需要添加的jquery 对象 (html代码)
      样例:
            gis_layout.createRightWidget("endtoend_createBuildKpiDetailHtml", highSpeedRailBJ.loadGrid);
                </pre>

        <br/>

        <a href="mapapi.jsp" type="button" class="btn-link">上一页</a>
        <a href="commresapi.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>

      </div>
    </div>
  </section>
</div>
</body>
</html>
