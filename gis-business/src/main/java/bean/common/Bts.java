package bean.common;

import java.util.ArrayList;
import java.util.List;

import bean.enums.ResType;

/**
 * 基站
 * 
 * @author Xiaobing
 *
 */
public class Bts extends Res {

	public Bts() {
		cells = new ArrayList<Cell>();
	}

	public Bts(ResType resType) {
		setResType(resType);
		cells = new ArrayList<Cell>();
	}

	private List<Cell> cells;

	/**
	 * 小区
	 * 
	 * @return abccell extend cell
	 */
	public List<Cell> getCells() {
		return cells;
	}

	/**
	 * 小区
	 * 
	 * @param cells
	 */
	public void setCells(List<Cell> cells) {
		this.cells = cells;
	}
}
