package gis.serviceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bean.common.Bts;
import bean.common.Cell;
import gis.service.TestService;
import gis.sqlbase.*;

public class TestServiceImpl extends TestService {
	private static final SQLHelper db = DatabaseManage.Instance(DatabaseName.oracle);

	@Override
	public void Test() {
		ResultSet resultSet = db.getResultSet("select * from t_poi_info");
		db.close(resultSet);
		System.out.println("hello word");

	}

	@Override
	public List<Bts>  ShowTestPoint() {
		String strSQL = "select distinct cell_lng,cell_lat, p_lng,p_lat from ads_dia_loc_mro";
		
		Map<String, Bts> map = new HashMap<String, Bts>();
		Bts bts = null;
		Cell cell =null;
		String btsId = null;
		
		ResultSet rs = db.getResultSet(strSQL);
		try {
			while (rs.next()) {
				btsId = rs.getString("cell_lng")+rs.getString("cell_lat");
				if(!map.containsKey(btsId))
				{
					bts = new Bts();
					bts.setId(btsId);
					
					bts.setLongitude(rs.getDouble("cell_lng"));
					bts.setLatitude(rs.getDouble("cell_lat"));
					map.put(btsId, bts);
				}
				
				cell = new Cell();
				cell.setId(rs.getString("p_lng")+rs.getString("p_lat"));
				cell.setLongitude(rs.getDouble("p_lng"));
				cell.setLatitude(rs.getDouble("p_lat"));
				
				map.get(btsId).getCells().add(cell);
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			db.close(rs);
		}

		List<Bts> list = new ArrayList<Bts>();
		for (String key : map.keySet()) {
			list.add(map.get(key));
		}
		
		return list;
	}

}
