package bean.ltecover;

/**
 * 端到端分析 连续弱覆盖网格
 * Created by Xiaobing on 2016/4/18.
 */
public class SerialWeakGrid {

    private String id;
    private  int time;
    private int cityId;
    private String cityName;
    private double minLongitude;
    private double maxLongitude;
    private double minLatitude;
    private double maxLatitude;
    private int weakGridCount;
    private double avgWeakRat;


    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public double getMinLongitude() {
        return minLongitude;
    }

    public void setMinLongitude(double minLongitude) {
        this.minLongitude = minLongitude;
    }

    public double getMaxLongitude() {
        return maxLongitude;
    }

    public void setMaxLongitude(double maxLongitude) {
        this.maxLongitude = maxLongitude;
    }

    public double getMinLatitude() {
        return minLatitude;
    }

    public void setMinLatitude(double minLatitude) {
        this.minLatitude = minLatitude;
    }

    public double getMaxLatitude() {
        return maxLatitude;
    }

    public void setMaxLatitude(double maxLatitude) {
        this.maxLatitude = maxLatitude;
    }

    public int getWeakGridCount() {
        return weakGridCount;
    }

    public void setWeakGridCount(int weakGridCount) {
        this.weakGridCount = weakGridCount;
    }

    public double getAvgWeakRat() {
        return avgWeakRat;
    }

    public void setAvgWeakRat(double avgWeakRat) {
        this.avgWeakRat = avgWeakRat;
    }
}
