<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">

    <%String root_gisapp = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>
    <%String root_arcgis = request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>

    <title><c:out value="${module.name}"/></title>
    <link rel="stylesheet" href="/script/arcgis_js_api/library/3.9/3.9/js/dojo/dijit/themes/dijit.css">
    <link rel="stylesheet" href="/script/arcgis_js_api/library/3.9/3.9/js/esri/css/esri.css">

    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/public.css">
    <link rel="stylesheet" type="text/css" href="/css/topic.css">

    <script type="text/javascript" src="/script/base/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/script/base/json-js-master/json2.js"></script>

    <script type="text/javascript">
        var root_gisapp = '<%=root_gisapp%>';
        var root_arcgis = '<%=root_arcgis%>';
        var dojoConfig = {
            parseOnLoad: true,
            packages: [{
                name: 'plugin',
                location: "/script/arcgis_js_api/plugin"
            }, {
                name: 'modules',
                location: "/script/modules"
            }]
        };
    </script>
    <script type="text/javascript" src="/script/arcgis_js_api/library/3.9/3.9/init.js"></script>


    <script type="text/javascript">
        // 功能插件
        var features = [];
        features.push("esri/map");
        features.push("esri/layers/ArcGISTiledMapServiceLayer");
        features.push("dojo/domReady!");

        require(features, function (Map, ArcGISTiledMapServiceLayer) {
            var map = new Map("map", {
                logo: false
            });
            var layer = new ArcGISTiledMapServiceLayer("/mapservice/gistype_gaode");
            map.addLayer(layer);

        });
    </script>
</head>
<body>
<div id="map" style="width:1366px; height: 768px;"></div>
</body>
</html>
