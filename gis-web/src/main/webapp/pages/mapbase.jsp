<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div id="map" class="map-wrap"></div>

<c:if test="${module.showSearch == 'true'}">
    <!--搜索框-->
    <div class="search-panel">
        <div class="srh-l">
            <input type="text" class="srh-inp" placeholder="关键字搜索">
        </div>
        <div class="srh-r">
            <button type="submit" class="btn btn-primary btn-search" onClick="gis_common.query();">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </div>
    <!--end搜索框-->

    <!--搜索弹出面板-->
    <div id='commonquery_content' class="modal-search">
        <div class="panel">
            <div class="panel-body" style="padding:0;">
                <div class="search-cont">
                    <div class="hide-search">鼠标移入 展开</div>
                    <ul class="search-cont-list">

                    </ul>
                </div>
            </div>
        </div>
    </div>
</c:if>
<!--end搜索弹出面板-->

<!--end搜索弹出面板-->
<!--导航-->
<div class="nav-slide">
    <div class="nav-btn"><i class="fa fa-navicon"></i></div>
</div>
<ul class="nav-list">
    <c:forEach items="${module.menus}" var="item">
        <li><a href="javascript:void(0);" onClick="<c:out value="${item.fun}"></c:out>;"><i
                class="fa fa-subway"></i><br><c:out value="${item.name}"></c:out></a></li>
    </c:forEach>
</ul>
<!--end导航-->

<%--返回菜单页面--%>
<div class="nav-slide-home">
    <a href="/default">
        <div class="nav-btn"><img style="margin-top: 9px" src="/images/nav/home.png" alt="返回菜单页面"/></div>
    </a>
</div>

<!--导航二级弹出-->
<div id='div_menu'>
</div>
<!--end导航二级弹出-->

<!--工具栏-->
<c:if test="${module.showToolBars != 'false'}">
    <div class="toolbox" style="display:block;">
        <div class="toolbox-wrap">
            <ul class="toolbar">
                <c:forEach items="${module.toolBars}" var="item">
                    <li did="<c:out value="${item.fun}"></c:out>"><a href="javascript:void(0);"
                                                                     did="<c:out value="${item.fun}"></c:out>"
                                                                     onclick="<c:out value="${item.fun}"></c:out>;"><i
                            class="fa fa-files-o"></i><c:out value="${item.name}"></c:out></a></li>
                </c:forEach>
            </ul>
            <div class="tabs-modal" id="mymodal">
            </div>
        </div>
    </div>
</c:if>

<div id="div_toolbox_content">
</div>
<!--end工具栏-->