<%--
  Created by IntelliJ IDEA.
  User: Xiaobing
  Date: 2016/11/17
  Time: 11:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">

    <%String root_gisapp = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>
    <%String root_arcgis = request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";%>

    <title><c:out value="${module.name}"/></title>
    <link rel="stylesheet" href="/script/arcgis_js_api/library/3.9/3.9/js/dojo/dijit/themes/dijit.css">
    <link rel="stylesheet" href="/script/arcgis_js_api/library/3.9/3.9/js/esri/css/esri.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/script/base/jqueryui/jquery-ui.min.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/script/base/jqgrid/ui.jqgrid.css"/>

    <link rel="stylesheet" type="text/css" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome-4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/public.css">
    <link rel="stylesheet" type="text/css" href="/css/topic.css">
    <link rel="stylesheet" type="text/css" href="/script/base/artcloudui/skins/default/css/artcloudui.css">

    <script type="text/javascript" src="/script/base/My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript" src="/script/base/jquery-1.11.0.js"></script>
    <script type="text/javascript" src="/script/base/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/script/base/json-js-master/json2.js"></script>
    <script type="text/javascript" src="/script/base/artcloudui/artcloudui.dialog.min.js"></script>
    <script type="text/javascript" src="/script/base/jqgrid/js/i18n/grid.locale-cn.js"></script>
    <script type="text/javascript" src="/script/base/jqgrid/js/jquery.jqGrid.min.js"></script>
    <script type="text/javascript" src="/script/base/html2canvas.js"></script>

    <script type="text/javascript">
        var root_gisapp = '<%=root_gisapp%>';
        var root_arcgis = '<%=root_arcgis%>';
        var dojoConfig = {
            parseOnLoad: true,
            packages: [{
                name: 'plugin',
                location: "/script/arcgis_js_api/plugin"
            }, {
                name: 'modules',
                location: "/script/modules"
            }]
        };
    </script>
    <script type="text/javascript" src="/script/arcgis_js_api/library/3.9/3.9/init.js"></script>
    <script type="text/javascript" src="/script/base.js"></script>
    <script type="text/javascript" src="/script/modules/common.js"></script>
    <script type="text/javascript" src="/script/modules/fivenet.js"></script>

    <script type="text/javascript">

        gis_core.mapLoaded(function () {
            $("#map_zoom_slider").css("right", "5px").css("bottom", "5px");
        });

        function initgis() {
            fivenet_queryCellRing();
        }

    </script>
</head>
<body>
<div class="map-main">
    <div id="map" class="map-wrap"></div>
</div>
</body>
</html>
