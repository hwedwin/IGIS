package bean.common;

/**
 * Created by Xiaobing on 2016/9/1.
 */
public class NavItemInfo {
    private String name;
    private String fun;
    private String ico;
    private String page;
    private String bgcolor;
    private String defaultPage;
    private String showsearch;
    private  String arcgisUrl;
    private String isTransfer;
    private String gisType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFun() {
        return fun;
    }

    public void setFun(String fun) {
        this.fun = fun;
    }

    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getBgcolor() {
        return bgcolor;
    }

    public void setBgcolor(String bgcolor) {
        this.bgcolor = bgcolor;
    }

    public String getDefaultPage() {
        return defaultPage;
    }

    public void setDefaultPage(String defaultPage) {
        this.defaultPage = defaultPage;
    }

    public String getShowsearch() {
        return showsearch;
    }

    public void setShowsearch(String showsearch) {
        this.showsearch = showsearch;
    }

    public String getArcgisUrl() {
        return arcgisUrl;
    }

    public void setArcgisUrl(String arcgisUrl) {
        this.arcgisUrl = arcgisUrl;
    }

    public String getIsTransfer() {
        return isTransfer;
    }

    public void setIsTransfer(String isTransfer) {
        this.isTransfer = isTransfer;
    }

    public String getGisType() {
        return gisType;
    }

    public void setGisType(String gisType) {
        this.gisType = gisType;
    }
}
