$(function () {
	$('.nav-slide').click(function () {
		$('.nav-list').css('overflow', 'hidden');
		var navList = $('.nav-list').css('display');
		if (navList == 'block') {
			$(this).find('i.fa-list').removeClass('fa-list').addClass('fa-navicon');
			$(this).children('.nav-btn').removeClass('active');
			/*$('.nav-list').show().animate({
				width : 0
			}, 80).hide();*/
			$('.nav-list').hide();
		} else {
			$(this).find('i.fa-navicon').removeClass('fa-navicon').addClass('fa-list');
			$(this).children('.nav-btn').addClass('active');
			/*$('.nav-list').show().animate({
				width : 259
			}, 80);*/
			$('.nav-list').show();
		}
	});

	$('.close').click(function () {
		$(this).closest('.modal-nav').animate({
			height : 'auto'
		}, 100).hide();
	});

	$('#nav_fir').click(function () {
		var modalPanel = $('#modal_nav_fir');
		var modalNavH = $('#modal_nav_fir').height();
		modalPanel.css('height', '0');
		modalPanel.animate({
			height : modalNavH
		}, 100).show();
	});

	$('.minus').click(function () {
		$(this).closest('.modal-nav').css('overflow', 'hidden').animate({
			height : 32,
			bottom : 72
		});
	});

	/**
	 * bootstrap 下拉框
	 */
	//工具栏弹出
	$('.tabs-modal').css({
		height : 0,
		overflow : 'hidden'
	});
	$('.toolbar li').click(function () {
		$(this).children('.tabs-modal').animate({
			height : 300
		});
	});
});

//十六进制颜色值域RGB格式颜色值之间的相互转换
//-------------------------------------
//十六进制颜色值的正则表达式
//var sRgb = "RGB(255, 255, 255)" , sHex = "#00538b";
//var sHexColor = sRgb.colorHex();//转换为十六进制方法<code></code>
//var sRgbColor = sHex.colorRgb();//转为RGB颜色值的方法

var reg_string_color_convert = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/;
/*RGB颜色转换为16进制*/
String.prototype.colorHex = function () {
	var that = this;
	if (/^(rgb|RGB)/.test(that)) {
		var aColor = that.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(",");
		var strHex = "#";
		for (var i = 0; i < aColor.length; i++) {
			var hex = Number(aColor[i]).toString(16);
			if (hex === "0") {
				hex += hex;
			}
			strHex += hex;
		}
		if (strHex.length !== 7) {
			strHex = that;
		}
		return strHex;
	} else if (reg_string_color_convert.test(that)) {
		var aNum = that.replace(/#/, "").split("");
		if (aNum.length === 6) {
			return that;
		} else if (aNum.length === 3) {
			var numHex = "#";
			for (var i = 0; i < aNum.length; i += 1) {
				numHex += (aNum[i] + aNum[i]);
			}
			return numHex;
		}
	} else {
		return that;
	}
};
//-------------------------------------------------
/*16进制颜色转为RGB格式*/
String.prototype.colorRgb = function () {
	var sColor = this.toLowerCase();
	if (sColor && reg_string_color_convert.test(sColor)) {
		if (sColor.length === 4) {
			var sColorNew = "#";
			for (var i = 1; i < 4; i += 1) {
				sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
			}
			sColor = sColorNew;
		}
		//处理六位的颜色值
		var sColorChange = [];
		for (var i = 1; i < 7; i += 2) {
			sColorChange.push(parseInt("0x" + sColor.slice(i, i + 2)));
		}
		return "RGB(" + sColorChange.join(",") + ")";
	} else {
		return sColor;
	}
};

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符，
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字)
// 例子：
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18
Date.prototype.Format = function (fmt) { //author: meizz
	var o = {
		"M+": this.getMonth() + 1, //月份
		"d+": this.getDate(), //日
		"h+": this.getHours(), //小时
		"m+": this.getMinutes(), //分
		"s+": this.getSeconds(), //秒
		"q+": Math.floor((this.getMonth() + 3) / 3), //季度
		"S": this.getMilliseconds() //毫秒
	};
	if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	for (var k in o)
		if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	return fmt;
}
/**
 * 计算当前月一共多少天
 * @param y 年,若为空则为当前时间
 * @param Mm 月,0开始,若为空则为当前时间
 * @returns {*}
 */
Date.getDayOfMonth = function (y, Mm) {
	if (typeof y == 'undefined') { y = (new Date()).getFullYear(); }
	if (typeof Mm == 'undefined') { Mm = (new Date()).getMonth(); }
	var Feb = (y % 4 == 0) ? 29 : 28;
	var aM = new Array(31, Feb, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	return  aM[Mm];
};
/**
 * 返回上一月的日期
 * @param dt 参照日期，若为空则为当前时间
 * @returns {Date}
 */
Date.getDateOfPreMonth = function (dt) {
	if (typeof dt == 'undefined') { dt = (new Date()); }
	var y = (dt.getMonth() == 0) ? (dt.getFullYear() - 1) : dt.getFullYear();
	var m = (dt.getMonth() == 0) ? 11 : dt.getMonth() - 1;
	var preM = Date.getDayOfMonth(y, m);
	var d = (preM < dt.getDate()) ? preM : dt.getDate();
	return new Date(y, m, d);
};

var dynamicLoading = {
	css: function(path){
		if(!path || path.length === 0){
			throw new Error('argument "path" is required !');
		}
		var head = document.getElementsByTagName('head')[0];
		var link = document.createElement('link');
		link.href = path;
		link.rel = 'stylesheet';
		link.type = 'text/css';
		head.appendChild(link);
	},
	js: function(path){
		if(!path || path.length === 0){
			throw new Error('argument "path" is required !');
		}
		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.src = path;
		script.type = 'text/javascript';
		head.appendChild(script);
	}
}