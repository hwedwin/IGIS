package gis.toolkit.Bean;

import gis.algorithm.GeoPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xiaobing on 2016/10/31.
 */
public class RegionRing {
    private  int id;
    private String name;
    private List<GeoPoint> rings = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GeoPoint> getRings() {
        return rings;
    }

    public void setRings(List<GeoPoint> rings) {
        this.rings = rings;
    }
}
