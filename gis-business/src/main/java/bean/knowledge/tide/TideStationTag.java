package bean.knowledge.tide;

import gis.core.bean.KeyValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xiexb on 2017/8/1.
 */
public class TideStationTag {
    private List<TideStationBean> hotStations = new ArrayList<>();
    private List<KeyValue> StationTag = new ArrayList<>();

    public List<TideStationBean> getHotStations() {
        return hotStations;
    }

    public void setHotStations(List<TideStationBean> hotStations) {
        this.hotStations = hotStations;
    }

    public List<KeyValue> getStationTag() {
        return StationTag;
    }

    public void setStationTag(List<KeyValue> stationTag) {
        StationTag = stationTag;
    }
}
