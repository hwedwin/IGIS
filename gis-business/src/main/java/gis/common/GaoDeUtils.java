package gis.common;

import gis.algorithm.PointTransform;
//import org.gds.offset.CoordOffset;

/**
 * Created by Xiaobing on 2016/5/5.
 */
public class GaoDeUtils {
    private static String path = GisConfig.GetInstance().getBasePath() + "files/packfile.dat";
    //private static CoordOffset offset = CoordOffset.getInstance(path, true);
    private static MapFix fix = MapFix.getInstance();

    /**
     * 高德提供 火星坐标（wgs84）到 GPS 坐标
     *
     * @param x 经度
     * @param y 纬度
     * @return
     */
    public static double[] offset(double x, double y) {
        double[] e = PointTransform.gcj_decrypt(y, x);
        double[] xy = new double[2];
        //offset.offsetCoord(e[1], e[0], xy);
        return xy;
    }

    /**
     * 高德提供 火星坐标（wgs84）到 GPS 坐标
     *
     * @param x 经度
     * @param y 纬度
     * @return
     */
    /*public static double[] offsetGd(double x, double y) {//.gcj_encrypt(y, x);
        //e=PointTransform.gcj_encrypt(e[0],e[1]);
        double[] xy1 = new double[2];
        offset.offsetCoord(x, y, xy1);
        double[] e1 = PointTransform.gcj_decrypt(xy1[1], xy1[0]);
        xy1[0] = e1[1];
        xy1[1] = e1[0];

        double[] e = PointTransform.gcj_encrypt(y, x);//.gcj_encrypt(y, x);
        //e=PointTransform.gcj_encrypt(e[0],e[1]);
        double[] xy2 = new double[2];
        xy2[0]=e[1];
        xy2[1]=e[0];
        //offset.offsetCoord(e[1], e[0], xy);

        double[] xy = new double[2];
        xy[1]=2*xy1[1]-xy2[1];
        xy[0]= 2*xy1[0]-xy2[0];
        return xy;
    }*/
    public static double[] offsetGd(double x, double y) {
        //double[] e = PointTransform.gcj_decrypt(y, x);
        //double[] e=PositionUtil.gcj_To_Gps84(y,x);
        //double[] e =fix.fix(x,y);
        //double[] e =EvilTransform.transform(y,x);
        double[] xy = new double[2];
        //xy[0]=e[0];
        //xy[1]=e[1];
        //offset.offsetCoord(x, y, xy);
        xy[0] = 2*x-xy[0];
        xy[1] = 2*y-xy[1];

        /*offset.offsetCoord(x, y, xy);
        x = 2 * x - xy[0];
        y = 2 * y - xy[1];
        double[] e1= PointTransform.gcj_encrypt(y,x);
        xy[0] = e1[1];
        xy[1] = e1[0];*/

        /*double[] xy1 = new double[2];
        offset.offsetCoord(x, y, xy1);

        double[] xy2 = new double[2];
        offset.offsetCoord(xy1[0], xy1[1], xy2);
        xy[0] = 2*xy1[0]-xy2[0];
        xy[1] = 2*xy1[1]-xy2[1];*/



        /*double[] e1 = PointTransform.gcj_decrypt(xy[1], xy[0]);
        double[] e2 = PointTransform.gcj_decrypt(e1[0], e1[1]);
        xy[0] = e2[1];
        xy[1] = e2[0];*/
        /*double[] temp = GaoDeUtils.offset(xy[0], xy[1]);
        double x1 = temp[0] - xy[0];
        double y1 = temp[1] - xy[1];
        xy[0] = xy[0] - 8 * x1;*/
        return xy;
    }
}
