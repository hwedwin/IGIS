/**
 * Created by Xiaobing on 2017/3/29.
 */
function HighSpeedRailBJ() {
    var wmsLayer = null; //渲染图层

    //渲染栅格

    var loadWmsLayer = function (url) {//添加渲染图层
        if (wmsLayer != null || wmsLayer != undefined) {
            gis_core.removeLayer("highspeedrailbjgridwmsLayer")
        }
        if (url == null || url == undefined) {
            return;
        }
        wmsLayer = new esri.layers.WMSLayer(url, {
            resourceInfo: {
                extent: new esri.geometry.Extent(73.2672757, 18.37574, -109.66552734375, 133.29657, {
                    wkid: 4326
                }),
                layerInfos: []
            },
            visibleLayers: ['highspeedrailbjgrid']
        });
        wmsLayer.id = "highspeedrailbjgridwmsLayer";
        gis_core.insertLayer("highspeedrailbjgridwmsLayer", "网格覆盖", 2, wmsLayer);
    };


    /**
     * 初始化
     */
    this.init = function () {
        //加载库文件
        dojo.require("esri.layers.WMSLayerInfo");
        dojo.require("esri.layers.WMSLayer");
        //gis_core.addExtentChange("highspeedrailbj", extentchange);
        gis_core.addPlugin(this);


    };
    /**
     * 销毁
     */
    this.destroy = function () {
        gis_layout.removeLeftWidget();
        gis_layout.removeRightWidget();
        gis_core.removeLayer("highspeedrailbjgridwmsLayer")
        highSpeedRail = null;
    };
    /**
     * 模块信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "HighSpeedRailBJ",
        name: "高铁分析"
    };
    /**
     * 渲染栅格
     */
    this.loadGrid = function () {
        var url = "/dynamicmap/highspeedrailbjgridexpend/kpi/param";
        loadWmsLayer(url);
    };
}

var highSpeedRailBJ = null;
function initHightSpeedRailBJ() {
    if (highSpeedRailBJ == null) {
        highSpeedRailBJ = new HighSpeedRailBJ();
        highSpeedRailBJ.init();
    }
}
function highSpeedRailbj_loadGrid(obj) {
    initHightSpeedRailBJ();
    gis_layout.createMenubar("highSpeedRailbj_loadGrid", obj, highSpeedRailBJ.loadGrid)
    //gis_layout.createMenubar("school_createQueryParamHtml", obj, schoolObj.initData)
}