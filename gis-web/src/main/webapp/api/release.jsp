<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
  <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
  <%@ include file="header.jsp" %>
  <section>
    <div class="container">
      <%@ include file="leftmenu.jsp" %>
      <input id="pageid" type="hidden" value="release">
      <div class="docs-content" style="height: 850px;">
        <h3 id="welcome" class="welcome"> 部署说明</h3>
        <p>部署一共分为 打包及站点维护</p>

        <br/>
        <h3 id="welcome" class="welcome"> 打包</h3>
        <p>1.本项目是用maven管理的项目，因此，使用maven进行打包即可，为了每次能保证代码打包最新，因此，必须先clean,然后再compile,package,最后install</p>
        <p>2.项目打包顺序，首先打包 gis-alorithm,gis-business,gis-core。最后打包gis-web。</p>
        <p>3.通常情况下只会修改gis-web项目，因此只需要打包gis-web项目即可。</p>
        <br/>

        <h3 id="welcome" class="welcome"> 站点维护</h3>
        <p>1.站点使用jetty作为服务站点，所有维护操作和jetty操作一样。下面以例子讲述站点维护：</p>
        <p>站点部署目录为：jetty-igis-5898</p>
        <pre class="prettyprint">
    站点启动：jetty-igis-5898/bin/start.sh
    站点停止：jetty-igis-5898/bin/stop.sh
    站点重启：jetty-igis-5898/bin/restart.sh
    启动日志：jetty-igis-5898/logs
    运行日志：jetty-igis-5898/log
        </pre>
        <a href="rendering.jsp" type="button" class="btn-link">上一页</a>
        <%--<a href="/api/igis.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>--%>

      </div>
    </div>
  </section>
</div>
</body>
</html>
