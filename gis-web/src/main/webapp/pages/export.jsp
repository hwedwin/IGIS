<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.slf4j.Logger" %>
<%@ page import="org.slf4j.LoggerFactory" %>
<%@ page import="gis.controller.Common" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%
    Logger logger = LoggerFactory.getLogger(Common.class);
    try {
        String fileId = request.getParameter("fid");
        String type = request.getParameter("typ");
        if (!StringUtils.isNotEmpty(type)) {
            type = "application/csv";
        }
        if (fileId != null && !fileId.equals("")) {
            java.io.OutputStream o = response.getOutputStream();
            byte b[] = new byte[500];
            String filePath = this.getClass().getResource("/temp/" + fileId).getFile();
            java.io.File fileLoad = new java.io.File(filePath);

            response.reset();
            response.setContentType(type);
            response.setHeader("content-disposition", "attachment; filename=" + fileId);
            long fileLength = fileLoad.length();
            String length1 = String.valueOf(fileLength);
            response.setHeader("Content_Length", length1);
            java.io.FileInputStream in = new java.io.FileInputStream(fileLoad);
            int n;
            while ((n = in.read(b)) != -1) {
                o.write(b, 0, n);
            }
            in.close();
            o.close();
        }
    } catch (Exception e) {
        logger.error("文件导出出错:" + e.getMessage(), e);
    }
%>