package bean.common;

/**
 * Created by hupeng on 16/6/7.
 * 用户圈定的场景维度（公共功能）
 */
public class CellDimScene {
    /**
     * 场景标识
     */
    private String scene_id;
    /**
     * 场景名称
     */
    private String scene_name;
    /**
     * 场景类型标识
     */
    private String scene_type_id;
    /**
     * 场景类型名称
     */
    private String scene_type_name;
    /**
     * 场景边界
     */
    private String range;
    /**
     * 地市ID
     */
    private String cityId;
    /**
     * 地市名称
     */
    private String cityName;
    /**
     * 场景小区类型
     */
    private String cellTypes;
    /**
     * 应用专题ID
     */
    private String appTopicsId;
    /**
     * 应用专题名称
     */
    private String appTopicsName;


    public String getScene_id() {
        return scene_id;
    }

    public void setScene_id(String scene_id) {
        this.scene_id = scene_id;
    }

    public String getScene_name() {
        return scene_name;
    }

    public void setScene_name(String scene_name) {
        this.scene_name = scene_name;
    }

    public String getScene_type_id() {
        return scene_type_id;
    }

    public void setScene_type_id(String scene_type_id) {
        this.scene_type_id = scene_type_id;
    }

    public String getScene_type_name() {
        return scene_type_name;
    }

    public void setScene_type_name(String scene_type_name) {
        this.scene_type_name = scene_type_name;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCellTypes() {
        return cellTypes;
    }

    public void setCellTypes(String cellTypes) {
        this.cellTypes = cellTypes;
    }

    public String getAppTopicsId() {
        return appTopicsId;
    }

    public void setAppTopicsId(String appTopicsId) {
        this.appTopicsId = appTopicsId;
    }

    public String getAppTopicsName() {
        return appTopicsName;
    }

    public void setAppTopicsName(String appTopicsName) {
        this.appTopicsName = appTopicsName;
    }
}
