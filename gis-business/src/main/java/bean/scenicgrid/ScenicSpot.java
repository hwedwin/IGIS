package bean.scenicgrid;

/**
 * Created by hupeng on 16/6/13.
 * 景区数据
 */
public class ScenicSpot {
    /**
     * 风景区标识 bigint
     */
    private String spot_id;

    /**
     * 风景区名称 string
     */
    private String spot_name;
    /**
     * 地市标识 int
     */
    private String city_id;
    /**
     * 地市标识
     */
    private String city_name;
    /**
     * 边界
     */
    private String bord;
    /**
     * 标签
     */
    private String labels;

    public String getSpot_id() {
        return spot_id;
    }

    public void setSpot_id(String spot_id) {
        this.spot_id = spot_id;
    }

    public String getSpot_name() {
        return spot_name;
    }

    public void setSpot_name(String spot_name) {
        this.spot_name = spot_name;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getBord() {
        return bord;
    }

    public void setBord(String bord) {
        this.bord = bord;
    }

    public String getLabels() {
        return labels;
    }

    public void setLabels(String labels) {
        this.labels = labels;
    }
}
