package bean.knowledge.tide;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by xiexb on 2017/8/17.
 */
public class WeatherBean {
    private String stationNo;
    private String stationName;
    private String ew;
    private String ns;
    private String timeZone;
    //private Date time;
    private long time;
    private String timeLable;
    private String weather;
    private String temp;
    private String winD;
    private String winS;
    private int dataType;
    private String countryName;
    private String weatherLable;
    private List<WeatherKpiBean> kpiBeanList;

    public String getStationNo() {
        return stationNo;
    }

    public void setStationNo(String stationNo) {
        this.stationNo = stationNo;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getEw() {
        return ew;
    }

    public void setEw(String ew) {
        this.ew = ew;
    }

    public String getNs() {
        return ns;
    }

    public void setNs(String ns) {
        this.ns = ns;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getTimeLable() {
        return timeLable;
    }

    public void setTimeLable(String timeLable) {
        this.timeLable = timeLable;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getWinD() {
        return winD;
    }

    public void setWinD(String winD) {
        this.winD = winD;
    }

    public String getWinS() {
        return winS;
    }

    public void setWinS(String winS) {
        this.winS = winS;
    }

    public int getDataType() {
        return dataType;
    }

    public void setDataType(int dataType) {
        this.dataType = dataType;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getWeatherLable() {
        return weatherLable;
    }

    public void setWeatherLable(String weatherLable) {
        this.weatherLable = weatherLable;
    }

    public List<WeatherKpiBean> getKpiBeanList() {
        return kpiBeanList;
    }

    public void setKpiBeanList(List<WeatherKpiBean> kpiBeanList) {
        this.kpiBeanList = kpiBeanList;
    }
}
