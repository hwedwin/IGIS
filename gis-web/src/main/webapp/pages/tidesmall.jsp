<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="renderer" content="ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=7,IE=9">
    <meta http-equiv="X-UA-Compatible" content="IE=7,9">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">
    <title>潮汐应用</title>

    <link rel="stylesheet" type="text/css" href="/css/modules/tide/css/c-data-small.css"/>
    <link rel="stylesheet" type="text/css" href="/script/base/artcloudui/skins/default/css/artcloudui.css">
    <script type="text/javascript" src="/script/base/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="/script/plugins/scrollbar/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="/script/base/artcloudui/artcloudui.dialog.min.js"></script>
    <script type="text/javascript" src="/script/base/json-js-master/json2.js"></script>
    <script type="text/javascript" src="/script/base.js"></script>
    <script type="text/javascript" src="/script/plugins/echartslcz/echarts.js"></script>
    <script type="text/javascript">
        requiretx.config({
            paths: {
                echarts: '/script/plugins/echartslcz/'
            }
        });
    </script>
    <script type="text/javascript" src="/script/modules/knowledge/tidesmall.js"></script>
</head>
<body>
<div class="tide-data">
    <div class="graph">
        <h3 class="reset-h3">潮汐曲线图</h3>

        <div id="tide-data-hour-line" class="graph-cont">

        </div>
    </div>
    <div class="port">
        <h2 class="reset-h2" id="tide-station-detail-title"><span>无数据</span></h2>
        <%--<h3 class="reset-h3 m-18">潮汐表</h3>--%>
        <div class="port-cont">
            <table class="data-table-port">
                <tbody id="tide-data-day-table">
                <%--<tr>
                    <td>潮时(Hrs)</td>
                    <td>03:49</td>
                    <td>11:01</td>
                    <td>03:49</td>
                    <td>11:01</td>
                </tr>
                <tr>
                    <td>潮高(cm)</td>
                    <td>25</td>
                    <td>12</td>
                    <td>4</td>
                    <td>134</td>
                </tr>
                <tr>
                    <td colspan="5" class="sp-td" style="font-size:10px;">
                        时区:<span class="m-r-10"> -0800  (东八区) </span> 潮高基准面：在平均海平面上下241CN
                    </td>
                </tr>--%>
                </tbody>
            </table>
        </div>
    </div>
    <div class="weather">
        <%--<h3 class="reset-h3">气象数据</h3>--%>
        <%-- <h3 class="reset-h3"></h3>--%>
        <div class="mete-data"><!--气象数据-->
            <div class="mete-cont">
                <a href="javascript:void(0);" class="front-scroll-ico"></a>
                <a href="javascript:void(0);" class="back-scroll-ico"></a>

                <div class="mete-cont-list">
                    <ul id="tide-ul-weather-item">
                        <%-- <li>
                             <p>08/17 15时</p>

                             <p>小雨转阴</p>

                             <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                             <p>21~29.</p>

                             <p>西北风</p>

                             <p>3.6 km/h</p>
                         </li>
                         <li>
                             <p>08/17 15时</p>

                             <p>小雨转阴</p>

                             <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                             <p>21~29.</p>

                             <p>西北风</p>

                             <p>3.6 km/h</p>
                         </li>
                         <li>
                             <p>08/17 15时</p>

                             <p>小雨转阴</p>

                             <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                             <p>21~29.</p>

                             <p>西北风</p>

                             <p>3.6 km/h</p>
                         </li>
                         <li>
                             <p>08/17 15时</p>

                             <p>小雨转阴转阴</p>

                             <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                             <p>21~29.</p>

                             <p>西北风</p>

                             <p>3.6 km/h</p>
                         </li>
                         <li>
                             <p>08/17 15时</p>

                             <p>小雨转阴</p>

                             <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                             <p>21~29.</p>

                             <p>西北风</p>

                             <p>3.6 km/h</p>
                         </li>
                         <li>
                             <p>08/17 15时</p>

                             <p>小雨转阴</p>

                             <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                             <p>21~29.</p>

                             <p>西北风</p>

                             <p>3.6 km/h</p>
                         </li>
                         <li>
                             <p>08/17 15时</p>

                             <p>小雨转阴</p>

                             <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                             <p>21~29.</p>

                             <p>西北风</p>

                             <p>3.6 km/h</p>
                         </li>
                         <li>
                             <p>08/17 15时</p>

                             <p>小雨转阴转阴</p>

                             <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                             <p>21~29.</p>

                             <p>西北风</p>

                             <p>3.6 km/h</p>
                         </li>
                         <li>
                             <p>08/17 15时</p>

                             <p>小雨转阴</p>

                             <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                             <p>21~29.</p>

                             <p>西北风</p>

                             <p>3.6 km/h</p>
                         </li>
                         <li>
                             <p>08/17 15时</p>

                             <p>小雨转阴</p>

                             <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                             <p>21~29.</p>

                             <p>西北风</p>

                             <p>3.6 km/h</p>
                         </li>
                         <li>
                             <p>08/17 15时</p>

                             <p>小雨转阴转阴</p>

                             <p><img src="/css/modules/tide/img/c-weather-ico/0.png" width="30"></p>

                             <p>21~29.</p>

                             <p>西北风</p>

                             <p>3.6 km/h</p>
                         </li>--%>
                    </ul>
                </div>
            </div>
        </div>
        <%--<div class="weather-cont" style="margin-top: 20px;">
            <table class="data-table-weather">
                <thead id="weater-table-title">
                <tr>
                    <th>6时</th>
                    <th>7时</th>
                    <th>8时</th>
                    <th>9时</th>
                    <th>10时</th>
                    <th>11时</th>
                    <th>10时</th>
                    <th>11时</th>
                </tr>
                </thead>
                <tbody id="weater-table-detail">
                &lt;%&ndash;<tr>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                </tr>&ndash;%&gt;
                <tr class="temp">
                    <td class="invalid">18∘</td>
                    <td>25°C</td>
                    <td>12°C</td>
                    <td>4°C</td>
                    <td>34°C</td>
                    <td>19°C</td>
                    <td>34°C</td>
                    <td>19°C</td>
                </tr>
                <tr>
                    <td class="invalid">昨天</td>
                    <td>今天</td>
                    <td>周三</td>
                    <td>周四</td>
                    <td>周五</td>
                    <td>周六</td>
                    <td>周五</td>
                    <td>周六</td>
                </tr>
                <tr>
                    <td class="invalid">03/31</td>
                    <td>04/1</td>
                    <td>04/2</td>
                    <td>04/3</td>
                    <td>04/4</td>
                    <td>04/5</td>
                    <td>04/4</td>
                    <td>04/5</td>
                </tr>
                <tr class="wea">
                    <td class="invalid">多云</td>
                    <td>雷阵雨</td>
                    <td>多云</td>
                    <td>雷阵雨</td>
                    <td>多云</td>
                    <td>雷阵雨</td>
                    <td>多云</td>
                    <td>雷阵雨</td>
                </tr>
                <tr>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                    <td><img style="width: 35px;height: 35px;" src="/css/modules/tide/img/c-weather-ico/0.png"></td>
                </tr>
                </tbody>
            </table>
        </div>--%>
    </div>
    <div class="c-tip">
        数据来源于:国家海洋信息中心、国家基础地理信息中心、国家气象信息中心、中国工程科技知识中心
    </div>
</div>

<script>
    var _hmt = _hmt || [];
    (function () {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?789fd650fa0be6a2a064d019d890b87f";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

</body>
</html>
