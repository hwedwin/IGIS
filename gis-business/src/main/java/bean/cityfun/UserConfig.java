package bean.cityfun;

/**
 * Created by hupeng on 16/5/16.
 */
public class UserConfig {

    /**
     * id
     */
    private String id;
    /**
     * scene_id
     */
    private String scene_id;
    /**
     * 场景名称
     */
    private String scene_name;
    /**
     * 图形类型\n1 矩形 2 多边形
     */
    private String graphic_type;

    /**
     * 图形类型\n1 矩形 2 多边形
     */
    private String rings;

    /**
     * 场景分类
     */
    private String scene_type;
    /**
     * 场景分类名称
     */
    private String scene_type_name;
    /**
     * 可视区域范围
     */
    private String area_extend;


    /**
     * 兴趣点分类
     */
    private String poi_category;


    public String getArea_extend() {
        return area_extend;
    }

    public void setArea_extend(String area_extend) {
        this.area_extend = area_extend;
    }

    public String getScene_type_name() {
        return scene_type_name;
    }

    public void setScene_type_name(String scene_type_name) {
        this.scene_type_name = scene_type_name;
    }

    public String getScene_type() {
        return scene_type;
    }

    public void setScene_type(String scene_type) {
        this.scene_type = scene_type;
    }

    public String getRings() {
        return rings;
    }

    public void setRings(String rings) {
        this.rings = rings;
    }

    public String getGraphic_type() {
        return graphic_type;
    }

    public void setGraphic_type(String graphic_type) {
        this.graphic_type = graphic_type;
    }

    public String getScene_name() {
        return scene_name;
    }

    public void setScene_name(String scene_name) {
        this.scene_name = scene_name;
    }

    public String getScene_id() {
        return scene_id;
    }

    public void setScene_id(String scene_id) {
        this.scene_id = scene_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPoi_category() {
        return poi_category;
    }

    public void setPoi_category(String poi_category) {
        this.poi_category = poi_category;
    }
}
