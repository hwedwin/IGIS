package bean.cityfun;

/**
 * Created by hupeng on 16/5/30.
 * <p/>
 * 自定义场景分类
 */
public class AreaDimSceneType {
    /**
     * 场景分类标识
     */
    private String scene_type_id;
    /**
     * 场景分类名称
     */
    private String scene_type_name;

    public String getScene_type_id() {
        return scene_type_id;
    }

    public void setScene_type_id(String scene_type_id) {
        this.scene_type_id = scene_type_id;
    }

    public String getScene_type_name() {
        return scene_type_name;
    }

    public void setScene_type_name(String scene_type_name) {
        this.scene_type_name = scene_type_name;
    }

    public AreaDimSceneType() {
    }

    public AreaDimSceneType(String scene_type_id, String scene_type_name) {
        this.scene_type_id = scene_type_id;
        this.scene_type_name = scene_type_name;
    }
}
