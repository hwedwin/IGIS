/**
 * Created by Xiaobing on 2016/7/18.
 * 江苏移动 欺诈流量 专题  GIS部分 00_04_00
 */
function CheatFlow() {
    /**
     * 初始化
     */
    this.init = function () {
        //注册模块
        gis_core.addPlugin(this);
    };
    /**
     * 销毁
     */
    this.destroy = function () {
    };
    /**
     * 模块信息
     * @type {{id: string, name: string}}
     */
    this.title = {
        id: "00_04_00",
        name: "拓扑GIS"
    };
}

var cheatFlow = null;

function initCheatFlow() {
    if (cheatFlow == null) {
        cheatFlow = new CheatFlow();
    }
    cheatFlow.init();
}

function cheatFlow_trail(obj){
    initCheatFlow();
}