package bean.common;

import bean.enums.ResType;
import gis.core.bean.ResValue;

/**
 * 资源
 * @author Xiaobing
 *
 */
public class Res extends ResValue {
	private double originalLongitude;
	private double originalLatitude;
	private String cityId;
	private String cityName;
	private String countryId;
	private String countyName;
	private ResType resType;

	/**
	 * 原始经度
	 * @return
	 */
	public double getOriginalLongitude() {
		return originalLongitude;
	}

	/**
	 * 原始经度
	 * @param originalLongitude
	 */
	public void setOriginalLongitude(double originalLongitude) {
		this.originalLongitude = originalLongitude;
	}

	/**
	 * 原始纬度
	 * @return
	 */
	public double getOriginalLatitude() {
		return originalLatitude;
	}

	/**
	 * 原始纬度
	 * @param originalLatitude
	 */
	public void setOriginalLatitude(double originalLatitude) {
		this.originalLatitude = originalLatitude;
	}

	/**
	 * 地市ID
	 * @return
	 */
	public String getCityId() {
		return cityId;
	}

	/**
	 * 地市ID
	 * @param cityId
	 */
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	/**
	 * 地市名称
	 * @return
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * 地市名称
	 * @param cityName
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/**
	 * 区县ID
	 * @return
	 */
	public String getCountryId() {
		return countryId;
	}

	/**
	 * 区县ID
	 * @param countryId
	 */
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	/**
	 * 区县名称
	 * @return
	 */
	public String getCountyName() {
		return countyName;
	}

	/**
	 * 区县名称
	 * @param countyName
	 */
	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	/**
	 * 资源类型
	 * @return
	 */
	public ResType getResType() {
		return resType;
	}

	/**
	 * 资源类型
	 * @param resType
	 */
	public void setResType(ResType resType) {
		this.resType = resType;
	}

}
