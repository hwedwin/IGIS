define(["dojo/_base/declare", "esri/layers/tiled"],
	function (declare) {
	return declare(esri.layers.TiledMapServiceLayer, {
		constructor : function () {
			this.spatialReference = new esri.SpatialReference({
					wkid : 4326
				});
			this.initialExtent = new esri.geometry.Extent(
					103.47903736310876, 29.246665555064755,
					112.01059044576762, 32.365727972380895,
					this.spatialReference);
			this.fullExtent = new esri.geometry.Extent(
					103.21998810833078, 27.43766789216696,
					112.4237432788823, 33.024642333856455,
					this.spatialReference);
			this.tileInfo = new esri.layers.TileInfo({
					"rows" : 256,
					"cols" : 256,
					"dpi" : 96,
					"origin" : {
						"x" : -400.0,
						"y" : 400.0
					},
					"spatialReference" : {
						"wkid" : 4326
					},

					"lods" : [{
							"level" : 0,
							"resolution" : 0.01903568804664224,
							"scale" : 8000000
						}, {
							"level" : 1,
							"resolution" : 0.00951784402332112,
							"scale" : 4000000
						}, {
							"level" : 2,
							"resolution" : 0.00475892201166056,
							"scale" : 2000000
						}, {
							"level" : 3,
							"resolution" : 0.00237946100583028,
							"scale" : 1000000
						}, {
							"level" : 4,
							"resolution" : 0.00118973050291514,
							"scale" : 500000
						}, {
							"level" : 5,
							"resolution" : 5.9486525145757E-4,
							"scale" : 250000
						}, {
							"level" : 6,
							"resolution" : 2.97432625728785E-4,
							"scale" : 125000
						}, {
							"level" : 7,
							"resolution" : 1.5228550437313792E-4,
							"scale" : 64000
						}, {
							"level" : 8,
							"resolution" : 7.614275218656896E-5,
							"scale" : 32000
						}, {
							"level" : 9,
							"resolution" : 3.807137609328448E-5,
							"scale" : 16000
						}, {
							"level" : 10,
							"resolution" : 1.903568804664224E-5,
							"scale" : 8000
						}, {
							"level" : 11,
							"resolution" : 9.51784402332112E-6,
							"scale" : 4000
						}, {
							"level" : 12,
							"resolution" : 4.75892201166056E-6,
							"scale" : 2000
						}, {
							"level" : 13,
							"resolution" : 2.37946100583028E-6,
							"scale" : 1000
						}, {
							"level" : 14,
							"resolution" : 7.13838301749084E-7,
							"scale" : 300
						}
					]
				});
			this.loaded = true;
			this.onLoad(this);
		},
		getTileUrl : function (level, row, col) {
			return "http://10.190.95.90:6080/arcgis/rest/services/cqesri/MapServer/tile/"
			 + level + "/" + row + "/" + col;
		}
	});
});
