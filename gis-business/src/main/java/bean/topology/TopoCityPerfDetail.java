package bean.topology;

/**
 * Created by Xiaobing on 2016/6/28.
 */
public class TopoCityPerfDetail {
    /**
     * 时间
     */
    private String time;
    /**
     * 地市
     */
    private String cityName;
    /**
     * 机房
     */
    private String room;
    /**
     * 小区总数
     */
    private int cellCount;
    /**
     * 关站小区数
     */
    private int closedCellCount;
    /**
     * 告警小区
     */
    private int alarmCellCount;
    /**
     * 性能差小区
     */
    private int badCellCount;
    /**
     * 告警小区用户数
     */
    private int alarmCellUserCount;
    /**
     *性能差小区用户数
     */
    private int badCellUserCount;
    /**
     * 告警小区流量
     */
    private double alarmCellFlow;
    /**
     * 性能差小区流量
     */
    private double badCellFlow;

    /**
    * 附着成功率低小区占比
     */
    private double badAttachCellRat;

    /**
     *承载建立成功率低小区占比
     */
    private double badEpsCellRat;

    public double getBadAttachCellRat() {
        return badAttachCellRat;
    }

    public double getBadEpsCellRat() {
        return badEpsCellRat;
    }

    public void setBadAttachCellRat(double badAttachCellRat) {
        this.badAttachCellRat = badAttachCellRat;
    }

    public void setBadEpsCellRat(double badEpsCellRat) {
        this.badEpsCellRat = badEpsCellRat;
    }

    /**
     * 时间
     * @return
     */
    public String getTime() {
        return time;
    }

    /**
     * 时间
     * @param time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * 地市
     * @return
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * 地市
     * @param cityName
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * 机房
     * @return
     */
    public String getRoom() {
        return room;
    }

    /**
     * 机房
     * @param room
     */
    public void setRoom(String room) {
        this.room = room;
    }

    /**
     * 小区总数
     * @return
     */
    public int getCellCount() {
        return cellCount;
    }

    /**
     * 小区总数
     * @param cellCount
     */
    public void setCellCount(int cellCount) {
        this.cellCount = cellCount;
    }

    /**
     * 关站小区数
     * @return
     */
    public int getClosedCellCount() {
        return closedCellCount;
    }

    /**
     * 关站小区数
     * @param closedCellCount
     */
    public void setClosedCellCount(int closedCellCount) {
        this.closedCellCount = closedCellCount;
    }

    /**
     * 告警小区
     * @return
     */
    public int getAlarmCellCount() {
        return alarmCellCount;
    }

    /**
     * 告警小区
     * @param alarmCellCount
     */
    public void setAlarmCellCount(int alarmCellCount) {
        this.alarmCellCount = alarmCellCount;
    }

    /**
     * 性能差小区
     * @return
     */
    public int getBadCellCount() {
        return badCellCount;
    }

    /**
     * 性能差小区
     * @param badCellCount
     */
    public void setBadCellCount(int badCellCount) {
        this.badCellCount = badCellCount;
    }

    /**
     * 告警小区用户数
     * @return
     */
    public int getAlarmCellUserCount() {
        return alarmCellUserCount;
    }

    /**
     * 告警小区用户数
     * @param alarmCellUserCount
     */
    public void setAlarmCellUserCount(int alarmCellUserCount) {
        this.alarmCellUserCount = alarmCellUserCount;
    }

    /**
     * 性能差小区用户数
     * @return
     */
    public int getBadCellUserCount() {
        return badCellUserCount;
    }

    /**
     * 性能差小区用户数
     * @param badCellUserCount
     */
    public void setBadCellUserCount(int badCellUserCount) {
        this.badCellUserCount = badCellUserCount;
    }

    /**
     * 告警小区流量
     * @return
     */
    public double getAlarmCellFlow() {
        return alarmCellFlow;
    }

    /**
     * 告警小区流量
     * @param alarmCellFlow
     */
    public void setAlarmCellFlow(double alarmCellFlow) {
        this.alarmCellFlow = alarmCellFlow;
    }

    /**
     * 性能差小区流量
     * @return
     */
    public double getBadCellFlow() {
        return badCellFlow;
    }

    /**
     * 性能差小区流量
     * @param badCellFlow
     */
    public void setBadCellFlow(double badCellFlow) {
        this.badCellFlow = badCellFlow;
    }
}
