package bean.knowledge.tide;

import java.io.Serializable;

/**
 * Created by xiexb on 2017/7/28.
 * 潮汐  监测站  资源数据
 */

public class TideStationBean {
    private String id_;
    private String stationNo;
    private String stationName;
    private String ewNs;
    private String ew;
    private String ns;
    private String seaArea;
    private String continent;
    private String country;
    private String prov;
    private String city;
    private String address;
    private String desc;
    private int hot;
    private String weatherStationId;
    private String weatherStationName;
    private String weahterStationEw;
    private String weatherStationNs;

    public String getId_() {
        return id_;
    }

    public void setId_(String id_) {
        this.id_ = id_;
    }

    public String getStationNo() {
        return stationNo;
    }

    public void setStationNo(String stationNo) {
        this.stationNo = stationNo;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getEwNs() {
        return ewNs;
    }

    public void setEwNs(String ewNs) {
        this.ewNs = ewNs;
    }

    public String getEw() {
        return ew;
    }

    public void setEw(String ew) {
        this.ew = ew;
    }

    public String getNs() {
        return ns;
    }

    public void setNs(String ns) {
        this.ns = ns;
    }

    public String getSeaArea() {
        return seaArea;
    }

    public void setSeaArea(String seaArea) {
        this.seaArea = seaArea;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProv() {
        return prov;
    }

    public void setProv(String prov) {
        this.prov = prov;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getHot() {
        return hot;
    }

    public void setHot(int hot) {
        this.hot = hot;
    }

    public String getWeatherStationId() {
        return weatherStationId;
    }

    public void setWeatherStationId(String weatherStationId) {
        this.weatherStationId = weatherStationId;
    }

    public String getWeatherStationName() {
        return weatherStationName;
    }

    public void setWeatherStationName(String weatherStationName) {
        this.weatherStationName = weatherStationName;
    }

    public String getWeahterStationEw() {
        return weahterStationEw;
    }

    public void setWeahterStationEw(String weahterStationEw) {
        this.weahterStationEw = weahterStationEw;
    }

    public String getWeatherStationNs() {
        return weatherStationNs;
    }

    public void setWeatherStationNs(String weatherStationNs) {
        this.weatherStationNs = weatherStationNs;
    }
}

