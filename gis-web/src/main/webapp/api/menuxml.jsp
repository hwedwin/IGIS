<%--
  Created by IntelliJ IDEA.
  User: xiexb
  Date: 2017/8/22
  Time: 17:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
  <%@ include file="head.jsp" %>
</head>
<body>
<div class="wrapper">
  <%@ include file="header.jsp" %>
  <section>
    <div class="container">
      <%@ include file="leftmenu.jsp" %>
      <input id="pageid" type="hidden" value="menuxml">

      <div class="docs-content" style="height: 2400px;">
        <h3 id="welcome" class="welcome">菜单配置</h3>
        <br/>

        <p>下面介绍一下menu.*****.properties菜单配置</p>
            <pre class="prettyprint">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;menuConfigs&gt;
    &lt;common&gt;
        &lt;defaultPage&gt;pages/gisdefault.jsp&lt;/defaultPage&gt;
        &lt;navPage&gt;pages/nav.jsp&lt;/navPage&gt;
        &lt;showSearch&gt;true&lt;/showSearch&gt;
        &lt;toolBars&gt;
            &lt;toolBar fun="gis_common.queryPointFun(this)" name="经纬度查询" isShow="true"&gt;&lt;/toolBar&gt;
            &lt;toolBar fun="gis_common.clearDataCache()" name="清理缓存" isShow="true"&gt;&lt;/toolBar&gt;
        &lt;/toolBars&gt;
    &lt;/common&gt;

    &lt;modules&gt;
        &lt;module name="LTE栅格化分析" m="endtoend" isShow="true" ico="nav_grid.png" bgcolor="165,248,44"
                page="pages/gisdefault.jsp"&gt;
            &lt;jsPaths&gt;
                &lt;jsPath path="modules/endtoend.js"&gt;&lt;/jsPath&gt;
                &lt;jsPath path="modules/scenicschool.js"&gt;&lt;/jsPath&gt;
            &lt;/jsPaths&gt;
            &lt;toolBars&gt;
                &lt;toolBar fun="endToEnd_createTootbar_cells(this)" name="MRO分布"&gt;&lt;/toolBar&gt;
                &lt;toolBar fun="endToEnd_createTootbar_advancedAnalysis(this)" name="高级指标分析"&gt;&lt;/toolBar&gt;
                &lt;toolBar fun="gis_common.distanceLoad(this)" name="测距" isShow="true"&gt;&lt;/toolBar&gt;
            &lt;/toolBars&gt;
            &lt;menus&gt;
                &lt;menu fun="endToEnd_createQueryParamHtml(this)" name="弱覆盖栅格分析"&gt;&lt;/menu&gt;
                &lt;menu fun="endToEnd_createQueryBuildParamHtml(this)" name="室内网络质量分析"&gt;&lt;/menu&gt;
                &lt;menu fun="endToEnd_createSerialWeakGridHtml(this)" name="连续弱覆盖分析"&gt;&lt;/menu&gt;
            &lt;/menus&gt;
            &lt;initFuns&gt;
                &lt;initFun init="endtoend_init_query()"&gt;&lt;/initFun&gt;
            &lt;/initFuns&gt;
        &lt;/module&gt;
        &lt;module name="潮汐图" m="tide" isShow="true" page="pages/purplishblue.jsp" gistype="arcgis" arcgisurl="http://cache1.arcgisonline.cn/arcgis/rest/services/ChinaOnlineStreetPurplishBlue/MapServer" showsearch="false" istransfer="false" showToolBars = "false"&gt;
            &lt;jsPaths&gt;
                &lt;jsPath path="modules/knowledge/tide.js"&gt;&lt;/jsPath&gt;
            &lt;/jsPaths&gt;
            &lt;initFuns&gt;
                &lt;initFun init="loadTide();"&gt;&lt;/initFun&gt;
            &lt;/initFuns&gt;
        &lt;/module&gt;
    &lt;/modules&gt;
&lt;/menuConfigs&gt;
            </pre>

        <p>部分参数说明</p>

        <p>common:部分公有参数默认值，后面单个应用可以重新定义这些值。</p>

        <h3 class="welcome">modules部分为具体菜单配置</h3>
        <p>每个module为独立应用，可以配置页面，引用的JS文件等，具体如下：</p>
        <p>module的属性name:应用名称，导航页面显示。</p>
        <p>module的属性m:应用关键字，通过此关键字加载菜单配置信息</p>
        <p>module的属性isShow:是否在导航页显示 默认：true</p>
        <p>module的属性ico:导航页面该应用的图标 不配置使用默认图标</p>
        <p>module的属性bgcolor:导航页该应用的背景颜色  默认为随机</p>
        <p>module的属性page:指定跳转页面，不指定时默认到公共配置指定页面</p>
        <p>module的属性gistype:指定该应用的底图类型，不指是使用baseinfo.***.properties的配置内容</p>
        <p>module的属性showsearch:是否显示搜索框 默认：true</p>
        <p>module的属性istransfer:是否转包 默认：false</p>
        <p>module的属性showToolBars:是否显示工具栏 默认：true</p>

        <h3 class="welcome">jsPaths部分配置</h3>
        <p>jsPath:配置该引用的JS文件，基础JS包已经引用，次处只引用独立的。</p>
        <h3 class="welcome">toolBars部分配置</h3>
        <p>toolBar:配置工具栏，fun属性为js方法名，此处菜单和公共菜单同时显示</p>
        <h3 class="welcome">menus配置</h3>
        <p>menu:配置该应用的菜单，fun属性为js方法名</p>
        <h3 class="welcome">initFuns配置</h3>
        <p>initFun:配置该应用加载完成候调用的方法，fun属性为js方法名</p>
        <a href="baseinfoxml.jsp" type="button" class="btn-link">上一页</a>
        <a href="otherxml.jsp" type="button" class="btn-link" style="margin-left: 40px;">下一页</a>
      </div>
    </div>
  </section>
</div>
</body>
</html>
