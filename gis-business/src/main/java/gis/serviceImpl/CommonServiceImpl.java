package gis.serviceImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cache.DataCache;
import bean.common.Region;
import gis.common.GisConfig;
import gis.service.CommonService;
import gis.sqlbase.DatabaseManage;
import gis.sqlbase.DatabaseName;
import gis.sqlbase.SQLHelper;

public class CommonServiceImpl extends CommonService {
	
	private Logger logger = LoggerFactory.getLogger(CommonServiceImpl.class);
	private static final SQLHelper db = DatabaseManage.Instance(DatabaseName.common);
	private GisConfig config = GisConfig.GetInstance();

	@Override
	public List<Region> GetRegions() throws Exception {

		List<Region> list = null;
		String key = "GetRegions";
		Object obj = DataCache.getData(key);
		if (obj != null) {
			list = (List<Region>) obj;
		}
		
		String sql = config.getSQL("common_city");
		if (list == null && StringUtils.isNotEmpty(sql)) {
			ResultSet rs = db.getResultSet(sql);
			if (rs != null) {
				try {
					list = GetRegionEntity(rs);
					DataCache.addData(key, list);
				}
				catch (SQLException e) {
					logger.error("调用CommonImpl 的方法 GetRegions出错," + e);
					e.printStackTrace();
					throw new java.lang.Exception("调用CommonImpl 的方法 GetRegions出错");
				}
				finally {
					db.close(rs);
				}
			}
		}

		return list;
	}
}
