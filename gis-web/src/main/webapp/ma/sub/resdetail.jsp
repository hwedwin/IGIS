<%--
  Created by IntelliJ IDEA.
  User: Xiaobing
  Date: 2017/6/20
  Time: 11:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no">
<html>
<head>
    <title>添加资源</title>
    <%@ include file="../common/baseref.jsp" %>
    <link rel="stylesheet" type="text/css" href="/ma/assets/js/bwizard/bwizard.min.css">
    <link rel="stylesheet" type="text/css" href="/ma/assets/css/bootstrap-responsive.min.css">
    <script src="/ma/assets/js/bwizard/bwizard.min.js"></script>

    <script type="text/javascript">
        function openIntro() {
            $("#add-database-connection").modal({backdrop: 'static', keyboard: false});
        }

        $(function () {
            $("#wizard").bwizard();
        })

    </script>
</head>
<body>
<jsp:include page="../common/header.jsp"/>
<div id="page-wrapper" header-menu="restable.jsp">
    <div class="header">
        <h3 class="page-header">
            新建资源
            <small>Add Resource</small>
        </h3>
        <ol class="breadcrumb">
            <li><a href="restable.jsp">资源配置管理</a></li>
            <li class="active">新建资源</li>
        </ol>
    </div>

    <div id="page-inner">
        <div id="wizard">
            <ol>
                <li><span class="bwizard-label">1</span>基本信息</li>
                <li><span class="bwizard-label">2</span>数据源配置</li>
                <li><span class="bwizard-label">3</span>模型配置</li>
                <li><span class="bwizard-label">4</span>其他高级配置</li>
                <li><span class="bwizard-label">5</span>完成</li>
            </ol>
            <div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="input-res-base-title" class="col-sm-2 control-label">资源名称：</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" id="input-res-base-title">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-res-base-des" class="col-sm-2 control-label">描述：</label>

                            <div class="col-sm-6">
                                <textarea class="form-control" id="input-res-base-des" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">保 存</button>
                            </div>
                        </div>
                    </div>
                    <span class="help-block">说明：测试测试测试什么什么什么.</span>
                </div>
            </div>
            <div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">数据库连接信息：</label>

                            <div class="col-sm-3">
                                <select class="form-control" id="select-res-data-link">
                                    <option title="jdbc:gbase://10.193.225.74:5258/dw">common</option>
                                    <option title="jdbc:oracle:thin:@10.190.45.184:1521/irmsdb">oracle</option>
                                    <option title="jdbc:mysql://localhost:3306/inspur_city_fun?useUnicode=true&amp;characterEncoding=utf8">
                                        mysql
                                    </option>
                                </select>
                            </div>
                            <button type="button" class="btn btn-link" title="点击新建连接" onclick="openIntro();">不能满足我
                            </button>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">SQL语句：</label>

                            <div class="col-sm-8">
                                <textarea class="form-control" id="input-res-data-sql" rows="7"></textarea>
                            </div>
                            <button type="button" class="btn btn-link" style="margin-top: 120px;">测试一下</button>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">保 存</button>
                            </div>
                        </div>
                    </div>

                    <span class="help-block">说明：数据源配置说明.</span>
                </div>
            </div>
            <div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">选择模型：</label>

                            <div class="col-md-3">
                                <select class="form-control">
                                    <option>普通点模型</option>
                                    <option>通信小区模型</option>
                                    <option>面状模型</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">模型映射：</label>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">ID</span>
                                            <select class="form-control">
                                                <option>字段1</option>
                                                <option>字段2</option>
                                                <option>字段3</option>
                                            </select>
                                            <span class="input-group-addon">
                                                <input type="checkbox" title="勾选后，点击此资源时候，显示此属性值">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"> 名称 </span>
                                            <select class="form-control">
                                                <option>字段1</option>
                                                <option>字段2</option>
                                                <option>字段3</option>
                                            </select>
                                            <span class="input-group-addon">
                                                <input type="checkbox" title="勾选后，点击此资源时候，显示此属性值">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"> 经度 </span>
                                            <select class="form-control">
                                                <option>字段1</option>
                                                <option>字段2</option>
                                                <option>字段3</option>
                                            </select>
                                            <span class="input-group-addon">
                                                <input type="checkbox" title="勾选后，点击此资源时候，显示此属性值">
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"> 纬度 </span>
                                            <select class="form-control">
                                                <option>字段1</option>
                                                <option>字段2</option>
                                                <option>字段3</option>
                                            </select>
                                            <span class="input-group-addon">
                                                <input type="checkbox" title="勾选后，点击此资源时候，显示此属性值">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-md-2 control-label">其他属性：</label>

                            <div class="col-md-10">
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">属性名称</span>
                                            <input type="text" class="form-control" placeholder="填写属性名称"
                                                   aria-describedby="basic-addon2">
                                            <span class="input-group-addon">字段</span>
                                            <select class="form-control">
                                                <option>字段1</option>
                                                <option>字段2</option>
                                                <option>字段3</option>
                                            </select>
                                            <span class="input-group-addon">
                                                <a class="glyphicon glyphicon-minus" aria-hidden="true"></a>
                                            </span>
                                            <span class="input-group-addon">
                                                <a class="glyphicon glyphicon-plus" aria-hidden="true"></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">属性名称</span>
                                            <input type="text" class="form-control" placeholder="填写属性名称"
                                                   aria-describedby="basic-addon2">
                                            <span class="input-group-addon">字段</span>
                                            <select class="form-control">
                                                <option>字段1</option>
                                                <option>字段2</option>
                                                <option>字段3</option>
                                            </select>
                                            <span class="input-group-addon">
                                                <a class="glyphicon glyphicon-minus" aria-hidden="true"></a>
                                            </span>
                                            <span class="input-group-addon">
                                                <a class="glyphicon glyphicon-plus" aria-hidden="true"></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">属性名称</span>
                                            <input type="text" class="form-control" placeholder="填写属性名称"
                                                   aria-describedby="basic-addon2">
                                            <span class="input-group-addon">字段</span>
                                            <select class="form-control">
                                                <option>字段1</option>
                                                <option>字段2</option>
                                                <option>字段3</option>
                                            </select>
                                            <span class="input-group-addon">
                                                <a class="glyphicon glyphicon-minus" aria-hidden="true"></a>
                                            </span>
                                            <span class="input-group-addon">
                                                <a class="glyphicon glyphicon-plus" aria-hidden="true"></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary">保 存</button>
                            </div>
                        </div>

                    </div>

                    <span class="help-block">说明：模型配置说明.</span>
                </div>
            </div>
            <div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="card-title">
                                    <div class="title">资源图标</div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">备选图标：</label>

                                        <div class="col-sm-10">
                                            <%--<input type="email" class="form-control" id="inputEmail3" placeholder="Email">--%>
                                            <img src="/images/point.png" alt="...">
                                            <img src="/images/point-green.png" alt="...">
                                            <img src="/images/lte.png" alt="...">
                                            <img src="/images/gsm.png" alt="...">
                                            <img src="/images/td.png" alt="...">
                                            <img src="/images/wlan.png" alt="...">
                                            <img src="/images/ico10.png" alt="...">
                                            <img src="/images/ico10h.png" alt="...">
                                            <%--<img src="/images/poi.png" alt="...">--%>
                                            <img src="/images/bts.png" alt="...">
                                            <img src="/images/cell-1.png" alt="...">
                                            <img src="/images/cell-2.png" alt="...">
                                            <img src="/images/Build28.png" alt="...">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">已选图片：</label>

                                        <div class="col-sm-10">
                                            <img src="/images/point.png" alt="...">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">自定义图标：</label>
                                        <input type="file" id="exampleInputFile" data-toggle="tooltip"
                                               title="建议图片尺寸为28px*28px">
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button class="btn btn-primary">保存</button>
                                        </div>
                                    </div>
                                </div>
                                <span class="help-block">说明：图标配置说明.</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="card-title">
                                    <div class="title">数据量处理</div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <span class="help-block">说明：数据稀疏配置说明.</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="card-title">
                                    <div class="title">URL参数配置</div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">参数配置：</label>

                                        <div class="col-sm-10">
                                            <div class="form-group">
                                                <div class="col-md-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">RUL参数名称</span>
                                                        <input type="text" class="form-control"
                                                               placeholder="填写url地址包含的参数名"
                                                               aria-describedby="basic-addon2">
                                                        <span class="input-group-addon">对应字段</span>
                                                        <select class="form-control">
                                                            <option>字段1</option>
                                                            <option>字段2</option>
                                                            <option>字段3</option>
                                                        </select>
                                                        <span class="input-group-addon">
                                                            <a class="glyphicon glyphicon-minus" aria-hidden="true"></a>
                                                        </span>
                                                        <span class="input-group-addon">
                                                            <a class="glyphicon glyphicon-plus" aria-hidden="true"></a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">RUL参数名称</span>
                                                        <input type="text" class="form-control"
                                                               placeholder="填写url地址包含的参数名"
                                                               aria-describedby="basic-addon2">
                                                        <span class="input-group-addon">对应字段</span>
                                                        <select class="form-control">
                                                            <option>字段1</option>
                                                            <option>字段2</option>
                                                            <option>字段3</option>
                                                        </select>
                                                        <span class="input-group-addon">
                                                            <a class="glyphicon glyphicon-minus" aria-hidden="true"></a>
                                                        </span>
                                                        <span class="input-group-addon">
                                                            <a class="glyphicon glyphicon-plus" aria-hidden="true"></a>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button class="btn btn-default" data-toggle="tooltip" title="无需求此步跳过">保存
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <span class="help-block">说明：URL传入参数进行结果集过滤，无需求此步跳过.</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <div class="card-title">
                                    <div class="title">图例配置</div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">图例：</label>

                                        <div class="col-sm-10 ">
                                            <div class="form-group form-inline">
                                                <div class="input-group col-sm-3">
                                                    <span class="input-group-addon">名称</span>
                                                    <input type="text" class="form-control" placeholder="图例名称"
                                                           aria-describedby="basic-addon2">
                                                </div>
                                                <div class="input-group form-inline">
                                                    <div class="input-group has-success">
                                                        <input type="text" class="form-control" style="width: 60px;"
                                                               placeholder="值">
                                                    </div>
                                                    <div class="input-group">
                                                        <select class="form-control label label-info" style="width:90px;">
                                                            <option selected="selected">运算符</option>
                                                            <option>=</option>
                                                            <option>></option>
                                                            <option>>=</option>
                                                            <option><</option>
                                                            <option><=</option>
                                                        </select>
                                                    </div>
                                                    <div class="input-group has-error">
                                                        <select class="form-control">
                                                            <option>字段1</option>
                                                            <option>字段2</option>
                                                            <option>字段3</option>
                                                        </select>
                                                    </div>
                                                    <div class="input-group">
                                                        <select class="form-control label label-info" style="width:90px;">
                                                            <option selected="selected">运算符</option>
                                                            <option>=</option>
                                                            <option>></option>
                                                            <option>>=</option>
                                                            <option><</option>
                                                            <option><=</option>
                                                        </select>
                                                    </div>
                                                    <div class="input-group has-success">
                                                        <input type="text" class="form-control" style="width: 60px;"  placeholder="值">
                                                    </div>
                                                </div>
                                                <div class="input-group form-inline">
                                                    <a href="#" title="点击更换图标">
                                                        <img src="/images/point.png" style="width: 28px;height: 28px;">
                                                    </a>
                                                <span class="input-group-addon">
                                                     <a class="glyphicon glyphicon-minus" aria-hidden="true"></a>
                                                </span>
                                                <span class="input-group-addon">
                                                     <a class="glyphicon glyphicon-plus" aria-hidden="true"></a>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="form-group form-inline">
                                                <div class="input-group col-sm-3">
                                                    <span class="input-group-addon">名称</span>
                                                    <input type="text" class="form-control" placeholder="图例名称">
                                                </div>
                                                <div class="input-group form-inline">
                                                    <div class="input-group has-success">
                                                        <input type="text" class="form-control" style="width: 60px;"
                                                               placeholder="值">
                                                    </div>
                                                    <div class="input-group">
                                                        <select class="form-control label label-info" style="width:90px;">
                                                            <option selected="selected">运算符</option>
                                                            <option>=</option>
                                                            <option>></option>
                                                            <option>>=</option>
                                                            <option><</option>
                                                            <option><=</option>
                                                        </select>
                                                    </div>
                                                    <div class="input-group has-error">
                                                        <select class="form-control">
                                                            <option>字段1</option>
                                                            <option>字段2</option>
                                                            <option>字段3</option>
                                                        </select>
                                                    </div>
                                                    <div class="input-group">
                                                        <select class="form-control label label-info"
                                                                style="width:90px;">
                                                            <option selected="selected">运算符</option>
                                                            <option>=</option>
                                                            <option>></option>
                                                            <option>>=</option>
                                                            <option><</option>
                                                            <option><=</option>
                                                        </select>
                                                    </div>
                                                    <div class="input-group has-success">
                                                        <input type="text" class="form-control" style="width: 60px;"
                                                               placeholder="值">
                                                    </div>
                                                </div>
                                                <div class="input-group form-inline">
                                                    <a href="#" title="点击更换图标">
                                                        <img src="/images/point.png" style="width: 28px;height: 28px;">
                                                    </a>
                                                <span class="input-group-addon">
                                                     <a class="glyphicon glyphicon-minus" aria-hidden="true"></a>
                                                </span>
                                                <span class="input-group-addon">
                                                     <a class="glyphicon glyphicon-plus" aria-hidden="true"></a>
                                                </span>
                                                </div>
                                            </div>
                                            <div class="form-group form-inline">
                                                <div class="input-group col-sm-3">
                                                    <span class="input-group-addon">名称</span>
                                                    <input type="text" class="form-control" placeholder="图例名称">
                                                </div>
                                                <div class="input-group form-inline">
                                                    <div class="input-group has-success">
                                                        <input type="text" class="form-control" style="width: 60px;"
                                                               placeholder="值">
                                                    </div>
                                                    <div class="input-group">
                                                        <select class="form-control label label-info"
                                                                style="width:90px;">
                                                            <option selected="selected">运算符</option>
                                                            <option>=</option>
                                                            <option>></option>
                                                            <option>>=</option>
                                                            <option><</option>
                                                            <option><=</option>
                                                        </select>
                                                    </div>
                                                    <div class="input-group has-error">
                                                        <select class="form-control">
                                                            <option>字段1</option>
                                                            <option>字段2</option>
                                                            <option>字段3</option>
                                                        </select>
                                                    </div>
                                                    <div class="input-group">
                                                        <select class="form-control label label-info" style="width:90px;">
                                                            <option selected="selected">运算符</option>
                                                            <option>=</option>
                                                            <option>></option>
                                                            <option>>=</option>
                                                            <option><</option>
                                                            <option><=</option>
                                                        </select>
                                                    </div>
                                                    <div class="input-group has-success">
                                                        <input type="text" class="form-control" style="width: 60px;"
                                                               placeholder="值">
                                                    </div>
                                                </div>
                                                <div class="input-group form-inline">
                                                    <a href="#" title="点击更换图标">
                                                        <img src="/images/point.png" style="width: 28px;height: 28px;">
                                                    </a>
                                                <span class="input-group-addon">
                                                     <a class="glyphicon glyphicon-minus" aria-hidden="true"></a>
                                                </span>
                                                <span class="input-group-addon">
                                                     <a class="glyphicon glyphicon-plus" aria-hidden="true"></a>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button class="btn btn-default" data-toggle="tooltip" title="无需求此步跳过">保存
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <span class="help-block">说明：根据指标值结合图例显示图标，无需求此步跳过.</span>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <h1>恭喜 完成设置</h1>

                <p><strong></strong></p>
            </div>
        </div>
    </div>
</div>

<div id="add-database-connection" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">新建数据库连接</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">名称</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Drive</label>

                        <div class="col-sm-10">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown">
                                        选择驱动
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">oracle</a></li>
                                        <li><a href="#">gp</a></li>
                                        <li><a href="#">mysql</a></li>
                                        <li><a href="#">gbase</a></li>
                                        <li><a href="#">sybase</a></li>
                                    </ul>
                                </div>
                                <input type="text" class="form-control" value="com.gbase.jdbc.Driver">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Url</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">User</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Password</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="button" class="btn btn-link" title="点击测试" onclick="openIntro();">连接测试
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                <button type="button" class="btn btn-primary">保存</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
