# IGIS

#### Description
地理信息系统（GIS）在日常生产、生活中起到越来越重的作用。GIS涉及到的行业多，覆盖面广，有着广阔的业务前景。将含有地理信息的数据，在地图上呈现出来，从一个全新的视角来观察这些数据，给使用者一种新的感知。
iGIS是一套开放的平台。灵活的部署方法，插件式的开发模式，使平台以更灵活的方式存在，更利于扩展、二次开发和集成。平台集成了ArcGis、高德地图、天地图、百度地图等主流底图服务，无论是在内网还是外网，都能完成部署。iGIS前端开发框架以模块化的设计思路为主，对主要的方法和功能进行深度封装；在平台API的支持下，功能开发人员或二次开发人员不用关心底层的逻辑关系，将更多的注意力放在业务功能上，便可轻松的完成插件功能开发。除了前端的开发框架外，后端框架还提供了大量的算法支持。经过多年的项目积累和钻研，实现了一大批有关GIS的算法，这些算法便是整个平台的核心部分；算法以接口的方式提供给使用者，使用者只需按照模型输入数据便可以得到想要的结果。后期的平台将提供更为强大功能来满足用户的需求，例如：自定义资源显示，用户以SQL配置的方式来添加，部分基于算法的图形渲染也走配置化的道路，以此来简化开

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
