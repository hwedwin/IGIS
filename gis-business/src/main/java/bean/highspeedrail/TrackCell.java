package bean.highspeedrail;

import bean.common.Cell;
import bean.common.Res;

/**
 * Created by Xiaobing on 2016/6/2.
 */
public class TrackCell extends Cell {
    private String trackId;
    private String trackName;
    private String stationInterId;
    private String stationInterName;
    private String taclac;
    private String ecigci;
    private String cellType;//小区类型   高铁小区/非高铁小区

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getStationInterId() {
        return stationInterId;
    }

    public void setStationInterId(String stationInterId) {
        this.stationInterId = stationInterId;
    }

    public String getStationInterName() {
        return stationInterName;
    }

    public void setStationInterName(String stationInterName) {
        this.stationInterName = stationInterName;
    }

    public String getTaclac() {
        return taclac;
    }

    public void setTaclac(String taclac) {
        this.taclac = taclac;
    }

    public String getEcigci() {
        return ecigci;
    }

    public void setEcigci(String ecigci) {
        this.ecigci = ecigci;
    }

    public String getCellType() {
        return cellType;
    }

    public void setCellType(String cellType) {
        this.cellType = cellType;
    }
}
