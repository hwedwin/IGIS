package gis.service;

import bean.common.LegendItemEx;
import gis.core.bean.ResValue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xiaobing on 2016/9/6.
 */
public abstract class HotCellService {

    /**
     * 获取小区渲染规则
     *
     * @return
     */
    public abstract List<LegendItemEx> getHotCellRules() throws SQLException;

    /**
     * 根据条件  查询热点小区
     *
     * @param time
     * @param min
     * @param max
     * @return
     * @throws SQLException
     */
    public abstract List<ResValue> getHotCells(String time, String min, String max) throws SQLException;

    protected List<ResValue> getHotCellsEntity(ResultSet rs) throws SQLException {
        List<ResValue> list = new ArrayList<>();
        if (rs != null) {
            while (rs.next()) {
                ResValue resValue = new ResValue();
                resValue.setId(rs.getString("cell_key"));
                resValue.setName(rs.getString("cell_name"));
                resValue.setValue(rs.getInt("life_count"));
                list.add(resValue);
            }
        }
        return list;
    }
}
